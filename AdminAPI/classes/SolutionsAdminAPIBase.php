<?php

namespace FS;

use FS\Common\Auth;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class SolutionsAdminAPIBase
{
    protected $responseArr;
    protected $timezone;
    protected $auth;
    protected $pdo;
    protected $data;

    public function __construct($securable = null)
    {
        $this->responseArr = IO::initResponseArray();
        $this->auth        = new Auth();
        $this->timezone    = date_default_timezone_get();

        // VALIDATION
        if (empty($this->timezone)) {
            $this->timezone = 'NZ';
        }

        // Used only if an entire handler has the same permissions, otherwise it's done within the handler
        if (!is_null($securable)) {
            if (!is_array($securable)) {
                throw new InvalidParameterException('Error: Securable should be array');
            }

            $this->auth->hasPermission($securable);
        }

        // Used only if an entire handler uses the same connection, otherwise it's done within the handler
        $this->pdo = $this->getConnection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

        $this->auth        = new Auth();
        $this->responseArr = IO::initResponseArray();

        // Take POST as the default request method, can be overridden in specified method
        $this->data = IO::getPostParameters();
    }

    protected function getConnection($dbHost, $dbName, $dbUsername, $dbPassword)
    {
        $pdo = new \PDO('mysql:host=' . $dbHost . ';dbname=' . $dbName, $dbUsername, $dbPassword);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

        return $pdo;
    }

    protected function parseResult($result, $status = 200)
    {
        $statusCode = $status;
        $status     = 'success';

        if ($statusCode != 200) {
            $status = 'error';
        }

        http_response_code($statusCode);

        if (is_bool($result)) {
            return [
                'response' => [
                    'status' => $status
                ]
            ];
        }

        return [
            'response' => [
                'status' => $status,
                'data'   => $result
            ]
        ];
    }

    protected function appendSearch($searchString, $query)
    {
        return $searchString . (strpos($searchString, 'WHERE') === false ? ' WHERE ' : ' AND ') . $query;
    }

    protected function bindParams(\PDOStatement $stmt, $parameters)
    {
        foreach ($parameters as $parameter => $value) {
            $stmt->bindValue($parameter, $value);
        }

        return $stmt;
    }
}
