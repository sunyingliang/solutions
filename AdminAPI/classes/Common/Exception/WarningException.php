<?php

namespace FS\Common\Exception;

class WarningException extends \Exception
{
    public function __toString()
    {
        return "Warning: {$this->message} in {$this->file}" . PHP_EOL;
    }
}
