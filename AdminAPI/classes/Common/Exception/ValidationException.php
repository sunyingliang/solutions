<?php

namespace FS\Common\Exception;

class ValidationException extends FSException
{
    public function __construct($message, $code = 1701, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
