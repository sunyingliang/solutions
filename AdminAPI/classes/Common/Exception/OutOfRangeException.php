<?php

namespace FS\Common\Exception;

class OutOfRangeException extends FSException
{
    public function __construct($message, $code = 1702, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
