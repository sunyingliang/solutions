<?php

namespace FS\Common\Exception;

class FillTaskException extends FSException
{
    public function __construct($message, $code = 1801, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}