<?php

namespace FS\Common;

use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class Database
{
    private $host;
    private $name;
    private $port;
    private $username;
    private $password;
    private $pdo;

    public function __construct($options)
    {
        if (!is_array($options)) {
            throw new InvalidParameterException('Passed in parameter {options} must be an array.');
        }

        if (($validation = IO::required($options, 'DB_HOST', 'DB_PORT', 'DB_NAME', 'DB_USERNAME', 'DB_PASSWORD'))['valid'] === false) {
            throw new InvalidParameterException('Could not initialise database: ' . $validation['message']);
        }

        $this->host     = $options['DB_HOST'];
        $this->port     = $options['DB_PORT'];
        $this->name     = $options['DB_NAME'];
        $this->username = $options['DB_USERNAME'];
        $this->password = $options['DB_PASSWORD'];

        $this->open();
    }

    public function getHost() {
        return $this->host;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPDO()
    {
        return $this->pdo;
    }

    public function open()
    {
        try {
            $this->pdo = new \PDO('mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->name, $this->username, $this->password);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            $this->pdo = null;
            throw new PDOCreationException('Could not initialise customer database: ' . $e->getMessage());
        }
    }

    public function close()
    {
        unset($this->pdo);
    }

    public function test()
    {
        $sql = "SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" . $this->name . "'";

        $result = $this->pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);

        return $result !== false;
    }
}
