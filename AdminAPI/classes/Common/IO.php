<?php

namespace FS\Common;

use FS\Common\Exception\InvalidParameterException;

class IO
{
    public static function initResponseArray()
    {
        return [
            "status" => "success"
        ];
    }

    public static function formatResponseArray($responseArr)
    {
        return [
            "response" => $responseArr
        ];
    }

    public static function guid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        $md5 = strtoupper(md5(uniqid(mt_srand(intval(microtime(true) * 1000)), true)));

        return substr($md5, 0, 8) . '-' . substr($md5, 8, 4) . '-' . substr($md5, 12, 4) . '-' . substr($md5, 16, 4) . '-' . substr($md5, 20, 12);
    }

    public static function generateRandom($length)
    {
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            // Upper VS lower case 50/50, entropy low~ (MySQL Compatable)
            $string .= (rand(0, 1) === 1) ? chr(rand(65, 90)) : chr(rand(97, 122));
        }

        return $string;
    }

    public static function getQueryParameters()
    {
        return $_GET;
    }

    public static function getPostParameters()
    {
        $contentType = '';

        if (isset($_SERVER['CONTENT_TYPE'])) {
            $contentType = strtolower($_SERVER['CONTENT_TYPE']);
        }

        if (strpos($contentType, 'json') !== false) {
            $input = json_decode(file_get_contents('php://input'), true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                $input = null;
            }
        } else {
            $input = $_POST;
        }

        return $input;
    }

    public static function default($haystack, $needle, $default = null)
    {
        if (is_numeric($default)) {
            return (isset($haystack[$needle]) && is_numeric($haystack[$needle])) ? intval($haystack[$needle]) : $default;
        } else {
            return (isset($haystack[$needle]) && !is_null($haystack[$needle])) ? $haystack[$needle] : $default;
        }
    }

    public static function shell($cmd, $stdIn = null, &$stdOut = null, &$stdErr = null)
    {
        $descriptorSpec = [
            0 => ["pipe", "r"],
            1 => ["pipe", "w"],
            2 => ["pipe", "w"]
        ];

        $process = proc_open($cmd, $descriptorSpec, $pipes);

        if (!is_resource($process)) {
            throw new \Exception('Error opening command process');
        }

        fwrite($pipes[0], $stdIn);
        fclose($pipes[0]);

        $stdOut = stream_get_contents($pipes[1]);
        $stdErr = stream_get_contents($pipes[2]);

        proc_close($process);

        return $stdOut . $stdErr;
    }

    public static function cloneDatabase(&$databaseFrom, &$databaseTo)
    {
        // Prepare locations
        $fromLocation       = getcwd() . '/' . $databaseFrom->getName() . '.sql';
        $fromConfigLocation = getcwd() . '/' . $databaseFrom->getName() . '.conf';
        $toConfigLocation   = getcwd() . '/' . $databaseTo->getName() . '.conf';

        // Clear out anything in case we're re-processing
        if (file_exists($fromLocation))       unlink($fromLocation);
        if (file_exists($fromConfigLocation)) unlink($fromConfigLocation);
        if (file_exists($toConfigLocation))   unlink($toConfigLocation);

        // Write FROM conf file, we're doing this to avoid showing user/pass in ps/history for security
        $fromConfigFile = fopen($fromConfigLocation, "w");
        fwrite($fromConfigFile, '[client]' . PHP_EOL);
        fwrite($fromConfigFile, 'host = ' . $databaseFrom->getHost() . PHP_EOL);
        fwrite($fromConfigFile, 'user = ' . $databaseFrom->getUsername() . PHP_EOL);
        fwrite($fromConfigFile, 'password = ' . $databaseFrom->getPassword() . PHP_EOL);
        fclose($fromConfigFile);

        // Write TO conf file, we're doing this to avoid showing user/pass in ps/history for security
        $toConfigFile = fopen($toConfigLocation, "w");
        fwrite($toConfigFile, '[client]' . PHP_EOL);
        fwrite($toConfigFile, 'host = ' . $databaseTo->getHost() . PHP_EOL);
        fwrite($toConfigFile, 'user = ' . $databaseTo->getUsername() . PHP_EOL);
        fwrite($toConfigFile, 'password = ' . $databaseTo->getPassword() . PHP_EOL);
        fclose($toConfigFile);

        // Find views and exclude them (these cannot be excluded in mysqldump command natively and require super to import)
        $excludeViewSQL = "SELECT `TABLE_NAME` AS `table`
                           FROM `INFORMATION_SCHEMA`.`TABLES` 
                           WHERE `TABLE_TYPE` = 'VIEW'
                               AND `TABLE_SCHEMA` = '" . $databaseFrom->getName() . "'";

        $stmt = $databaseFrom->getPDO()->query($excludeViewSQL);

        $excludeViewData = '';

        if ($stmt !== false) {
            while ($row = $stmt->fetch()) {
                $excludeViewData .= '--ignore-table=' . $databaseFrom->getName() . '.' . $row['table'] . ' ';
            }
        }

        // Create the command (Generate dump of from database)
        $fromCommand = 'mysqldump --defaults-extra-file="' . $fromConfigLocation . '" ' . $databaseFrom->getName() . ' --no-create-db --skip-add-drop-table ' . $excludeViewData . ' > ' . $fromLocation;

        // Execute the command
        $response = IO::shell($fromCommand);

        if (!empty($response)) {
            throw new \Exception('Error cloning [' . $databaseFrom->getHost() . '/' . $databaseFrom->getName() . '] to [' . $databaseTo->getHost() . '/' . $databaseTo->getName() . '] [1/2] ' . $response);
        }

        // Create the command
        $toCommand = 'mysql --defaults-extra-file=' . $toConfigLocation . ' --connect_timeout=600 --max_allowed_packet=512M  ' . $databaseTo->getName() . ' < ' . $fromLocation;

        // Execute the command
        $response = IO::shell($toCommand);

        if (!empty($response)) {
            throw new \Exception('Error cloning [' . $databaseFrom->getHost() . '/' . $databaseFrom->getName() . '] to [' . $databaseTo->getHost() . '/' . $databaseTo->getName() . '] [2/2] ' . $response);
        }

        // Delete the configuration and mysqldump files after we have finished using it
        unlink($fromLocation);
        unlink($fromConfigLocation);
        unlink($toConfigLocation);

        // Close the customer DB connections
        $databaseFrom->close();
        $databaseTo->close();
    }

    public static function required($data, ...$args)
    {
        $required = [];

        foreach ($args as $arg) {
            if (!isset($data[$arg])) {
                $required[] = $arg;
            }
        }

        if (empty($required)) {
            return ['valid' => true];
        }

        return [
            'valid'   => false,
            'message' => 'Required parameters: ' . implode(', ', $required)
        ];
    }

    public static function getPDOConnection($connection)
    {
        if (!is_array($connection) || !isset($connection['dsn']) || !isset($connection['username']) || !isset($connection['password'])) {
            throw new InvalidParameterException('The PDO connection information is not wrapped correctly');
        }

        try {
            $pdo = new \PDO($connection['dsn'], $connection['username'], $connection['password'], [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $pdo;
        } catch (\PDOException $e) {
            return false;
        }
    }

    public static function message($message, $object = null, $die = false)
    {
        echo '[' . date('Y-m-d H:i:s') . '] ' . $message . PHP_EOL;

        if (!empty($object)) {
            print_r($object);
            echo PHP_EOL;
        }

        if ($die) {
            exit();
        }
    }

    public static function slack($url, $channel, $username, $message, $console = false)
    {
        $data = '{"channel": "' . $channel . '", "username": "' . $username . '", "text": "' . $message . '"}';

        $opts = [
            CURLOPT_URL            => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HEADER         => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $data
        ];

        $curl = CURL::getCurl($opts);

        CURL::getResultAndClose($curl);

        if ($console) {
            self::message($message);
        }
    }

    public static function backgroundExec($cmd)
    {
        if (strtoupper(substr(php_uname(), 0, 3)) == 'WIN') {
            pclose(popen('start /B ' . $cmd, 'r'));
        } else {
            exec($cmd . ' > /dev/null &');
        }
    }
}
