<?php

namespace FS\Common;

class Generic
{
    public static function validateDBConfig()
    {
        if (!defined('DB_HOST') || !defined('DB_USER') || !defined('DB_PASS') || !defined('DB_TABLE')) {
            self::message('Error: Configuration constant(s) not defined', true);
        }
    }

    public static function message($message, $die = false)
    {
        $message = date('H:i:s') . ' -> ' . $message;

        if ($die) {
            self::writeLog($message);
            die(PHP_EOL . $message . PHP_EOL);
        }

        self::writeLog($message . '...');
        echo(PHP_EOL . $message . '...');
    }

    public static function writeLog($message)
    {
        // Audit log (Limit 1MB)
        $file        = __DIR__ . '/../../' . date("Ymd") . '.log';
        $fileContent = '';

        if (file_exists($file)) {
            $fileContent = file_get_contents($file);
        } else {
            fclose(fopen($file, "wb"));
        }

        if (is_writable($file)) {
            $fileContent = date('Y/m/d') . ' ' . $message  . PHP_EOL . $fileContent;
            $fileContent = substr($fileContent, 0, 1048576);
            file_put_contents($file, $fileContent);
        }
    }

    public static function fileTypeCSV($fileName)
    {
        return strtolower(substr($fileName, -3)) == 'csv';
    }
}
