<?php

namespace FS\Common;

use FS\Common\LIM\FirmstepLIM;

class HTTPLIMProxyClient
{
    private $useLIM;
    private $limClient;

    public function __construct(FirmstepLIM $limClient = null)
    {
        $this->useLIM    = !is_null($limClient);
        $this->limClient = $limClient;
    }

    public function call($request, $location)
    {
        if ($this->useLIM) {
            $limRequest  = $this->createLIMRequest($request, $location);
            $limResponse = $this->limClient->run($limRequest);

            return $this->retrieveLIMResponse($limResponse);
        } else {
            $curl = CURL::getCurl([
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $request,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL            => $location
            ]);

            return CURL::getResultAndClose($curl);
        }
    }

    private function createLIMRequest($request, $location)
    {
        $writer = new \XMLWriter();

        $writer->openMemory();
        $writer->startElement('Requests');
        $writer->startElement('Request');
        $writer->writeAttribute('id', uniqid());
        $writer->writeAttribute('provider', 'http');
        $writer->startElement('Command');
        $writer->writeElement('Method', 'POST');
        $writer->writeElement('Url', $location);
        $writer->writeElement('Body', $request);
        $writer->endElement(); // Command
        $writer->endElement(); // Request
        $writer->endElement(); // Requests

        return $writer->outputMemory();
    }

    private function retrieveLIMResponse($response)
    {
        $reader = new \XMLReader();

        $reader->xml($response);

        while ($reader->read()) {
            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->name == 'Body') {
                $content = $reader->readString();
                $reader->close();

                return $content;
            }
        }

        $reader->close();

        return '';
    }
}