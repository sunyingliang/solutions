<?php

namespace FS\Common;

use FS\Common\LIM\FirmstepLIM;

class SOAPLIMProxyClient extends \SoapClient
{
    private $useLIM;
    private $limClient;

    public function __construct($wsdl, array $options = null, FirmstepLIM $limClient = null)
    {
        parent::__construct($wsdl, $options);

        $this->useLIM    = !is_null($limClient);
        $this->limClient = $limClient;
    }

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        if ($this->useLIM) {
            $limRequest = $this->createLIMRequest($request, $location, $action);

            $limResponse = $this->limClient->run($limRequest);

            return $this->retrieveLIMResponse($limResponse);
        } else {
            return parent::__doRequest($request, $location, $action, $version, $one_way);
        }
    }

    private function createLIMRequest($request, $location, $action)
    {
        $writer = new \XMLWriter();

        $writer->openMemory();
        $writer->startElement('Requests');
        $writer->startElement('Request');
        $writer->writeAttribute('id', uniqid());
        $writer->writeAttribute('provider', 'http');
        $writer->startElement('Command');
        $writer->writeElement('Method', 'POST');
        $writer->writeElement('Url', $location);
        $writer->startElement('Headers');
        $writer->writeElement('Header', 'Content-Type: text/xml; charset=utf-8');
        $writer->writeElement('Header', 'SOAPAction: ' . $action);
        $writer->endElement(); // Headers
        $writer->writeElement('Body', $request);
        $writer->endElement(); // Command
        $writer->endElement(); // Request
        $writer->endElement(); // Requests

        return $writer->outputMemory();
    }

    private function retrieveLIMResponse($response)
    {
        $reader = new \XMLReader();

        $reader->xml($response);

        while ($reader->read()) {
            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->name == 'Body') {
                $content = $reader->readString();

                $reader->close();

                return $content;
            }
        }

        $reader->close();

        return '';
    }

}