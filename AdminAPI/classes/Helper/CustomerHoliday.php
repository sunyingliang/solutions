<?php

namespace FS\Helper;

use FS\Common\CURL;
use FS\Common\Exception\CURLException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;

class CustomerHoliday
{
    private $dns;
    private $uri;
    private $auth;
    private $pdo;
    private $retry = 3;

    public function __construct(array $connection = null)
    {
        if (isset($connection)) {
            $this->connect($connection);
        }

        $this->dns = defined('ENVIRONMENT') ? (ENVIRONMENT == 'test' ? 'firmsteptest.com' : 'firmstep.com') : 'firmsteptest.com';
    }

    #region Methods
    public function connect(array $connection)
    {
        if (!is_array($connection) || !isset($connection['name']) || !isset($connection['uriPassword'])) {
            throw new InvalidParameterException('The parameter {connection} to ' . __CLASS__ . ' is invalid');
        }

        $this->uri  = $connection['name'] . '.solutions.' . $this->dns . '/api/util/deadline';
        $this->auth = 'Authorization: Basic ' . base64_encode($connection['name'] . ':' . $connection['uriPassword']);
        $this->pdo  = IO::getPDOConnection($connection);

        if ($this->pdo === false) {
            throw new PDOCreationException('Failed to create database connection via PDO');
        }
    }

    public function test(array &$data)
    {
        $message   = '';
        $spResult  = [];
        $apiResult = [];

        // Checking sp & api functions against each item in array $data
        if (($stmt = $this->pdo->prepare("SELECT `add_working_days`(?, ?, ?)")) === false) {
            throw new \Exception('Failed to prepare PDOStatement');
        }

        $curl = CURL::getCurl();

        foreach ($data as $row) {
            if ($this->checkDBFunction($stmt, $row) !== true) {
                IO::message('Failed to test function for row with id {' . $row['id'] . '}');
                $spResult[] = $row['id'];
            }

            if ($this->checkAPI($curl, $row) !== true) {
                IO::message('Failed to test API for row with id {' . $row['id'] . '}');
                $apiResult[] = $row['id'];
            }
        }

        CURL::closeCurl($curl);

        // Construct message and send it to slack if required
        if (!empty($spResult)) {
            $message .= 'Function `add_working_days` did not pass all tests for table `holiday_test`. The failed rows have the following id: {' . implode(', ', $spResult) . '}';
        }

        if (!empty($apiResult)) {
            if (!empty($message)) {
                $message .= PHP_EOL;
            }

            $message .= 'API `deadline` did not pass all tests for table `holiday_test`. The failed rows have the following id: {' . implode(', ', $apiResult) . '}';
        }

        if (!empty($message)) {
            throw new \Exception($message);
        }
    }

    public function checkDBFunction(&$stmt, array &$row)
    {
        if ($stmt->execute([$row['date'], $row['days'], $row['calendar']]) === false) {
            return false;
        }

        return $stmt->fetchColumn() === $row['expected'];
    }

    public function checkAPI(&$curl, array &$row)
    {
        $header = ['Content-Type: application/json', $this->auth];
        $data   = [
            [
                'resultname' => 'deadlineDate',
                'start'      => $this->formatDateTime($row['date']),
                'cutoff'     => '00:00:00',
                'add'        => $row['days'],
                'calendar'   => $row['calendar']
            ]
        ];

        $opt = [
            CURLOPT_URL            => $this->uri,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTPHEADER     => $header,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => json_encode($data)
        ];

        for ($i = 0; $i < $this->retry; $i++) {
            try {
                CURL::setOptArray($curl, $opt);
                $curlResponse = CURL::getResult($curl);
                if (empty($curlResponse)) {
                    throw new CURLException('CURL returned an empty response with http code: ' . curl_getinfo($curl, CURLINFO_HTTP_CODE));
                }
                break;
            } catch (\Exception $ce) {
                IO::message($ce->getMessage());
                sleep(1);
                continue;
            }
        }

        if ($i >= $this->retry) {
            if (isset($ce)) {
                IO::message($ce->getMessage());
            }
            return false;
        }

        try {
            $response = json_decode(isset($curlResponse) ? $curlResponse : '', true);

            return $response['deadlineDate'] == $this->formatDateTime($row['expected']);
        } catch (\Exception $e) {
            IO::message($e->getMessage());
            return false;
        }
    }
    #endregion

    #region Utils
    private function formatDateTime($dateTime)
    {
        if (strpos($dateTime, '-') !== false) {
            $dateTime = preg_replace('@(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})@i', '$3/$2/$1 $4:$5:$6', $dateTime);
        }

        return $dateTime;
    }
    #endregion
}
