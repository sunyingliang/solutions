<?php

namespace FS\Integration\Configuration;

class MayriseConfig implements ILastRunConfig
{
    public function getLastRun(\PDO $connection)
    {
        $stmt = $connection->query("SELECT `value` FROM `mayrise_config` WHERE `name` = 'changeSince' LIMIT 1");

        return $stmt->fetchColumn();
    }

    public function setLastRun(\PDO $connection, \DateTime $runTime): bool
    {
        $runValue = $runTime->format('Y-m-d H:i:s');
        $stmt     = $connection->prepare("UPDATE `mayrise_config` SET `value` = :changeSince WHERE `name` = 'changeSince'");

        return $stmt->execute(['changeSince' => $runValue]);
    }
}