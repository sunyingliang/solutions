<?php

namespace FS\Integration\Configuration;

class FeatureConfig
{
    private $customerConfig;
    private $featureConfig;

    /**
     * FeatureConfig constructor.
     *
     * @param string $customer
     * @param string $feature
     * @param \PDO   $connection
     */
    public function __construct($customer, $feature, \PDO $connection)
    {
        $sql = "SELECT `customer`.`integration_config`, `customer_feature`.`config`
                FROM `customer`
                    INNER JOIN `customer_feature` ON `customer_feature`.`customer_id` = `customer`.`id`
                    INNER JOIN `feature` ON `feature`.`id` = `customer_feature`.`feature_id`
                        AND `feature`.`name` = :feature
                WHERE `customer`.`name` = :customer";

        $stmt = $connection->prepare($sql);

        $stmt->execute([
            'customer' => $customer,
            'feature'  => $feature
        ]);

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!$row) {
            return;
        }

        $this->customerConfig = json_decode($row['integration_config']);
        $this->featureConfig  = json_decode($row['config']);
    }

    /**
     * @param string $app
     * @param string $key
     *
     * @return mixed
     * @throws \Exception
     */
    public function get($app, $key)
    {
        $appConfig = $this->getConfig($app);

        if (!isset($appConfig->$key)) {
            throw new \Exception('Configuration vale for ' . $key);
        }

        return $appConfig->$key;
    }

    /**
     * @param string $app
     *
     * @return \stdClass
     * @throws \Exception
     */
    public function getConfig($app)
    {
        if (isset($this->featureConfig->$app)) {
            return $this->featureConfig->$app;
        }

        if (isset($this->customerConfig->$app)) {
            return $this->customerConfig->$app;
        }

        throw new \Exception('Configuration missing for ' . $app);
    }
}
