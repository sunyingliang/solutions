<?php

namespace FS\Integration\Configuration;

interface ILastRunConfig
{
    public function getLastRun(\PDO $connection);

    public function setLastRun(\PDO $connection, \DateTime $runTime): bool;
}