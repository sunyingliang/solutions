<?php

namespace FS\Integration;

use FS\Common\NZLumberjack\Logger;
use FS\Common\NZLumberjack\LoggerMDC;
use FS\Database\Manager;
use FS\Integration\Configuration\FeatureConfig;

abstract class IntegrationBase
{
    protected $log;
    protected $customer;
    protected $feature;
    protected $dbManager;
    protected $configuration;

    function __construct($customer, $feature)
    {
        $this->log = Logger::getLogger('Watchdog.' . $feature);
        LoggerMDC::put('Customer', $customer);

        $this->customer      = $customer;
        $this->feature       = $feature;
        $this->dbManager     = new Manager($customer);
        $this->configuration = new FeatureConfig($customer, $feature, $this->dbManager->getMasterConnection());
    }

    public function run()
    {
        try {
            $this->updateStatus(1);
            $this->log->debug('Executing');

            return $this->process();
        } catch (\Exception $e) {
            $this->log->error('Error occurred', $e);

            return false;
        } finally {
            $this->updateStatus(0);
        }
    }

    protected abstract function process(): bool;

    protected function updateStatus($started)
    {
        $sql = "UPDATE `customer_feature`
                    INNER JOIN `customer` ON `customer`.`id` = `customer_feature`.`customer_id`
                        AND `customer`.`name` = :customer
                    INNER JOIN `feature` ON `feature`.`id` = `customer_feature`.`feature_id`
                        AND `feature`.`name` = :feature
                SET `customer_feature`.`started` = :started";

        $stmt = $this->dbManager->getMasterConnection()->prepare($sql);

        if (!$stmt->execute([
            'started'  => $started,
            'customer' => $this->customer,
            'feature'  => $this->feature
        ])) {
            throw new \RuntimeException($stmt->errorInfo()[2]);
        }
    }
}
