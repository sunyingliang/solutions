<?php

namespace FS\Integration\Reportit;

use FS\Common\NZLumberjack\LoggerMDC;
use FS\Integration\Handler\FillTask;
use FS\Integration\Handler\IFillTask;
use FS\Integration\Handler\ISymologyReader;
use FS\Integration\Handler\SymologyReader;
use FS\Integration\IntegrationBase;

class Symology extends IntegrationBase
{
    private $symologyReader;
    private $fillTask;

    public function __construct($customer, ISymologyReader $symologyReader = null, IFillTask $fillTask = null)
    {
        parent::__construct($customer, 'Symology');

        $this->symologyReader = $symologyReader ?? new SymologyReader($this->configuration);
        $this->fillTask       = $fillTask ?? new FillTask($this->log);
    }

    protected function process(): bool
    {
        $result = true;
        $jobs   = $this->getPendingJobs();

        foreach ($jobs as $job) {
            $result = $this->processJob($job) && $result;
        }

        return $result;
    }

    private function getPendingJobs()
    {
        try {
            $sql        = "SELECT * FROM `symology_holding` WHERE `pending_filltask_flag` = 0";
            $connection = $this->dbManager->getCustomerConnection();
            $stmt       = $connection->query($sql);

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            $this->log->error('Error occurred', $e);

            throw $e;
        }
    }

    private function processJob(array $job)
    {
        try {
            $caseReference = $job['case_reference'];

            LoggerMDC::put('Reference', $caseReference);

            $this->log->debug('Processing: ' . json_encode($job));

            $getRequestAdditionalGroupResult = $this->symologyReader->getRequestAdditionalGroup('', $caseReference);

            $item          = $getRequestAdditionalGroupResult->Request;
            $jobStatus     = $item->StageDescripton;
            $dateCompleted = null;

            // Check for HistoryDate where HistoryEvent = “7”
            foreach ($item->EventHistory->EventHistoryGet as $eventHistory) {
                if ($eventHistory->HistoryType == 7) {
                    $dateCompleted = $eventHistory->HistoryDate;
                    break;
                }
            }

            // No date completed
            if (is_null($dateCompleted)) {
                $this->updateJobNotCompleted($job['id'], $jobStatus);

                return true;
            }

            $this->updateJobCompleted($job['id'], $jobStatus, $dateCompleted);

            $fillTaskRequest = $this->createCloseFillTaskRequest($caseReference, $job['stage_name'], $jobStatus, $dateCompleted, $job['notes']);

            $this->fillTask->fillTask($this->configuration->get('fillTask', 'fillTaskSite'), $fillTaskRequest);

            return true;
        } catch (\Exception $e) {
            $this->log->error('Error occurred', $e);

            return false;
        }
    }

    private function updateJobNotCompleted($id, $jobStatus)
    {
        $sql        = "UPDATE `symology_holding` SET `job_status` = :jobStatus, `last_api_check` = UTC_TIMESTAMP() WHERE `id` = :id";
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare($sql);

        $stmt->execute([
            'jobStatus' => $jobStatus,
            'id'        => $id
        ]);
    }

    private function updateJobCompleted($id, $jobStatus, $dateCompleted)
    {
        $sql        = "UPDATE `symology_holding` SET `job_status` = :jobStatus, `date_completed` = :dateCompleted, `last_api_check` = UTC_TIMESTAMP(), `pending_filltask_flag` = 1 WHERE `id` = :id";
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare($sql);

        $stmt->execute([
            'jobStatus'     => $jobStatus,
            'dateCompleted' => $dateCompleted,
            'id'            => $id
        ]);
    }

    private function createCloseFillTaskRequest($caseRef, $stageName, $jobStatus, $dateCompleted, $notes)
    {
        return [
            'case_id'        => $caseRef,
            'stage_name'     => $stageName,
            'data'           => [
                'action'         => 'close',
                'job_status'     => $jobStatus,
                'date_completed' => $dateCompleted,
                'notes'          => $notes
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }
}
