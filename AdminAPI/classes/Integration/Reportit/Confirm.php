<?php

namespace FS\Integration\Reportit;

use FS\Common\ConversionsLatLong;
use FS\Common\NZLumberjack\LoggerMDC;
use FS\Integration\Handler\ConfirmReader;
use FS\Integration\Handler\IConfirmReader;
use FS\Integration\Handler\FillTask;
use FS\Integration\Handler\IFillTask;
use FS\Integration\IntegrationBase;

class Confirm extends IntegrationBase
{
    private $api;
    private $fillTask;

    public function __construct($customer, IConfirmReader $confirmReader = null, IFillTask $fillTask = null)
    {
        parent::__construct($customer, 'Confirm');

        $this->api      = $confirmReader ?? new ConfirmReader($this->configuration);
        $this->fillTask = $fillTask ?? new FillTask($this->log);
    }

    public function process(): bool
    {
        $importResult  = $this->importList();
        $processResult = $this->processList();

        return $importResult && $processResult;
    }

    public function importList()
    {
        $enquiriesResult = $this->importEnquiries();
        $defectsResult   = $this->importDefects();

        return $enquiriesResult && $defectsResult;
    }

    private function importEnquiries()
    {
        try {
            $result = true;
            $list   = $this->api->getEnquiryList();

            $this->log->debug('Enquiries retrieved from Confirm: ' . json_encode($list));

            $connection = $this->dbManager->getCustomerConnection();
            $stmt       = $connection->prepare('INSERT INTO `confirm_enquiry_import`(`enquiry_number`, `date_last_updated`, `date_created`, `enquiry_status_code`, `enquiry_status_name`, `outstanding_flag`, `enquiry_description`, `subject_code`, `subject_name`, `external_reference`, `logged_date`) VALUES (:enquiryNumber, :dateLastUpdated, :dateCreated, :enquiryStatusCode, :enquiryStatusName, :outstandingFlag, :enquiryDescription, :subjectCode, :subjectName, :externalReference, :loggedDate) ON DUPLICATE KEY UPDATE `enquiry_number`=`enquiry_number`');

            foreach ($list as $entry) {
                $csv = explode("\t", $entry);

                if (count($csv) >= 11) {
                    $result = $stmt->execute([
                            'enquiryNumber'      => $csv[0],
                            'dateLastUpdated'    => $this->convertDateFormat($csv[1]),
                            'dateCreated'        => $this->convertDateFormat($csv[2]),
                            'enquiryStatusCode'  => $csv[3],
                            'enquiryStatusName'  => $csv[4],
                            'outstandingFlag'    => $csv[5],
                            'enquiryDescription' => $csv[6],
                            'subjectCode'        => $csv[7],
                            'subjectName'        => $csv[8],
                            'externalReference'  => $csv[9],
                            'loggedDate'         => $this->convertDateFormat($csv[10])
                        ])
                        && $result;
                }
            }

            return $result;
        } catch (\Exception $e) {
            $this->log->error('Importing Enquiries failed', $e);

            throw $e;
        }
    }

    private function importDefects()
    {
        try {
            $result = true;
            $list   = $this->api->getDefectList();

            $this->log->debug('Defects retrieved from Confirm: ' . json_encode($list));

            $connection = $this->dbManager->getCustomerConnection();
            $stmt       = $connection->prepare('INSERT INTO `confirm_defect_import` (`defect_number`, `defect_easting`, `defect_northing`, `site_code`, `site_name`, `defect_type_name`, `defect_type_code`, `priority_code`, `target_date`, `defect_date`, `defect_status_flag`, `job_number`, `job_entry_date`, `job_actual_comp_date`, `job_status_flag`, `job_status_code`, `job_status_name`, `job_status_complete`, `date_last_updated`, `job_notes`, `job_location`, `job_type_key`, `job_type_name`, `enquiry_number`, `external_system_ref`) VALUES (:defectNumber, :defectEasting, :defectNorthing, :siteCode, :siteName, :defectTypeName, :defectTypeCode, :priorityCode, :targetDate, :defectDate, :defectStatusFlag, :jobNumber, :jobEntryDate, :jobActualCompDate, :jobStatusFlag, :jobStatusCode, :jobStatusName, :jobStatusComplete, :dateLastUpdated, :jobNotes, :jobLocation, :jobTypeKey, :jobTypeName, :enquiryNumber, :externalSystemRef) ON DUPLICATE KEY UPDATE `defect_number`=`defect_number`');

            foreach ($list as $entry) {
                $csv = explode("\t", $entry);

                if (count($csv) >= 10) {
                    $result = $stmt->execute([
                            'defectNumber'      => $csv[0],
                            'defectEasting'     => $csv[1],
                            'defectNorthing'    => $csv[2],
                            'siteCode'          => $csv[3],
                            'siteName'          => $csv[4],
                            'defectTypeName'    => $csv[5],
                            'defectTypeCode'    => $csv[6],
                            'priorityCode'      => $csv[7],
                            'targetDate'        => $this->convertDateFormat($csv[8]),
                            'defectDate'        => $this->convertDateFormat($csv[9]),
                            'defectStatusFlag'  => $csv[10],
                            'jobNumber'         => $csv[11],
                            'jobEntryDate'      => $this->convertDateFormat($csv[12]),
                            'jobActualCompDate' => $this->convertDateFormat($csv[13]),
                            'jobStatusFlag'     => $csv[14],
                            'jobStatusCode'     => $csv[15],
                            'jobStatusName'     => $csv[16],
                            'jobStatusComplete' => $csv[17],
                            'dateLastUpdated'   => $this->convertDateFormat($csv[18]),
                            'jobNotes'          => $csv[19],
                            'jobLocation'       => $csv[20],
                            'jobTypeKey'        => $csv[21],
                            'jobTypeName'       => $csv[22],
                            'enquiryNumber'     => $csv[23],
                            'externalSystemRef' => $csv[24]
                        ])
                        && $result;
                }
            }

            return $result;
        } catch (\Exception $e) {
            $this->log->error('Importing Defects failed', $e);

            throw $e;
        }
    }

    public function processList()
    {
        $enquiriesResult = $this->processEnquiryList();
        $defectsResult   = $this->processDefectList();

        return $enquiriesResult && $defectsResult;
    }

    private function processEnquiryList()
    {
        try {
            $enquiries = $this->getEnquiriesFromDatabase();

            foreach ($enquiries as $enquiry) {
                $this->processEnquiry($enquiry);
            }
        } catch (\Exception $e) {
            $this->log->error('Processing Enquiry failed', $e);

            throw $e;
        }

        return true;
    }

    private function processDefectList()
    {
        try {
            $defects = $this->getDefectsFromDatabase();

            foreach ($defects as $defect) {
                $this->processDefect($defect);
            }
        } catch (\Exception $e) {
            $this->log->error('Processing Defect failed', $e);

            throw $e;
        }

        return true;
    }

    private function getEnquiriesFromDatabase()
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->query(
            'SELECT `confirm_enquiry_import`.`enquiry_number`, `confirm_enquiry_import`.`enquiry_status_code`, `confirm_enquiry_import`.`outstanding_flag`, `confirm_enquiry_case`.`case_ref` 
            FROM `confirm_enquiry_import` 
            LEFT JOIN `confirm_enquiry_case` ON `confirm_enquiry_case`.`case_ref` = `confirm_enquiry_import`.`external_reference`');

        return $stmt->fetchAll();
    }

    private function getDefectsFromDatabase()
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->query(
            'SELECT `confirm_defect_import`.`defect_number`, `confirm_defect_import`.`defect_easting`, `confirm_defect_import`.`defect_northing`, `confirm_defect_import`.`site_code`, `confirm_defect_import`.`site_name`, `confirm_defect_import`.`defect_type_code`, `confirm_defect_import`.`job_status_code`, `confirm_defect_import`.`job_notes`, `confirm_defect_import`.`job_location`, cd_case.`case_ref`, cd_defect.`defect_number` AS existing_defect  
            FROM `confirm_defect_import`
            LEFT JOIN `confirm_defect_case` AS cd_case ON cd_case.`case_ref` = `confirm_defect_import`.`external_system_ref`
            LEFT JOIN `confirm_defect_case` AS cd_defect ON cd_defect.`defect_number` = `confirm_defect_import`.`defect_number`');

        return $stmt->fetchAll();
    }

    private function processEnquiry(array $enquiry)
    {
        LoggerMDC::put('Reference', $enquiry['enquiry_number']);

        if (empty($enquiry['case_ref'])) {
            $result = true;
        } else {
            $fillTaskRequest = $this->mapEnquiryToFillTask($enquiry);

            $this->fillTask->fillTask($this->configuration->get('fillTask', 'fillTaskSite'), $fillTaskRequest);

            if ($enquiry['outstanding_flag'] == 'Y') {
                $result = true;
            } else {
                $result = $this->removeEnquiryFromDatabase($enquiry['case_ref']);
            }
        }

        if ($result) {
            $this->removeEnquiryFromImportTable($enquiry['enquiry_number']);
        }

        return $result;
    }

    private function processDefect(array $defect)
    {
        LoggerMDC::put('Reference', $defect['defect_number']);

        if (empty($defect['case_ref'])) {
            if (empty($defect['existing_defect'])) {
                // New
                $issueType = $this->getIssueType($defect['defect_type_code']);

                switch ($defect['job_status_code']) {
                    case '0500':
                    case '0750':
                    case '0800':
                        $result = true;
                        break;
                    default:
                        $startThreadRequest = $this->mapDefectToStartThread($defect, $issueType[0], $issueType[1]);
                        $caseRef            = $this->fillTask->startThread($this->configuration->get('fillTask', 'startThreadSite'), $startThreadRequest);
                        $result             = $this->addDefectToDatabase($caseRef, $defect['defect_number']);
                        break;
                }
            } else {
                $result = true;
            }
        } else {
            // Correspond to active report-it case
            switch ($defect['job_status_code']) {
                case '0500':
                case '0750':
                case '0800':
                    $close = true;
                    break;
                default:
                    $close = false;
                    break;
            }
            $fillTaskRequest = $this->mapDefectToFillTask($defect, $close);

            $this->fillTask->fillTask($this->configuration->get('fillTask', 'fillTaskSite'), $fillTaskRequest);

            $result = $close ? $this->removeDefectFromDatabase($defect['case_ref']) : true;
        }

        if ($result) {
            $this->removeDefectFromImportTable($defect['defect_number']);
        }

        return $result;
    }

    private function removeEnquiryFromDatabase(string $caseRef)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('DELETE FROM `confirm_enquiry_case` WHERE `case_ref` = :caseRef');

        return $stmt->execute(['caseRef' => $caseRef]);
    }

    private function removeDefectFromDatabase(string $caseRef)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('DELETE FROM `confirm_defect_case` WHERE `case_ref` = :caseRef');

        return $stmt->execute(['caseRef' => $caseRef]);
    }

    private function addDefectToDatabase(string $caseRef, string $defectNumber)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `confirm_defect_case` (`case_ref`, `defect_number`) VALUES (:caseRef, :defectNumber)');

        return $stmt->execute(['caseRef' => $caseRef, 'defectNumber' => $defectNumber]);
    }

    private function removeEnquiryFromImportTable(string $enquiryNumber)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('DELETE FROM `confirm_enquiry_import` WHERE `enquiry_number` = :enquiryNumber');

        return $stmt->execute(['enquiryNumber' => $enquiryNumber]);
    }

    private function removeDefectFromImportTable(string $defectNumber)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('DELETE FROM `confirm_defect_import` WHERE `defect_number` = :defectNumber');

        return $stmt->execute(['defectNumber' => $defectNumber]);
    }

    private function getIssueType(string $defectTypeCode)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT `issue_type`, `statements` FROM `confirm_defect_reportit_mapping` WHERE `defect_type_code` = :defectTypeCode');

        $stmt->execute(['defectTypeCode' => $defectTypeCode]);

        $result = $stmt->fetchAll();

        if (empty($result)) {
            return ['', ''];
        }

        return [$result[0]['issue_type'], $result[0]['statements']];
    }

    private function mapDefectToStartThread(array $confirmDefect, string $issueType, string $statements)
    {
        $latLng = (new ConversionsLatLong())->osgb36_to_wgs84($confirmDefect['defect_easting'], $confirmDefect['defect_northing']);

        return [
            'process_id'     => $this->configuration->get('fillTask', 'startThreadProcessID'),
            'app_id'         => $this->configuration->get('fillTask', 'appID'),
            'data'           => [
                'externalRef'       => $confirmDefect['defect_number'],
                'issuetype'         => $issueType,
                'statements'        => $statements,
                'pinpointmap'       => "[$latLng[0], $latLng[1]]",
                'locationspecifics' => "$confirmDefect[site_code] $confirmDefect[site_name] $confirmDefect[job_location]",
                'description'       => $confirmDefect['job_notes'],
                'sendemail'         => '',
                'Email_Address'     => ''
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }

    private function mapDefectToFillTask(array $confirmDefect, bool $close)
    {
        if ($close) {
            $action           = 'close';
            $activeStatus     = '';
            $completionStatus = 'Confirm Job ' . $confirmDefect['job_status_code'];

            switch ($confirmDefect['job_status_code']) {
                case '0500':
                    $completionStatus = $completionStatus . ' – Contractor Completed';
                    break;
                case '0750':
                    $completionStatus = $completionStatus . ' – Job Cancelled';
                    break;
                case '0800':
                    $completionStatus = $completionStatus . ' – Job Closed';
                    break;
            }
        } else {
            $action           = 'open';
            $activeStatus     = 'Confirm Job ' . $confirmDefect['job_status_code'];
            $completionStatus = '';

            switch ($confirmDefect['job_status_code']) {
                case '0000':
                    $completionStatus = $completionStatus . ' – Job Raised';
                    break;
                case '0007':
                    $completionStatus = $completionStatus . ' – Held by OCR';
                    break;
                case '0050':
                    $completionStatus = $completionStatus . ' – Ready to commit – Approval required';
                    break;
                case '0100':
                    $completionStatus = $completionStatus . ' – Job Committed';
                    break;
                case '0200':
                    $completionStatus = $completionStatus . ' – Job Received (By Cont)';
                    break;
            }
        }

        return [
            'case_id'        => $confirmDefect['case_ref'],
            'stage_name'     => 'action',
            'data'           => [
                'action'             => $action,
                'summaryNote'        => '',
                'completionStatus'   => $completionStatus,
                'completionOutcomes' => '',
                'activeStatus'       => $activeStatus,
                'currentStatus'      => '',
                'notes'              => ''
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }

    private function mapEnquiryToFillTask(array $confirmEnquiry)
    {
        if (empty($confirmEnquiry['case_ref'])) {
            return null;
        }

        if ($confirmEnquiry['outstanding_flag'] == 'Y') {
            $action           = 'open';
            $activeStatus     = 'Confirm Enquiry ' . $confirmEnquiry['enquiry_status_code'];
            $completionStatus = '';

            switch ($confirmEnquiry['enquiry_status_code']) {
                case '0050':
                    $activeStatus = $activeStatus . ' – New enquiry received';
                    break;
                case '0100':
                    $activeStatus = $activeStatus . ' – Acknowledgement Sent';
                    break;
                case '0200':
                    $activeStatus = $activeStatus . ' – Under Investigation';
                    break;
                case '0400':
                    $activeStatus = $activeStatus . ' – Assign to Another Department';
                    break;
                case '1301':
                    $activeStatus = $activeStatus . ' – Hedge 2 sent';
                    break;
            }
        } else {
            $action           = 'close';
            $activeStatus     = '';
            $completionStatus = 'Confirm Enquiry ' . $confirmEnquiry['enquiry_status_code'];

            switch ($confirmEnquiry['enquiry_status_code']) {
                case '0080':
                    $completionStatus = $completionStatus . ' – Street Lighting Alert Sent';
                    break;
                case '0350':
                    $completionStatus = $completionStatus . ' – Customer Updated – Completed';
                    break;
                case '1200':
                    $completionStatus = $completionStatus . ' – Dealt with at time';
                    break;
            }
        }

        return [
            'case_id'        => $confirmEnquiry['case_ref'],
            'stage_name'     => 'action',
            'data'           => [
                'action'             => $action,
                'summaryNote'        => '',
                'completionStatus'   => $completionStatus,
                'completionOutcomes' => '',
                'activeStatus'       => $activeStatus,
                'currentStatus'      => '',
                'notes'              => ''
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }

    private function convertDateFormat(string $inputDate)
    {
        if (empty($inputDate)) {
            return null;
        }

        $date = \DateTime::createFromFormat('d/m/Y H:i:s', $inputDate);

        return $date->format('Y-m-d H:i:s');
    }
}