<?php

namespace FS\Integration\Reportit;

use FS\Common\NZLumberjack\LoggerMDC;
use FS\Integration\Configuration\ILastRunConfig;
use FS\Integration\Configuration\MayriseConfig;
use FS\Integration\Handler\IMayriseReader;
use FS\Integration\Handler\MayriseReader;
use FS\Integration\Handler\IFillTask;
use FS\Integration\Handler\FillTask;
use FS\Integration\IntegrationBase;

class Mayrise extends IntegrationBase
{
    private $api;
    private $fillTask;
    private $mayriseConfig;

    public function __construct($customer, IMayriseReader $mayriseReader = null, IFillTask $fillTask = null, ILastRunConfig $lastRunConfig = null)
    {
        parent::__construct($customer, 'Mayrise');

        $this->api           = $mayriseReader ?? new MayriseReader($this->configuration);
        $this->fillTask      = $fillTask ?? new FillTask($this->log);
        $this->mayriseConfig = $lastRunConfig ?? new MayriseConfig();
    }

    public function process(): bool
    {
        $importResult  = $this->importList();
        $processResult = $this->processList();

        return $importResult && $processResult;
    }

    public function importList()
    {
        $result   = true;
        $callTime = $this->mayriseConfig->getLastRun($this->dbManager->getCustomerConnection());

        if ($callTime === false) {
            throw new \Exception('changeSince is empty in the database. You must initialise it with a suitable starting value.');
        }

        $runStartTime = new \DateTime();
        $list         = $this->api->getJobsChanged($callTime, true, false);

        $this->log->debug('Retrieved from Mayrise: ' . json_encode($list));

        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `mayrise_import` (`job_number`, `external_reference`, `user_job_status_code`, `work_in_progress_datetime`, `completed_datetime`, `cancelled_datetime`, `logical_status`) VALUES (:jobNumber, :externalReference, :userJobStatusCode, :workInProgressDateTime, :completedDateTime, :cancelledDateTime, :logicalStatus) ON DUPLICATE KEY UPDATE `job_number`=`job_number`');

        foreach ($list as $jobStatus) {
            $result = $stmt->execute([
                    'jobNumber'              => $jobStatus->JobNumber,
                    'externalReference'      => $jobStatus->ExternalReference,
                    'userJobStatusCode'      => $jobStatus->UserJobStatusCode,
                    'workInProgressDateTime' => $jobStatus->WorkInProgressDateTime,
                    'completedDateTime'      => $jobStatus->CompletedDateTime,
                    'cancelledDateTime'      => $jobStatus->CancelledDateTime,
                    'logicalStatus'          => $jobStatus->LogicalStatus,
                ])
                && $result;
        }
        if ($result) {
            $this->mayriseConfig->setLastRun($this->dbManager->getCustomerConnection(), $runStartTime);
        }

        return $result;
    }

    public function processList()
    {
        $result  = true;
        $results = $this->getJobListFromDb();

        foreach ($results as $jobStatus) {
            $result = $this->routeJob($jobStatus) && $result;
        }

        return $result;
    }

    private function getJobListFromDb()
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->query('SELECT * FROM `mayrise_import`');

        return $stmt->fetchAll();
    }

    private function routeJob(array $jobStatus)
    {
        try {
            $result    = true;
            $jobNumber = $jobStatus['job_number'];

            LoggerMDC::put('Reference', $jobNumber);

            $this->log->debug('Processing: ' . json_encode($jobStatus));

            $jobRow = $this->getFromDatabaseByJobId($jobNumber);

            // If job exist in report-it, update
            if ($jobRow) {
                // Check if close
                if (!empty($jobStatus['completed_datetime'])) {
                    $statusCode      = $jobStatus['user_job_status_code'];
                    $fillTaskRequest = $this->mapStatusToFillTask($jobStatus, $jobRow['case_ref']);

                    $this->fillTask->fillTask($this->configuration->get('fillTask', 'fillTaskSite'), $fillTaskRequest);

                    if ($statusCode != 'FI' && $statusCode != 'FN') {
                        // Close job
                        $result = $this->removeFromDatabase($jobRow['case_ref']);
                    }
                }
            } else if (empty($jobStatus['completed_datetime'])) {
                // If job is not active, ignore
                $job = $this->api->getJobByJobNumber($jobNumber);

                $this->log->debug('Retrieved: ' . json_encode($job));

                if ($this->checkIfIgnore($job)) {
                    $result = true;
                } else {
                    $assetId  = $job->AffectedUnits[0]->UnitID;
                    $assetRow = $this->getFromDatabaseByAssetId($assetId);

                    if ($assetRow) {
                        // Follow-up job
                        $caseRef         = $assetRow['case_ref'];
                        $fillTaskRequest = $this->mapStatusToFillTask($jobStatus, $caseRef, true);

                        $this->fillTask->fillTask($this->configuration->get('fillTask', 'fillTaskSite'), $fillTaskRequest);

                        $result = $this->updateToDatabase($caseRef, $jobNumber);

                    } else {
                        // Create new case in Report-it using StartThread
                        $unit               = $this->api->getUnitByUnitID($assetId);
                        $issueType          = $this->getIssueType($unit);
                        $statements         = $this->getStatements($job, $issueType);
                        $startThreadRequest = $this->mapJobToThreadStart($job, $issueType, $statements);
                        $caseRef            = $this->fillTask->startThread($this->configuration->get('fillTask', 'startThreadSite'), $startThreadRequest);
                        $result             = $this->addToDatabase($caseRef, $jobNumber, $assetId);
                    }
                }
            }
            if ($result) {
                $this->removeFromImportTable($jobNumber);
            }

            return $result;
        } catch (\Exception $e) {
            $this->log->error('Error occurred', $e);

            throw $e;
        }
    }

    private function getFromDatabaseByJobId(string $jobNumber)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT `case_ref` FROM `mayrise_case` WHERE `job_number` = :jobNumber');

        $stmt->execute(['jobNumber' => $jobNumber]);

        return $stmt->fetch();
    }

    private function getFromDatabaseByAssetId(string $assetId)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT `case_ref` FROM `mayrise_case` WHERE `asset_id` = :assetId');

        $stmt->execute(['assetId' => $assetId]);

        return $stmt->fetch();
    }

    private function addToDatabase(string $caseRef, string $jobNumber, string $assetId)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `mayrise_case` (`case_ref`, `job_number`, `asset_id`) VALUES (:caseRef, :jobNumber, :assetId)');

        return $stmt->execute(['caseRef' => $caseRef, 'jobNumber' => $jobNumber, 'assetId' => $assetId]);
    }

    private function updateToDatabase(string $caseRef, string $jobNumber)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('UPDATE `mayrise_case` SET `job_number`=:jobNumber WHERE `case_ref`=:caseRef');

        return $stmt->execute(['caseRef' => $caseRef, 'jobNumber' => $jobNumber]);
    }

    private function removeFromDatabase(string $caseRef)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('DELETE FROM `mayrise_case` WHERE `case_ref` = :caseRef');

        return $stmt->execute(['caseRef' => $caseRef]);
    }

    private function removeFromImportTable(string $jobNumber)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('DELETE FROM `mayrise_import` WHERE `job_number` = :jobNumber');

        return $stmt->execute(['jobNumber' => $jobNumber]);
    }

    private function mapStatusToFillTask(array $jobStatus, string $caseRef, bool $assetMatch = false)
    {
        $action           = 'close';
        $completionStatus = '';
        $activeStatus     = '';

        switch ($jobStatus['user_job_status_code']) {
            case 'WC':
                $completionStatus = 'Mayrise job fixed';
                break;
            case 'NF':
                $completionStatus = 'Mayrise no fault';
                break;
            case 'SF':
                $completionStatus = 'Mayrise outside control';
                break;
            case 'FI':
            case 'FN':
                $activeStatus = 'Mayrise job pending';
                $action       = 'open';
                break;
        }
        if ($assetMatch) {
            $activeStatus = 'Mayrise job recommenced';
        }

        return [
            'case_id'        => $caseRef,
            'stage_name'     => 'action',
            'data'           => [
                'action'             => $action,
                'summaryNote'        => $jobStatus['job_number'],
                'completionStatus'   => $completionStatus,
                'completionOutcomes' => '',
                'activeStatus'       => $activeStatus,
                'currentStatus'      => '',
                'notes'              => ''
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }

    private function mapJobToThreadStart(\stdClass $job, string $issueType, string $statements)
    {
        return [
            'process_id'     => $this->configuration->get('fillTask', 'startThreadProcessID'),
            'app_id'         => $this->configuration->get('fillTask', 'appID'),
            'data'           => [
                'externalRef'       => $job->JobNumber,
                'issuetype'         => $issueType,
                'statements'        => $statements,
                'pinpointmap'       => '',
                'mapassetid'        => $job->AffectedUnits[0]->UnitID,
                'locationspecifics' => '',
                'description'       => end($job->MultipleDescriptionCodes)->DescriptionCode,
                'sendemail'         => '',
                'Email_Address'     => '',
                'team'              => 'Mayrise integration'
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }

    private function checkIfIgnore(\stdClass $job)
    {
        switch (end($job->MultipleDescriptionCodes)->DescriptionCode) {
            case '1 LR':
            case '2 CR':
            case '3 NCR':
            case '4 ACR':
            case '9 HIT':
            case 'CLD-B':
            case 'CLD-F':
            case 'CLD-I':
            case 'CLD-L':
            case 'CLD-M':
            case 'CLD-T':
            case 'CLD-W':
            case 'G95':
            case 'WPD X':
            case 'Z99XX':
                return true;
            default:
                return false;
        }
    }

    private function getIssueType(\stdClass $unit)
    {
        $issueType = '';

        switch ($unit->UnitTypeCode) {
            case 'A':
            case 'B':
            case 'D':
            case 'E':
            case 'G':
            case 'H':
            case 'L':
            case 'U':
            case 'V':
            case 'W':
            case 'Y':
            case 'Z':
                $issueType = 'Streetlight';
                break;
            case 'F':
                $issueType = 'Non-illuminated bollards and signs';
                break;
            case 'I':
                $issueType = 'School Patrol lights';
                break;
            case 'K':
            case 'N':
            case 'R':
                $issueType = 'Illuminated traffic bollard';
                break;
            case 'S':
                $issueType = 'Illuminated sign';
                break;
            case 'X':
                $issueType = 'Pedestrian Crossing';
                break;
        }

        return $issueType;
    }

    private function getStatements(\stdClass $job, string $issueType)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT `statements` FROM `mayrise_defect_reportit_mapping` WHERE `job_code` = :jobCode AND `issue_type` = :issueType');

        $stmt->execute([
            'jobCode'   => end($job->MultipleDescriptionCodes)->DescriptionCode,
            'issueType' => $issueType
        ]);

        return $stmt->fetchColumn();
    }
}