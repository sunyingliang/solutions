<?php

namespace FS\Integration\Reportit;

use FS\Common\NZLumberjack\LoggerMDC;
use FS\Integration\Configuration\ILastRunConfig;
use FS\Integration\Configuration\UniformConfig;
use FS\Integration\Handler\IFillTask;
use FS\Integration\Handler\FillTask;
use FS\Integration\Handler\IUniformReader;
use FS\Integration\Handler\UniformReader;
use FS\Integration\IntegrationBase;

class Uniform extends IntegrationBase
{
    private $uniformReader;
    private $fillTask;
    private $uniformConfig;

    public function __construct($customer, IUniformReader $uniformReader = null, IFillTask $fillTask = null, ILastRunConfig $lastRunConfig = null)
    {
        parent::__construct($customer, 'Uniform');

        $this->uniformReader = $uniformReader ?? new UniformReader($this->configuration);
        $this->fillTask      = $fillTask ?? new FillTask($this->log);
        $this->uniformConfig = $lastRunConfig ?? new UniformConfig();
    }

    protected function process(): bool
    {
        $result       = true;
        $runStartTime = new \DateTime();
        $jobs         = $this->getPendingJobs();

        foreach ($jobs as $job) {
            $result = $this->processJob($job) && $result;
        }

        if ($result) {
            $this->uniformConfig->setLastRun($this->dbManager->getCustomerConnection(), $runStartTime);
        }

        return $result;
    }

    private function getPendingJobs()
    {
        $callTime = $this->uniformConfig->getLastRun($this->dbManager->getCustomerConnection());

        if ($callTime === false) {
            throw new \Exception('changeSince is empty in the database. You must initialise it with a suitable starting value.');
        }

        $rawXml    = $this->uniformReader->GetChangedService($callTime);
        $jobs      = [];
        $job       = [];
        $xmlReader = new \XMLReader();

        $xmlReader->XML($rawXml);

        while ($xmlReader->read()) {
            switch ($xmlReader->nodeType) {
                case \XMLReader::ELEMENT:
                    switch ($xmlReader->name) {
                        case 'RefVals':
                            $job = [];
                            break;
                        case 'ReferenceValue':
                            $job['ReferenceValue'] = $xmlReader->readString();
                            break;
                        case 'RequestType':
                            $job['RequestType'] = $xmlReader->readString();
                            break;
                    }
                    break;
                case \XMLReader::END_ELEMENT:
                    if ($xmlReader->name == 'RefVals') {
                        $jobs[] = $job;
                    }
                    break;
            }
        }
        $xmlReader->close();

        return $jobs;
    }

    private function processJob(array $job)
    {
        try {
            $reference = $job['ReferenceValue'];

            LoggerMDC::put('Reference', $reference);
            $this->log->debug('Processing: ' . json_encode($job));

            $sql        = 'SELECT * FROM `uniform_holding` WHERE `uniform_reference` = :reference AND `pending_filltask_flag` = 0';
            $connection = $this->dbManager->getCustomerConnection();
            $stmt       = $connection->prepare($sql);

            $stmt->execute(['reference' => $reference]);

            $dbJob = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!$dbJob) {
                return false;
            }

            switch (strtoupper($job['RequestType'])) {
                case 'GENERAL':
                    $serviceResponse = $this->uniformReader->GetGeneralServiceRequest($reference);
                    break;
                case 'DOG':
                    $serviceResponse = $this->uniformReader->GetDogServiceRequest($reference);
                    break;
                case 'FOOD':
                    $serviceResponse = $this->uniformReader->GetFoodServiceRequest($reference);
                    break;
                case 'NOISE':
                    $serviceResponse = $this->uniformReader->GetNoiseServiceRequest($reference);
                    break;
                case 'PEST':
                    $serviceResponse = $this->uniformReader->GetPestServiceRequest($reference);
                    break;
                default:
                    throw new \Exception('Invalid RequestType: ' . $job['RequestType']);
            }

            $jobStatus     = '';
            $dateCompleted = '';
            $xmlReader     = new \XMLReader();

            $xmlReader->XML($serviceResponse);

            while ($xmlReader->read()) {
                if ($xmlReader->nodeType == \XMLReader::ELEMENT && $xmlReader->name == 'InspectionDetails') {
                    while ($xmlReader->read()) {
                        if ($xmlReader->nodeType == \XMLReader::END_ELEMENT && $xmlReader->name == 'InspectionDetails') {
                            break;
                        } else if ($xmlReader->nodeType == \XMLReader::ELEMENT) {
                            switch ($xmlReader->name) {
                                case 'StatusCode':
                                    $jobStatus = $xmlReader->readString();
                                    break;
                                case 'DateClosed':
                                    $dateCompleted = $xmlReader->readString();
                                    break;
                            }
                        }
                    }
                    break;
                }
            }
            $xmlReader->close();

            // Status not closed
            if (strpos($jobStatus, 'CLO') === false) {
                $this->updateJobNotCompleted($dbJob['id'], $jobStatus);

                return true;
            }

            $this->updateJobCompleted($dbJob['id'], $jobStatus, $dateCompleted);

            $fillTaskRequest = $this->createCloseFillTaskRequest($dbJob['case_reference'], $dbJob['stage_name'], $jobStatus, $dateCompleted, $dbJob['notes']);

            $this->fillTask->fillTask($this->configuration->get('fillTask', 'fillTaskSite'), $fillTaskRequest);

            return true;
        } catch (\Exception $e) {
            $this->log->error('Error occurred', $e);

            return false;
        }
    }

    private function updateJobNotCompleted($id, $jobStatus)
    {
        $sql        = 'UPDATE `uniform_holding` SET `job_status` = :jobStatus, `last_api_check` = UTC_TIMESTAMP() WHERE `id` = :id';
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare($sql);

        $stmt->execute([
            'jobStatus' => $jobStatus,
            'id'        => $id
        ]);
    }

    private function updateJobCompleted($id, $jobStatus, $dateCompleted)
    {
        $sql        = 'UPDATE `uniform_holding` SET `job_status` = :jobStatus, `date_completed` = :dateCompleted, `last_api_check` = UTC_TIMESTAMP(), `pending_filltask_flag` = 1 WHERE `id` = :id';
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare($sql);

        $stmt->execute([
            'jobStatus'     => $jobStatus,
            'dateCompleted' => $dateCompleted,
            'id'            => $id
        ]);
    }

    private function createCloseFillTaskRequest($caseRef, $stageName, $jobStatus, $dateCompleted, $notes)
    {
        return [
            'case_id'        => $caseRef,
            'stage_name'     => $stageName,
            'data'           => [
                'action'         => 'close',
                'job_status'     => $jobStatus,
                'date_completed' => $dateCompleted,
                'notes'          => $notes
            ],
            'ucrn'           => $this->configuration->get('fillTask', 'ucrn'),
            'submissionType' => 'new',
            'published'      => 'true'
        ];
    }
}