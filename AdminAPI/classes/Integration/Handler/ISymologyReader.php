<?php

namespace FS\Integration\Handler;

interface ISymologyReader
{
    public function getRequestAdditionalGroup($serviceCode, $webRequestID): \stdClass;
}