<?php

namespace FS\Integration\Handler;

use FS\Common\LIM\FirmstepLIM;
use FS\Common\SOAPLIMProxyClient;
use FS\Integration\Configuration\FeatureConfig;

abstract class SOAPAccessBase
{
    const RETRY_LIMIT = 3;
    private $soapClient;
    private $wsdl;
    private $location;
    private $useLIM;
    private $config;

    public function __construct($wsdl, $location, $useLIM, FeatureConfig $config)
    {
        $this->wsdl     = $wsdl;
        $this->location = $location;
        $this->useLIM   = $useLIM;
        $this->config   = $config;
    }

    protected function call(string $functionName, array $arguments)
    {
        if (!isset($this->soapClient)) {
            if ($this->useLIM) {
                $this->soapClient = new SOAPLIMProxyClient($this->wsdl, [
                    'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                    'location' => $this->location
                ], $this->setupLIMClient());
            } else {
                $this->soapClient = new \SoapClient($this->wsdl, [
                    'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                    'location' => $this->location
                ]);
            }
        }

        $retry  = 0;
        $result = null;

        while (true) {
            try {
                $result = $this->soapClient->__soapCall($functionName, $arguments);
                break;
            } catch (\Exception $e) {
                $retry++;

                if ($retry > self::RETRY_LIMIT) {
                    throw $e;
                }
            }
        }

        return $result;
    }

    private function setupLIMClient()
    {
        $limConfig = $this->config->getConfig('lim');

        return new FirmstepLIM($limConfig->url, $limConfig->key, $limConfig->iv);
    }
}
