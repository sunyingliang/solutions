<?php

namespace FS\Integration\Handler;

use FS\Integration\Configuration\FeatureConfig;

class MayriseReader extends SOAPAccessBase implements IMayriseReader
{
    public function __construct(FeatureConfig $config)
    {
        $wsdl      = __DIR__ . '/../../../config/mayrise.wsdl';
        $appConfig = $config->getConfig('mayrise');
        $location  = $appConfig->location;
        $useLIM    = $appConfig->useLIM ?? false;

        parent::__construct($wsdl, $location, $useLIM, $config);
    }

    public function getJobsChanged(string $changesSince, bool $includeBackOfficeCreatedJobs, bool $onlyStatusChanges): array
    {
        return $this->call('GetJobsChanged', [$changesSince, $includeBackOfficeCreatedJobs, $onlyStatusChanges]);
    }

    public function getJobByJobNumber(string $jobNumber): \stdClass
    {
        $result = $this->call('GetJobByJobNumber', [$jobNumber]);

        if (!empty($result) && is_array($result)) {
            return $result[0];
        }

        return $result;
    }

    public function getUnitByUnitID(string $unitID): \stdClass
    {
        return $this->call('GetUnitByUnitID', [$unitID]);
    }
}