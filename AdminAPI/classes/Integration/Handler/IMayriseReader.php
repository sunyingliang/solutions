<?php

namespace FS\Integration\Handler;

interface IMayriseReader
{
    public function getJobsChanged(string $changesSince, bool $includeBackOfficeCreatedJobs, bool $onlyStatusChanges): array;

    public function getJobByJobNumber(string $jobNumber): \stdClass;

    public function getUnitByUnitID(string $unitID): \stdClass;
}