<?php

namespace FS\Integration\Handler;

use FS\Common\CURL;
use FS\Common\Exception\FillTaskException;
use FS\Common\NZLumberjack\Logger;

class FillTask implements IFillTask
{
    private $log;

    public function __construct(Logger $logger)
    {
        $this->log = $logger;
    }

    public function startThread(string $url, array $request): string
    {
        $requestString = json_encode($request);

        $this->log->debug('StartThread called: ' . $requestString);

        $response = $this->sendCurlRequest($url, $requestString);

        $this->log->debug('StartThread response: ' . $response);

        return $this->parseStartThreadResponse($response);
    }

    public function fillTask(string $url, array $request): bool
    {
        $requestString = json_encode($request);

        $this->log->debug('FillTask called: ' . $requestString);

        $response = $this->sendCurlRequest($url, $requestString);

        $this->log->debug('FillTask response: ' . $response);

        return $this->parseFillTaskResult($response);
    }

    private function sendCurlRequest(string $url, string $data)
    {
        $ch = CURL::getCurl([
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_HTTPHEADER     => ['Content-Type: application/json'],
            CURLOPT_POSTFIELDS     => $data
        ]);

        if (defined('PROXY_URL') && !empty(PROXY_URL)) {
            CURL::setOptArray($ch, [CURLOPT_PROXY => PROXY_URL]);
        }

        $response = CURL::getResult($ch);
        $status   = CURL::getHTTPCode($ch);

        CURL::closeCurl($ch);

        if ($status == 502) {
            throw new FillTaskException('502: Site not found', $status);
        } else if ($status == 403) {
            throw new FillTaskException('403: Incorrect key or pass', $status);
        }

        return $response;
    }

    private function parseFillTaskResult(string $jsonResponse)
    {
        $json = json_decode($jsonResponse);

        foreach ($json->messages as $messageJson) {
            if ($messageJson->type == 'ERROR') {
                $message = $messageJson->message;

                throw new FillTaskException($message); // FillTaskV2 Failure
            }
        }

        return true;
    }

    private function parseStartThreadResponse(string $jsonResponse)
    {
        $json = json_decode($jsonResponse);

        if (isset($json->data->status)) {
            return $json->data->case_id; // StartThreadV2 Success
        } else {
            foreach ($json->messages as $messageJson) {
                if ($messageJson->type == 'ERROR') {
                    throw new FillTaskException($messageJson->message); // StartThreadV2 Failure
                }
            }
        }

        throw new FillTaskException('Error parsing JSON response'); // Should never be hit
    }
}
