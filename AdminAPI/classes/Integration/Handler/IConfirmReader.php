<?php

namespace FS\Integration\Handler;

interface IConfirmReader
{
    public function getEnquiryList(): array;

    public function getDefectList(): array;
}