<?php

namespace FS\Integration\Handler;

interface IUniformReader
{
    public function GetChangedService(string $changesSince): string;

    public function GetGeneralServiceRequest(string $reference): string;

    public function GetDogServiceRequest(string $reference): string;

    public function GetFoodServiceRequest(string $reference): string;

    public function GetNoiseServiceRequest(string $reference): string;

    public function GetPestServiceRequest(string $reference): string;
}