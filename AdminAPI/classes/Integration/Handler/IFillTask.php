<?php

namespace FS\Integration\Handler;

interface IFillTask
{
    public function startThread(string $url, array $request): string;

    public function fillTask(string $url, array $request): bool;
}
