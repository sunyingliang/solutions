<?php

namespace FS\Integration\Handler;

use FS\Common\HTTPLIMProxyClient;
use FS\Common\LIM\FirmstepLIM;
use FS\Integration\Configuration\FeatureConfig;

class UniformReader implements IUniformReader
{
    private $curlClient;
    private $featureConfig;
    private $config;
    private $useLIM;

    public function __construct(FeatureConfig $config)
    {
        $this->featureConfig = $config;
        $this->config        = $config->getConfig('uniform');
        $this->useLIM        = $this->config->useLIM ?? false;
    }

    public function GetChangedService(string $changesSince): string
    {
        $postUrl  = $this->config->postUrl;
        $location = $this->config->serviceLocation;
        $action   = 'http://www.caps-solutions.co.uk/webservices/connectors/servicerequest/actions/GetDogServiceRequestForUpdateByReferenceValue';

        $request = '<GetChangedServiceRequestRefVals xmlns="http://www.caps-solutions.co.uk/webservices/connectors/731/servicerequest/messagetypes">
            <LastUpdated>' . $changesSince . '</LastUpdated>
        </GetChangedServiceRequestRefVals>';

        $request = $this->wrapLogonRequest($request, $location, $action);

        return $this->call($postUrl, $request);
    }

    public function GetGeneralServiceRequest(string $reference): string
    {
        $postUrl  = $this->config->postUrl;
        $location = $this->config->serviceLocation;
        $action   = 'http://www.caps-solutions.co.uk/webservices/connectors/servicerequest/actions/GetGeneralServiceRequestForUpdateByReferenceValue';

        $request = '<GetGeneralServiceRequestForUpdateByReferenceValue xmlns="http://www.caps-solutions.co.uk/webservices/connectors/74b/servicerequest/messagetypes">
            <ReferenceValue>' . $reference . '</ReferenceValue> 
          </GetGeneralServiceRequestForUpdateByReferenceValue>';

        $request = $this->wrapLogonRequest($request, $location, $action);

        return $this->call($postUrl, $request);
    }

    public function GetDogServiceRequest(string $reference): string
    {
        $postUrl  = $this->config->postUrl;
        $location = $this->config->serviceLocation;
        $action   = 'http://www.caps-solutions.co.uk/webservices/connectors/servicerequest/actions/GetDogServiceRequestForUpdateByReferenceValue';

        $request = '<GetDogServiceRequestForUpdateByReferenceValue xmlns="http://www.caps-solutions.co.uk/webservices/connectors/74b/servicerequest/messagetypes">
            <ReferenceValue>' . $reference . '</ReferenceValue>
        </GetDogServiceRequestForUpdateByReferenceValue>';

        $request = $this->wrapLogonRequest($request, $location, $action);

        return $this->call($postUrl, $request);
    }

    public function GetFoodServiceRequest(string $reference): string
    {
        $postUrl  = $this->config->postUrl;
        $location = $this->config->serviceLocation;
        $action   = 'http://www.caps-solutions.co.uk/webservices/connectors/servicerequest/actions/GetFoodServiceRequestForUpdateByReferenceValue';

        $request = '<GetFoodServiceRequestForUpdateByReferenceValue xmlns="http://www.caps-solutions.co.uk/webservices/connectors/74b/servicerequest/messagetypes">
            <ReferenceValue>' . $reference . '</ReferenceValue>
        </GetFoodServiceRequestForUpdateByReferenceValue>';

        $request = $this->wrapLogonRequest($request, $location, $action);

        return $this->call($postUrl, $request);
    }

    public function GetNoiseServiceRequest(string $reference): string
    {
        $postUrl  = $this->config->postUrl;
        $location = $this->config->serviceLocation;
        $action   = 'http://www.caps-solutions.co.uk/webservices/connectors/servicerequest/actions/GetNoiseServiceRequestForUpdateByReferenceValue';

        $request = '<GetNoiseServiceRequestForUpdateByReferenceValue xmlns="http://www.caps-solutions.co.uk/webservices/connectors/74b/servicerequest/messagetypes">
            <ReferenceValue>' . $reference . '</ReferenceValue>
        </GetNoiseServiceRequestForUpdateByReferenceValue>';

        $request = $this->wrapLogonRequest($request, $location, $action);

        return $this->call($postUrl, $request);
    }

    public function GetPestServiceRequest(string $reference): string
    {
        $postUrl  = $this->config->postUrl;
        $location = $this->config->serviceLocation;
        $action   = 'http://www.caps-solutions.co.uk/webservices/connectors/servicerequest/actions/GetPestServiceRequestForUpdateByReferenceValue';

        $request = '<GetPestServiceRequestForUpdateByReferenceValue xmlns="http://www.caps-solutions.co.uk/webservices/connectors/74b/servicerequest/messagetypes">
            <ReferenceValue>' . $reference . '</ReferenceValue>
        </GetPestServiceRequestForUpdateByReferenceValue>';

        $request = $this->wrapLogonRequest($request, $location, $action);

        return $this->call($postUrl, $request);
    }

    private function wrapLogonRequest($request, $location, $action)
    {
        return '<?xml version="1.0" encoding="utf-8"?>
<MultiPostAction>
  <Post URL="' . $location . '">
    <Header Name="SOAPAction">"http://www.caps-solutions.co.uk/webservices/connectors/common/actions/LogonToConnector"</Header>
    <Header Name="Content-Type">text/xml;charset=utf-8</Header>
    <Body>
      <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
          <LogonToConnector xmlns="http://www.caps-solutions.co.uk/webservices/connectors/75/servicerequest/service">
            <UniformLoginCredentials>
              <DatabaseID>' . $this->config->databaseID . '</DatabaseID>
              <UniformUserName>' . $this->config->userName . '</UniformUserName>
              <UniformPassword>' . $this->config->password . '</UniformPassword>
            </UniformLoginCredentials>
          </LogonToConnector>
        </soap:Body>
      </soap:Envelope>
    </Body>
  </Post>
  <Post URL="' . $location . '">
    <Header Name="SOAPAction">' . $action . '</Header>
    <Header Name="Content-Type">text/xml;charset=utf-8</Header>
    <Body>
      <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>' . $request . '</soap:Body>
      </soap:Envelope>
    </Body>
  </Post>
</MultiPostAction>';
    }

    private function call($request, $location)
    {
        if (!isset($this->curlClient)) {
            $limClient        = $this->useLIM ? $this->setupLIMClient() : null;
            $this->curlClient = new HTTPLIMProxyClient($limClient);
        }

        return $this->curlClient->call($request, $location);
    }

    private function setupLIMClient()
    {
        $limConfig = $this->featureConfig->getConfig('lim');

        return new FirmstepLIM($limConfig->url, $limConfig->key, $limConfig->iv);
    }
}