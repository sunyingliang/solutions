<?php

namespace FS\Integration\Handler;

use FS\Integration\Configuration\FeatureConfig;

class SymologyReader extends SOAPAccessBase implements ISymologyReader
{
    public function __construct(FeatureConfig $config)
    {
        $wsdl      = __DIR__ . '/../../../config/symology.wsdl';
        $appConfig = $config->getConfig('symology');
        $location  = $appConfig->location;
        $useLIM    = $appConfig->useLIM ?? false;

        parent::__construct($wsdl, $location, $useLIM, $config);
    }

    public function getRequestAdditionalGroup($serviceCode, $webRequestID): \stdClass
    {
        $result = $this->call('GetRequestAdditionalGroup', [
            [
                'ServiceCode'     => $serviceCode,
                'InCRNo'          => 0,
                'InWebRequestID'  => $webRequestID,
                'InInterfaceType' => 0
            ]
        ]);

        return $result->GetRequestAdditionalGroupResult;
    }
}
