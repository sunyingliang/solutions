<?php

namespace FS\Integration\Handler;

use FS\Integration\Configuration\FeatureConfig;

class ConfirmReader implements IConfirmReader
{
    private $files;
    private $ftpConn;
    private $config;

    public function __construct(FeatureConfig $config)
    {
        $this->config = $config->getConfig('confirm');
    }

    public function getEnquiryList(): array
    {
        $files     = $this->listDirectory();
        $enquiries = [];

        foreach ($files as $file) {
            if (strpos($file, $this->config->sftpEnquiries) !== false) {
                $enquiries = array_merge($enquiries, $this->downloadFile($file));

                $this->deleteFile($file);
            }
        }

        $this->closeFTPConnection();

        return $enquiries;
    }

    public function getDefectList(): array
    {
        $files   = $this->listDirectory();
        $defects = [];

        foreach ($files as $file) {
            if (strpos($file, $this->config->sftpDefects) !== false) {
                $defects = array_merge($defects, $this->downloadFile($file));

                $this->deleteFile($file);
            }
        }

        $this->closeFTPConnection();

        return $defects;
    }

    function listDirectory()
    {
        if (!isset($this->files)) {
            $conn      = $this->getFTPConnection();
            $dir       = 'ssh2.sftp://' . intval($conn) . $this->config->sftpDirectory;
            $tempArray = [];
            $handle    = opendir($dir);

            // List all the files
            while (false !== ($file = readdir($handle))) {
                if (substr($file, 0, 1) != '.') {
                    if (!is_dir($file)) {
                        $tempArray[] = $file;
                    }
                }
            }

            closedir($handle);

            $this->files = $tempArray;
        }

        return $this->files;
    }

    private function downloadFile($file)
    {
        $conn   = $this->getFTPConnection();
        $data   = null;
        $stream = fopen('ssh2.sftp://' . intval($conn) . $this->config->sftpDirectory . $file, 'r');

        if ($stream) {
            $data = stream_get_contents($stream);
        }

        $lines = preg_split("/(\r\n|\n|\r)/", $data, null, PREG_SPLIT_NO_EMPTY);

        if (count($lines) > 0) {
            array_shift($lines);
        }

        return $lines;
    }

    private function deleteFile($file)
    {
        $conn = $this->getFTPConnection();

        ssh2_sftp_unlink($conn, $this->config->sftpDirectory . $file);
    }

    private function getFTPConnection()
    {
        if (!isset($this->ftpConn)) {
            $sshConn = ssh2_connect($this->config->sftpURL);

            ssh2_auth_password($sshConn, $this->config->sftpUsername, $this->config->sftpPassword);

            $this->ftpConn = ssh2_sftp($sshConn);
        }

        return $this->ftpConn;
    }

    private function closeFTPConnection()
    {
        if (isset($this->ftpConn)) {
            $this->ftpConn = null;
        }
    }
}