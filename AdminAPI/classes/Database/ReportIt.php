<?php

namespace FS\Database;

class ReportIt
{
    public function generateReportIt(\PDO $connection)
    {
        $this->processReportIt($connection);
        $this->processAdjustment($connection);
    }

    private function processReportIt(\PDO $connection)
    {
        $connection->exec('CREATE TABLE IF NOT EXISTS `reportit_asset` (
            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            `fk` varchar(50) DEFAULT NULL,
            `asset_type` varchar(200) NOT NULL,
            `name` varchar(500) NOT NULL,
            `description` varchar(5000) DEFAULT NULL,
            `lat` double NOT NULL,
            `lng` double NOT NULL,
            `iconurl` varchar(500) DEFAULT NULL,
            `point` geometry NOT NULL,
            `circle` geometry NOT NULL,
            PRIMARY KEY (`id`),
            SPATIAL KEY `point` (`point`),
            SPATIAL KEY `circle` (`circle`),
            KEY `asset_type` (`asset_type`)
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1;');
    }

    private function processAdjustment(\PDO $connection)
    {
        // Adjustment for table `reportit_subscriber`
        $sql = 'CREATE TABLE IF NOT EXISTS `reportit_subscriber` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `reference` VARCHAR(50) NOT NULL,
                    `email` VARCHAR(200) NOT NULL,
                    PRIMARY KEY (`id`)
                )';

        if ($connection->exec($sql) === false) {
            Generic::message('Error: Failed to add table {reportit_subscriber}', true);
        }

        // Adjustment for table `reportit_asset`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'issue_type'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `issue_type` VARCHAR(200) NULL DEFAULT NULL AFTER `circle`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {issue_type} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'easting'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `easting` BIGINT(20) NULL DEFAULT NULL AFTER `issue_type`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {easting} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'northing'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `northing` BIGINT(20) NULL DEFAULT NULL AFTER `easting`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {northing} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'okiconurl'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `okiconurl` VARCHAR(500) NULL DEFAULT NULL AFTER `northing`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {okiconurl} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'activeiconurl'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `activeiconurl` VARCHAR(500) NULL DEFAULT NULL AFTER `okiconurl`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {activeiconurl} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'faulticonurl'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `faulticonurl` VARCHAR(500) NULL DEFAULT NULL AFTER `activeiconurl`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {faulticonurl} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'fk'")->fetch(\PDO::FETCH_ASSOC);

        if ($result != false) {
            $sql = 'ALTER TABLE `reportit_asset` DROP COLUMN `fk`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to drop column {fk} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'iconurl'")->fetch(\PDO::FETCH_ASSOC);

        if ($result != false) {
            $sql = 'ALTER TABLE `reportit_asset` DROP COLUMN `iconurl`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to drop column {iconurl} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'id'")->fetch(\PDO::FETCH_ASSOC);

        if ($result != false) {
            $sql = 'ALTER TABLE `reportit_asset` MODIFY COLUMN `id` VARCHAR(50) NOT NULL';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to modify column {id} required in PHP-107 to table {reportit_asset}', true);
            }
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = 'reportit_asset' and COLUMN_NAME = 'radius'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $sql = 'ALTER TABLE `reportit_asset` ADD COLUMN `radius` TINYINT UNSIGNED NOT NULL DEFAULT 100 AFTER `circle`';

            if ($connection->exec($sql) === false) {
                Generic::message('Error: Failed to append new column {radius} required in ISSUE #110 to table {reportit_asset}', true);
            }
        }
    }
}
