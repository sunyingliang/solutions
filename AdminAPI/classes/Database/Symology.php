<?php


namespace FS\Database;

use FS\Common\Generic;

class Symology
{
    public function generateSymology(\PDO $connection)
    {
        $this->process($connection);
    }

    private function process(\PDO $connection)
    {
        if ($connection->exec("CREATE TABLE IF NOT EXISTS `symology_holding` (
          `id` int unsigned NOT NULL AUTO_INCREMENT,
          `case_reference` varchar(255) NOT NULL,
          `task_id` varchar(255) NOT NULL,
          `process_name` varchar(255) NOT NULL,
          `stage_name` varchar(255) NOT NULL,
          `symology_reference` varchar(255) NOT NULL,
          `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `job_status` varchar(255) DEFAULT NULL,
          `date_completed` datetime DEFAULT NULL,
          `notes` text,
          `last_api_check` datetime DEFAULT NULL,
          `pending_filltask_flag` tinyint NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
        )") === false) {
            Generic::message('Error: Failed to add table {symology_holding}');
        }
    }
}