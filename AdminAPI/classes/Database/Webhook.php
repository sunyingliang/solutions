<?php

namespace FS\Database;

use FS\Common\Generic;

class Webhook
{
    public function generateWebhook(\PDO $connection, array $featureList)
    {
        $this->processBase($connection);
        $this->processFeatureList($connection, $featureList);
    }

    private function processBase(\PDO $connection)
    {
        if ($connection->exec("CREATE TABLE IF NOT EXISTS `webhook_router` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `issue_type` varchar(50) NOT NULL,
              `info_statements` varchar(255) NOT NULL DEFAULT '',
              `feature` varchar(128) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `issue_type_info_statements` (`issue_type`,`info_statements`)
            )") === false) {
            Generic::message('Error: Failed to add table {webhook_router}');
        }
    }

    private function processFeatureList(\PDO $connection, array $featureList)
    {
        foreach ($featureList as $featureRow) {
            switch ($featureRow['name']) {
                case 'Confirm':
                    $this->processConfirm($connection);
                    break;
                case 'Mayrise':
                    $this->processMayrise($connection);
                    break;
            }
        }
    }

    private function processConfirm(\PDO $connection)
    {
        if ($connection->exec("CREATE TABLE IF NOT EXISTS `webhook_confirm_router` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `issue_type` varchar(50) NOT NULL,
              `info_statements` varchar(255) NOT NULL DEFAULT '',
              `service_code` varchar(4) NOT NULL,
              `subject_code` varchar(4) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `issue_type_info_statements` (`issue_type`,`info_statements`)
            )") === false) {
            Generic::message('Error: Failed to add table {symology_holding}');
        }
        if ($connection->exec("INSERT INTO `webhook_confirm_router` (`issue_type`, `info_statements`, `service_code`, `subject_code`) VALUES
            ('Drainage', '', 'HIGH', 'HI15'),
            ('Pothole in road', '', 'HIGH', 'HI32'),
            ('Trees', '', 'ARBO', 'T004'),
            ('Hedges', '', 'HIGH', 'HI24'),
            ('Grass Verges', '', 'HIGH', 'HI21'),
            ('Weeds', '', 'HIGH', 'HI21'),
            ('Pavement and Kerbs', 'pothole', 'HIGH', 'HI33'),
            ('Pavement and Kerbs', 'kerb', 'HIGH', 'HI25'),
            ('Pavement and Kerbs', '', 'HIGH', 'HI19'),
            ('Public Rights of Way', 'Bridge', 'PROW', 'PR02'),
            ('Public Rights of Way', 'Gate', 'PROW', 'PR09'),
            ('Public Rights of Way', 'Sign', 'PROW', 'PR16'),
            ('Public Rights of Way', 'Stile', 'PROW', 'PR18'),
            ('Public Rights of Way', 'Obstruction', 'PROW', 'PR13'),
            ('Public Rights of Way', 'Path illegally diverted', 'PROW', 'PR23'),
            ('Public Rights of Way', 'Path not reinstated', 'PROW', 'PR15'),
            ('Public Rights of Way', \"Path doesn't match map\", 'PROW', 'PR06'),
            ('Public Rights of Way', 'Path misuse', 'PROW', 'PR11'),
            ('Public Rights of Way', 'Surface', 'PROW', 'PR20'),
            ('Public Rights of Way', 'Drainage', 'PROW', 'PR07'),
            ('Public Rights of Way', 'Crops', 'PROW', 'PR05'),
            ('Public Rights of Way', 'Intimidating people', 'PROW', 'PR03'),
            ('Public Rights of Way', 'Animals', 'PROW', 'PR01'),
            ('Traffic lights', '', '', 'HI53'),
            ('Non-illuminated bollards and signs', 'non-illuminated traffic bollard', '', 'HI02'),
            ('Non-illuminated bollards and signs', 'non-illuminated post', '', 'HI02'),
            ('Non-illuminated bollards and signs', 'non-illuminated sign', '', 'HI43'),
            ('unlit bollard', '', '', 'HI02'),
            ('unlit sign', '', '', 'HI43')
            ON DUPLICATE KEY UPDATE `id` = `id`") === false) {
            Generic::message('Error: Failed to add records {confirm_defect_reportit_mapping}');
        }
        if ($connection->exec("INSERT INTO `webhook_confirm_router` (`issue_type`, `info_statements`, `service_code`, `subject_code`) VALUES
            ('Drainage', '', 'HIGH', 'HI15'),
            ('Pothole in road', '', 'HIGH', 'HI32'),
            ('Trees', '', 'ARBO', 'T004'),
            ('Hedges', '', 'HIGH', 'HI24'),
            ('Grass Verges', '', 'HIGH', 'HI21'),
            ('Weeds', '', 'HIGH', 'HI21'),
            ('Pavement and Kerbs', 'pothole', 'HIGH', 'HI33'),
            ('Pavement and Kerbs', 'kerb', 'HIGH', 'HI25'),
            ('Pavement and Kerbs', '', 'HIGH', 'HI19'),
            ('Public Rights of Way', 'Bridge', 'PROW', 'PR02'),
            ('Public Rights of Way', 'Gate', 'PROW', 'PR09'),
            ('Public Rights of Way', 'Sign', 'PROW', 'PR16'),
            ('Public Rights of Way', 'Stile', 'PROW', 'PR18'),
            ('Public Rights of Way', 'Obstruction', 'PROW', 'PR13'),
            ('Public Rights of Way', 'Path illegally diverted', 'PROW', 'PR23'),
            ('Public Rights of Way', 'Path not reinstated', 'PROW', 'PR15'),
            ('Public Rights of Way', \"Path doesn't match map\", 'PROW', 'PR06'),
            ('Public Rights of Way', 'Path misuse', 'PROW', 'PR11'),
            ('Public Rights of Way', 'Surface', 'PROW', 'PR20'),
            ('Public Rights of Way', 'Drainage', 'PROW', 'PR07'),
            ('Public Rights of Way', 'Crops', 'PROW', 'PR05'),
            ('Public Rights of Way', 'Intimidating people', 'PROW', 'PR03'),
            ('Public Rights of Way', 'Animals', 'PROW', 'PR01'),
            ('Traffic lights', '', 'HIGH', 'HI53'),
            ('Non-illuminated bollards and signs', 'non-illuminated traffic bollard', 'HIGH', 'HI02'),
            ('Non-illuminated bollards and signs', 'non-illuminated post', 'HIGH', 'HI02'),
            ('Non-illuminated bollards and signs', 'non-illuminated sign', 'HIGH', 'HI43'),
            ('unlit bollard', '', 'HIGH', 'HI02'),
            ('unlit sign', '', 'HIGH', 'HI43')
            ON DUPLICATE KEY UPDATE `id` = `id`") === false) {
            Generic::message('Error: Failed to add records {webhook_confirm_router}');
        }
        if ($connection->exec("INSERT INTO `webhook_router` (`issue_type`, `info_statements`, `feature`) VALUES
            ('Pothole in road', '', 'Confirm'),
            ('Pavement and Kerbs', '', 'Confirm'),
            ('Drainage', '', 'Confirm'),
            ('Trees', '', 'Confirm'),
            ('Hedges', '', 'Confirm'),
            ('Grass Verges', '', 'Confirm'),
            ('Weeds', '', 'Confirm'),
            ('Spillages and obstructions on the road', '', 'Confirm'),
            ('Public Rights of Way', '', 'Confirm'),
            ('Non-illuminated bollards and signs', '', 'Confirm')
            ON DUPLICATE KEY UPDATE `id` = `id`") === false) {
            Generic::message('Error: Failed to add records {webhook_router}');
        }
    }

    private function processMayrise(\PDO $connection)
    {
        if ($connection->exec("INSERT INTO `webhook_router` (`issue_type`, `info_statements`, `feature`) VALUES
            ('Streetlight', '', 'Mayrise'),
            ('Illuminated traffic bollard', '', 'Mayrise'),
            ('Illuminated speed/warning sign', '', 'Mayrise'),
            ('Pedestrian Crossing', 'Belisha Beacon', 'Mayrise')
            ON DUPLICATE KEY UPDATE `id` = `id`") === false) {
            Generic::message('Error: Failed to add records {webhook_router}');
        }
    }
}