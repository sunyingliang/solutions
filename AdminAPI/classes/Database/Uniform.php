<?php

namespace FS\Database;

use FS\Common\Generic;

class Uniform
{
    public function generateUniform(\PDO $connection)
    {
        $this->process($connection);
    }

    private function process(\PDO $connection)
    {
        if ($connection->exec("CREATE TABLE IF NOT EXISTS `uniform_holding` (
          `id` int unsigned NOT NULL AUTO_INCREMENT,
          `case_reference` varchar(255) NOT NULL,
          `task_id` varchar(255) NOT NULL,
          `process_name` varchar(255) NOT NULL,
          `stage_name` varchar(255) NOT NULL,
          `uniform_reference` varchar(255) NOT NULL,
          `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `job_status` varchar(255) DEFAULT NULL,
          `date_completed` datetime DEFAULT NULL,
          `notes` text,
          `last_api_check` datetime DEFAULT NULL,
          `pending_filltask_flag` tinyint NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
        )") === false) {
            Generic::message('Error: Failed to add table {uniform_holding}');
        }

        if ($connection->exec("CREATE TABLE IF NOT EXISTS `uniform_config` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(50) NOT NULL,
              `value` varchar(50) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `name` (`name`)
            )") === false) {
            Generic::message('Error: Failed to add table {uniform_config}');
        }

        if ($connection->exec("INSERT INTO `uniform_config` (`name`, `value`) VALUES
            ('changeSince', UTC_TIMESTAMP()) ON DUPLICATE KEY UPDATE `id` = `id`") === false) {
            Generic::message('Error: Failed to add record {uniform_config}');
        }
    }
}