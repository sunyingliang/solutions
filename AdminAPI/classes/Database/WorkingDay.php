<?php

namespace FS\Database;

use FS\Common\Generic;

class WorkingDay
{
    public function generateWorkingDay(\PDO $connection, $clearExisting = false)
    {
        $error = false;

        if ($clearExisting === true) {
            $error = $this->clearExisting($connection);
        }

        if ($error === false) {
            $this->process($connection);
        }
    }

    private function clearExisting(\PDO $connection)
    {
        try {
            $connection->beginTransaction();

            $connection->exec("DROP PROCEDURE IF EXISTS `add_working_days`");
            $connection->exec("DROP PROCEDURE IF EXISTS `add_working_days_sp`");
            $connection->exec("DROP FUNCTION IF EXISTS `add_working_days`");
            $connection->exec("DROP FUNCTION IF EXISTS `add_working_days_cutoff`");

            $connection->commit();
        } catch (Exception $e) {
            $connection->rollBack();
            Generic::message('Failed [Working Day] function: [' . __FUNCTION__ . '] Error: ' . $e->getMessage());
            return false;
        }

        return true;
    }

    private function process(\PDO $connection)
    {
        try {
            $connection->beginTransaction();

            // Check if holiday table exists, If not create it
            if ($connection->query("SELECT 1 FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA` = DATABASE() AND `TABLE_NAME` = 'holiday'")->fetch() === false) {
                Generic::message('Adding missing holiday table');
                $connection->exec("CREATE TABLE `holiday` (
                    `id` INT NOT NULL AUTO_INCREMENT,
                    `holiday` DATE NOT NULL DEFAULT '0000-00-00',
                    `calendar` VARCHAR(100) NOT NULL,
                    `notes` VARCHAR(255) DEFAULT NULL,
                    `firmstep_id` INT DEFAULT NULL,
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `holiday_calendar` (`holiday`, `calendar`),
                    INDEX `calendar` (`calendar`)
                );");
            }

            // Check if calendar column exists, if not create it
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'holiday' AND column_name = 'calendar'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing holiday table calendar column');
                $connection->exec("ALTER TABLE `holiday` ADD COLUMN `calendar` VARCHAR(100) NOT NULL AFTER `holiday`");
            }

            // Check if notes column exists, if not create it
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'holiday' AND column_name = 'notes'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing holiday table notes column');
                $connection->exec("ALTER TABLE `holiday` ADD COLUMN `notes` VARCHAR(255) DEFAULT NULL AFTER `calendar`");
            }

            // Check if firmstep_id column exists, if not create it
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'holiday' AND column_name = 'firmstep_id'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing holiday table firmstep_id column');
                $connection->exec("ALTER TABLE `holiday` ADD COLUMN `firmstep_id` INT DEFAULT NULL AFTER `notes`");
            }

            // Check if calendar index exists, if not create it
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'holiday' AND index_name = 'calendar'")->fetchColumn(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Creating missing index calendar');
                $connection->exec("ALTER TABLE `holiday` ADD INDEX `calendar` (`calendar`);");
            }

            // Check if holiday is unique key, if not create it
            if ($connection->query("SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'holiday' AND CONSTRAINT_NAME = 'holiday_calendar' AND CONSTRAINT_TYPE = 'UNIQUE'")->fetchColumn(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Creating missing unique key holiday_calendar');
                $connection->exec("ALTER TABLE `holiday` ADD UNIQUE KEY `holiday_calendar` (`holiday`, `calendar`)");
            }

            // CREATE PROCEDURE `add_working_days` IF NOT EXISTS
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_NAME = 'add_working_days'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing procedure add_working_days');
                $connection->exec("CREATE PROCEDURE `add_working_days`(IN StartDate DATETIME, days INT, cal VARCHAR(100), OUT EndDate DATETIME)
BEGIN
    DECLARE TotalDays INT;
    DECLARE FirstPart INT;
--  DECLARE EndDate DATETIME;
    DECLARE LastNum INT;
    DECLARE LastPart INT;
    DECLARE Hols INT;

    SET @@SESSION.max_sp_recursion_depth = 15;

    IF days < 0 THEN
        -- If going back from a weekend, first go forward to the monday, so subtracting one working day gives Friday
        IF DATE_FORMAT(StartDate, '%W') = 'Saturday' THEN 
            SET StartDate = DATE_ADD(StartDate, INTERVAL 2 DAY);
        END IF;

        IF DATE_FORMAT(StartDate, '%W') = 'Sunday' THEN 
                SET StartDate = DATE_ADD(StartDate, INTERVAL 1 DAY);
        END IF;

        SET FirstPart = CASE DATE_FORMAT(StartDate, '%W') 
            WHEN 'Sunday' THEN 0
            WHEN 'Monday' THEN 1
            WHEN 'Tuesday' THEN 2
            WHEN 'Wednesday' THEN 3
            WHEN 'Thursday' THEN 4
            WHEN 'Friday' THEN 5
            WHEN 'Saturday' THEN 0
        END;

        IF ABS(days) < FirstPart THEN
            SET EndDate = DATE_ADD(StartDate, INTERVAL days DAY); 
        ELSE
            -- Count the number of weekends to deduct by removing the free days, then dividing by 5 and adding one
            SET TotalDays = ((ABS(days) - FirstPart) div 5) + 1;
            -- Deduct two days for each weekend, plus the original number of days
            SET TotalDays = (TotalDays * -2) + days;
            -- Add back in an extra day if we started on saturday, because that's half a weekend of adjustment
            IF DATE_FORMAT(StartDate, '%W') = 'Saturday' THEN 
                SET TotalDays = TotalDays + 1;
            END IF;

            SET EndDate = DATE_ADD(StartDate, INTERVAL TotalDays DAY);
        END IF;
    ELSE
        -- Calculate the number of days we can add 'for free' without having to consider weekends
        SET FirstPart = CASE DATE_FORMAT(StartDate, '%W')
            WHEN 'Sunday' THEN 0
            WHEN 'Monday' THEN 5
            WHEN 'Tuesday' THEN 4
            WHEN 'Wednesday' THEN 3
            WHEN 'Thursday' THEN 2
            WHEN 'Friday' THEN 1
            WHEN 'Saturday' THEN 0 
        END;

        IF days < FirstPart THEN
            -- If we can add the working days without hitting a weekend, do so
            SET EndDate = DATE_ADD(StartDate, INTERVAL days DAY); 
        ELSE
            -- We have to add in at least one weekend
            -- Count the number of weekends to add by removing the free days, then dividing by 5 and adding one
            SET TotalDays = ((days - FirstPart) div 5) + 1;
            -- Add two days for each weekend, plus the original number of days
            SET TotalDays = (TotalDays * 2) + days;
            -- Subtract one day (half a weekend) if we started on a Sunday
            IF DATE_FORMAT(StartDate, '%W') = 'Sunday' THEN 
                SET TotalDays = TotalDays - 1;
            END IF;

            SET EndDate = DATE_ADD(StartDate, INTERVAL TotalDays DAY); 
        END IF;
    END IF;

    SET hols = (SELECT COUNT(1) AS total
        FROM holiday 
        WHERE calendar = cal 
          AND ((holiday <= EndDate AND holiday > StartDate) OR (holiday > EndDate AND holiday <= StartDate))
    );

    IF hols > 0 THEN
        -- Insert into debug(msg) values (concat('recursing ',StartDate, hols, cal, EndDate));
        SET StartDate = EndDate;
        CALL add_working_days(StartDate, hols, cal, EndDate);
        -- SET EndDate = add_working_days2(EndDate, hols, cal);
    END IF;
    -- RETURN EndDate;
END");
            }

            // CREATE PROCEDURE `add_working_days_sp` IF NOT EXISTS
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_NAME = 'add_working_days_sp'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing procedure add_working_days_sp');
                $connection->exec("CREATE PROCEDURE `add_working_days_sp`(IN `StartDate` DATETIME, IN `days` INT, IN `cal` VARCHAR(100), IN `rlevel` INT, OUT `EndDate` DATETIME)
BEGIN
    DECLARE TotalDays INT;
    DECLARE FirstPart INT;
--  DECLARE EndDate DATETIME;
    DECLARE LastNum INT;
    DECLARE LastPart INT;
    DECLARE Hols INT;

    SET @@SESSION.max_sp_recursion_depth = 15;

    IF cal = '7' THEN -- Ignore the clever stuff and just add the days
        SET EndDate = DATE_ADD(StartDate, INTERVAL days DAY);
    ELSE
        IF days < 0 THEN
            -- If going back from a weekend, first go forward to the monday, so subtracting one working day gives Friday
            IF DATE_FORMAT(StartDate, '%W') = 'Saturday' THEN 
                SET StartDate = DATE_ADD(StartDate, INTERVAL 2 DAY);
            END IF;

            IF DATE_FORMAT(StartDate,'%W') = 'Sunday' THEN 
                SET StartDate=DATE_ADD(StartDate, INTERVAL 1 DAY);
            END IF;
    
            SET FirstPart = CASE DATE_FORMAT(StartDate, '%W') 
                WHEN 'Sunday' THEN 0
                WHEN 'Monday' THEN 1
                WHEN 'Tuesday' THEN 2
                WHEN 'Wednesday' THEN 3
                WHEN 'Thursday' THEN 4
                WHEN 'Friday' THEN 5
                WHEN 'Saturday' THEN 0
            END;

            IF ABS(days) < FirstPart THEN
                SET EndDate = DATE_ADD(StartDate, INTERVAL days DAY); 
            ELSE
                -- Count the number of weekends to deduct by removing the free days, then dividing by 5 and adding one
                SET TotalDays = ((ABS(days) - FirstPart) div 5) + 1;
                -- Deduct two days for each weekend, plus the original number of days
                SET TotalDays = (TotalDays * -2) + days;
                -- Add back in an extra day if we started on saturday, because that's half a weekend of adjustment
                IF DATE_FORMAT(StartDate,'%W') = 'Saturday' THEN 
                    SET TotalDays=TotalDays+1;
                END IF;

                SET EndDate = DATE_ADD(StartDate, INTERVAL TotalDays DAY);
            END IF;
        ELSE
            -- Calculate the number of days we can add 'for free' without having to consider weekends
            SET FirstPart = CASE DATE_FORMAT(StartDate, '%W')
                WHEN 'Sunday' THEN 0
                WHEN 'Monday' THEN 5
                WHEN 'Tuesday' THEN 4
                WHEN 'Wednesday' THEN 3
                WHEN 'Thursday' THEN 2
                WHEN 'Friday' THEN 1
                WHEN 'Saturday' THEN 0 
            END;

            IF days < FirstPart THEN
                -- If we can add the working days without hitting a weekend, do so.
                SET EndDate = DATE_ADD(StartDate, INTERVAL days DAY); 
            ELSE
                -- We have to add in at least one weekend
                -- Count the number of weekends to add by removing the free days, then dividing by 5 and adding one
                SET TotalDays = ((days - FirstPart) div 5) + 1;
                -- Add two days for each weekend, plus the original number of days
                SET TotalDays = (TotalDays * 2) + days;
                -- Subtract one day (half a weekend) if we started on a Sunday
                IF DATE_FORMAT(StartDate, '%W') = 'Sunday' THEN 
                    SET TotalDays = TotalDays-1;
                END IF;

                SET EndDate = DATE_ADD(StartDate, INTERVAL TotalDays DAY); 
            END IF;
        END IF;

        IF days < 0 THEN
            SET hols = 0 - (SELECT COUNT(1) AS total
                FROM holiday 
                WHERE calendar = cal 
                    AND((holiday >= EndDate AND holiday < StartDate))
            ); 
            -- or (holiday = StartDate and rlevel = 0)
            -- The start date might be a holiday, but if we're recursing then we can ignore that. 
        ELSE
            SET hols = (
                SELECT COUNT(1) AS total
                FROM holiday 
                WHERE calendar = cal AND (
                    (holiday <= EndDate AND holiday > StartDate)
                     OR (holiday=StartDate AND rlevel = 0))
            ); -- The start date might be a holiday, but if we're recursing then we can ignore that.            
        END IF;

        IF hols <> 0 THEN
            -- Insert into debug(msg) values (concat('recursing start:',StartDate, ' hols:',hols, ' cal:',cal, ' end:',EndDate,' rlevel:',rlevel));
            SET StartDate = EndDate;
            CALL add_working_days_sp(StartDate, hols, cal, rlevel + 1, EndDate);
            -- SET EndDate = add_working_days2(EndDate,hols,cal);
        END IF;
    END IF;
    -- RETURN EndDate;
END");
            }

            // CREATE FUNCTION `add_working_days` IF NOT EXISTS
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'FUNCTION' AND ROUTINE_NAME = 'add_working_days'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing function add_working_days');
                $connection->exec("CREATE FUNCTION `add_working_days`(StartDate DATETIME, days INT, cal VARCHAR(100)) RETURNS DATETIME
BEGIN
    DECLARE EndDate DATETIME;
    
    CALL add_working_days_sp(StartDate, days, cal, 0, EndDate);
    
    RETURN EndDate;
END");
            }

            // CREATE FUNCTION `add_working_days_cutoff` IF NOT EXISTS
            if ($connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'FUNCTION' AND ROUTINE_NAME = 'add_working_days_cutoff'")->fetch(\PDO::FETCH_ASSOC) === false) {
                Generic::message('Adding missing function add_working_days_cutoff');
                $connection->exec("CREATE FUNCTION `add_working_days_cutoff`(`StartDate` DATETIME, cutoff TIME, `days` INT, `cal` VARCHAR(100)) RETURNS DATETIME
BEGIN
    -- If the startdate is a working day and the time is after the cutoff time, add one working day
    -- also strip off the time component of the startdate
    DECLARE EndDate DATETIME;

    IF DATE_FORMAT(StartDate,'%W') <> 'Saturday' 
        AND DATE_FORMAT(StartDate,'%W') <> 'Sunday' 
        AND (DATE_FORMAT(StartDate,'%T') > TIME_FORMAT(cutoff,'%T')) 
        -- Should really check the holiday table here too. something like:
        AND NOT EXISTS(SELECT 1 FROM holiday WHERE calendar = cal AND holiday = DATE(StartDate))
        THEN CALL add_working_days_sp(DATE(StartDate), days + 1, cal, 0, EndDate);
        ELSE CALL add_working_days_sp(DATE(StartDate), days, cal, 0, EndDate); 
    END IF;

    RETURN EndDate;
END");
            }

            $connection->commit();
        } catch (Exception $e) {
            $connection->rollBack();
            Generic::message('Failed [Working Day] function [' . __FUNCTION__ . '] error: ' . $e->getMessage());
            return false;
        }

        return true;
    }
}
