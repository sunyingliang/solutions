<?php

namespace FS\Database;

class LLPG
{
    public function generateLLPG(\PDO $connection)
    {
        $this->processLLPG($connection);
        $this->processGeocache($connection);
        $this->processAdjustment($connection);
    }

    private function processLLPG(\PDO $connection)
    {
        $connection->exec('CREATE TABLE IF NOT EXISTS `APPLICATION_CROSS_REFERENCE` (
          `PRO_ORDER` varchar(16) DEFAULT NULL,
          `UPRN` varchar(12) DEFAULT NULL,
          `XREF_KEY` varchar(14) DEFAULT NULL,
          `START_DATE` varchar(16) DEFAULT NULL,
          `LAST_UPDATE_DATE` varchar(16) DEFAULT NULL,
          `ENTRY_DATE` varchar(16) DEFAULT NULL,
          `END_DATE` varchar(16) DEFAULT NULL,
          `CROSS_REFERENCE` varchar(50) DEFAULT NULL,
          `SOURCE` varchar(6) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');

        $connection->exec('CREATE TABLE IF NOT EXISTS `BASIC_LAND_AND_PROPERTY_UNIT` (
          `PRO_ORDER` varchar(16) DEFAULT NULL,
          `UPRN` varchar(12) DEFAULT NULL,
          `LOGICAL_STATUS` varchar(1) DEFAULT NULL,
          `BLPU_STATE` varchar(1) DEFAULT NULL,
          `BLPU_STATE_DATE` varchar(16) DEFAULT NULL,
          `BLPU_CLASS` varchar(4) DEFAULT NULL,
          `PARENT_UPRN` varchar(12) DEFAULT NULL,
          `X_COORDINATE` varchar(16) DEFAULT NULL,
          `Y_COORDINATE` varchar(16) DEFAULT NULL,
          `RPC` varchar(1) DEFAULT NULL,
          `LOCAL_CUSTODIAN_CODE` varchar(4) DEFAULT NULL,
          `START_DATE` varchar(16) DEFAULT NULL,
          `END_DATE` varchar(16) DEFAULT NULL,
          `LAST_UPDATE_DATE` varchar(16) DEFAULT NULL,
          `ENTRY_DATE` varchar(16) DEFAULT NULL,
          `ORGANISATION` varchar(100) DEFAULT NULL,
          `WARD_CODE` varchar(10) DEFAULT NULL,
          `PARISH_CODE` varchar(10) DEFAULT NULL,
          `CUSTODIAN_ONE` varchar(2) DEFAULT NULL,
          `CUSTODIAN_TWO` varchar(2) DEFAULT NULL,
          `CAN_KEY` varchar(14) DEFAULT NULL,
          `LNG` varchar(20) DEFAULT NULL,
          `LAT` varchar(20) DEFAULT NULL,
          KEY `index2` (`UPRN`) USING BTREE
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');

        $connection->exec('CREATE TABLE IF NOT EXISTS `LAND_AND_PROPERTY_IDENTIFIER` (
          `PRO_ORDER` varchar(16) DEFAULT NULL,
          `UPRN` varchar(12) DEFAULT NULL,
          `LPI_KEY` varchar(14) DEFAULT NULL,
          `LANGUAGE` varchar(3) DEFAULT NULL,
          `LOGICAL_STATUS` varchar(1) DEFAULT NULL,
          `START_DATE` varchar(16) DEFAULT NULL,
          `END_DATE` varchar(16) DEFAULT NULL,
          `ENTRY_DATE` varchar(16) DEFAULT NULL,
          `LAST_UPDATE_DATE` varchar(16) DEFAULT NULL,
          `SAO_START_NUMBER` varchar(4) DEFAULT NULL,
          `SAO_START_SUFFIX` varchar(2) DEFAULT NULL,
          `SAO_END_NUMBER` varchar(4) DEFAULT NULL,
          `SAO_END_SUFFIX` varchar(2) DEFAULT NULL,
          `SAO_TEXT` varchar(90) DEFAULT NULL,
          `PAO_START_NUMBER` varchar(4) DEFAULT NULL,
          `PAO_START_SUFFIX` varchar(2) DEFAULT NULL,
          `PAO_END_NUMBER` varchar(4) DEFAULT NULL,
          `PAO_END_SUFFIX` varchar(2) DEFAULT NULL,
          `PAO_TEXT` varchar(90) DEFAULT NULL,
          `USRN` varchar(8) DEFAULT NULL,
          `LEVEL` varchar(30) DEFAULT NULL,
          `POSTAL_ADDRESS` varchar(1) DEFAULT NULL,
          `POSTCODE` varchar(8) DEFAULT NULL,
          `POST_TOWN` varchar(30) DEFAULT NULL,
          `OFFICIAL_FLAG` varchar(1) DEFAULT NULL,
          `CUSTODIAN_ONE` varchar(2) DEFAULT NULL,
          `CUSTODIAN_TWO` varchar(2) DEFAULT NULL,
          `CAN_KEY` varchar(14) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');

        $connection->exec('CREATE TABLE `llpg` (
            `ID` VARCHAR(12) NULL COLLATE \'latin1_swedish_ci\',
            `UPRN` VARCHAR(12) NULL COLLATE \'latin1_swedish_ci\',
            `LAT` BINARY(0) NULL,
            `LNG` BINARY(0) NULL,
            `HOUSE_NAME_NUMBER` VARCHAR(206) NULL COLLATE \'latin1_swedish_ci\',
            `STREET_NAME` VARCHAR(100) NULL COLLATE \'latin1_swedish_ci\',
            `LOCALITY_NAME` VARCHAR(35) NULL COLLATE \'latin1_swedish_ci\',
            `TOWN_NAME` VARCHAR(30) NULL COLLATE \'latin1_swedish_ci\',
            `COUNTY_NAME` CHAR(0) NOT NULL COLLATE \'utf8_general_ci\',
            `POST_TOWN` VARCHAR(30) NULL COLLATE \'latin1_swedish_ci\',
            `POSTCODE` VARCHAR(8) NULL COLLATE \'latin1_swedish_ci\',
            `USRN` VARCHAR(8) NULL COLLATE \'latin1_swedish_ci\',
            `WARD` VARCHAR(10) NULL COLLATE \'latin1_swedish_ci\',
            `PARISH` VARCHAR(10) NULL COLLATE \'latin1_swedish_ci\',
            `ADMINISTRATIVE_AREA` VARCHAR(30) NULL COLLATE \'latin1_swedish_ci\'
        ) ENGINE=MyISAM');

        $connection->exec('CREATE TABLE IF NOT EXISTS `llpgcache` (
            ID int, 
            UPRN varchar(200), 
            LAT varchar(20), 
            LNG varchar(20), 
            PAO varchar(1000), 
            SAO varchar(1000), 
            HOUSE_NAME_NUMBER varchar(200), 
            STREET_NAME varchar(200), 
            LOCALITY_NAME varchar(200), 
            TOWN_NAME varchar(200), 
            COUNTY_NAME varchar(200), 
            POST_TOWN varchar(200), 
            POSTCODE varchar(20), 
            USRN varchar(200), 
            WARD varchar(200), 
            PARISH varchar(200), 
            ADMINISTRATIVE_AREA varchar(200),
            ORGANISATION VARCHAR(200), 
            LANGUAGE VARCHAR(3), 
            BLPU_CLASS VARCHAR(4)
        )');

        $connection->exec('CREATE TABLE IF NOT EXISTS `LLPG_METADATA` (
          `GAZ_NAME` varchar(60) DEFAULT NULL,
          `GAZ_SCOPE` varchar(60) DEFAULT NULL,
          `TER_OF_USE` varchar(60) DEFAULT NULL,
          `LINKED_DATA` varchar(100) DEFAULT NULL,
          `GAZ_OWNER` varchar(60) DEFAULT NULL,
          `NGAZ_FREQ` varchar(1) DEFAULT NULL,
          `CUSTODIAN_NAME` varchar(40) DEFAULT NULL,
          `CUSTODIAN_UPRN` varchar(12) DEFAULT NULL,
          `LOCAL_CUSTODIAN_CODE` varchar(4) DEFAULT NULL,
          `CO_ORD_SYSTEM` varchar(40) DEFAULT NULL,
          `CO_ORD_UNIT` varchar(10) DEFAULT NULL,
          `META_DATE` varchar(16) DEFAULT NULL,
          `CLASS_SCHEME` varchar(40) DEFAULT NULL,
          `GAZ_DATE` varchar(16) DEFAULT NULL,
          `LANGUAGE` varchar(3) DEFAULT NULL,
          `CHARACTER_SET` varchar(30) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');

        $connection->exec('CREATE TABLE IF NOT EXISTS `STREET_DESCRIPTOR` (
          `PRO_ORDER` varchar(16) DEFAULT NULL,
          `USRN` varchar(8) DEFAULT NULL,
          `STREET_DESCRIPTOR` varchar(100) DEFAULT NULL,
          `LOCALITY_NAME` varchar(35) DEFAULT NULL,
          `TOWN_NAME` varchar(30) DEFAULT NULL,
          `ADMINISTRATIVE_AREA` varchar(30) DEFAULT NULL,
          `LANGUAGE` varchar(3) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');

        $connection->exec('CREATE TABLE IF NOT EXISTS `STREET_RECORD` (
          `PRO_ORDER` varchar(16) DEFAULT NULL,
          `USRN` varchar(8) DEFAULT NULL,
          `RECORD_TYPE` varchar(1) DEFAULT NULL,
          `SWA_ORG_REF_NAMING` varchar(4) DEFAULT NULL,
          `STATE` varchar(1) DEFAULT NULL,
          `STATE_DATE` varchar(16) DEFAULT NULL,
          `STREET_SURFACE` varchar(1) DEFAULT NULL,
          `STREET_CLASSIFICATION` varchar(1) DEFAULT NULL,
          `VERSION` varchar(3) DEFAULT NULL,
          `RECORD_ENTRY_DATE` varchar(16) DEFAULT NULL,
          `LAST_UPDATE_DATE` varchar(16) DEFAULT NULL,
          `STREET_START_DATE` varchar(16) DEFAULT NULL,
          `STREET_END_DATE` varchar(16) DEFAULT NULL,
          `STREET_START_X` varchar(16) DEFAULT NULL,
          `STREET_START_Y` varchar(16) DEFAULT NULL,
          `STREET_END_X` varchar(16) DEFAULT NULL,
          `STREET_END_Y` varchar(16) DEFAULT NULL,
          `STREET_TOLERANCE` varchar(3) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'tcase'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE  FUNCTION `tcase`(str text) RETURNS text CHARSET latin1
                begin
                  declare result text default '';
                  declare i int default 1;
                  if(str is null) then
                    return null;
                  end if;
                  while(i <= length(str)) do
                    if(i = 1) or ((i > 1) and ((lower(substring(result, i - 1, 1) < 'a'))
                        or (lower(substring(result, i - 1, 1) > 'z')))) then
                      set result = concat(result, upper(substr(str, i, 1)));
                    else
                      set result = concat(result, lower(substr(str, i, 1)));
                    end if;
                    set i = i + 1;
                  end while;
                  return result;
                end");
        }
    }

    private function processGeocache(\PDO $connection)
    {
        $connection->exec("CREATE TABLE IF NOT EXISTS `geocache` (
            `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `uprn` VARCHAR(200) NOT NULL,
            `usrn` VARCHAR(200) NOT NULL,
            `lat` DOUBLE NOT NULL,
            `lng` DOUBLE NOT NULL,
            `point` GEOMETRY NOT NULL,
            `circle` GEOMETRY NOT NULL,
            `radius` INT(11) NOT NULL,
            PRIMARY KEY (`id`),
            SPATIAL INDEX `point` (`point`),
            SPATIAL INDEX `circle` (`circle`),
            INDEX `radius` (`radius`),
            INDEX `uprn` (`uprn`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
        AUTO_INCREMENT=1;");

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'getpolygon'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE FUNCTION `getpolygon`(`lat` DOUBLE, `lng` DOUBLE, `rm` SMALLINT, `corner` TINYINT)
                    RETURNS geometry
                    LANGUAGE SQL
                    DETERMINISTIC
                    CONTAINS SQL
                    SQL SECURITY DEFINER
                    COMMENT ''
                BEGIN  
                DECLARE radius double;  
                DECLARE i TINYINT DEFAULT 1;  
                DECLARE a, b, c DOUBLE;  
                DECLARE res TEXT;
                set radius=rm/1000;
                  
                IF corner < 3 || radius > 500 THEN  
                    RETURN NULL;  
                END IF;  
                  
                SET res = CONCAT(lat + radius / 111.12, ' ', lng, ',');  
                  
                WHILE i < corner  do
                    SET c = RADIANS(360 / corner * i);  
                    SET a = lat + COS(c) * radius / 111.12;  
                    SET b = lng + SIN(c) * radius / (COS(RADIANS(lat + COS(c) * radius / 111.12 / 111.12)) * 111.12);  
                    SET res = CONCAT(res, a, ' ', b, ',');  
                    SET i = i + 1;  
                END WHILE;  
                  
                RETURN ST_GEOMFROMTEXT(CONCAT('POLYGON((', res, lat + radius / 111.12, ' ', lng, '))'));  
                END");
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'distance'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE FUNCTION `distance`(`a` POINT, `b` POINT)
                RETURNS double
                LANGUAGE SQL
                DETERMINISTIC
                CONTAINS SQL
                SQL SECURITY DEFINER
                COMMENT ''
            RETURN ifnull(acos(sin(ST_X(a)) * sin(ST_X(b)) + cos(ST_X(a)) * cos(ST_X(b)) * cos(ST_Y(b) - ST_Y(a))) * 6380, 0)");
        }

        // Procedure `nearestusrnuprn`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'nearestusrnuprn'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE PROCEDURE `nearestusrnuprn`(IN `llat` double, IN `llng` double, IN `coords` VARCHAR(100))
                LANGUAGE SQL
                NOT DETERMINISTIC
                CONTAINS SQL
                SQL SECURITY DEFINER
                COMMENT ''
            begin
                declare i bigint;
                declare u varchar(200);
                declare s varchar(200);
                declare p point;
                declare r int;
                declare lat double;
                declare lng double;
                    -- we can pass in lat and lng as doubles, or as a string in the form 'lat,lng'
                if llat=0 and locate(',',coords)>0 then 
                    set lat=convert(trim(substring_index(coords,',',1)) , dec(24,20));
                else
                    set lat=llat;	
                end if;
                if llng=0 and locate(',',coords)>0 then 
                    set lng=convert(trim(substring_index(coords,',',-1)) , dec(24,20));
                else
                    set lng=llng;
                end if;
            
                set p = ST_GEOMFROMTEXT(CONCAT('POINT(', lat, ' ', lng, ')'));
                if exists(select 0 from geocache where radius=100 and MBRContains(circle, p)) then	
                    set r=100;		
                elseif exists(select 0 from geocache where radius=500 and MBRContains(circle, p)) then	
                    set r=500;
                elseif exists(select 0 from geocache where radius=1000 and MBRContains(circle, p)) then	
                    set r=1000;
                elseif exists(select 0 from geocache where radius=2000 and MBRContains(circle, p)) then	
                    set r=2000;
                else
                    set r=0;
                end if;
                
                select 
                    llpgcache.uprn as nearest_uprn, 
                    llpgcache.usrn as nearest_usrn, 
                    llpgcache.usrn as street, 
                    llpgcache.uprn as property, 
                    r as radius from llpgcache 
                    inner join geocache on llpgcache.uprn=geocache.uprn and geocache.radius=r
                    and MBRContains(circle, p) order by ST_Distance(point,p) limit 1;
            
                -- return u;
            end");
        }

        // Procedure `buildgeocache`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'buildgeocache'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE PROCEDURE `buildgeocache`()
                LANGUAGE SQL
                NOT DETERMINISTIC
                CONTAINS SQL
                SQL SECURITY DEFINER
                COMMENT ''
            begin
                -- remove geocache records that don't match the llpgcache (without temporary table as we don't have permissions) 
                delete from geocache  
                where uprn in( 
                  select a.uprn
                  from llpgcache a
                      left join (select uprn, lat, lng,radius from geocache) b on a.uprn=b.uprn
                          and a.lng=b.lng
                          and a.lat=b.lat
                          and b.radius=500
                  where b.uprn is null);

                -- insert 100m radius geocaches
                insert into geocache(uprn, usrn,lat,lng,point,circle, radius)
                select uprn,usrn, lat,lng, 
                    GEOMFROMTEXT(CONCAT('POINT(', lat, ' ', lng, ')')), 
                    GETPOLYGON(lat, lng, 100, 12), 100
                from llpgcache 
                where uprn not in (select uprn from geocache where radius=100);
            
                -- insert 500m radius geocaches
                insert into geocache(uprn, usrn,lat,lng,point,circle, radius)
                select uprn,usrn, lat,lng, 
                    GEOMFROMTEXT(CONCAT('POINT(', lat, ' ', lng, ')')), 
                    GETPOLYGON(lat, lng, 500, 12), 500
                from llpgcache
                where uprn not in (select uprn from geocache where radius=500);
            
                -- insert 1000m radius geocaches
                insert into geocache(uprn, usrn,lat,lng,point,circle, radius)
                select uprn,usrn, lat,lng, 
                    GEOMFROMTEXT(CONCAT('POINT(', lat, ' ', lng, ')')), 
                    GETPOLYGON(lat, lng, 1000, 12), 1000
                from llpgcache
                where uprn not in (select uprn from geocache where radius=1000);
            
                -- insert 2000m radius geocaches
                insert into geocache(uprn, usrn,lat,lng,point,circle, radius)
                select uprn,usrn, lat,lng, 
                    GEOMFROMTEXT(CONCAT('POINT(', lat, ' ', lng, ')')), 
                    GETPOLYGON(lat, lng, 2000, 12), 2000
                from llpgcache
                where uprn not in (select uprn from geocache where radius=2000);
            end");
        }
    }

    private function processAdjustment(\PDO $connection)
    {
        // Adjustment for procedure `nearestusrnuprn`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'nearestusrnuprn'")->fetch(\PDO::FETCH_ASSOC);

        if ($result !== false) {
            $connection->exec("DROP PROCEDURE `nearestusrnuprn`");
        }

        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'nearestproperty'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE PROCEDURE `nearestproperty`(IN `coords` VARCHAR(100), IN `userlat` double, IN `userlng` double)
                LANGUAGE SQL
                NOT DETERMINISTIC
                CONTAINS SQL
                SQL SECURITY DEFINER
                COMMENT ''
            begin
                declare i bigint;
                declare u varchar(200);
                declare s varchar(200);
                declare p point;
                declare r int;
                declare lat double;
                declare lng double;
                    -- we can pass in lat and lng as doubles, or as a string in the form 'lat,lng'
                if locate(',',coords)>0 and coords<>',' then 
                    set lat=convert(trim(substring_index(coords,',',1)) , dec(24,20));
                    set lng=convert(trim(substring_index(coords,',',-1)) , dec(24,20));
                else
                    set lat=userlat;
                    set lng=userlng;
                end if;
            
                set p = GEOMFROMTEXT(CONCAT('POINT(', lat, ' ', lng, ')'));
                if exists(select 0 from geocache where radius=100 and contains(circle, p)) then	
                    set r=100;		
                elseif exists(select 0 from geocache where radius=500 and contains(circle, p)) then	
                    set r=500;
                elseif exists(select 0 from geocache where radius=1000 and contains(circle, p)) then	
                    set r=1000;
                elseif exists(select 0 from geocache where radius=2000 and contains(circle, p)) then	
                    set r=2000;
                else
                    set r=0;
                end if;
                
                select 
                    llpgcache.uprn as nearest_uprn, 
                    llpgcache.usrn as nearest_usrn, 
                    llpgcache.POSTCODE as nearest_postcode,
                    concat(coalesce(concat(sao,' '),''),pao,' ',street_name,', ',town_name) as nearest_address,
                    r as radius from llpgcache 
                    inner join geocache on llpgcache.uprn=geocache.uprn and geocache.radius=r
                    and contains(circle, p) order by distance(point,p) limit 1;
            
                -- return u;
            end");
        }

        // Adjustment for table `llpg`
        $connection->exec("DROP TABLE IF EXISTS `llpg`");

        $connection->exec("CREATE OR REPLACE VIEW `llpg` AS 
                               select `a`.`UPRN` AS `ID`,
                                      `a`.`UPRN` AS `UPRN`,
                                      NULL AS `LAT`,
                                      NULL AS `LNG`,
                                      concat(`a`.`PAO_TEXT`,
                                             `a`.`PAO_START_NUMBER`,
                                             `a`.`PAO_START_SUFFIX`,
                                             (case when (`a`.`PAO_END_NUMBER` <> '') then concat('-',`a`.`PAO_END_NUMBER`,`a`.`PAO_END_SUFFIX`) else '' end),
                                             `a`.`SAO_TEXT`,
                                             `a`.`SAO_START_NUMBER`,
                                             `a`.`SAO_START_SUFFIX`,
                                             (case when (`a`.`SAO_END_NUMBER` <> '') then concat('-',`a`.`SAO_END_NUMBER`) else '' end),
                                             `a`.`SAO_END_SUFFIX`) AS `HOUSE_NAME_NUMBER`,
                                      `b`.`STREET_DESCRIPTOR` AS `STREET_NAME`,
                                      `b`.`LOCALITY_NAME` AS `LOCALITY_NAME`,
                                      `b`.`TOWN_NAME` AS `TOWN_NAME`,
                                      '' AS `COUNTY_NAME`,
                                      `a`.`POST_TOWN` AS `POST_TOWN`,
                                      `a`.`POSTCODE` AS `POSTCODE`,
                                      `a`.`USRN` AS `USRN`,
                                      `c`.`WARD_CODE` AS `WARD`,
                                      `c`.`PARISH_CODE` AS `PARISH`,
                                      `b`.`ADMINISTRATIVE_AREA` AS `ADMINISTRATIVE_AREA` 
                               from (
                                        (
                                            `LAND_AND_PROPERTY_IDENTIFIER` `a` 
                                             join 
                                             `STREET_DESCRIPTOR` `b` 
                                             on(
                                                (`a`.`USRN` = `b`.`USRN`)
                                             )
                                         ) 
                                         join 
                                         `BASIC_LAND_AND_PROPERTY_UNIT` `c` 
                                         on(
                                            (`a`.`UPRN` = `c`.`UPRN`)
                                         )
                                     ) 
                               where (`a`.`POSTAL_ADDRESS` = 'Y')");

        // Adjustment for procedure `explode`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'explode'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE PROCEDURE `explode`(IN `x` varchar(6000), IN `delim` varchar(10))
                                LANGUAGE SQL
                                NOT DETERMINISTIC
                                CONTAINS SQL
                                SQL SECURITY DEFINER
                                COMMENT ''
                            begin
                                SET @Valcount = ROUND ((CHAR_LENGTH(x) - CHAR_LENGTH( REPLACE ( x, delim, '') )  ) / CHAR_LENGTH(delim)    ) +1;
                            -- 	substrCount(x,delim)+1;
                                SET @v1=0;
                                drop table if exists splitResults;
                                create temporary table splitResults (split_value varchar(255));
                                WHILE (@v1 < @Valcount) DO
                                    set @val = REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, @v1+1), LENGTH(SUBSTRING_INDEX(x, delim, @v1)) + 1), delim, '');
                                    
                            --		stringSplit(x,delim,@v1+1);
                                    INSERT INTO splitResults (split_value) VALUES (@val);
                                    SET @v1 = @v1 + 1;
                                END WHILE;
                                select split_value as exploded from splitResults;
                                drop table splitResults;
                            END");
        }

        // Adjustment for procedure `reportis_getpins`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'reportit_getpins'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE PROCEDURE `reportit_getpins`(IN `pissuetype` varchar(500) , IN `passetpoint` varchar(50) 
                                , IN `onicon` VARCHAR(500), IN `officon` VARCHAR(500))
                                LANGUAGE SQL
                                NOT DETERMINISTIC
                                CONTAINS SQL
                                SQL SECURITY DEFINER
                                COMMENT ''
                            begin
                                select 
                                    reference as display,
                                    concat(reference,' ',
                                        coalesce(infostatements,''),' ',
                                        coalesce(activestatus,''),
                                        ' [Near ',address,
                                        ', first raised ',date_format(date_created,'%a %D %b %Y'),']') as name,
                                        a1.description,
                                    i.lat,i.lng,
                                    coalesce(a1.activeiconurl,onicon) as mapIconSelected,
                                    coalesce(a1.faulticonurl,officon) as mapIconUnselected,
                                    activestatus,
                                    i.reference as mergeref,
                                    a1.id as assetid,
                                    a1.name as assetname,
                                    a1.description as assetdescription
                                from reportit_issue i
                                    left join reportit_asset a1 on i.assetid=a1.id
                                where	i.issue_type=pissuetype	and status='open'
                                UNION
                                select 
                                  id as display,
                                  name,
                                  description,
                                  lat,lng, 
                                  coalesce(activeiconurl,onicon) as mapIconSelected, 
                                  coalesce(okiconurl,officon) as mapIconUnselected,
                                  '' as activestatus,
                                  '' as mergeref,
                                  id as assetid,
                                    name as assetname,
                                    description as assetdescription
                                from reportit_asset a2
                                where a2.issue_type=pissuetype 
                                and contains(a2.circle,GEOMFROMTEXT(passetpoint))
                                and not exists (select 0 from reportit_issue i2 where i2.status='open' and i2.assetid=a2.id);
                            end");
        }

        // Adjustment for table `reportit_issue`
        $connection->exec("CREATE TABLE IF NOT EXISTS `reportit_issue` (
                                `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                                `taskid` VARCHAR(50) NOT NULL,
                                `reference` VARCHAR(50) NOT NULL,
                                `uprn` VARCHAR(200) NOT NULL,
                                `usrn` VARCHAR(200) NOT NULL,
                                `lat` DOUBLE NOT NULL,
                                `lng` DOUBLE NOT NULL,
                                `status` VARCHAR(100) NOT NULL,
                                `issue_type` VARCHAR(50) NOT NULL,
                                `summary` VARCHAR(1000) NOT NULL,
                                `description` VARCHAR(6000) NULL DEFAULT NULL,
                                `date_created` DATETIME NOT NULL,
                                `date_updated` DATETIME NOT NULL,
                                `point` GEOMETRY NOT NULL,
                                `circle` GEOMETRY NOT NULL,
                                `radius` INT(11) NULL DEFAULT NULL,
                                `easting` BIGINT(20) NULL DEFAULT NULL,
                                `northing` BIGINT(20) NULL DEFAULT NULL,
                                `team` VARCHAR(200) NULL DEFAULT NULL,
                                `address` VARCHAR(500) NULL DEFAULT NULL,
                                `activeStatus` VARCHAR(500) NULL DEFAULT NULL,
                                `activeAttributes` VARCHAR(1000) NULL DEFAULT NULL,
                                `usedStatus` VARCHAR(1000) NULL DEFAULT NULL,
                                `infoStatements` VARCHAR(200) NULL DEFAULT NULL,
                                `print` LONGTEXT NULL,
                                `assetid` BIGINT(20) NULL DEFAULT NULL,
                                `mergeref` VARCHAR(50) NULL DEFAULT NULL,
                                `teamname` VARCHAR(100) NULL DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                SPATIAL INDEX `point` (`point`),
                                SPATIAL INDEX `circle` (`circle`),
                                INDEX `speeder` (`status`, `issue_type`)
                            )
                            COLLATE='latin1_swedish_ci'
                            ENGINE=MyISAM;");

        // Adjustment for procedure `reportit_updateissue`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() and ROUTINE_NAME = 'reportit_updateissue'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE PROCEDURE `reportit_updateissue`(IN `preference` varchar(50), IN `puprn` varchar(200), IN `pusrn` varchar(200), IN `pcoords` VARCHAR(100), IN `pstatus` varchar(100), IN `pissue_type` varchar(50), IN `psummary` varchar(100), IN `pdescription` varchar(6000), IN `pradius` int, IN `peasting` bigint, IN `pnorthing` bigint, IN `pemail` varchar(200), IN `pteam` varchar(200), IN `pmergeref` VARCHAR(50), IN `paddress` VARCHAR(500), IN `pactivestatus` VARCHAR(500), IN `pactiveattributes` VARCHAR(1000), IN `pinfostatements` VARCHAR(200), IN `pmapassetid` BIGINT, IN `pmergedbid` VARCHAR(50), IN `pteamname` VARCHAR(100))
                                LANGUAGE SQL
                                NOT DETERMINISTIC
                                CONTAINS SQL
                                SQL SECURITY DEFINER
                                COMMENT ''
                            begin
                                declare plat double;
                                declare plng double;
                                set plat=convert(trim(substring_index(pcoords,',',1)) , dec(24,20));
                                set plng=convert(trim(substring_index(pcoords,',',-1)) , dec(24,20));
                                
                                -- this is called on submission of all reportit stages. It creates or updates the issue record, and adds a subscriber if necessary
                                -- on creation of all reportit stages, an integration sets the taskid with: update reportit_issue set taskid={db_id} where reference={case_ref}
                                if pemail is null then
                                    set pemail=''; 
                                end if;
                                if not exists (select 0 from reportit_issue where reference=preference) then
                                    -- insert record
                                    insert into reportit_issue(taskid,reference,uprn,usrn,lat,lng,status,issue_type,
                                        summary,description, date_created,date_updated,point,circle,radius,easting,northing,team,address,activestatus,activeattributes,infoStatements,assetid,mergeref,teamname)
                                        values
                                        ('',preference,puprn,pusrn,plat,plng,pstatus,pissue_type,
                                        psummary,pdescription, now(),now(),
                                        GEOMFROMTEXT(CONCAT('POINT(', plat, ' ', plng, ')')),
                                        GETPOLYGON(plat, plng, pradius, 12),
                                        pradius,peasting,pnorthing,pteam,paddress,pactivestatus,pactiveattributes,pinfostatements,pmapassetid,pmergedbid,pteamname);
                            
                                else
                                    -- update record. We use coalesce because FillTask won't preserve important stuff. This is a hack and should be removed when FillTask is improved
                                    update reportit_issue 
                                        set taskid='', 
                                        status=pstatus, 
                                        summary=coalesce(psummary,summary), 
                                        description=coalesce(pdescription,description),
                                        date_updated=now(),
                                        team=coalesce(pteam,team),
                                        activestatus=pactivestatus, -- coalesce(pactivestatus,activestatus),
                                        activeattributes=pactiveattributes, -- coalesce(pactiveattributes,activeattributes),
                                        usedstatus=concat(coalesce(usedstatus,''),',',coalesce(pactivestatus,'')),
                                        infostatements=pinfostatements, -- coalesce(pinfostatements,infostatements),
                                        lat=case when plat<>0 then plat else lat end,
                                        lng=case when plng<>0 then plng else lng end,
                                        easting=peasting,
                                        northing=pnorthing,
                                        uprn=puprn,
                                        usrn=pusrn,
                                        assetid=pmapassetid,
                                        mergeref=pmergedbid,
                                        teamname=coalesce(pteamname,teamname)
                                        where reference=preference;
                                end if;
                                
                                -- add a subscriber if a new one was specified that isn't already there
                                if pemail<>'' and pemail is not null  and not exists (select 0 from reportit_subscriber where reference=preference and email=pemail) then
                                    insert into reportit_subscriber(reference,email) values (preference, pemail);
                                end if;
                                
                                if pmergeref<>'' then -- we've merged another case with this one. move their subscribers to us.
                                    update reportit_subscriber set reference=preference 
                                        where reference=pmergeref 
                                        and email not in (select email from (select * from reportit_subscriber where reference=preference) as mysql_is_crap ); -- if they are not already subscribed
                                    delete from reportit_subscriber where reference=pmergeref; -- tidy up any duplicates that were not merged in
                                end if;
                            end");
        }

        // Add index on table `LAND_AND_PROPERTY_IDENTIFIER` including columns `PAO_TEXT`, `USRN`, `POSTAL_ADDRESS`, `LOGICAL_STATUS`
        $result = $connection->query("SHOW INDEX FROM `LAND_AND_PROPERTY_IDENTIFIER`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('IDX_PAO_TEXT', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` 
                                ADD INDEX `IDX_PAO_TEXT`(`PAO_TEXT`)");
        }

        if (!in_array('IDX_POSTAL_ADDRESS_LOGICAL_STATUS', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER`
                                ADD INDEX `IDX_POSTAL_ADDRESS_LOGICAL_STATUS`(`POSTAL_ADDRESS`,`LOGICAL_STATUS`)");
        }

        if (!in_array('IDX_USRN', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER`
                                ADD INDEX `IDX_USRN` (`USRN`)");
        }

        // Add index on table `STREET_DESCRIPTOR` including columns `USRN`
        $result = $connection->query("SHOW INDEX FROM `STREET_DESCRIPTOR`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('IDX_USRN', $indexArr)) {
            $connection->exec("ALTER TABLE `STREET_DESCRIPTOR` 
                                ADD INDEX `IDX_USRN` (`USRN`)");
        }

        // Add index on table `llpgcache` including columns `UPRN`
        $result = $connection->query("SHOW INDEX FROM `llpgcache`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('index_cache', $indexArr)) {
            $connection->exec("ALTER TABLE `llpgcache` ADD INDEX `index_cache` USING BTREE (UPRN ASC)");
        }

        if (!in_array('index_postcode', $indexArr)) {
            $connection->exec("ALTER TABLE `llpgcache` ADD INDEX `index_postcode` USING BTREE (POSTCODE ASC)");
        }

        if (in_array('IDX_UPRN', $indexArr)) {
            $connection->exec("ALTER TABLE `llpgcache` DROP INDEX `IDX_UPRN`");
        }

        #region Add FOI & SAR to make llpg compatible with them
        // Add table `foi_contribution`
        $sql = "CREATE TABLE IF NOT EXISTS `foi_contribution` (
                    `contribution_id` varchar(50) NOT NULL DEFAULT '',
                    `case_ref` varchar(50) NOT NULL,
                    `deadline` datetime DEFAULT NULL,
                    `assignment_id` varchar(50) DEFAULT NULL,
                    `assignment_name` varchar(100) DEFAULT NULL,
                    `contribution` varchar(5000) DEFAULT NULL,
                    `response` text,
                    `status` varchar(10) DEFAULT NULL,
                    PRIMARY KEY (`contribution_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $connection->exec($sql);

        // Add table `foi_disclosure`
        $sql = "CREATE TABLE IF NOT EXISTS `foi_disclosure` (
                    `case_ref` varchar(50) NOT NULL DEFAULT '',
                    `status` varchar(50) DEFAULT NULL,
                    `date_created` datetime DEFAULT NULL,
                    `subject` varchar(250) DEFAULT NULL,
                    `details` text,
                    `response` text,
                    `url` varchar(1000) DEFAULT NULL,
                    PRIMARY KEY (`case_ref`),
                    FULLTEXT KEY `subject` (`subject`,`details`,`response`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $connection->exec($sql);

        // Add table `foi_stopwords`
        $sql = "CREATE TABLE IF NOT EXISTS `foi_stopwords` (
                    `stopword` varchar(255) DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $connection->exec($sql);

        // Add table `sar_contribution`
        $sql = "CREATE TABLE IF NOT EXISTS `sar_contribution` (
                    `contribution_id` varchar(50) NOT NULL DEFAULT '',
                    `case_ref` varchar(50) NOT NULL,
                    `deadline` datetime DEFAULT NULL,
                    `assignment_id` varchar(50) DEFAULT NULL,
                    `assignment_name` varchar(100) DEFAULT NULL,
                    `contribution` varchar(5000) DEFAULT NULL,
                    `response` text,
                    `status` varchar(10) DEFAULT NULL,
                    PRIMARY KEY (`contribution_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $connection->exec($sql);

        // Add procedure `foi_match`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'PROCEDURE'AND ROUTINE_NAME = 'foi_match'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE  PROCEDURE `foi_match`(IN keywords varchar(1000), IN mylimit int)
                                begin
                                    select *, date_format(date_created,'%d %M %Y') as date2  
                                    from foi_disclosure 
                                    where status='public' and match(subject, details, response) against (keywords) LIMIT mylimit;
                                end");
        }

        // Add procedure `foi_rebuildstopwords`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'PROCEDURE'AND ROUTINE_NAME = 'foi_rebuildstopwords'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE  PROCEDURE `foi_rebuildstopwords`(in stopwords varchar(2000))
                                BEGIN
                                    SET @Valcount = substrCount(stopwords,' ')+1;
                                    SET @v1=0;
                                    delete from foi_stopwords;
                                    WHILE (@v1 < @Valcount) DO
                                        set @val = stringSplit(stopwords,' ',@v1+1);
                                        INSERT INTO foi_stopwords (stopword) VALUES (@val);
                                        SET @v1 = @v1 + 1;
                                    END WHILE;

                                END");
        }

        // Add function `foi_prepquery`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'FUNCTION'AND ROUTINE_NAME = 'foi_prepquery'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE  FUNCTION `foi_prepquery`(`queryphrase` varchar(255)) RETURNS varchar(255) CHARSET latin1
                                BEGIN
                                    set queryphrase=replace(replace(replace(replace(replace(replace(queryphrase,'&#x27;',''''),'''s',' '),'!',' '),'.',' '),',',' '),'?',' ');
                                    SET @Valcount = substrCount(queryphrase,' ')+1;
                                    SET @v1=0;
                                    set @newqueryphrase='';
                                    WHILE (@v1 < @Valcount) DO
                                        set @val = stringSplit(queryphrase,' ',@v1+1);
                                        if not exists(select 0 from foi_stopwords where ucase(@val)=ucase(stopword)) and length(@val)>3 THEN
                                            set @newqueryphrase=concat(@newqueryphrase,' ',@val);
                                        end if;
                                        SET @v1 = @v1 + 1;
                                    END WHILE;

                                    return @newqueryphrase;	
                                END"
                            );
        }

        // Add function `stringSplit`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'FUNCTION'AND ROUTINE_NAME = 'stringSplit'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE  FUNCTION `stringSplit`(
                                    x VARCHAR(255),
                                    delim VARCHAR(12),
                                    pos INT
                                )
                                RETURNS varchar(255) CHARSET latin1
                                RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos), LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1), delim, '')"
                            );
        }

        // Add function `substrCount`
        $result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'FUNCTION'AND ROUTINE_NAME = 'substrCount'")->fetch(\PDO::FETCH_ASSOC);

        if ($result === false) {
            $connection->exec("CREATE  FUNCTION `substrCount`(s VARCHAR(255), ss VARCHAR(255)) RETURNS tinyint(3) unsigned
                                    READS SQL DATA
                                BEGIN
                                    DECLARE count TINYINT(3) UNSIGNED;
                                    DECLARE offset TINYINT(3) UNSIGNED;
                                    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET s = NULL;
                                    SET count = 0;
                                    SET offset = 1;
                                    REPEAT
                                    IF NOT ISNULL(s) AND offset > 0 THEN
                                    SET offset = LOCATE(ss, s, offset);
                                    IF offset > 0 THEN
                                    SET count = count + 1;
                                    SET offset = offset + 1;
                                    END IF;
                                    END IF;
                                    UNTIL ISNULL(s) OR offset = 0 END REPEAT;
                                    RETURN count;
                                END"
                            );
        }
        #endregion

        #region Add table `llpgcache_import` index based on llpg v31 changes
        $connection->exec('CREATE TABLE IF NOT EXISTS `llpgcache_import` ( 
            ID int, 
            UPRN varchar(200), 
            LAT varchar(20), 
            LNG varchar(20), 
            PAO varchar(1000), 
            SAO varchar(1000), 
            HOUSE_NAME_NUMBER varchar(200), 
            STREET_NAME varchar(200), 
            LOCALITY_NAME varchar(200), 
            TOWN_NAME varchar(200), 
            COUNTY_NAME varchar(200), 
            POST_TOWN varchar(200), 
            POSTCODE varchar(20), 
            USRN varchar(200), 
            WARD varchar(200), 
            PARISH varchar(200), 
            ADMINISTRATIVE_AREA varchar(200),
            ORGANISATION VARCHAR(200), 
            LANGUAGE VARCHAR(3), 
            BLPU_CLASS VARCHAR(4),
            INDEX `index_cache` USING BTREE (UPRN ASC),
            INDEX `index_postcode` USING BTREE (POSTCODE ASC)
        )');

        // Add index on table `BASIC_LAND_AND_PROPERTY_UNIT`
        $result = $connection->query("SHOW INDEX FROM `BASIC_LAND_AND_PROPERTY_UNIT`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('index_BLPU_UPRN', $indexArr)) {
            $connection->exec("ALTER TABLE `BASIC_LAND_AND_PROPERTY_UNIT` ADD INDEX `index_BLPU_UPRN` USING BTREE (`UPRN` ASC)");
        }

        // Add index on table `STREET_DESCRIPTOR`
        $result = $connection->query("SHOW INDEX FROM `STREET_DESCRIPTOR`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('index_SD_USRN', $indexArr)) {
            $connection->exec("ALTER TABLE `STREET_DESCRIPTOR` ADD INDEX `index_SD_USRN` USING BTREE (`USRN` ASC)");
        }

        if (!in_array('index_SD_LANGUAGE', $indexArr)) {
            $connection->exec("ALTER TABLE `STREET_DESCRIPTOR` ADD INDEX `index_SD_LANGUAGE` USING BTREE (`LANGUAGE` ASC)");
        }

        // Add index on table `LAND_AND_PROPERTY_IDENTIFIER`
        $result = $connection->query("SHOW INDEX FROM `LAND_AND_PROPERTY_IDENTIFIER`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('index_LPI_LPI_KEY', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_LPI_KEY` USING BTREE (`LPI_KEY` ASC)");
        }

        if (!in_array('index_LPI_LANG', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_LANG` USING BTREE (`LANGUAGE` ASC)");
        }

        if (!in_array('index_LPI_USRN', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_USRN` USING BTREE (`USRN` ASC)");
        }

        if (!in_array('index_LPI_UPRN', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_UPRN` USING BTREE (`UPRN` ASC)");
        }

        if (!in_array('index_LPI_POSTAL_ADDRESS', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_POSTAL_ADDRESS` USING BTREE (`POSTAL_ADDRESS` ASC)");
        }

        if (!in_array('index_LPI_LOGICAL_STATUS', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_LOGICAL_STATUS` USING BTREE (`LOGICAL_STATUS` ASC)");
        }

        if (!in_array('index_LPI_PAO_TEXT', $indexArr)) {
            $connection->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_PAO_TEXT` USING BTREE (`PAO_TEXT` ASC)");
        }

        // Add index on table `STREET_RECORD`
        $result = $connection->query("SHOW INDEX FROM `STREET_RECORD`");

        $indexArr = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $indexArr[] = $row['Key_name'];
        }

        if (!in_array('index_SR_USRN', $indexArr)) {
            $connection->exec("ALTER TABLE `STREET_RECORD` ADD INDEX `index_SR_USRN` USING BTREE (`USRN` ASC)");
        }
        #endregion
    }
}
