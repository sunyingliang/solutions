<?php

namespace FS\Database;

class DFG
{
    public function generateDFG(\PDO $connection)
    {
        $this->process($connection);
    }

    private function process(\PDO $connection)
    {
        $connection->exec("CREATE TABLE IF NOT EXISTS `MyService_DFG` (
            `UID` BIGINT NOT NULL AUTO_INCREMENT,
            `Case_RefNo` VARCHAR(20) NOT NULL,
            `Case_Status` VARCHAR(30) NOT NULL,
            `Case_SelfID` VARCHAR(15) NOT NULL,
            `Case_Email` VARCHAR(150) NOT NULL,
            `Case_Updated` DATETIME NOT NULL,
            `Case_Closed` DATETIME DEFAULT NULL,	
            `Case_Archive` DATETIME NOT NULL,
            `ProcessType` VARCHAR(15) NOT NULL,
            `FirstContact_Date` DATETIME NOT NULL,
            `OTAssessment_Required` VARCHAR(5) DEFAULT NULL,	
            `OTAssessment_Date` DATETIME DEFAULT NULL,
            `OTAssessment_Name` DATETIME DEFAULT NULL,	
            `Organisation` VARCHAR(100) DEFAULT NULL,
            `Referral_Type` VARCHAR(50) DEFAULT NULL,
            `Referral_Date` DATETIME DEFAULT NULL,	
            `Referral_Name` VARCHAR(150) DEFAULT NULL,
            `FastTrackReferral` VARCHAR(5) DEFAULT NULL,
            `DiscretionaryRequest` VARCHAR(5) DEFAULT NULL,
            `Client_UCRN` VARCHAR(15) DEFAULT NULL,
            `Property_Postcode` VARCHAR(10) NOT NULL,
            `Property_Address` VARCHAR(200) DEFAULT NULL,
            `Property_UPRN` VARCHAR(15) DEFAULT NULL,	
            `Property_Tenure` VARCHAR(50) DEFAULT NULL,
            `WorkType_Summary` VARCHAR(13380) DEFAULT NULL,
            `WorkType_Shower` VARCHAR(5) DEFAULT NULL,
            `WorkType_Ramp` VARCHAR(5) DEFAULT NULL,
            `WorkType_StairLift` VARCHAR(5) DEFAULT NULL,
            `WorkType_Extension` VARCHAR(5) DEFAULT NULL,
            `WorkType_Other` VARCHAR(5) DEFAULT NULL,
            `EstimatedCost` DECIMAL(9, 2) DEFAULT NULL,
            `FeasibilityStudy_Required` VARCHAR(5) DEFAULT NULL,
            `FeasibilityStudy_RequestDate` DATETIME DEFAULT NULL,
            `FeasibilityStudy_Decision` VARCHAR(25) DEFAULT NULL,
            `FeasibilityStudy_Date` DATETIME DEFAULT NULL,
            `FeasibilityStudy_Name` VARCHAR(150) DEFAULT NULL,
            `Authorisation_Date` DATETIME NOT NULL,
            `Authorisation_Name` VARCHAR(150) DEFAULT NULL,
            `LandlordConsent_Required` VARCHAR(5) DEFAULT NULL,
            `LandlordConsent_Date` DATETIME DEFAULT NULL,
            `LandlordConsent_Name` VARCHAR(150) DEFAULT NULL,
            `HIAApproval_Date` DATETIME DEFAULT NULL,
            `HIAApproval_Name` VARCHAR(150) DEFAULT NULL,
            `HIARefNo` VARCHAR(15) DEFAULT NULL,
            `TechnicalVist_Date` DATETIME DEFAULT NULL,
            `TechnicalVisit_Name` VARCHAR(150) DEFAULT NULL,
            `DFGForms_Date` DATETIME DEFAULT NULL,
            `DFGForms_Name` VARCHAR(150) DEFAULT NULL,
            `MeansTestP_Required` VARCHAR(5) DEFAULT NULL,
            `MeansTestP_Date` DATETIME DEFAULT NULL,
            `MeansTestP_Name` VARCHAR(150) DEFAULT NULL,
            `MeansTestP_Result` VARCHAR(150) DEFAULT NULL,
            `MeansTest_Required` VARCHAR(5) DEFAULT NULL,
            `MeansTest_Date` DATETIME DEFAULT NULL,
            `MeansTest_Name` VARCHAR(150) DEFAULT NULL,
            `MeansTest_Contribution_Owner` DECIMAL(9, 2) DEFAULT NULL,
            `MeansTest_Contribution_Tenant` DECIMAL(9, 2) DEFAULT NULL,
            `Contractor_Allocated_Date` DATETIME DEFAULT NULL,
            `Contractor_Start_Date` DATETIME DEFAULT NULL,
            `Contractor_End_Date` DATETIME DEFAULT NULL,
            `Contractor_InvoiceAmount` DECIMAL(9, 2) DEFAULT NULL,
            `Contractor_InvoiceDate` DATETIME DEFAULT NULL,
            `TEMP_Date1` DATETIME DEFAULT NULL,
            `TEMP_Date2` DATETIME DEFAULT NULL,
            `TEMP_Date3` DATETIME DEFAULT NULL,
            `TEMP_Text1` VARCHAR(100) DEFAULT NULL,
            `TEMP_Text2` VARCHAR(100) DEFAULT NULL,
            `TEMP_Text3` VARCHAR(100) DEFAULT NULL,
            PRIMARY KEY (`UID`),
            UNIQUE INDEX `uniq_idx_case_refno` (`Case_RefNo`)
        ) DEFAULT CHARACTER SET UTF8MB4 COLLATE 'utf8mb4_unicode_ci'");
    }
}
