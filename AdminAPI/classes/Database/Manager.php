<?php

namespace FS\Database;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;

class Manager
{
    public $customerId;
    public $customerName;
    public $customerDBUsername;
    public $customerDBPassword;
    public $customerDBHost;
    public $customerDBDatabase;
    public $masterPDO;
    public $customerPDO;

    function __construct($customer)
    {
        $this->customerName = $customer;
    }

    function __destruct()
    {
        $this->masterPDO   = null;
        $this->customerPDO = null;
    }

    private function setCustomerConnection()
    {
        $masterConnection = $this->getMasterConnection();

        $sql = 'SELECT `id`, `name`, `db_host`, `db_user`, `db_password`, `db_database` 
                FROM  ' . DB_TABLE .
              ' WHERE `name` = :name';

        $stmt = $masterConnection->prepare($sql);

        $stmt->execute(['name' => $this->customerName]);

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!$row) {
            return false;
        }

        $this->customerId         = $row['id'];
        $this->customerName       = $row['name'];
        $this->customerDBUsername = $row['db_user'];
        $this->customerDBPassword = $row['db_password'];
        $this->customerDBHost     = $row['db_host'];
        $this->customerDBDatabase = $row['db_database'];

        return true;
    }

    public function getMasterConnection()
    {
        if (!isset($this->masterPDO)) {
            if (!defined('DB_HOST') || !defined('DB_USER') || !defined('DB_PASS') || !defined('DB_TABLE')) {
                throw new InvalidParameterException('Required config variable undefined');
            }

            $dns = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
            $this->masterPDO = new \PDO($dns, DB_USER, DB_PASS);
        }

        if (!$this->masterPDO) {
            throw new PDOCreationException('Could not connect to master database');
        }

        return $this->masterPDO;
    }

    public function getCustomerConnection()
    {
        if (!isset($this->customerPDO)) {
            if (!isset($this->customerId) || !isset($this->customerDBUsername) || !isset($this->customerDBPassword) || !isset($this->customerDBHost) || !isset($this->customerDBDatabase)) {
                $this->setCustomerConnection();
            } else if (empty($this->customerId) || empty($this->customerDBUsername) || empty($this->customerDBPassword) || empty($this->customerDBHost) || empty($this->customerDBDatabase)) {
                throw new InvalidParameterException('Required customer database detail invalid');
            }

            $this->customerPDO = new \PDO('mysql:host=' . $this->customerDBHost . ';dbname=' . $this->customerDBDatabase, $this->customerDBUsername, $this->customerDBPassword);
            $this->customerPDO->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        }

        if (!$this->customerPDO) {
            throw new PDOCreationException('Could not connect to customer database');
        }

        return $this->customerPDO;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    protected function boundInputParameters(
        $parameters,
        $parameterNames,
        $lowerCase = false
    )
    {
        $inputParameters = [];

        foreach ($parameterNames as $parameterName) {
            $inputParameters[':' . $parameterName] = $parameters[$lowerCase ? strtolower($parameterName) : $parameterName];
        }

        return $inputParameters;
    }
}
