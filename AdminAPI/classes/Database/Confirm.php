<?php

namespace FS\Database;

use FS\Common\Generic;

class Confirm
{
    public function generateConfirm(\PDO $connection)
    {
        $this->processConfirmDefect($connection);
        $this->processConfirmEnquiry($connection);
    }

    private function processConfirmDefect(\PDO $connection)
    {
        if ($connection->exec("CREATE TABLE IF NOT EXISTS `confirm_defect_case` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `case_ref` varchar(50) NOT NULL,
              `defect_number` int(10) unsigned NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `case_ref` (`case_ref`),
              UNIQUE KEY `defect_number` (`defect_number`)
            )") === false) {
            Generic::message('Error: Failed to add table {confirm_defect}');
        }

        if ($connection->exec("CREATE TABLE IF NOT EXISTS `confirm_defect_import` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `defect_number` int(10) unsigned DEFAULT NULL,
              `defect_easting` decimal(10,2) NOT NULL DEFAULT '0.00',
              `defect_northing` decimal(10,2) NOT NULL DEFAULT '0.00',
              `site_code` varchar(10) DEFAULT NULL,
              `site_name` varchar(40) DEFAULT NULL,
              `defect_type_name` varchar(30) DEFAULT NULL,
              `defect_type_code` varchar(4) DEFAULT NULL,
              `priority_code` varchar(4) DEFAULT NULL,
              `target_date` datetime DEFAULT NULL,
              `defect_date` datetime DEFAULT NULL,
              `defect_status_flag` char(1) DEFAULT NULL,
              `job_number` varchar(255) DEFAULT NULL,
              `job_entry_date` datetime DEFAULT NULL,
              `job_actual_comp_date` datetime DEFAULT NULL,
              `job_status_flag` char(1) DEFAULT NULL,
              `job_status_code` varchar(4) DEFAULT NULL,
              `job_status_name` varchar(30) DEFAULT NULL,
              `job_status_complete` char(1) DEFAULT NULL,
              `date_last_updated` datetime DEFAULT NULL,
              `job_notes` varchar(2000) DEFAULT NULL,
              `job_location` varchar(2000) DEFAULT NULL,
              `job_type_key` int(10) unsigned DEFAULT NULL,
              `job_type_name` varchar(50) DEFAULT NULL,
              `enquiry_number` int(10) unsigned DEFAULT NULL,
              `external_system_ref` varchar(50) DEFAULT NULL,
              `logged_date` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            )") === false) {
            Generic::message('Error: Failed to add table {confirm_defect_import}');
        }

        if ($connection->exec("CREATE TABLE IF NOT EXISTS `confirm_defect_reportit_mapping` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `defect_type_code` varchar(4) NOT NULL,
              `issue_type` varchar(50) NOT NULL,
              `statements` varchar(255) NOT NULL DEFAULT '',
              PRIMARY KEY (`id`),
              UNIQUE KEY `defectTypeCode` (`defect_type_code`)
            )") === false) {
            Generic::message('Error: Failed to add table {confirm_defect_reportit_mapping}');
        }

        if ($connection->exec("INSERT INTO `confirm_defect_reportit_mapping` (`defect_type_code`, `issue_type`, `statements`) VALUES
            ('BLOK', 'Drainage', ''),
            ('FLOD', 'Drainage', ''),
            ('IBCK', 'Drainage', ''),
            ('STWT', 'Drainage', ''),
            ('OVER', 'Grass Verges', ''),
            ('OBST', 'Hedges', ''),
            ('COPT', 'Non-illuminated bollards and signs', ''),
            ('CRSI', 'Non-illuminated bollards and signs', ''),
            ('DIRT', 'Non-illuminated bollards and signs', ''),
            ('LAMP', 'Non-illuminated bollards and signs', ''),
            ('OBSG', 'Non-illuminated bollards and signs', ''),
            ('VISA', 'Non-illuminated bollards and signs', ''),
            ('CHAL', 'Pavement and Kerbs', ''),
            ('LODT', 'Pavement and Kerbs', ''),
            ('SROK', 'Pavement and Kerbs', ''),
            ('STEP', 'Pavement and Kerbs', ''),
            ('UNEV', 'Pavement and Kerbs', ''),
            ('UNST', 'Pavement and Kerbs', ''),
            ('DEFM', 'Pothole in road', ''),
            ('DEPR', 'Pothole in road', ''),
            ('FPOT', 'Pothole in road', ''),
            ('MPOT', 'Pothole in road', ''),
            ('MSRF', 'Pothole in road', ''),
            ('POTH', 'Pothole in road', ''),
            ('SCRK', 'Pothole in road', ''),
            ('DBTL', 'Spillages and obstructions on the road', ''),
            ('DAMG', 'Traffic Signals', 'The traffic signals are temporary ones'),
            ('DTRE', 'Trees', '')
            ON DUPLICATE KEY UPDATE `id` = `id`") === false) {
            Generic::message('Error: Failed to add records {confirm_defect_reportit_mapping}');
        }
    }

    private function processConfirmEnquiry(\PDO $connection)
    {
        if ($connection->exec("CREATE TABLE IF NOT EXISTS `confirm_enquiry_case` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `case_ref` varchar(50) DEFAULT NULL,
              `enquiry_number` int(10) unsigned DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `case_ref` (`case_ref`),
              UNIQUE KEY `enquiry_number` (`enquiry_number`)
            )") === false) {
            Generic::message('Error: Failed to add table {confirm_enquiry}');
        }

        if ($connection->exec("CREATE TABLE IF NOT EXISTS `confirm_enquiry_import` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `enquiry_number` int(10) unsigned DEFAULT NULL,
              `date_last_updated` datetime DEFAULT NULL,
              `date_created` datetime DEFAULT NULL,
              `enquiry_status_code` varchar(4) DEFAULT NULL,
              `enquiry_status_name` varchar(30) DEFAULT NULL,
              `outstanding_flag` char(1) DEFAULT NULL,
              `enquiry_description` varchar(2000) DEFAULT NULL,
              `subject_code` varchar(4) DEFAULT NULL,
              `subject_name` varchar(30) DEFAULT NULL,
              `external_reference` varchar(50) DEFAULT NULL,
              `logged_date` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            )") === false) {
            Generic::message('Error: Failed to add table {confirm_enquiry_import}');
        }
    }
}