<?php

namespace FS\Database;

class MBCollection
{
    public function generateMBCollection(\PDO $connection)
    {
        $this->processMBCollection($connection);
        $this->processAdjustment($connection);
    }

    private function processMBCollection(\PDO $connection)
    {
        try {
            $connection->beginTransaction();
            // `collection_issue`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_issue` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `ref` varchar(50) DEFAULT NULL,
                        `date_created` datetime DEFAULT NULL,
                        `uprn` varchar(200) DEFAULT NULL,
                        `usrn` varchar(200) DEFAULT NULL,
                        `collection_date` datetime NOT NULL,
                        `collection_type` varchar(200) DEFAULT NULL,
                        `round_name` varchar(200) DEFAULT NULL,
                        `lookupaction` varchar(15) NOT NULL,
                        `lookupmessage` varchar(1000) NOT NULL,
                        PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            // `collection_missed`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_missed` (
                        `uprn` varchar(200) NOT NULL,
                        `ref` varchar(200) NOT NULL,
                        `status` varchar(200) NOT NULL,
                        `missed_date` datetime NOT NULL,
                        `round_name` varchar(200) NOT NULL,
                        `collection_type` varchar(200) NOT NULL,
                        `outcome` varchar(1000) DEFAULT NULL,
                        `investigation_outcome` varchar(1000) DEFAULT NULL,
                        `collection_outcome` varchar(1000) DEFAULT NULL,
                        PRIMARY KEY (`ref`),
                        KEY `ix_missed_uprn` (`uprn`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            // `collection_type`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_type` (
                        `name` varchar(200) NOT NULL,
                        `description` varchar(1000) NOT NULL DEFAULT '',
                        `permissions_group_id` varchar(50) DEFAULT NULL,
                        PRIMARY KEY (`name`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            // `collection_point`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_point` (
                        `property_uprn` varchar(200) NOT NULL,
                        `point_uprn` varchar(200) NOT NULL,
                        `point_usrn` varchar(200) DEFAULT NULL,
                        PRIMARY KEY (`property_uprn`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            // `collection_round`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_round` (
                        `round_name` varchar(200) NOT NULL,
                        `uprn` varchar(200) DEFAULT NULL,
                        `usrn` varchar(200) DEFAULT NULL,
                        `permissions_group_id` varchar(50) DEFAULT NULL,
                        KEY `ix_uprn` (`uprn`),
                        KEY `ix_usrn` (`usrn`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            // `collection_schedule`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_schedule` (
                        `round_name` varchar(200) NOT NULL,
                        `collection_type` varchar(200) NOT NULL,
                        `collection_date` datetime NOT NULL,
                        KEY `ix_collection_date` (`collection_date`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            // `collection_assisted`
            $sql = "CREATE TABLE IF NOT EXISTS `collection_assisted` (
                        `uprn` varchar(200) NOT NULL,
                        `expiry_date` datetime DEFAULT NULL,
                        `explanation` varchar(1000) DEFAULT NULL,
                        `notes` varchar(1000) DEFAULT NULL,
                        PRIMARY KEY (`uprn`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

            if ($connection->exec($sql) === false) {
                throw new PDOExecutionException('Failed to create table');
            }

            $connection->commit();
        } catch (Exception $e) {
            $connection->rollBack();
        }
    }

    private function processAdjustment(\PDO $connection)
    {
        // Nothing adjustment needed for this type
    }
}
