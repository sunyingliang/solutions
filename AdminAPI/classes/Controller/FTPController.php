<?php

namespace FS\Controller;

use FS\Common\Auth;
use FS\Common\IO;

class FTPController extends \FS\SolutionsAdminAPIBase
{
    public function __construct()
    {
        parent::__construct(['read']);
    }

    public function getCustomer()
    {
        $this->responseArr['data'] = [["id" => '', "name" => "All"]];
        $sql                       = 'SELECT `id`, `name` FROM `customer` ORDER BY `name` ASC';
        $stmt                      = $this->pdo->query($sql);

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row;
        }

        return $this->responseArr;
    }
}
