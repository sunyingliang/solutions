<?php

namespace FS\Controller;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\ValidationException;
use FS\Common\IO;
use FS\Common\Database;
use FS\Entity\Customer;

class CustomerController extends \FS\SolutionsAdminAPIBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCustomer()
    {
        $this->auth->hasPermission(['read']);

        $valid = IO::required($this->data, 'id');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql = "SELECT * FROM `customer` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute(['id' => $this->data['id']]);

        $row = $stmt->fetch();

        if (!$row) {
            throw new InvalidParameterException('A customer with id "' . $this->data['id'] . '" does not exist');
        }

        $this->responseArr['data'] = $row;

        return $this->responseArr;
    }

    public function getMasterPool()
    {
        $this->auth->hasPermission(['read']);

        $valid = IO::required($this->data, 'id');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql = "SELECT `id`, `name`
                FROM `customer`
                WHERE `id` != ? 
                    AND `master_id` != ?
                ORDER BY `name`";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([$this->data['id'], $this->data['id']]);

        $this->responseArr['data'] = $stmt->fetchAll();

        return $this->responseArr;
    }

    public function getFeatureList()
    {
        $this->auth->hasPermission(['read']);

        $valid = IO::required($this->data, 'id');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        // TODO: Research union
        $sql = "SELECT `feature`.`id`, `feature`.`name`, `feature`.`alias`, `feature`.`description`, cf.customer_id 
                FROM `feature`
                    LEFT JOIN (
                        SELECT `customer_id`,`feature_id`
                        FROM `customer_feature` 
                        WHERE `customer_id` = :id 
                            AND `enabled` = 1
                    ) AS cf ON cf.feature_id = `feature`.`id`
                WHERE `feature`.`enabled` = 1
                    OR cf.customer_id IS NOT NULL
                ORDER BY `feature`.`name`";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute(['id' => $this->data['id']]);

        $this->responseArr['data']['available_features'] = [];
        $this->responseArr['data']['enabled_features']   = [];

        while ($row = $stmt->fetch()) {
            $customerId = $row['customer_id'];
            unset($row['customer_id']);

            if (is_null($customerId)) {
                $this->responseArr['data']['available_features'][] = $row;
            } else {
                $this->responseArr['data']['enabled_features'][] = $row;
            }
        }

        return $this->responseArr;
    }

    public function getCustomerList()
    {
        $this->auth->hasPermission(['read']);

        $name      = IO::default($this->data, 'name');
        $alias     = IO::default($this->data, 'alias');
        $enabled   = IO::default($this->data, 'enabled');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'name');
        $sortOrder = IO::default($this->data, 'sortOrder', 'ASC');

        if (!in_array($sort, ['name', 'db_host', 'db_database', 'enabled'])) {
            $sort = 'name';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'ASC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($name)) {
            $searchString       = $this->appendSearch($searchString, 'name LIKE :name');
            $parameters['name'] = '%' . $name . '%';
        }

        if (!empty($alias)) {
            $searchString        = $this->appendSearch($searchString, 'alias LIKE :alias');
            $parameters['alias'] = '%' . $alias . '%';
        }

        // Note, all reports are either enabled or not now
        if (!is_null($enabled)) {
            $searchString          = $this->appendSearch($searchString, 'enabled = :enabled');
            $parameters['enabled'] = $enabled;
        }

        $stmt = $this->pdo->prepare("SELECT COUNT(1) FROM `customer` " . $searchString);

        $stmt->execute($parameters);

        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT `id`, `name`, `alias`, `ftp_pass`, `ftp_pass_raw`, `http_pass`, `home_folder`, `db_host`, `db_user`, `db_password`, `db_user_read`, `db_password_read`, 
                    `db_database`, `enabled`, `notes`, `master_id`, `last_tested`, `last_tested_response` , `token`
                FROM `customer`" . $searchString
            . " ORDER BY " . $sort . " " . $sortOrder . " LIMIT :offset, :limit";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);

        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        $valid = IO::required($this->data, 'id', 'name', 'alias', 'ftpPassword', 'httpPassword', 'homeFolder', 'dbHost', 'dbUser', 'dbPassword', 'dbDatabase', 'dbUserRead', 'dbPasswordRead', 'enabled');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        if ($this->data['enabled'] != 1 && $this->data['enabled'] != 0) {
            throw new ValidationException('Customer enabled must be 1 or 0');
        }

        // Check if customer already exists
        $sql = "SELECT `name` FROM `customer` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $oldName = $row['name'];

            if ($oldName !== $this->data['name']) {
                // Check if customer already exists
                $sql = "SELECT COUNT(1) AS `total` FROM `customer` WHERE `name` = :name";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement prepare failed');
                }

                if ($stmt->execute(['name' => $this->data['name']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed');
                }

                if ($row = $stmt->fetch()) {
                    $count = $row['total'];
                }

                if (intval($count) > 0) {
                    throw new DBDuplicationException('Customer with the name "' . $this->data['name'] . '" already exists');
                }
            }
        }

        // Set customer entity
        $customer = new Customer($this->data);

        // Validate customer entity
        $customer->validate();

        // Validate customer database (existing mode)
        $customer->validateDatabaseConectionAndPermissions(false, 'read');
        $customer->validateDatabaseConectionAndPermissions(false, 'write');

        // We're valid and have working databases at this point
        $sql = "UPDATE `customer` 
                SET `name`                = :name,
                    `alias`               = :alias,
                    `ftp_pass`            = :ftp_pass, 
                    `ftp_pass_raw`        = :ftp_pass_raw,
                    `http_pass`           = :http_pass, 
                    `home_folder`         = :home_folder, 
                    `db_host`             = :db_host, 
                    `db_user`             = :db_user, 
                    `db_password`         = :db_password, 
                    `db_database`         = :db_database, 
                    `db_user_read`        = :db_user_read, 
                    `db_password_read`    = :db_password_read,
                    `enabled`             = :enabled, 
                    `notes`               = :notes,
                    `master_id`           = :master_id
                WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        $parameters = [
            'id'               => $customer->getId(),
            'name'             => $customer->getName(),
            'alias'            => $customer->getAlias(),
            'ftp_pass'         => md5($customer->getFTPPassword()),
            'ftp_pass_raw'     => $customer->getFTPPassword(),
            'http_pass'        => $customer->getHTTPPassword(),
            'home_folder'      => $customer->getHomeFolder(),
            'db_host'          => $customer->getDBHost(),
            'db_user'          => $customer->getDBUser(),
            'db_password'      => $customer->getDBPassword(),
            'db_database'      => $customer->getDBDatabase(),
            'db_user_read'     => $customer->getDBUserRead(),
            'db_password_read' => $customer->getDBPasswordRead(),
            'enabled'          => $customer->getEnabled(),
            'notes'            => $customer->getNotes(),
            'master_id'        => $customer->getMasterId()
        ];

        if ($stmt->execute($parameters) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        // Update features
        // Add enabled features
        if (isset($this->data['enabled_features'])) {
            $sql = "INSERT INTO `customer_feature` (`customer_id`, `feature_id`, `enabled`)
                    VALUES (:customer_id, :feature_id, 1)
                    ON DUPLICATE KEY UPDATE `enabled` = 1";

            $stmt = $this->pdo->prepare($sql);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement prepare failed');
            }

            foreach ($this->data['enabled_features'] as $feature) {
                $parameters = [
                    'customer_id' => $customer->getId(),
                    'feature_id'  => $feature['id']
                ];

                $stmt->execute($parameters);
            }
        }

        // Remove disabled features
        if (isset($this->data['available_features'])) {
            $sql = "UPDATE `customer_feature`
                    SET `enabled` = 0
                    WHERE `customer_id` = :customer_id AND `feature_id` = :feature_id";

            $stmt = $this->pdo->prepare($sql);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement prepare failed');
            }

            foreach ($this->data['available_features'] as $feature) {
                $parameters = [
                    'customer_id' => $customer->getId(),
                    'feature_id'  => $feature['id']
                ];

                $stmt->execute($parameters);
            }
        }

        return $this->responseArr;
    }

    public function updateEnabled()
    {
        $this->auth->hasPermission(['write']);

        $valid = IO::required($this->data, 'id', 'enabled');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        if ($this->data['enabled'] != 1 && $this->data['enabled'] != 0) {
            throw new ValidationException('Customer enabled must be 1 or 0');
        }

        // Check if customer already exists
        $sql = "SELECT `name` FROM `customer` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        // Set customer entity
        $customer = new Customer($this->data);

        // We're valid and have working databases at this point
        $sql = "UPDATE `customer` 
                SET `enabled` = :enabled
                WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        $parameters = ['id' => $customer->getId(), 'enabled' => $customer->getEnabled()];

        if ($stmt->execute($parameters) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function create()
    {
        // Hour timeout for create function
        ini_set('max_execution_time', CURL_TIMEOUT_LONG);

        $this->auth->hasPermission(['write']);

        $new    = false;
        $method = '';

        // If dbHost does not have a value we have a new customer request
        if (empty(IO::default($this->data, 'dbHost', ''))) {
            $valid = IO::required($this->data, 'name');

            // Three parameters as data includes token
            if (count($this->data) > 3) {
                throw new InvalidParameterException('Too many parameters passed when creating new customer, required: name, optional: notes');
            }

            $new = true;
        } else if (IO::default($this->data, 'method', '') === 'clone') {
            $valid  = IO::required($this->data, 'name', 'dbHost', 'dbUser', 'dbPassword', 'dbDatabase', 'method');
            $method = IO::default($this->data, 'method', '');
        } else {
            $valid  = IO::required($this->data, 'name', 'dbHost', 'dbUser', 'dbPassword', 'dbDatabase', 'dbUserRead', 'dbPasswordRead', 'method');
            $method = IO::default($this->data, 'method', '');
        }

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        // Validation for allowed method types
        if (!$new && preg_match('(clone|migrate_soft|migrate_hard)', $method) !== 1) {
            throw new ValidationException('Parameter "method" must be [clone|migrate_soft|migrate_hard]');
        }

        // Check if customer already exists
        $sql = "SELECT COUNT(1) AS `total` FROM `customer` WHERE `name` = :name";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['name' => $this->data['name']]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $count = $row['total'];
        }

        if (intval($count) > 0) {
            throw new DBDuplicationException('Customer with the name "' . $this->data['name'] . '" already exists');
        }

        // Set customer entity
        $customer = new Customer($this->data);

        // Validate customer entity
        $customer->validate();

        // Validate new customer
        if ($new) {
            $customer->validateNew();
            $customer->processNew();
        }

        // Validate write customer database
        $customer->validateDatabaseConectionAndPermissions($new, 'write');

        if ($method === 'clone') {
            // Clone means we're adding an existing customer & cloning an old database
            $customerClone = new Customer(['name' => $customer->getName()]);
            $customerClone->validateNew();
            $customerClone->processNew();
            $customerClone->validateDatabaseConectionAndPermissions(true, 'read');
            $customerClone->validateDatabaseConectionAndPermissions(true, 'write');

            // Prepare database to clone from
            $dbFrom = new Database([
                'DB_HOST'     => $customer->getDBHost(),
                'DB_PORT'     => 3306,
                'DB_NAME'     => $customer->getDBDatabase(),
                'DB_USERNAME' => $customer->getDBUser(),
                'DB_PASSWORD' => $customer->getDBPassword()
            ]);

            // Prepare database to clone to
            $dbTo = new Database([
                'DB_HOST'     => $customerClone->getDBHost(),
                'DB_PORT'     => 3306,
                'DB_NAME'     => $customerClone->getDBDatabase(),
                'DB_USERNAME' => $customerClone->getDBUser(),
                'DB_PASSWORD' => $customerClone->getDBPassword()
            ]);

            // Clone database to generated default customer database
            IO::cloneDatabase($dbFrom, $dbTo);

            // Overwrite original customer database detail with new customer database
            $customer->setDBHost($customerClone->getDBHost());
            $customer->setDBUser($customerClone->getDBUser());
            $customer->setDBPassword($customerClone->getDBPassword());
            $customer->setDBDatabase($customerClone->getDBDatabase());
            $customer->setDBUserRead($customerClone->getDBUserRead());
            $customer->setDBPasswordRead($customerClone->getDBPasswordRead());

            // Re-check write user given we just replaced it
            $customer->validateDatabaseConectionAndPermissions($new, 'write');

            // This will enable the customer immediately and start the migration
            $customer->setEnabled(1);
        } else if ($method === 'migrate_soft') {
            // This will not enable the customer immediately and won't start the migration
            $customer->setEnabled(0);
        } else if ($method === 'migrate_hard') {
            // This will enable the customer immediately and start the migration
            $customer->setEnabled(1);
        }

        // Validate read customer database
        $customer->validateDatabaseConectionAndPermissions($new, 'read');

        // We're valid and have working databases at this point
        $this->pdo->beginTransaction();

        $sql = "INSERT INTO `customer` (`name`, `alias`, `ftp_pass`, `ftp_pass_raw`, `http_pass`, `home_folder`, `db_host`, `db_user`, `db_password`, `db_database`, `db_user_read`, `db_password_read`, `enabled`, `notes`, `token`, `last_tested`, `last_tested_response`, `uid`)
                 VALUES (:name, :alias, :ftp_pass, :ftp_pass_raw, :http_pass, :home_folder, :db_host, :db_user, :db_password, :db_database, :db_user_read, :db_password_read, :enabled, :notes, :token, CURRENT_TIMESTAMP, :last_tested_response, :uid)";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            $this->pdo->rollBack();
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        $parameters = [
            'name'                 => $customer->getName(),
            'alias'                => $customer->getAlias(),
            'ftp_pass'             => md5($customer->getFTPPassword()),
            'ftp_pass_raw'         => $customer->getFTPPassword(),
            'http_pass'            => $customer->getHTTPPassword(),
            'home_folder'          => $customer->getHomeFolder(),
            'db_host'              => $customer->getDBHost(),
            'db_user'              => $customer->getDBUser(),
            'db_password'          => $customer->getDBPassword(),
            'db_database'          => $customer->getDBDatabase(),
            'db_user_read'         => $customer->getDBUserRead(),
            'db_password_read'     => $customer->getDBPasswordRead(),
            'enabled'              => $customer->getEnabled(),
            'notes'                => $customer->getNotes(),
            'token'                => $customer->getToken(),
            'last_tested_response' => 'Success',
            'uid'                  => $customer->getUID()
        ];

        if ($stmt->execute($parameters) === false) {
            $this->pdo->rollBack();
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [
            'id' => $this->pdo->lastInsertId()
        ];

        $sqlUGID = "INSERT INTO `grouplist`(`gid`, `uid`, `username`) VALUES(?, ?, ?)";

        $stmtUGID = $this->pdo->prepare($sqlUGID);

        if ($stmtUGID === false) {
            $this->pdo->rollBack();
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmtUGID->execute([Customer::GID, $customer->getUID(), $customer->getName()]) === false) {
            $this->pdo->rollBack();
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->pdo->commit();

        return $this->responseArr;
    }

    function delete()
    {
        $this->auth->hasPermission(['write']);

        $errorMessage = 'Error: Failed to delete customer from database';

        $valid = IO::required($this->data, 'id');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        // Check if customer already exists
        $sql = "SELECT COUNT(1) AS `total`, `uid` FROM `customer` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $row = $stmt->fetch();

        if ($row === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        if ($row['total'] == 0) {
            throw new InvalidParameterException('A customer with id "' . $this->data['id'] . '" does not exist');
        }

        $this->pdo->beginTransaction();

        $sql = "DELETE FROM `customer` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            $this->pdo->rollBack();
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            $this->pdo->rollBack();
            throw new InvalidParameterException($errorMessage);
        }

        $sql = "DELETE FROM `grouplist` WHERE `uid` = :uid";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            $this->pdo->rollBack();
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['uid' => $row['uid']]) === false) {
            $this->pdo->rollBack();
            throw new InvalidParameterException($errorMessage);
        }

        $sql = "DELETE FROM `customer_feature` WHERE `customer_id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            $this->pdo->rollBack();
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            $this->pdo->rollBack();
            throw new InvalidParameterException($errorMessage);
        }

        $this->pdo->commit();

        return $this->responseArr;
    }

    public function testConnectionAndPermission()
    {
        $error = '';
        $valid = IO::required($this->data, 'id', 'dbHost', 'dbDatabase', 'dbUser', 'dbPassword', 'dbUserRead', 'dbPasswordRead');

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $customer = new Customer($this->data);

        try {
            $customer->validateDatabaseConectionAndPermissions(false, 'read');
        } catch (\Exception $e) {
            $error .= $e->getMessage() . PHP_EOL;
        }

        try {
            $customer->validateDatabaseConectionAndPermissions(false, 'write');
        } catch (\Exception $e) {
            $error .= $e->getMessage() . PHP_EOL;
        }

        if (!empty($error)) {
            $customer->updateLastTested($error, $this->data['id']);
            throw new \Exception($error);
        } else {
            $customer->updateLastTested('Success', $this->data['id']);
        }

        return $this->responseArr;
    }

    public function syncDB()
    {
        // Hour timeout for create function
        ini_set('max_execution_time', CURL_TIMEOUT_LONG);

        $this->auth->hasPermission(['write']);

        if (!($valid = IO::required($this->data, 'name', 'dbHost', 'dbUser', 'dbPassword', 'dbDatabase', 'syncHost', 'syncDbName', 'syncUserName', 'syncPassword'))['valid']) {
            throw new InvalidParameterException($valid['message']);
        }

        // Prepare customer objects from customerSyncFrom and customerSyncTo
        $customerSyncFrom = new Customer([
            'name'           => $this->data['name'],
            'dbHost'         => $this->data['syncHost'],
            'dbDatabase'     => $this->data['syncDbName'],
            'dbUserRead'     => $this->data['syncUserName'],
            'dbPasswordRead' => $this->data['syncPassword']
        ]);

        $customerSyncFrom->validateDatabaseConectionAndPermissions(false, 'read');

        $customerSyncTo = new Customer($this->data);

        $customerSyncTo->validateDatabaseConectionAndPermissions(false, 'write');

        // Clean the current db
        $customerSyncTo->setEnabled(0);
        $customerSyncTo->persistEnabled();

        $customerSyncTo->truncateDB();

        // Synchronize from remote db
        $this->syncFromRemoteDB($customerSyncFrom, $customerSyncTo);

        $customerSyncTo->setEnabled(1);
        $customerSyncTo->persistEnabled();

        //$this->responseArr['data'] = $this->data;

        return $this->responseArr;
    }

    #region Utils
    private function syncFromRemoteDB($customerSyncFrom, $customerSyncTo)
    {
        // Prepare database to clone from
        $dbFrom = new Database([
            'DB_HOST'     => $customerSyncFrom->getDBHost(),
            'DB_PORT'     => 3306,
            'DB_NAME'     => $customerSyncFrom->getDBDatabase(),
            'DB_USERNAME' => $customerSyncFrom->getDBUserRead(),
            'DB_PASSWORD' => $customerSyncFrom->getDBPasswordRead()
        ]);

        // Prepare database to clone to
        $dbTo = new Database([
            'DB_HOST'     => $customerSyncTo->getDBHost(),
            'DB_PORT'     => 3306,
            'DB_NAME'     => $customerSyncTo->getDBDatabase(),
            'DB_USERNAME' => $customerSyncTo->getDBUser(),
            'DB_PASSWORD' => $customerSyncTo->getDBPassword()
        ]);

        // Clone database to generated default customer database
        IO::cloneDatabase($dbFrom, $dbTo);
    }
    #endregion

}
