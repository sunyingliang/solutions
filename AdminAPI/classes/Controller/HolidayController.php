<?php

namespace FS\Controller;

use FS\Common\Auth;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class HolidayController extends \FS\SolutionsAdminAPIBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listCalendars()
    {
        $this->auth->hasPermission(['read']);

        $sql = "SELECT `calendar` FROM `holiday` GROUP BY `calendar` ORDER BY `calendar`";

        $this->responseArr['data'] = $this->pdo->query($sql)->fetchAll();

        return $this->responseArr;
    }

    public function listHolidayYearByCalendar()
    {
        $this->auth->hasPermission(['read']);

        $valid        = IO::required($this->data, 'calendar');
        $errorMessage = 'Error: Failed to retrieve years from database';

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql = "SELECT DATE_FORMAT(`holiday`, '%Y') AS `year`
                FROM `holiday`
                WHERE `calendar` = :calendar
                GROUP BY `year`
                ORDER BY `year` DESC";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['calendar' => $this->data['calendar']]) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $this->responseArr['data'] = $stmt->fetchAll();

        return $this->responseArr;
    }

    public function listHolidayByCalendarAndYear()
    {
        $this->auth->hasPermission(['read']);

        $valid        = IO::required($this->data, 'calendar', 'year');
        $errorMessage = 'Error: Failed to retrieve holiday from database';

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql = "SELECT `id`,`holiday`,`notes`
                FROM `holiday`
                WHERE `calendar` = :calendar 
                    AND `holiday` BETWEEN :yearStart AND :yearEnd
                ORDER BY `holiday`";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $parameters = [
            'calendar'  => $this->data['calendar'],
            'yearStart' => $this->data['year'] . '-01-01',
            'yearEnd'   => $this->data['year'] . '-12-31'
        ];

        if ($stmt->execute($parameters) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $this->responseArr['data'] = $stmt->fetchAll();

        return $this->responseArr;
    }

    public function addHoliday()
    {
        $this->auth->hasPermission(['write']);

        $valid        = IO::required($this->data, 'holiday', 'calendar');
        $errorMessage = 'Error: Failed to add holiday to database';

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql = "SELECT COUNT(1) AS `total`
                FROM `holiday`
                WHERE `holiday` = :holiday 
                    AND `calendar` = :calendar";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $parameters = [
            'holiday'  => $this->data['holiday'],
            'calendar' => $this->data['calendar']
        ];

        if ($stmt->execute($parameters) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $row = $stmt->fetch();

        if ($row === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($row['total'] > 0) {
            throw new InvalidParameterException('Error: The holiday "' . $this->data['holiday'] . '" on calendar "' . $this->data['calendar'] . '" already exists');
        }

        $sql  = "INSERT INTO `holiday`(`holiday`, `calendar`, `notes`) VALUES(:holiday, :calendar, :notes)";
        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['holiday' => $this->data['holiday'], 'calendar' => $this->data['calendar'], 'notes' => $this->data['notes'] ?? '']) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }

    public function editHoliday()
    {
        $this->auth->hasPermission(['write']);

        $valid        = IO::required($this->data, 'id', 'notes');
        $errorMessage = 'Error: Failed to edit holiday in database';

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql  = "SELECT COUNT(1) AS `total` FROM `holiday` WHERE `id` = :id";
        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $row = $stmt->fetch();

        if ($row === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($row['total'] == 0) {
            throw new InvalidParameterException('Error: The holiday with id "' . $this->data['id'] . '" does not exist');
        }

        $sql  = "UPDATE `holiday` SET `notes` = :notes WHERE `id` = :id";
        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['id' => $this->data['id'], 'notes' => $this->data['notes']]) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        return $this->responseArr;
    }

    public function deleteHoliday()
    {
        $this->auth->hasPermission(['write']);

        $valid        = IO::required($this->data, 'id');
        $errorMessage = 'Error: Failed to delete holiday from database';

        if ($valid['valid'] === false) {
            throw new InvalidParameterException($valid['message']);
        }

        $sql  = "SELECT COUNT(1) AS `total` FROM `holiday` WHERE `id` = :id";
        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        $row = $stmt->fetch();

        if ($row === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($row['total'] == 0) {
            throw new InvalidParameterException('Error: The holiday with id "' . $this->data['id'] . '" does not exist');
        }

        $sql  = "DELETE FROM `holiday` WHERE `id` = :id";
        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new InvalidParameterException($errorMessage);
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new InvalidParameterException($errorMessage);
        }

        return $this->responseArr;
    }
}
