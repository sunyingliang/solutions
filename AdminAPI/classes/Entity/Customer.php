<?php

namespace FS\Entity;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\OutOfRangeException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\ValidationException;
use FS\Common\IO;

class Customer extends \FS\SolutionsAdminAPIBase
{
    // Permission specific fields
    private $minimumWritePermissions = ['INSERT', 'SELECT', 'UPDATE', 'DELETE', 'EXECUTE', 'CREATE', 'ALTER', 'DROP', 'CREATE ROUTINE', 'ALTER ROUTINE', 'LOCK TABLES', 'SHOW VIEW', 'CREATE VIEW'];
    private $minimumReadPermissions  = ['SELECT', 'EXECUTE'];

    #region Fields
    const UID_MIN = 2000;
    const UID_MAX = 60000;
    const GID     = 10000;
    protected $id;
    protected $name;
    protected $alias;
    protected $ftpPassword;
    protected $httpPassword;
    protected $homeFolder;
    protected $dbHost;
    protected $dbUser;
    protected $dbPassword;
    protected $dbDatabase;
    protected $dbUserRead;
    protected $dbPasswordRead;
    protected $enabled;
    protected $notes;
    protected $masterId;
    protected $token;
    protected $method;
    protected $uid;
    #endregion

    #region Construct
    public function __construct($data = null)
    {
        if ($data !== null) {
            $this->setId(IO::default($data, 'id', 0));
            $this->setName(IO::default($data, 'name', ''));
            $this->setAlias(IO::default($data, 'alias', ''));
            $this->setFTPPassword(IO::default($data, 'ftpPassword', IO::generateRandom(16)));
            $this->setHTTPPassword(IO::default($data, 'httpPassword', IO::generateRandom(16)));
            $this->setHomeFolder(IO::default($data, 'homeFolder', '/data/' . $this->name));
            $this->setDBHost(IO::default($data, 'dbHost', DEFAULT_CUSTOMER_DB_HOST));
            $this->setDBUser(IO::default($data, 'dbUser', $this->generateDBUserName('write')));
            $this->setDBPassword(IO::default($data, 'dbPassword', IO::generateRandom(32)));
            $this->setDBDatabase(IO::default($data, 'dbDatabase', 'solutions_' . $this->name));
            $this->setDBUserRead(IO::default($data, 'dbUserRead', $this->generateDBUserName('read')));
            $this->setDBPasswordRead(IO::default($data, 'dbPasswordRead', IO::generateRandom(16)));
            $this->setEnabled(IO::default($data, 'enabled', 1) === 1 ? true : false);
            $this->setNotes(IO::default($data, 'notes', ''));
            $this->setMasterId(IO::default($data, 'master_id', 0));
            $this->setToken(IO::default($data, 'token', IO::guid()));
            $this->setMethod(IO::default($data, 'method', ''));
        }

        $this->uid = $this->generateUID();
    }
    #endregion

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    public function getFTPPassword()
    {
        return $this->ftpPassword;
    }

    public function setFTPPassword($ftpPassword)
    {
        $this->ftpPassword = $ftpPassword;
    }

    public function getHTTPPassword()
    {
        return $this->httpPassword;
    }

    public function setHTTPPassword($httpPassword)
    {
        $this->httpPassword = $httpPassword;
    }

    public function getHomeFolder()
    {
        return $this->homeFolder;
    }

    public function setHomeFolder($homeFolder)
    {
        $this->homeFolder = $homeFolder;
    }

    public function getDBHost()
    {
        return $this->dbHost;
    }

    public function setDBHost($dbHost)
    {
        $this->dbHost = $dbHost;
    }

    public function getDBUser()
    {
        return $this->dbUser;
    }

    public function setDBUser($dbUser)
    {
        $this->dbUser = $dbUser;
    }

    public function getDBPassword()
    {
        return $this->dbPassword;
    }

    public function setDBPassword($dbPassword)
    {
        $this->dbPassword = $dbPassword;
    }

    public function getDBDatabase()
    {
        return $this->dbDatabase;
    }

    public function setDBDatabase($dbDatabase)
    {
        $this->dbDatabase = $dbDatabase;
    }

    public function getDBUserRead()
    {
        return $this->dbUserRead;
    }

    public function setDBUserRead($dbUserRead)
    {
        $this->dbUserRead = $dbUserRead;
    }

    public function getDBPasswordRead()
    {
        return $this->dbPasswordRead;
    }

    public function setDBPasswordRead($dbPasswordRead)
    {
        $this->dbPasswordRead = $dbPasswordRead;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    public function getMasterId()
    {
        return $this->masterId;
    }

    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }
    #endregion

    #region Functions
    public function validate()
    {
        // Check valid username lengths
        if (strlen($this->getName()) < 4) {
            throw new ValidationException('Customer name minimum size is 4 characters');
        } else if (strlen($this->getName()) > 128) {
            throw new ValidationException('Customer name maximum size is 128 characters');
        } else if (!preg_match('/^[a-z][a-z0-9]*(_(?:[a-z0-9]))?[a-z0-9]*$/', $this->getName())) {
            throw new ValidationException("Customer name should be lowercase and cannot contain special characters except only one underscore (_) in middle");
        } else if (strpos($this->getName(), '/') !== false) {
            throw new ValidationException('Customer name must not contain a forward slash (/)');
        } else if (strpos($this->getName(), '\\') !== false) {
            throw new ValidationException('Customer name must not contain a back slash (\)');
        } else if (strpos($this->getName(), '.') !== false) {
            throw new ValidationException('Customer name must not contain a full stop (.)');
        } else if (strpos($this->getName(), '-') !== false) {
            throw new ValidationException('Customer name must not contain a hyphen (-), perhaps a underscore will be useful? (_)');
        }

        if (!empty($this->getAlias())) {
            if (strlen($this->getAlias()) > 128) {
                throw new ValidationException('Customer alias maximum size is 128 characters');
            }
        }

        // Check valid password lengths
        if (strlen($this->getFTPPassword()) < 8) {
            throw new ValidationException('Customer ftpPassword entropy too low, minimum 8 characters, recommend 16+ characters');
        } else if (strlen($this->getFTPPassword()) > 255) {
            throw new ValidationException('Customer ftpPassword maximum size is 255 characters, that\'s the DB limit folks! Well done!');
        }

        if (strlen($this->getHTTPPassword()) < 8) {
            throw new ValidationException('Customer httpPassword entropy too low, minimum 8 characters, recommend 16+ characters');
        } else if (strlen($this->getHTTPPassword()) > 255) {
            throw new ValidationException('Customer httpPassword maximum size is 255 characters, that\'s the DB limit folks! Well done!');
        }

        // Check valid token
        if (strlen($this->getToken()) != 36) {
            throw new ValidationException('Customer token should be exactly 36 characters');
        } else if (!preg_match('/[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}/', $this->getToken())) {
            throw new ValidationException("Customer token is invalid, e.g.: (XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX)");
        }

        // If method clone, the first time we don't check because we're not interested in validating the temporary customer DB password for copying
        if ($this->getMethod() !== 'clone') {
            if (strlen($this->getDBPassword()) < 8) {
                throw new ValidationException('Customer dbPassword entropy too low, minimum 8 characters, recommend 16+ characters');
            }
        }

        if (strlen($this->getDBPasswordRead()) < 8) {
            throw new ValidationException('Customer dbPasswordRead entropy too low, minimum 8 characters, recommend 16+ characters');
        }

        // Misc checks
        if (strlen($this->getHomeFolder()) < 4) {
            throw new ValidationException('Customer homeFolder minimum size is 4 characters');
        } else if (strlen($this->getHomeFolder()) > 255) {
            throw new ValidationException('Customer homeFolder maximum size is 255 characters');
        } else if (strpos($this->getHomeFolder(), '/') === false) {
            throw new ValidationException('Customer homeFolder must contain a forward slash (/)');
        } else if (strpos($this->getHomeFolder(), '\\') !== false) {
            throw new ValidationException('Customer homeFolder must not contain a back slash (\), perhaps you need a forward slash? (/)');
        } else if (strpos($this->getHomeFolder(), '//') !== false) {
            throw new ValidationException('Customer homeFolder must not contain two slashes in succession (//)');
        } else if (strpos($this->getHomeFolder(), '.') !== false) {
            throw new ValidationException('Customer homeFolder must not contain a full stop (.)');
        } else if (substr_count($this->getHomeFolder(), '/') < 2) {
            throw new ValidationException('Customer homeFolder must contain at least two slashes');
        }

        if (empty($this->getDBHost())) {
            throw new ValidationException('Customer dbHost is empty');
        } else if (strlen($this->getDBHost()) > 255) {
            throw new ValidationException('Customer dbHost maximum length is 255 characters');
        }

        if (empty($this->getDBDatabase())) {
            throw new ValidationException('Customer dbDatabase is empty');
        } else if (strlen($this->getDBDatabase()) > 255) {
            throw new ValidationException('Customer dbDatabase maximum length is 255 characters');
        }

        if (empty($this->getDBUser())) {
            throw new ValidationException('Customer dbUser is empty');
        } else if (strlen($this->getDBUser()) > 16) {
            throw new ValidationException('Customer dbUser maximum length is 16 characters');
        }

        if (empty($this->getDBUserRead())) {
            throw new ValidationException('Customer dbUserRead is empty');
        } else if (strlen($this->getDBUserRead()) > 16) {
            throw new ValidationException('Customer dbUserRead maximum length is 16 characters');
        }

        if (empty($this->getHomeFolder())) {
            throw new ValidationException('Customer homeFolder is empty');
        } else if (strlen($this->getHomeFolder()) > 255) {
            throw new ValidationException('Customer homeFolder maximum length is 255 characters');
        }

        if (strlen($this->getNotes()) > 255) {
            throw new ValidationException('Customer notes maximum length is 255 characters');
        }
    }

    public function validateNew()
    {
        $customerConnection = $this->getConnection(DEFAULT_CUSTOMER_DB_HOST, null, DEFAULT_CUSTOMER_DB_USER, DEFAULT_CUSTOMER_DB_PASS);

        // If new and database exists throw exception
        $sql    = "SELECT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" . $this->getDBDatabase() . "') AS `exists`";
        $result = $customerConnection->query($sql)->fetch();
        if ($result['exists'] == '1') {
            throw new DBDuplicationException('Database "' . $this->getDBDatabase() . '" already exists');
        }

        // If new and user exists throw exception
        $sql    = "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '" . $this->getDBUser() . "') AS `exists`";
        $result = $customerConnection->query($sql)->fetch();
        if ($result['exists'] == '1') {
            throw new DBDuplicationException('User "' . $this->getDBUser() . '" already exists');
        }

        // If new and read user exists throw exception
        $sql    = "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '" . $this->getDBUserRead() . "') AS `exists`";
        $result = $customerConnection->query($sql)->fetch();
        if ($result['exists'] == '1') {
            throw new DBDuplicationException('User "' . $this->getDBUserRead() . '" already exists');
        }
    }

    public function processNew()
    {
        $customerConnection = $this->getConnection(DEFAULT_CUSTOMER_DB_HOST, null, DEFAULT_CUSTOMER_DB_USER, DEFAULT_CUSTOMER_DB_PASS);

        // Create new customer database
        if ($customerConnection->exec("CREATE DATABASE " . $this->getDBDatabase()) === false) {
            throw new PDOExecutionException('Failed creating new database');
        }

        // Create new write user 
        if ($customerConnection->exec("CREATE USER " . $this->getDBUser() . "@'%' IDENTIFIED BY '" . $this->getDBPassword() . "'") === false) {
            throw new PDOExecutionException('Failed creating new write user');
        }

        // Add new write user permissions
        if ($customerConnection->exec("GRANT " . implode(', ', $this->minimumWritePermissions) . " ON " . $this->getDBDatabase() . ".* TO " . $this->getDBUser() . "@'%'") === false) {
            throw new PDOExecutionException('Failed granting new write user permissions');
        }

        // Create new read user
        if ($customerConnection->exec("CREATE USER " . $this->getDBUserRead() . "@'%' IDENTIFIED BY '" . $this->getDBPasswordRead() . "'") === false) {
            throw new PDOExecutionException('Failed creating new read user');
        }

        // Add new read user permissions
        if ($customerConnection->exec("GRANT " . implode(', ', $this->minimumReadPermissions) . " ON " . $this->getDBDatabase() . ".* TO " . $this->getDBUserRead() . "@'%'") === false) {
            throw new PDOExecutionException('Failed granting new read user permissions');
        }

        // Flush privileges
        if ($customerConnection->exec("FLUSH PRIVILEGES;") === false) {
            throw new PDOExecutionException('Failed flushing privileges');
        }
    }

    public function validateDatabaseConectionAndPermissions($new, $type)
    {
        if ($type === 'read') {
            // Do test read connection (will throw exception on fail) & check appropriate permissions if not new
            try {
                $customerConnection = $this->getConnection($this->getDBHost(), $this->getDBDatabase(), $this->getDBUserRead(), $this->getDBPasswordRead());

                if (!$new) {
                    $customerReadUserPermissions = $this->getUserPrivileges($customerConnection, $this->getDBUserRead());

                    if ($customerReadUserPermissions !== true) {
                        if (count(array_intersect($customerReadUserPermissions, $this->minimumReadPermissions)) < count($this->minimumReadPermissions)) {
                            throw new ValidationException('Read user does not meet minimum privilege requirements [' . implode(',', $this->minimumReadPermissions) . ']');
                        }
                    }
                }
            } catch (\PDOException $ex) {
                throw new PDOExecutionException('Connection test failed for read user: ' . $ex->getMessage());
            }
        } else if ($type === 'write') {
            // Do test write connection (will throw exception on fail) & check appropriate permissions if not new
            try {
                $customerConnection = $this->getConnection($this->getDBHost(), $this->getDBDatabase(), $this->getDBUser(), $this->getDBPassword());

                if (!$new) {
                    $customerWriteUserPermissions = $this->getUserPrivileges($customerConnection, $this->getDBUser());

                    if ($customerWriteUserPermissions !== true) {
                        if (count(array_intersect($customerWriteUserPermissions, $this->minimumWritePermissions)) < count($this->minimumWritePermissions)) {
                            throw new ValidationException('Write user does not meet minimum privilege requirements [' . implode(',', $this->minimumWritePermissions) . ']');
                        }
                    }
                }
            } catch (\PDOException $ex) {
                throw new PDOExecutionException('Connection test failed for write user: ' . $ex->getMessage());
            }
        }
    }

    private function generateDBUserName($type)
    {
        // $type can be [read|write]
        $prefix          = 'sln_';
        $suffix          = '';
        $middle          = '';
        $maxLength       = 16;
        $availableLength = 0;

        if ($type !== 'read' && $type !== 'write') {
            throw new ValidationException('Type must be [read|write]');
        }

        $dbUserName = $this->name;

        // If string contains [sandbox|test]; these are synonymous
        if (strpos($dbUserName, '_sandbox') !== false) {
            $dbUserName = str_replace('_sandbox', '', $dbUserName);
            $suffix     = $type === 'read' ? '_sr' : '_s';
        } else if (strpos($dbUserName, '_demo') !== false) {
            $dbUserName = str_replace('_demo', '', $dbUserName);
            $suffix     = $type === 'read' ? '_dr' : '_d';
        } else if (strpos($dbUserName, '_dev') !== false) {
            $dbUserName = str_replace('_dev', '', $dbUserName);
            $suffix     = $type === 'read' ? '_er' : '_e';
        } else if (strpos($dbUserName, '_test') !== false) {
            $dbUserName = str_replace('_test', '', $dbUserName);
            $suffix     = $type === 'read' ? '_tr' : '_t';
        } else if (strpos($dbUserName, '_training') !== false) {
            $dbUserName = str_replace('_training', '', $dbUserName);
            $suffix     = $type === 'read' ? '_ar' : '_a';
        } else if ($type === 'read') {
            $suffix = '_r';
        }

        $availableLength = $maxLength - strlen($prefix) - strlen($suffix);

        // Max length of entire name can only be $maxLength characters
        $middle = substr(str_replace('_', '', $dbUserName), 0, $availableLength);

        return $prefix . $middle . $suffix;
    }

    private function getUserPrivileges($pdo, $userName)
    {
        $permissionArray = [];

        // Must be done this way as we cannot select from MySQL restricted user tables with all accounts
        $results = $pdo->query("SHOW GRANTS FOR '" . $userName . "'@'%';")->fetchAll();

        foreach ($results as $result) {
            reset($result);
            $key = key($result);

            // Limit results to only those matching the customer database
            if (strpos(stripslashes($result[$key]), $this->getDBDatabase()) === false) {
                continue;
            }

            // IF GRANT ALL (SPECIAL CASE)
            if (strpos(stripslashes($result[$key]), 'GRANT ALL PRIVILEGES') !== false) {
                return true;
            }

            // Some fuzzy rules for finding permissions
            $grantString          = 'GRANT ';
            $positionOfFirstGrant = strpos($result[$key], $grantString) + strlen($grantString);
            $onString             = ' ON ';
            $positionOfLastOn     = strpos($result[$key], $onString) - strlen($grantString);
            $permissionArray      = array_merge($permissionArray, explode(',', substr($result[$key], $positionOfFirstGrant, $positionOfLastOn)));
            $permissionArray      = array_map('trim', $permissionArray);
        }

        if (count($permissionArray) <= 1) {
            throw new ValidationException('User ' . $userName . ' does not have enough permissions');
        }

        return array_unique($permissionArray);
    }

    public function truncateDB()
    {
        $customerConnection = $this->getConnection(DEFAULT_CUSTOMER_DB_HOST, null, DEFAULT_CUSTOMER_DB_USER, DEFAULT_CUSTOMER_DB_PASS);

        // Drop the customer database
        if ($customerConnection->exec("DROP DATABASE IF EXISTS " . $this->getDBDatabase()) === false) {
            throw new PDOExecutionException('Failed dropping database');
        }

        // Again create the customer database
        if ($customerConnection->exec("CREATE DATABASE " . $this->getDBDatabase()) === false) {
            throw new PDOExecutionException('Failed creating new database');
        }
    }

    private function generateUID()
    {
        $uid = self::UID_MIN;

        $connection = $this->getConnection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

        $uidArr = $connection->query("SELECT `uid` FROM " . DB_TABLE . " ORDER BY `uid`")->fetchAll(\PDO::FETCH_COLUMN);

        $length = count($uidArr);

        if ($length == 0) {
            return $uid;
        }

        $uid = $uidArr[$length - 1] + 1;

        if ($uid <= self::UID_MAX) {
            return $uid;
        }

        for ($uid = self::UID_MIN; $uid <= self::UID_MAX; $uid++) {
            if (!in_array($uid, $uidArr)) {
                break;
            }
        }

        if ($uid > self::UID_MAX) {
            throw new OutOfRangeException('Failed to create customer as UID is out of range');
        }

        return $uid;
    }

    #endregion

    public function updateLastTested($response, $id)
    {
        $sql = "UPDATE `customer` 
                 SET `last_tested`          = CURRENT_TIMESTAMP, 
                     `last_tested_response` = :response 
                 WHERE `id` = :id";

        $pdo = $this->getConnection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

        $stmt = $pdo->prepare($sql);

        if ($stmt->execute(['response' => $response, 'id' => $id]) === false) {
            throw new PDOExecutionException('Failed to execute ' . $sql);
        }
    }

    public function persistEnabled()
    {
        $sql = "UPDATE `customer` 
                SET `enabled` = :enabled 
                WHERE `name` = :name";

        $pdo = $this->getConnection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

        $stmt = $pdo->prepare($sql);

        if ($stmt->execute(['enabled' => $this->getEnabled(), 'name' => $this->getName()]) === false) {
            throw new PDOExecutionException('Failed to execute ' . $sql);
        }
    }
}
