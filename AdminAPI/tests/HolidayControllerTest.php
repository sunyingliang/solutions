<?php

class HolidayControllerTest extends PHPUnit_Framework_TestCase
{
    public function testGetListCalendar()
    {
        $response = $this->runCurl('api/holiday/list-calendar', [], 'token=data');
        $this->assertEquals(401, $response['status']);

        $response = $this->runCurl('api/holiday/list-calendar', [], 'token=FD040101-AFB7-15C7-6DD2-5492047F54F8');
        $this->assertEquals(200, $response['status']);

        $decode = json_decode($response['body']);
        $this->assertTrue(json_last_error() === JSON_ERROR_NONE);
        $this->assertTrue($decode->response->status == 'success');
    }

    public function testGetListYear()
    {
        $response = $this->runCurl('api/holiday/list-year', [], 'token=data');
        $this->assertEquals(401, $response['status']);

        $response = $this->runCurl('api/holiday/list-year', [], 'token=FD040101-AFB7-15C7-6DD2-5492047F54F8&calendar=test');
        $this->assertEquals(200, $response['status']);

        $decode = json_decode($response['body']);
        $this->assertTrue(json_last_error() === JSON_ERROR_NONE);
        $this->assertTrue($decode->response->status == 'success');
    }

    public function runCurl(
        $url,
        $header     = [],
        $post       = null,
        $userPass   = null,
        $override   = false
    ) {
        if (!$override) {
            $url = 'http://localhost:8081/' . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
