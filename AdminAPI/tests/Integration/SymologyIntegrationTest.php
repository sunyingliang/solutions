<?php

require_once __DIR__ . '/../../config/common.php';
require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/MockFillTask.php';
require_once __DIR__ . '/MockSymologyReader.php';

use FS\Database\Manager;
use FS\Integration\Reportit\Symology;
use PHPUnit\Framework\TestCase;

class SymologyIntegrationTest extends TestCase
{
    const TEST_CASE_REFERENCE = 'test-RIC2955041';
    const CUSTOMER            = 'integration';
    /** @var  Manager */
    private static $dbManager;

    public static function setUpBeforeClass()
    {
        self::$dbManager = new Manager(self::CUSTOMER);
    }

    public static function tearDownAfterClass()
    {
        self::$dbManager = null;
    }

    public function testRecordFillTaskShouldNotRun()
    {
        // clean slate
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare("DELETE FROM `symology_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // add record to database
        $stmt = $connection->prepare("INSERT INTO `symology_holding` (`case_reference`,`task_id`,`process_name`,`stage_name`,`symology_reference`)
                                     VALUES (:caseReference,'1','test process','test stage','20100827')");
        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // process
        $symology = new Symology(self::CUSTOMER, new MockSymologyReader(MockSymologyReader::NOT_COMPLETED_CASE), new MockFillTask());

        $symology->run();

        // check result
        $stmt = $connection->prepare("SELECT `job_status`, `pending_filltask_flag` FROM `symology_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        $result = $stmt->fetch();

        $this->assertEquals(0, $result['pending_filltask_flag']);
        $this->assertEquals('RECORDED', $result['job_status']);

        // clean up
        $stmt = $connection->prepare("DELETE FROM `symology_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);
    }

    public function testRecordFillTaskShouldRun()
    {
        // clean slate
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare("DELETE FROM `symology_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // add record to database
        $stmt = $connection->prepare("INSERT INTO `symology_holding` (`case_reference`,`task_id`,`process_name`,`stage_name`,`symology_reference`)
                                     VALUES (:caseReference,'1','test process','test stage','20100827')");
        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // process
        $symology = new Symology(self::CUSTOMER, new MockSymologyReader(MockSymologyReader::CLOSE_CASE), new MockFillTask());

        $symology->run();

        // check result
        $stmt = $connection->prepare("SELECT `job_status`, `pending_filltask_flag` FROM `symology_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        $result = $stmt->fetch();

        $this->assertEquals(1, $result['pending_filltask_flag']);
        $this->assertEquals('RECORDED', $result['job_status']);

        // clean up
        $stmt = $connection->prepare("DELETE FROM `symology_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);
    }
}
