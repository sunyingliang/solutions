<?php

require_once __DIR__ . '/../../config/common.php';
require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/MockFillTask.php';
require_once __DIR__ . '/MockMayriseConfig.php';
require_once __DIR__ . '/MockMayriseReader.php';

use FS\Database\Manager;
use FS\Integration\Reportit\Mayrise;
use PHPUnit\Framework\TestCase;

class MayriseIntegrationTest extends TestCase
{
    const TEST_CASE_REFERENCE = 'test-RIC2955043';
    const TEST_ASSET_ID       = '108542';
    const CUSTOMER            = 'integration';
    /** @var  Manager */
    private static $dbManager;

    public static function setUpBeforeClass()
    {
        self::$dbManager = new Manager(self::CUSTOMER);
    }

    public static function tearDownAfterClass()
    {
        self::$dbManager = null;
    }

    public function testImport()
    {
        // Import
        $mayrise = new Mayrise(self::CUSTOMER, new MockMayriseReader(MockMayriseReader::NEW_CASE), new MockFillTask(self::TEST_CASE_REFERENCE), new MockMayriseConfig());

        $mayrise->importList();

        // Check if inserted correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `mayrise_import` WHERE `job_number` = :jobNumber');

        $stmt->execute(['jobNumber' => '50077566']);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);
    }

    public function testAddNewRecord()
    {
        // Process
        $mayrise = new Mayrise(self::CUSTOMER, new MockMayriseReader(MockMayriseReader::NEW_CASE), new MockFillTask(self::TEST_CASE_REFERENCE), new MockMayriseConfig());

        $mayrise->processList();

        // Check if inserted correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `mayrise_case` WHERE `case_ref` = :caseRef AND `job_number` = :jobNumber AND `asset_id` = :assetId');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'jobNumber' => '50077566', 'assetId' => self::TEST_ASSET_ID]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);
    }

    public function testFollowupJob()
    {
        // Import
        $mayrise = new Mayrise(self::CUSTOMER, new MockMayriseReader(MockMayriseReader::FOLLOW_UP), new MockFillTask(self::TEST_CASE_REFERENCE), new MockMayriseConfig());

        $mayrise->importList();

        // Process
        $mayrise->processList();

        // Check if changed correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `mayrise_case` WHERE `case_ref` = :caseRef AND `job_number` = :jobNumber AND `asset_id` = :assetId');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'jobNumber' => '50077566', 'assetId' => self::TEST_ASSET_ID]);

        $result = $stmt->fetch();

        $this->assertTrue($result === false);
        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'jobNumber' => '50078075', 'assetId' => self::TEST_ASSET_ID]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);
    }

    public function testCloseJob()
    {
        // Import
        $mayrise = new Mayrise(self::CUSTOMER, new MockMayriseReader(MockMayriseReader::CLOSE_JOB), new MockFillTask(self::TEST_CASE_REFERENCE), new MockMayriseConfig());

        $mayrise->importList();

        // Process
        $mayrise->processList();

        // Check if removed correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `mayrise_case` WHERE `case_ref` = :caseRef AND `job_number` = :jobNumber AND `asset_id` = :assetId');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'jobNumber' => '50077566', 'assetId' => self::TEST_ASSET_ID]);

        $result = $stmt->fetch();

        $this->assertTrue($result === false);
        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'jobNumber' => '50078075', 'assetId' => self::TEST_ASSET_ID]);

        $result = $stmt->fetch();

        $this->assertTrue($result === false);
    }
}
