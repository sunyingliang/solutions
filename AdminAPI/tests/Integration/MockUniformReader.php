<?php

use \FS\Integration\Handler\IUniformReader;

class MockUniformReader implements IUniformReader
{
    const NOT_COMPLETED_CASE = 0;
    const CLOSE_CASE         = 1;

    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function GetChangedService(string $changesSince): string
    {
        return '<?xml version="1.0" encoding="utf-8"?>
<Envelope>
    <Body>
        <GetChangedServiceRequestRefValsResponse>
            <RefVals>
                <ReferenceValue>17/00017/SR</ReferenceValue>
                <RequestType>GENERAL</RequestType>
            </RefVals>
        </GetChangedServiceRequestRefValsResponse>
        <TransactionReport>
            <TransactionComplete>True</TransactionComplete>
            <TransactionSuccess>True</TransactionSuccess>
            <TransactionMessages/>
        </TransactionReport>
    </Body>
</Envelope>';
    }

    public function GetGeneralServiceRequest(string $reference): string
    {
        return $this->GetServiceRequest($reference);
    }

    public function GetDogServiceRequest(string $reference): string
    {
        return $this->GetServiceRequest($reference);
    }

    public function GetFoodServiceRequest(string $reference): string
    {
        return $this->GetServiceRequest($reference);
    }

    public function GetNoiseServiceRequest(string $reference): string
    {
        return $this->GetServiceRequest($reference);
    }

    public function GetPestServiceRequest(string $reference): string
    {
        return $this->GetServiceRequest($reference);
    }

    private function GetServiceRequest(string $reference)
    {
        $statusCode    = '';
        $dateCompleted = '';

        switch ($this->type) {
            case self::NOT_COMPLETED_CASE:
                $statusCode = '1_OPN';
                break;
            case self::CLOSE_CASE:
                $statusCode    = '9_CLO';
                $dateCompleted = '2017-12-01T00:00:00';
                break;
        }

        return '<?xml version="1.0" encoding="utf-8"?>
<Envelope>
    <Body>
        <GetDogServiceRequestForUpdateByReferenceValueResponse>
            <UpdateableDogServiceRequest>
                <UniformRecordID>
                    <TechnicalKey>OQGI5QBEJ5W00</TechnicalKey>
                    <UVERSION>"</UVERSION>
                </UniformRecordID>
                <ServiceRequestIdentification>
                    <ServiceRequestTechnicalKey>OQGI5QBEJ5W00</ServiceRequestTechnicalKey>
                    <ReferenceValue>' . $reference . '</ReferenceValue>
                    <AlternativeReferences>
                        <AlternativeReference>
                            <ReferenceValue>EH3294187/2683591</ReferenceValue>
                            <ReferenceType>FIRM</ReferenceType>
                        </AlternativeReference>
                    </AlternativeReferences>
                </ServiceRequestIdentification>
                <SiteLocation>
                    <Address>Civic Offices
2 Watling Street
Bexleyheath
Kent
DA6 7AT
</Address>
                    <UPRN>010023304993</UPRN>
                    <MapEast>549723</MapEast>
                    <MapNorth>175152</MapNorth>
                </SiteLocation>
                <SubjectDetails>
                    <IdentifiedSubjectContactDetails/>
                </SubjectDetails>
                <NatureOfComplaint>Animal type: TEST CASE ONLY
How often: Other
Time of day: Other
Date problem started: 01 Jan 2017
Further info: TEST CASE ONLY</NatureOfComplaint>
                <Customers>
                    <IdentifiedCustomer>
                        <UniformRecordID>
                            <TechnicalKey>OQGI5QBEJ5W04</TechnicalKey>
                            <UVERSION>"</UVERSION>
                        </UniformRecordID>
                        <IdentifiedCustomerDetails>
                            <CustomerTypeCode>PUBLIC</CustomerTypeCode>
                            <CustomerTypeText>Member of the Public</CustomerTypeText>
                            <CustomerName>
                                <FullName>Mr Test Test</FullName>
                                <Forenames>Test</Forenames>
                                <Surname>Test</Surname>
                                <Title>Mr</Title>
                            </CustomerName>
                            <IdentifiedCustomerContactDetails>
                                <IdentifiedCustomerContactDetail>
                                    <UniformRecordID>
                                        <TechnicalKey>OQGI5QBEJ5W05</TechnicalKey>
                                        <UVERSION>!</UVERSION>
                                    </UniformRecordID>
                                    <ContactTypeCode>EMAIL</ContactTypeCode>
                                    <ContactTypeText>EMAIL</ContactTypeText>
                                    <ContactAddress>testtest@test.co.uk</ContactAddress>
                                </IdentifiedCustomerContactDetail>
                                <IdentifiedCustomerContactDetail>
                                    <UniformRecordID>
                                        <TechnicalKey>OQGI5QBEJ5W06</TechnicalKey>
                                        <UVERSION>!</UVERSION>
                                    </UniformRecordID>
                                    <ContactTypeCode>PHONEH</ContactTypeCode>
                                    <ContactTypeText>Home Phone Number</ContactTypeText>
                                    <ContactAddress>02083029394</ContactAddress>
                                </IdentifiedCustomerContactDetail>
                                <IdentifiedCustomerContactDetail>
                                    <UniformRecordID>
                                        <TechnicalKey>OQGI5QBEJ5W07</TechnicalKey>
                                        <UVERSION>!</UVERSION>
                                    </UniformRecordID>
                                    <ContactTypeCode>PHONEM</ContactTypeCode>
                                    <ContactTypeText>Mobile Phone Number</ContactTypeText>
                                </IdentifiedCustomerContactDetail>
                                <IdentifiedCustomerContactDetail>
                                    <UniformRecordID>
                                        <TechnicalKey>OQGI5QBEJ5W08</TechnicalKey>
                                        <UVERSION>!</UVERSION>
                                    </UniformRecordID>
                                    <ContactTypeCode>PHONEW</ContactTypeCode>
                                    <ContactTypeText>Business Phone Number</ContactTypeText>
                                </IdentifiedCustomerContactDetail>
                            </IdentifiedCustomerContactDetails>
                            <Address>Civic Offices 2  Watling Street
Bexleyheath
Kent
DA6 7AT</Address>
                            <WaiveAnonymity>Null</WaiveAnonymity>
                            <SupportsLegalProceedings>Null</SupportsLegalProceedings>
                            <CallbackRequred>Null</CallbackRequred>
                            <TimeReceived>2017-05-24T09:16:41</TimeReceived>
                            <PerformanceDetails>
                                <TargetResolutionDateTime>2017-08-24T09:16:41</TargetResolutionDateTime>
                                <TargetResponseDateTime>2017-05-31T09:16:41</TargetResponseDateTime>
                                <ActualResolutionDateTime>2017-12-01T00:00:00</ActualResolutionDateTime>
                                <ElapsedResolutionTime>7</ElapsedResolutionTime>
                            </PerformanceDetails>
                        </IdentifiedCustomerDetails>
                    </IdentifiedCustomer>
                </Customers>
                <AdministrationDetails>
                    <RequestTypeCode>ANSMEL</RequestTypeCode>
                    <RequestTypeText>Animal Smell</RequestTypeText>
                    <RequestKindCode>D</RequestKindCode>
                    <RequestKindText>Dog Request</RequestKindText>
                    <AllocatedToCode>EHCALL</AllocatedToCode>
                    <AllocatedToText>EH Call Centre</AllocatedToText>
                    <TimeAllocated>2017-05-24T13:17:49</TimeAllocated>
                    <AllocationHistory>
                        <AllocationHistoryDetail>
                            <OfficerCode>EHCALL</OfficerCode>
                            <OfficerName>EH Call Centre</OfficerName>
                            <AllocationDateTime>2017-05-24T13:17:50</AllocationDateTime>
                        </AllocationHistoryDetail>
                    </AllocationHistory>
                    <HowReceivedCode>TELE</HowReceivedCode>
                    <HowReceivedText>telephone</HowReceivedText>
                    <TimeReceived>2017-05-24T09:16:41</TimeReceived>
                    <StatusCode>8_CLO</StatusCode>
                    <StatusText>8_CLO - Closed</StatusText>
                    <ClosedByOfficerCode>RF</ClosedByOfficerCode>
                    <ClosedByOfficerName>Mr Robert Flicker</ClosedByOfficerName>
                </AdministrationDetails>
                <InspectionDetails>
                    <UniformRecordID>
                        <TechnicalKey>OQGI5QBEJ5W01</TechnicalKey>
                        <UVERSION>"</UVERSION>
                    </UniformRecordID>
                    <ReferenceValue>17/00017/DCOMP</ReferenceValue>
                    <StatusCode>' . $statusCode . '</StatusCode>
                    <StatusText>Inspection Closed</StatusText>
                    <Address>Civic Offices
2 Watling Street
Bexleyheath
Kent
DA6 7AT
</Address>
                    <DateCompleted>' . $dateCompleted . '</DateCompleted>
                    <DateClosed>' . $dateCompleted . '</DateClosed>
                    <ClosedByOfficerCode>RF</ClosedByOfficerCode>
                    <ClosedByOfficerName>Mr Robert Flicker</ClosedByOfficerName>
                    <Visits/>
                </InspectionDetails>
                <PerformanceDetails>
                    <TargetResolutionDateTime>2017-08-24T09:16:41</TargetResolutionDateTime>
                    <TargetResponseDateTime>2017-05-31T09:16:41</TargetResponseDateTime>
                    <ActualResolutionDateTime>2017-12-01T00:00:00</ActualResolutionDateTime>
                    <ElapsedResolutionTime>7</ElapsedResolutionTime>
                </PerformanceDetails>
                <XtraValues>
                    <IdentifiedSR_Xtra>
                        <UniformRecordID>
                            <TechnicalKey>OQGI5QBEJ5W09</TechnicalKey>
                            <UVERSION>!</UVERSION>
                        </UniformRecordID>
                        <FieldDescription>Additional Complainant Info</FieldDescription>
                        <FieldName>ADDINFO</FieldName>
                        <FieldType>TEXT</FieldType>
                    </IdentifiedSR_Xtra>
                </XtraValues>
            </UpdateableDogServiceRequest>
        </GetDogServiceRequestForUpdateByReferenceValueResponse>
        <TransactionReport>
            <TransactionComplete>True</TransactionComplete>
            <TransactionSuccess>True</TransactionSuccess>
            <TransactionMessages/>
        </TransactionReport>
    </Body>
</Envelope>';
    }
}