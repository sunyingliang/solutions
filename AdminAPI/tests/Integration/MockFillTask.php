<?php

use \FS\Integration\Handler\IFillTask;

class MockFillTask implements IFillTask
{
    private $caseReference;

    public function __construct($caseReference = 'test-RIC2955040')
    {
        $this->caseReference = $caseReference;
    }

    public function startThread(string $url, array $request): string
    {
        return $this->caseReference;
    }

    public function fillTask(string $url, array $request): bool
    {
        return true;
    }
}