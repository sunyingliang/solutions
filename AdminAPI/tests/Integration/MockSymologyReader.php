<?php

use \FS\Integration\Handler\ISymologyReader;

class MockSymologyReader implements ISymologyReader
{
    const NOT_COMPLETED_CASE = 0;
    const CLOSE_CASE         = 1;

    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function getRequestAdditionalGroup($serviceCode, $webRequestID): \stdClass
    {
        $histories = null;

        switch ($this->type) {
            case self::NOT_COMPLETED_CASE:
                $histories = [
                    (object)[
                        'LineNo'      => '1',
                        'HistoryDate' => '2015-06-25T00:00:00',
                        'HistoryType' => 1
                    ]
                ];
                break;
            case self::CLOSE_CASE:
                $histories = [
                    (object)[
                        'LineNo'      => '1',
                        'HistoryDate' => '2015-06-25T00:00:00',
                        'HistoryType' => 1
                    ],
                    (object)[
                        'LineNo'      => '1',
                        'HistoryDate' => '2016-06-25T00:00:00',
                        'HistoryType' => 7
                    ]
                ];
                break;
        }

        $data = (object)[
            'StatusCode'    => '0',
            'StatusMessage' => '',
            'TimeTaken'     => '>00:00:00.0156001',
            'Request'       => (object)[
                'StageDescripton' => 'RECORDED',
                'EventHistory'    => (object)[
                    'EventHistoryGet' => $histories
                ]
            ]
        ];

        return $data;
    }
}
