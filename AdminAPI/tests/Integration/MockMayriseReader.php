<?php

use \FS\Integration\Handler\IMayriseReader;

class MockMayriseReader implements IMayriseReader
{
    const NEW_CASE  = 0;
    const FOLLOW_UP = 1;
    const CLOSE_JOB = 2;

    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function getJobsChanged(string $changesSince, bool $includeBackOfficeCreatedJobs, bool $onlyStatusChanges): array
    {
        $data = [
            (object)[
                'JobNumber'              => '50077566',
                'ExternalReference'      => 'PAINT15',
                'UserJobStatusCode'      => '',
                'WorkInProgressDateTime' => '2015-07-07T12:36:00',
                'CompletedDateTime'      => '2015-07-07T12:37:00',
                'CancelledDateTime'      => null,
                'LogicalStatus'          => 'jls_Completed'
            ]
        ];
        switch ($this->type) {
            case self::NEW_CASE:
                $data[0]->CompletedDateTime = null;
                break;
            case self::FOLLOW_UP:
                $data[0]->CompletedDateTime = null;
                $data[0]->JobNumber         = '50078075';
                break;
            case self::CLOSE_JOB:
                $data[0]->JobNumber = '50078075';
                break;

        }

        return $data;
    }

    public function getJobByJobNumber(string $jobNumber): \stdClass
    {
        $data = [
            'AffectedUnits'            => [
                [
                    'UnitID'                      => '108542',
                    'StreetID'                    => '005573',
                    'UniqueStreetReferenceNumber' => '38500560',
                    'UnitNumber'                  => '00004',
                    'Location'                    => 'NEXT TO GREEN / BY SWINGS\nPAGANHILL\nSTROUD',
                    'PAON'                        => [
                        'StartRange'  => ['Number' => 0, 'Suffix' => ''],
                        'EndRange'    => ['Number' => 0, 'Suffix' => ''],
                        'Description' => ''
                    ],
                    'Easting'                     => 383981,
                    'Northing'                    => 205687
                ]
            ],
            'JobNumber'                => '50077566',
            'ReportedDateTime'         => '2015-06-29T10:09:00',
            'TargetDateTime'           => '2015-07-20T10:09:00',
            'WorkInProgressDateTime'   => '2015-07-07T12:36:00',
            'CompletedDateTime'        => '2015-07-07T12:37:00',
            'CancelledDateTime'        => null,
            'LogicalStatus'            => 'jls_Completed',
            'ServiceCode'              => 'STL',
            'RecordTypeCode'           => 'F',
            'MultipleDescriptionCodes' => [
                [
                    'DescriptionCode' => 'K34',
                    'SeverityCode'    => '15',
                    'Quantity'        => 1,
                    'Length'          => 0,
                    'Width'           => 0,
                    'Depth'           => 0
                ]
            ],
            'CauseCode'                => 'OTH',
            'BackOfficeNotes'          => '',
            'ContractorNotes'          => '',
            'ExternalReference'        => '',
            'JobTypeCode'              => '',
            'JobDescription'           => '\'MLW (15 DAY) - COLUMN/PILLAR/POST NUMBER FADED/MISSING\'\nPlease number this PL as 4 (currently numbered 3/1)',
            'JobProgress'              => 'Reported 29/06/2015 10:09:00\nWork In Progress 07/07/2015 12:36:00\nCompleted 07/07/2015 12:37:00',
            'Callers'                  => [],
            'InspectorCode'            => '',
            'InspectorName'            => '',
            'PassedToCode'             => '',
            'PassedToName'             => ''
        ];
        switch ($this->type) {
            case self::NEW_CASE:
                $data['CompletedDateTime'] = null;
                break;
            case self::FOLLOW_UP:
                $data['CompletedDateTime'] = null;
                $data['JobNumber']         = '50078075';
                break;
            case self::CLOSE_JOB:
                $data['JobNumber'] = '50078075';
                break;
        }

        return json_decode(json_encode($data));
    }

    public function getUnitByUnitID(string $unitID): \stdClass
    {
        $data = [
            'UnitID'                      => '108542',
            'UnitNumber'                  => '00004',
            'StreetID'                    => '005573',
            'UniqueStreetReferenceNumber' => '38500560',
            'Location'                    => 'NEXT TO GREEN / BY SWINGS',
            'UnitTypeCode'                => 'B',
            'UnitTypeDesc'                => 'B-Class RL'
        ];

        return json_decode(json_encode($data));
    }
}