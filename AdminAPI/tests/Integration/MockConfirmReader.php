<?php

use FS\Integration\Handler\IConfirmReader;

class MockConfirmReader implements IConfirmReader
{
    const UPDATE_ENQUIRY = 0;
    const CLOSE_ENQUIRY  = 1;
    const CREATE_DEFECT  = 2;
    const UPDATE_DEFECT  = 3;
    const CLOSE_DEFECT   = 4;

    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function getEnquiryList(): array
    {
        switch ($this->type) {
            case self::UPDATE_ENQUIRY:
                return ["11125449\t25/01/2017 14:39:54\t25/01/2017 14:39:54\t0050\tNew Enquiry Received\tY\tDESCRIPTION HERE\tHI15\tDrainage\ttest-RIC2955042\t25/01/2017 14:39:54\t"];
            case self::CLOSE_ENQUIRY:
                return ["11125449\t25/01/2017 14:39:54\t25/01/2017 14:39:54\t0050\tNew Enquiry Received\tN\tDESCRIPTION HERE\tHI15\tDrainage\ttest-RIC2955042\t25/01/2017 14:39:54\t"];
            default:
                return [];
        }
    }

    public function getDefectList(): array
    {
        switch ($this->type) {
            case self::CREATE_DEFECT:
                return ["32037972\t389,999.29\t194,316.87\t9400541\tLONDON ROAD\tDirty or Obscured\tDIRT\t6MTH\t\t01/12/2016 08:50:03\tN\t42246706\t01/12/2016 08:52:06\t\tU\t0000\tJob Raised\tN\t01/12/2016 08:52:06\t1x dirty coso (Dirty or Obscured)\t\t45\tSigns: Non-routine\t\t\t"];
            case self::UPDATE_DEFECT:
                return ["32037972\t389,999.29\t194,316.87\t9400541\tLONDON ROAD\tDirty or Obscured\tDIRT\t6MTH\t\t01/12/2016 08:50:03\tN\t42246706\t01/12/2016 08:52:06\t\tU\t0000\tJob Raised\tN\t01/12/2016 08:52:06\t1x dirty coso (Dirty or Obscured)\t\t45\tSigns: Non-routine\t\ttest-RIC2955042\t"];
            case self::CLOSE_DEFECT:
                return ["32037972\t389,999.29\t194,316.87\t9400541\tLONDON ROAD\tDirty or Obscured\tDIRT\t6MTH\t\t01/12/2016 08:50:03\tN\t42246706\t01/12/2016 08:52:06\t\tU\t0500\tJob Raised\tN\t01/12/2016 08:52:06\t1x dirty coso (Dirty or Obscured)\t\t45\tSigns: Non-routine\t\ttest-RIC2955042\t"];
            default:
                return [];
        }
    }
}