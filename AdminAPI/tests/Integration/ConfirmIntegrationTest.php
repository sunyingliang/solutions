<?php

require_once __DIR__ . '/../../config/common.php';
require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/MockFillTask.php';
require_once __DIR__ . '/MockConfirmReader.php';

use FS\Database\Manager;
use FS\Integration\Reportit\Confirm;
use PHPUnit\Framework\TestCase;

class ConfirmIntegrationTest extends TestCase
{
    const TEST_CASE_REFERENCE = 'test-RIC2955042';
    const TEST_ENQUIRY_NUMBER = '11125449';
    const TEST_DEFECT_NUMBER  = '32037972';
    const CUSTOMER            = 'integration';
    /** @var  Manager */
    private static $dbManager;

    public static function setUpBeforeClass()
    {
        self::$dbManager = new Manager(self::CUSTOMER);
    }

    public static function tearDownAfterClass()
    {
        self::$dbManager = null;
    }

    public function testWatchdogConfirmImportEnquiry()
    {
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::UPDATE_ENQUIRY), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Check if imported correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `confirm_enquiry_import` WHERE `enquiry_number` = :enquiryNumber');

        $stmt->execute(['enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);
        $result = $stmt->fetch();

        $this->assertTrue($result !== false);

        // Clean up
        $stmt = $connection->prepare('DELETE FROM `confirm_enquiry_import` WHERE `enquiry_number` = :enquiryNumber');

        $stmt->execute(['enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);
    }

    public function testWatchdogConfirmUpdateEnquiry()
    {
        // Setup
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `confirm_enquiry_case` (`case_ref`, `enquiry_number`) VALUES (:caseRef, :enquiryNumber)');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);

        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::UPDATE_ENQUIRY), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Process
        $confirm->processList();

        // Check if updated correctly
        $stmt = $connection->prepare('SELECT * FROM `confirm_enquiry_case` WHERE `case_ref` = :caseRef AND `enquiry_number` = :enquiryNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);

        // Clean up
        $stmt = $connection->prepare('DELETE FROM `confirm_enquiry_case` WHERE `case_ref` = :caseRef AND `enquiry_number` = :enquiryNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);
    }

    public function testWatchdogConfirmCloseEnquiry()
    {
        // Setup
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `confirm_enquiry_case` (`case_ref`, `enquiry_number`) VALUES (:caseRef, :enquiryNumber)');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);

        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::CLOSE_ENQUIRY), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Process
        $confirm->processList();

        // Check if removed correctly
        $stmt = $connection->prepare('SELECT * FROM `confirm_enquiry_case` WHERE `case_ref` = :caseRef AND `enquiry_number` = :enquiryNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'enquiryNumber' => self::TEST_ENQUIRY_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result === false);
    }

    public function testWatchdogConfirmImportDefect()
    {
        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::CREATE_DEFECT), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Check if imported correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `confirm_defect_import` WHERE `defect_number` = :defectNumber');

        $stmt->execute(['defectNumber' => self::TEST_DEFECT_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);

        // Clean up
        $stmt = $connection->prepare('DELETE FROM `confirm_defect_import` WHERE `defect_number` = :defectNumber');

        $stmt->execute(['defectNumber' => self::TEST_DEFECT_NUMBER]);
    }

    public function testWatchdogConfirmCreateDefect()
    {
        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::CREATE_DEFECT), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Process
        $confirm->processList();

        // Check if updated correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `confirm_defect_case` WHERE `case_ref` = :caseRef AND `defect_number` = :defectNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);

        // Clean up
        $stmt = $connection->prepare('DELETE FROM `confirm_defect_case` WHERE `case_ref` = :caseRef AND `defect_number` = :defectNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);
    }

    public function testWatchdogConfirmUpdateDefect()
    {
        // Setup
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `confirm_defect_case` (`case_ref`, `defect_number`) VALUES (:caseRef, :defectNumber)');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);

        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::UPDATE_DEFECT), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Process
        $confirm->processList();

        // Check if updated correctly
        $stmt = $connection->prepare('SELECT * FROM `confirm_defect_case` WHERE `case_ref` = :caseRef AND `defect_number` = :defectNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);

        // Clean up
        $stmt = $connection->prepare('DELETE FROM `confirm_defect_case` WHERE `case_ref` = :caseRef AND `defect_number` = :defectNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);
    }

    public function testWatchdogConfirmCloseDefect()
    {
        // Setup
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `confirm_defect_case` (`case_ref`, `defect_number`) VALUES (:caseRef, :defectNumber)');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);

        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::CLOSE_DEFECT), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Process
        $confirm->processList();

        // Check if removed correctly
        $stmt = $connection->prepare('SELECT * FROM `confirm_defect_case` WHERE `case_ref` = :caseRef AND `defect_number` = :defectNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result === false);
    }

    public function testWatchdogConfirmIgnoreDefect()
    {
        // Import
        $confirm = new Confirm(self::CUSTOMER, new MockConfirmReader(MockConfirmReader::CLOSE_DEFECT), new MockFillTask(self::TEST_CASE_REFERENCE));

        $confirm->importList();

        // Process
        $confirm->processList();

        // Check if not added correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT * FROM `confirm_defect_case` WHERE `case_ref` = :caseRef AND `defect_number` = :defectNumber');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE, 'defectNumber' => self::TEST_DEFECT_NUMBER]);

        $result = $stmt->fetch();

        $this->assertTrue($result === false);
    }
}
