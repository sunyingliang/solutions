<?php

require_once __DIR__ . '/../../config/common.php';
require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/MockFillTask.php';
require_once __DIR__ . '/MockUniformConfig.php';
require_once __DIR__ . '/MockUniformReader.php';

use FS\Database\Manager;
use FS\Integration\Reportit\Uniform;
use PHPUnit\Framework\TestCase;

class UniformIntegrationTest extends TestCase
{
    const TEST_CASE_REFERENCE = 'test-RIC2955044';
    const CUSTOMER            = 'integration';
    /** @var  Manager */
    private static $dbManager;

    public static function setUpBeforeClass()
    {
        self::$dbManager = new Manager(self::CUSTOMER);
    }

    public static function tearDownAfterClass()
    {
        self::$dbManager = null;
    }

    public function testRecordFillTaskShouldNotRun()
    {
        // clean slate
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare("DELETE FROM `uniform_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // add record to database
        $stmt = $connection->prepare("INSERT INTO `uniform_holding` (`case_reference`,`task_id`,`process_name`,`stage_name`,`uniform_reference`)
                                     VALUES (:caseReference,'1','test process','test stage','17/00017/SR')");
        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // process
        $uniform = new Uniform(self::CUSTOMER, new MockUniformReader(MockUniformReader::NOT_COMPLETED_CASE), new MockFillTask(), new MockUniformConfig());

        $uniform->run();

        // check result
        $stmt = $connection->prepare("SELECT `job_status`, `pending_filltask_flag` FROM `uniform_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        $result = $stmt->fetch();

        $this->assertEquals(0, $result['pending_filltask_flag']);
        $this->assertEquals('1_OPN', $result['job_status']);

        // clean up
        $stmt = $connection->prepare("DELETE FROM `uniform_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);
    }

    public function testRecordFillTaskShouldRun()
    {
        // clean slate
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare("DELETE FROM `uniform_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // add record to database
        $stmt = $connection->prepare("INSERT INTO `uniform_holding` (`case_reference`,`task_id`,`process_name`,`stage_name`,`uniform_reference`)
                                     VALUES (:caseReference,'1','test process','test stage','17/00017/SR')");
        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        // process
        $uniform = new Uniform(self::CUSTOMER, new MockUniformReader(MockUniformReader::CLOSE_CASE), new MockFillTask(), new MockUniformConfig());

        $uniform->run();

        // check result
        $stmt = $connection->prepare("SELECT `job_status`, `pending_filltask_flag` FROM `uniform_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);

        $result = $stmt->fetch();

        $this->assertEquals(1, $result['pending_filltask_flag']);
        $this->assertEquals('9_CLO', $result['job_status']);

        // clean up
        $stmt = $connection->prepare("DELETE FROM `uniform_holding` WHERE `case_reference` = :caseReference");

        $stmt->execute(['caseReference' => self::TEST_CASE_REFERENCE]);
    }
}
