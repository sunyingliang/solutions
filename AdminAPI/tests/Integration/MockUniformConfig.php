<?php

use FS\Integration\Configuration\ILastRunConfig;

class MockUniformConfig implements ILastRunConfig
{
    public function getLastRun(\PDO $connection)
    {
        $stmt = $connection->query("SELECT `value` FROM `uniform_config` WHERE `name` = 'changeSince' LIMIT 1");

        return $stmt->fetchColumn();
    }

    public function setLastRun(\PDO $connection, \DateTime $runTime): bool
    {
        return true;
    }
}