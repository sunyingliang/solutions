<?php

class CustomerControllerTest extends PHPUnit_Framework_TestCase
{
    public $token;

    public function setup()
    {
        $this->token = 'FD040101-AFB7-15C7-6DD2-5492047F54F8';
    }

    public function testGetCustomer()
    {
        $response = $this->runCurl('api/solutions/customer/list', [], 'token=fake');
        $this->assertEquals(401, $response['status']);

        $response = $this->runCurl('api/solutions/customer/list', [], 'token=' . $this->token);
        $this->assertEquals(200, $response['status']);

        $decode = json_decode($response['body']);
        $this->assertTrue(json_last_error() === JSON_ERROR_NONE);
        $this->assertTrue($decode->response->status == 'success');
    }

    public function testCreateUpdateDeleteCustomer()
    {
        $randName = 't' . rand(9000,9999);
        $notes    = 'test';

        // Test create (1/2)
        $response = $this->runCurl('api/solutions/customer/create', [], 'token=fake');
        $this->assertEquals(401, $response['status']);

        // Test create (2/2)
        $data = 'token=' . $this->token
              . '&name=' . $randName
              . '&notes=' . $notes;

        $response = $this->runCurl('api/solutions/customer/create', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);

        $id = $decode->response->data->id;

        // Test Get (1/4)
        $response = $this->runCurl('api/solutions/customer/get', [], 'token=fake');
        $this->assertEquals(401, $response['status']);

        // Test Get (2/4)
        $data = 'token=' . $this->token
              . '&id=fake';

        $response = $this->runCurl('api/solutions/customer/get', [], $data);
        $this->assertEquals(500, $response['status']);

        // Test Get (3/4)
        $data = 'token=' . $this->token
              . '&id=' . $id;

        $response = $this->runCurl('api/solutions/customer/get', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($notes, $decode->response->data->notes);

        $prefix         = 't_';
        $name           = $decode->response->data->name;
        $alias          = $decode->response->data->alias;
        $ftpPassword    = $decode->response->data->ftp_pass_raw;
        $httpPassword   = $decode->response->data->http_pass;
        $homeFolder     = $decode->response->data->home_folder;
        $dbHost         = $decode->response->data->db_host;
        $dbUser         = $decode->response->data->db_user;
        $dbPassword     = $decode->response->data->db_password;
        $dbDatabase     = $decode->response->data->db_database;
        $dbUserRead     = $decode->response->data->db_user_read;
        $dbPasswordRead = $decode->response->data->db_password_read;
        $enabled        = $decode->response->data->enabled;

        // Test update
        $data = 'token='           . $this->token
              . '&id='             . $id
              . '&name='           . $prefix . $name
              . '&alias='          . $prefix . $alias
              . '&ftpPassword='    . $prefix . $ftpPassword
              . '&httpPassword='   . $prefix . $httpPassword
              . '&homeFolder='     . $homeFolder
              . '&dbHost='         . $dbHost
              . '&dbUser='         . $dbUser
              . '&dbPassword='     . $dbPassword
              . '&dbDatabase='     . $dbDatabase
              . '&dbUserRead='     . $dbUserRead
              . '&dbPasswordRead=' . $dbPasswordRead
              . '&enabled=0';

        $response = $this->runCurl('api/solutions/customer/update', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);

        // Test Get (4/4)
        $data = 'token=' . $this->token
              . '&id=' . $id;

        $response = $this->runCurl('api/solutions/customer/get', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);

        $this->assertEquals($prefix . $name,         $decode->response->data->name);
        $this->assertEquals($prefix . $alias,        $decode->response->data->alias);
        $this->assertEquals($prefix . $ftpPassword,  $decode->response->data->ftp_pass_raw);
        $this->assertEquals($prefix . $httpPassword, $decode->response->data->http_pass);
        $this->assertEquals($homeFolder,             $decode->response->data->home_folder);
        $this->assertEquals($dbHost,                 $decode->response->data->db_host);
        $this->assertEquals($dbUser,                 $decode->response->data->db_user);
        $this->assertEquals($dbPassword,             $decode->response->data->db_password);
        $this->assertEquals($dbDatabase,             $decode->response->data->db_database);
        $this->assertEquals($dbUserRead,             $decode->response->data->db_user_read);
        $this->assertEquals($dbPasswordRead,         $decode->response->data->db_password_read);
        $this->assertEquals(0,                       $decode->response->data->enabled);

        // Test updateEnabled
        $data = 'token=' . $this->token
              . '&id=' . $id
              . '&enabled=1';

        $response = $this->runCurl('api/solutions/customer/update-enabled', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);

        // Test Get (4/4)
        $data = 'token=' . $this->token
              . '&id=' . $id;

        $response = $this->runCurl('api/solutions/customer/get', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals(1, $decode->response->data->enabled);

        // Test delete
        $data = 'token=' . $this->token
              . '&id=' . $id;

        $response = $this->runCurl('api/solutions/customer/delete', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);
    }

    public function runCurl(
        $url,
        $header     = [],
        $post       = null,
        $userPass   = null,
        $override   = false
    ) {
        if (!$override) {
            $url = 'http://localhost:8081/' . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
