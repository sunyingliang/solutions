<?php

require __DIR__ . '/../autoload.php';
require __DIR__ . '/../config/common.php';

ini_set('max_execution_time', CURL_TIMEOUT);

use FS\Common\Router;

$router = new Router();

// API - Holiday
$router->group('/api/holiday/', function () {
    $this->post('list-calendar', function () { return (new \FS\Controller\HolidayController())->listCalendars(); });
    $this->post('list-year', function () { return (new \FS\Controller\HolidayController())->listHolidayYearByCalendar(); });
    $this->post('list', function () { return (new \FS\Controller\HolidayController())->listHolidayByCalendarAndYear(); });
    $this->post('create', function () { return (new \FS\Controller\HolidayController())->addHoliday(); });
    $this->post('edit', function () { return (new \FS\Controller\HolidayController())->editHoliday(); });
    $this->post('delete', function () { return (new \FS\Controller\HolidayController())->deleteHoliday(); });
});

// API - FTP Logs
$router->group('/api/ftp/log/', function () {
    $this->post('customer', function () { return (new \FS\Controller\FTPController())->getCustomer(); });
});

// API - Get Master Customers' Details
$router->group('/api/solutions/customer/', function () {
    $this->post('get', function () { return (new \FS\Controller\CustomerController())->getCustomer(); });
    $this->post('list', function () { return (new \FS\Controller\CustomerController())->getCustomerList(); });
    $this->post('masters', function () { return (new \FS\Controller\CustomerController())->getMasterPool(); });
    $this->post('update', function () { return (new \FS\Controller\CustomerController())->update(); });
    $this->post('update-enabled', function () { return (new \FS\Controller\CustomerController())->updateEnabled(); } );
    $this->post('create', function () { return (new \FS\Controller\CustomerController())->create(); });
    $this->post('delete', function () { return (new \FS\Controller\CustomerController())->delete(); });
    $this->post('test', function () { return (new \FS\Controller\CustomerController())->testConnectionAndPermission(); });
    $this->post('sync', function () { return (new \FS\Controller\CustomerController())->syncDB(); });
    $this->post('list-feature', function () { return (new \FS\Controller\CustomerController())->getFeatureList(); });
});

$router->execute();
