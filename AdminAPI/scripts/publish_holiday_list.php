<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../autoload.php';
require __DIR__ . '/../config/common.php';

use FS\Common\Generic;
use FS\Database\WorkingDay;

Generic::message('Starting Solutions publish holiday script');

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
$customerConnection = null;
$attempts           = 0;
$maxAttempts        = 10;

while ($attempts < $maxAttempts) {
    try {
        $masterConnection = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        $masterConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $masterConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        break;
    } catch (Exception $e) {
        Generic::message('Notice: Could not connect to database "' . DB_NAME . '" on "' . DB_HOST . '" server, retrying [' . $attempts . '/' . $maxAttempts . ']');
        $masterConnection = null;
        sleep(30);
        $attempts++;
    }
}

if ($masterConnection === null) {
    Generic::message('Error: Cannot connect to database "' . DB_NAME . '" on "' . DB_HOST . '" server', true);
}

#region Publish holiday list to customer database according to calendar
$customerList = [];
$stmt         = $masterConnection->query('SELECT `name`, `db_host`, `db_user`, `db_password`, `db_database` FROM ' . DB_TABLE);

if ($stmt === false) {
    die('Error: PDO query failed');
}

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

    // Ignore if empty database details
    if (empty($row['db_host'])
        || empty($row['db_database'])
        || empty($row['db_user'])
        || empty($row['db_password'])
    ) {
        continue;
    }

    $customerList[] = [
        'name'       => $row['name'],
        'dbHost'     => $row['db_host'],
        'dbUser'     => $row['db_user'],
        'dbPassword' => $row['db_password'],
        'dbDatabase' => $row['db_database']
    ];
}

$stmt = $masterConnection->prepare('SELECT `id`, `holiday`, `calendar`, `notes` FROM `holiday`');
$stmt->execute();

$holidayArr      = $stmt->fetchAll(PDO::FETCH_ASSOC);
$holidayArrCount = count($holidayArr);

// Populate array of customer specific holidays
for ($i = 0; $i < count($customerList); $i++) {
    
    $customerList[$i]['holidayArr'] = $holidayArr;

    for ($j = 0; $j < $holidayArrCount; $j++) {
        $customerList[$i]['holidayArr'][$j]['action'] = 1;
    }
}

foreach ($customerList as $customer) {

    Generic::message('Processing ' . $customer['name']);

    if (trim($customer['dbHost']) == DB_HOST) {
        $customerConnection = $masterConnection;

        if ($customerConnection->exec('USE `' . $customer['dbDatabase'] . '`') === false) {
            Generic::message('Error: Failed to change to database {' . $customer['dbDatabase'] . '}');
        }
    } else {
        try {
            $customerConnection = new PDO('mysql:host=' . $customer['dbHost'] . ';dbname=' . $customer['dbDatabase'], $customer['dbUser'], $customer['dbPassword']);
        } catch (Exception $e) {
            continue;
        }
    }

    // blow up the holiday table and related function
    (new WorkingDay())->generateWorkingDay($customerConnection);


    if (!empty($customer['holidayArr'])) {
        try {
            $firmstepIdsDelete = [];
            $firmstepIdsResult = $customerConnection->query('SELECT `firmstep_id` FROM `holiday` WHERE `firmstep_id` IS NOT NULL')->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($firmstepIdsResult as $firmstepIdsRow) {
                $exists           = 0;
                $holidayArrLength = count($customer['holidayArr']);

                for ($i = 0; $i < $holidayArrLength; $i++) {
                    if ($firmstepIdsRow['firmstep_id'] == $customer['holidayArr'][$i]['id']) {
                        $exists                               = 1;
                        $customer['holidayArr'][$i]['action'] = 0;
                        break;
                    }
                }

                if ($exists == 0) {
                    $firmstepIdsDelete[] = $firmstepIdsRow['firmstep_id'];
                }
            }

            if (!empty($firmstepIdsDelete)) {
                $customerConnection->exec('DELETE FROM `holiday` WHERE `firmstep_id` IN (' . (implode(',', $firmstepIdsDelete)) . ')');
            }

            $insertStmt = $customerConnection->prepare('INSERT INTO `holiday` (`holiday`, `calendar`, `notes`, `firmstep_id`) VALUES (:holiday, :calendar, :notes, :id)');
            $updateStmt = $customerConnection->prepare('UPDATE `holiday` SET `holiday` = :holiday, `calendar` = :calendar, `notes` = :notes WHERE `firmstep_id` = :id');

            foreach ($customer['holidayArr'] as $holidayRow) {
                if ($holidayRow['action'] === 0) {
                    if ($updateStmt->execute(['holiday' => $holidayRow['holiday'], 'calendar' => $holidayRow['calendar'], 'notes' =>  $holidayRow['notes'], 'id' => $holidayRow['id']]) === false) {
                        Generic::message('Notice: stmt update error');
                    }
                } else if ($holidayRow['action'] === 1) {
                    if ($insertStmt->execute(['holiday' => $holidayRow['holiday'], 'calendar' => $holidayRow['calendar'], 'notes' =>  $holidayRow['notes'], 'id' => $holidayRow['id']]) === false) {
                        Generic::message('Notice: stmt insert error');
                    }
                }
            }
        } catch (\Exception $e) {
            Generic::message('Notice: Exception processing customer ' . $customer['name'] . ' error: ' . $e->getMessage());
        }
    } else {
        echo 'holidayArr is empty' . PHP_EOL;
    }
}
#endregion

Generic::message('Finished Solutions publish holiday script');
