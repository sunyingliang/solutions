<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\IO;

IO::message('Starting Solutions holiday function validation script');

try {
    // get master pdo and holiday_test data
    $config = [
        'dsn'      => 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=UTF8',
        'username' => DB_USER,
        'password' => DB_PASS
    ];

    $masterPdo = IO::getPDOConnection($config);

    $sql  = "SELECT `id`, `date`, `days`, `expected`, `calendar` FROM `holiday_test` ORDER BY `id`";
    $data = $masterPdo->query($sql)->fetchAll();

    //get all customers and call `CustomerHoliday` method for each customer
    $sql       = "SELECT `id`, `name`, `http_pass`, `db_host`, `db_database`, `db_user`, `db_password` FROM `customer` ORDER BY `id`";
    $customers = $masterPdo->query($sql)->fetchAll();

    $customerHolidayHelper = new \FS\Helper\CustomerHoliday();

    foreach ($customers as $customer) {
        
        IO::message('Starting processing customer {' . $customer['name'] . '}');


        try {
            $customerHolidayHelper->connect([
                'name'        => $customer['name'],
                'uriPassword' => $customer['http_pass'],
                'dsn'         => 'mysql:host=' . $customer['db_host'] . ';dbname=' . $customer['db_database'] . ';charset=UTF8',
                'username'    => $customer['db_user'],
                'password'    => $customer['db_password']
            ]);

            $customerHolidayHelper->test($data);
            
            IO::message('Finished processing customer {' . $customer['name'] . '}');
        } catch (\Exception $e) {
            IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, 'Failed to test holiday functions for Customer {' . $customer['name'] . '} with error:' . PHP_EOL . htmlentities($e->getMessage()), true);
            continue;
        }
    }
} catch (\Exception $e) {
    IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, 'Failed to check customer holiday functionality with error:' . PHP_EOL . htmlentities($e->getMessage()), true);
}

IO::message('Finished Solutions holiday function validation script');