<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not enabled, please check configuration in php.ini');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;
use FS\Database\LLPG;
use FS\Database\ReportIt;
use FS\Database\MBCollection;
use FS\Database\WorkingDay;
use FS\Database\DFG;
use FS\Database\Symology;
use FS\Database\Confirm;
use FS\Database\Mayrise;
use FS\Database\Uniform;
use FS\Database\Webhook;

Generic::message('Starting Solutions database migration script');

Generic::validateDBConfig();

$mysql = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);

if (!$mysql) {
    Generic::message('Unable to establish MySQL connection, check connection settings', true);
} else {
    $results       = [];
    $sql           = "SELECT * FROM " . DB_TABLE . " ORDER BY `name`";
    $stmt          = $mysql->query($sql);
    $clearExisting = false;

    // Clear existing (optional parameter to re-sync)
    if (isset($argv[1]) && !empty($argv[1])) {
        $clearExisting = true;
    }

    if ($stmt) {

        $llpg         = new LLPG();
        $reportIt     = new ReportIt();
        $mbcollection = new MBCollection();
        $workingday   = new WorkingDay();
        $dfg          = new DFG();
        $symology     = new Symology();
        $confirm      = new Confirm();
        $mayrise      = new Mayrise();
        $uniform      = new Uniform();
        $webhook      = new Webhook();

        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {

            // Ignore if empty database details
            if (empty($row['db_host']) ||
                empty($row['db_database']) ||
                empty($row['db_user']) ||
                empty($row['db_password'])
            ) {
                Generic::message('Skipped ' . $row['name'] . ', INFO: host, database, user and/or password missing');
                continue;
            }

            // Loop through each customer with DB access
            Generic::message('Processing ' . $row['name']);

            if (trim($row['db_host']) == DB_HOST) {
                $connection = $mysql;

                if ($connection->exec('USE `' . $row['db_database'] . '`') === false) {
                    Generic::message('Error: Failed to change to database {' . $row['db_database'] . '}');
                }
            } else {
                try {
                    $connection = new \PDO('mysql:host=' . $row['db_host'] . ';dbname=' . $row['db_database'], $row['db_user'], $row['db_password']);
                } catch (Exception $e) {
                    Generic::message('Unable to establish MySQL connection for ' . $row['name'] . ', Error: ' . $e->getMessage());
                    continue;
                }
            }

            // Current primary indicator for enabled customer
            if ($row['enabled'] === '1') {
                Generic::message('Processing ' . $row['name'] . ' [LLPG]');
                $llpg->generateLLPG($connection);

                Generic::message('Processing ' . $row['name'] . ' [ReportIt]');
                $reportIt->generateReportIt($connection);

                Generic::message('Processing ' . $row['name'] . ' [Missed Bins Collection]');
                $mbcollection->generateMBCollection($connection);

                Generic::message('Processing ' . $row['name'] . ' [Working Day]');
                $workingday->generateWorkingDay($connection, $clearExisting);

                Generic::message('Processing ' . $row['name'] . ' [DFG]');
                $dfg->generateDFG($connection);
            }


            // Change database back to master
            if ($mysql->exec('USE `' . DB_NAME . '`') === false) {
                Generic::message('Error: Failed to change to database {' . $row['db_database'] . '}');
            }

            // Fetch enabled features
            $featureSql = "SELECT `feature`.`name`
                           FROM `feature`
                                INNER JOIN `customer_feature` ON `customer_feature`.`feature_id` = `feature`.`id`
                                   AND `customer_feature`.`customer_id` = :id";

            $featureStmt = $mysql->prepare($featureSql);

            if ($featureStmt->execute(['id' => $row['id']])) {
                // Change database back to customer
                if (trim($row['db_host']) == DB_HOST) {
                    if ($connection->exec('USE `' . $row['db_database'] . '`') === false) {
                        Generic::message('Error: Failed to change to database {' . $row['db_database'] . '}');
                    }
                }
                // Loop through enabled features
                $featureList = $featureStmt->fetchAll(\PDO::FETCH_ASSOC);
                foreach ($featureList as $featureRow) {
                    switch ($featureRow['name']) {
                        case 'Symology':
                            Generic::message('Processing ' . $row['name'] . ' [Symology]');
                            $symology->generateSymology($connection);
                            break;
                        case 'Confirm':
                            Generic::message('Processing ' . $row['name'] . ' [Confirm]');
                            $confirm->generateConfirm($connection);
                            break;
                        case 'Mayrise':
                            Generic::message('Processing ' . $row['name'] . ' [Mayrise]');
                            $mayrise->generateMayrise($connection);
                            break;
                        case 'Uniform':
                            Generic::message('Processing ' . $row['name'] . ' [Uniform]');
                            $uniform->generateUniform($connection);
                            break;
                        case 'Webhook':
                            Generic::message('Processing ' . $row['name'] . ' [Webhook]');
                            $webhook->generateWebhook($connection, $featureList);
                            break;
                    }
                }
            } else {
                Generic::message($mysql->errorInfo()[2], true);
            }

        }
    } else {
        Generic::message($mysql->errorInfo()[2], true);
    }
}

Generic::message('Finshed Solutions database migration script', true);
