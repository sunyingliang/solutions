<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;
use FS\Entity\Customer;
use FS\Common\Exception\ValidationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;

Generic::message('Starting Solutions database schema comparason script');

if (!defined('DEFAULT_SCHEMA_CUSTOMER') || empty(DEFAULT_SCHEMA_CUSTOMER)) {
    Generic::message('DEFAULT_SCHEMA_CUSTOMER constant not set, exiting', true);
}

Generic::validateDBConfig();

$pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);

if (!$pdo) {
    Generic::message('Unable to establish MySQL connection, check connection settings', true);
}

Generic::message('Preparing comparason schema [' . DEFAULT_SCHEMA_CUSTOMER . ']');

// GET DEFAULT_SCHEMA_CUSTOMER (DEFAULT SCHEMA COMPARASON)
$sql = "SELECT `name`, `db_host`, `db_user`, `db_password`, `db_database`
        FROM `customer` 
        WHERE `name` = '" . DEFAULT_SCHEMA_CUSTOMER . "'";

$stmt = $pdo->query($sql);

if (!$stmt) {
    Generic::message($pdo->errorInfo()[2], true);
}

// Loop through each enabled master record, do db test for read & write user
$row = $stmt->fetch(\PDO::FETCH_ASSOC);

if (!$row) {
    Generic::message('Could not find ' . DEFAULT_SCHEMA_CUSTOMER . ' master record, exiting.', true);
}

$errorMessage = '';

$customer = new Customer();

$customer->setName($row['name']);
$customer->setDBHost($row['db_host']);
$customer->setDBUser($row['db_user']);
$customer->setDBPassword($row['db_password']);
$customer->setDBDatabase($row['db_database']);

$defaultCustomerSchema = dumpCustomerSchema($customer);

Generic::message('Comparason schema prepared');

$sql = "SELECT `name`, `db_host`, `db_user`, `db_password`, `db_database`
        FROM `customer` 
        WHERE `enabled` = 1 
            AND `name` <> '" . DEFAULT_SCHEMA_CUSTOMER . "'";

$stmt = $pdo->query($sql);

if (!$stmt) {
    Generic::message($pdo->errorInfo()[2], true);
}

$errorMessageCombined = '';

// Loop through each enabled master record, do db test for read & write user
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {

    $errorMessage = '';

    $customer = new Customer();

    $customer->setName($row['name']);
    $customer->setDBHost($row['db_host']);
    $customer->setDBUser($row['db_user']);
    $customer->setDBPassword($row['db_password']);
    $customer->setDBDatabase($row['db_database']);

    Generic::message('Testing customer ' . $row['name']);

    // If write connection issue, add error to message
    $message = $customer->getDBHost() . '/' . $customer->getDBDatabase() . '/' . $customer->getDBUser() . '/write:';

    try {
        $customer->validateDatabaseConectionAndPermissions(false, 'write');
    } catch (ValidationException $e) {
        $errorMessage .= PHP_EOL . $message . ' ' . $e->getMessage();
        Generic::message('Failure: ' . $message . ' ' . $e->getMessage());
    } catch (PDOExecutionException $e) {
        $errorMessage .= PHP_EOL . $message . ' ' . $e->getMessage();
        Generic::message('Failure: ' . $message . ' ' . $e->getMessage());
    }

    // Dump customer schema and compare against default schema
    if (empty($errorMessage)) {
        $currentCustomerSchema = dumpCustomerSchema($customer);
        $schemaResponse        = compareCustomerSchema($defaultCustomerSchema, $currentCustomerSchema, $customer->getName());

        if (!empty($schemaResponse)) {
            $errorMessage .= PHP_EOL . $schemaResponse;
        }
    }

    if (!empty($errorMessage)) {
        $errorMessageCombined .= $errorMessage;
    }
}

// If there's an error, send to slack
if (!empty($errorMessageCombined)) {
    slack($errorMessageCombined);
}

Generic::message('Finshed Solutions database [read|write] permissions script', true);

function slack($message)
{
    if (defined('SLACK_URL')) {
        IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, 'At least one customer has failed automated schema testing, detail to follow: ' . $message);
    }
}

function dumpCustomerSchema($customer)
{
    try {
        $pdoComparasonCustomer = new \PDO('mysql:host=' . $customer->getDBHost() . ';dbname=' . $customer->getDBDatabase(), $customer->getDBUser(), $customer->getDBPassword());
    } catch (\Exception $e) {
        Generic::message('Could not connect to ' . DEFAULT_SCHEMA_CUSTOMER . ' database to fetch default schema, exiting.', true);
    }

    $schema               = [];
    $schema['tables']     = [];
    $schema['procedures'] = [];
    $schema['functions']  = [];

    // Fetch all tables (includes views)
    $sql = "SHOW TABLES FROM `" . $customer->getDBDatabase() . "`";

    $stmt = $pdoComparasonCustomer->query($sql);

    if (!$stmt) {
        Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
    }

    // For each table
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        reset($row);
        $key                          = key($row);
        $schema['tables'][$row[$key]] = ['body' => 1, 'indexes' => []];

        $sqlTabDef  = "SHOW CREATE TABLE `" . $row[$key] . "`";
        $stmtTabDef = $pdoComparasonCustomer->query($sqlTabDef);

        if (!$stmtTabDef) {
            Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
        }

        if ($rowDefinition = $stmtTabDef->fetch(\PDO::FETCH_NUM)) {
            $schema['tables'][$row[$key]]['body'] = sanitiseDefinition($rowDefinition[1], 'table');

        }

        $sql = "SHOW INDEX FROM `" . $row[$key] . "` FROM `" . $customer->getDBDatabase() . "`";

        $stmtIndex = $pdoComparasonCustomer->query($sql);

        if (!$stmtIndex) {
            Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
        }

        while ($rowIndex = $stmtIndex->fetch(\PDO::FETCH_ASSOC)) {
            $schema['tables'][$row[$key]]['indexes'][] = $rowIndex['Key_name'];
        }
    }

    $sql = "SHOW PROCEDURE STATUS WHERE `Db` = '" . $customer->getDBDatabase() . "'";

    $stmt = $pdoComparasonCustomer->query($sql);

    if (!$stmt) {
        Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
    }

    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $schema['procedures'][$row['Name']] = 1;

        $sqlProcDef = "SHOW CREATE PROCEDURE `" . $row['Name'] . "`";

        if (!($stmtProcDef = $pdoComparasonCustomer->query($sqlProcDef))) {
            Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
        }

        if ($rowProcDef = $stmtProcDef->fetch(\PDO::FETCH_NUM)) {
            $schema['procedures'][$row['Name']] = sanitiseDefinition($rowProcDef[1], 'procedure');
        }
    }

    $sql = "SHOW FUNCTION STATUS WHERE `Db` = '" . $customer->getDBDatabase() . "'";

    $stmt = $pdoComparasonCustomer->query($sql);

    if (!$stmt) {
        Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
    }

    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $schema['functions'][] = 1;

        $sqlFuncDef = "SHOW CREATE FUNCTION `" . $row['Name'] . "`";

        if (!($stmtFuncDef = $pdoComparasonCustomer->query($sqlFuncDef))) {
            Generic::message($pdoComparasonCustomer->errorInfo()[2], true);
        }

        if ($rowFuncDef = $stmtFuncDef->fetch(\PDO::FETCH_NUM)) {
            $schema['functions'][$row['Name']] = sanitiseDefinition($rowFuncDef[1], 'function');
        }
    }

    return $schema;
}

function compareCustomerSchema($defaultCustomerSchema, $currentCustomerSchema, $name)
{
    $response = '';

    // Check tables
    foreach ($defaultCustomerSchema['tables'] as $tableName => $table) {
        if ($tableName == 'llpgcache_import') {
            continue;
        }

        if (!array_key_exists($tableName, $currentCustomerSchema['tables'])) {
            $response .= 'Customer [' . $name . '] Table [' . $tableName . '] not found in database' . PHP_EOL;
            continue;
        }

        // Check indexes
        if (count($defaultCustomerSchema['tables'][$tableName]['indexes']) > 0) {
            foreach ($defaultCustomerSchema['tables'][$tableName]['indexes'] as $key => $index) {
                if (!in_array($index, $currentCustomerSchema['tables'][$tableName]['indexes'])) {
                    $response .= 'Customer [' . $name . '] Table [' . $tableName . '] index [' . $index . '] not found in database' . PHP_EOL;
                }
            }
        }
    }

    // Check procedures
    foreach ($defaultCustomerSchema['procedures'] as $procedure => $body) {
        if (!array_key_exists($procedure, $currentCustomerSchema['procedures'])) {
            $response .= 'Customer [' . $name . '] Procedure [' . $procedure . '] not found in database' . PHP_EOL;
        }

        if ($body != $currentCustomerSchema['procedures'][$procedure]) {
            $response .= 'Customer [' . $name . '] Procedure [' . $procedure . '] is out of date' . PHP_EOL;
        }
    }

    // Check functions
    foreach ($defaultCustomerSchema['functions'] as $function => $body) {
        if (!array_key_exists($function, $currentCustomerSchema['functions'])) {
            $response .= 'Customer [' . $name . '] Function [' . $function . '] not found in database' . PHP_EOL;
        }

        if ($body != $currentCustomerSchema['functions'][$function]) {
            $response .= 'Customer [' . $name . '] Function [' . $function . '] is out of date' . PHP_EOL;
        }
    }

    return $response;
}

function sanitiseDefinition($definition, $type)
{
    $type = strtoupper($type);

    if (!in_array($type, ['PROCEDURE', 'FUNCTION', 'TABLE'])) {
        return $definition;
    }

    if ($type === 'TABLE') {
        $posLast = strrpos($definition, ')');
        return substr($definition, 0, $posLast);
    } else {
        return stristr($definition, $type);
    }
}

