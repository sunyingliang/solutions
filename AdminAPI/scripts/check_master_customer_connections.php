<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;
use FS\Entity\Customer;
use FS\Common\Exception\ValidationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;

Generic::message('Starting Solutions database [read|write] permissions script');

Generic::validateDBConfig();

$pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);

if (!$pdo) {
    Generic::message('Unable to establish MySQL connection, check connection settings', true);
}

$sql = "SELECT `id`, `name`, `db_host`, `db_user`, `db_password`, `db_user_read`, `db_password_read`, `db_database` FROM `customer` WHERE `enabled` = 1";

$stmt = $pdo->query($sql);

if (!$stmt) {
    Generic::message($pdo->errorInfo()[2], true);
}

$errorMessageCombined = '';

// Loop through each enabled master record, do db test for read & write user
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    
    $errorMessage = '';
    $error = '';
    $customer = new Customer();

    $customer->setName(           $row['name']              );
    $customer->setDBHost(         $row['db_host']           );
    $customer->setDBUser(         $row['db_user']           );
    $customer->setDBPassword(     $row['db_password']       );
    $customer->setDBUserRead(     $row['db_user_read']      );
    $customer->setDBPasswordRead( $row['db_password_read']  );
    $customer->setDBDatabase(     $row['db_database']       );

    Generic::message('Testing customer ' . $row['name']);

    // If write connection issue, add error to message
    $message = $customer->getDBHost() . '/' . $customer->getDBDatabase() . '/' . $customer->getDBUser() . '/write:';

    try {
        $customer->validateDatabaseConectionAndPermissions(false, 'write');
        Generic::message('Success: ' . $message);
    } catch (ValidationException $e) {
        $error .= $e->getMessage() . PHP_EOL;
        $errorMessage .= PHP_EOL . $message . ' ' . $e->getMessage();
        Generic::message('Failure: ' . $message . ' ' . $e->getMessage());
    } catch (PDOExecutionException $e) {
        $error .= $e->getMessage() . PHP_EOL;
        $errorMessage .= PHP_EOL . $message . ' ' . $e->getMessage();
        Generic::message('Failure: ' . $message . ' ' . $e->getMessage());
    }

    // If read connection issue, add error to message
    $message = $customer->getDBHost() . '/' . $customer->getDBDatabase() . '/' . $customer->getDBUserRead() . '/read:';

    try {
        $customer->validateDatabaseConectionAndPermissions(false, 'read');
        Generic::message('Success: ' . $message);
    } catch (ValidationException $e) {
        $error .= $e->getMessage() . PHP_EOL;
        $errorMessage .= PHP_EOL . $message . ' ' . $e->getMessage();
        Generic::message('Failure: ' . $message . ' ' . $e->getMessage());
    } catch (PDOExecutionException $e) {
        $error .= $e->getMessage() . PHP_EOL;
        $errorMessage .= PHP_EOL . $message . ' ' . $e->getMessage();
        Generic::message('Failure: ' . $message . ' ' . $e->getMessage());
    }

    if (!empty($errorMessage)) {
        $customer->updateLastTested($error, $row['id']);
        $errorMessageCombined .= $errorMessage;
    } else {
        $customer->updateLastTested('Success', $row['id']);
    }
}

// If there's an error, send to slack
if (!empty($errorMessageCombined)) {
    slack($errorMessageCombined);
}

Generic::message('Finshed Solutions database [read|write] permissions script', true);

function slack($message)
{
    if (defined('SLACK_URL')) {
        IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, 'At least one customer has failed automated connection testing, detail to follow: ' . $message);
    }
}

?>
