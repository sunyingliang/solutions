<?php

error_reporting(E_ALL | E_STRICT);

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../autoload.php';
require __DIR__ . '/../config/common.php';

use FS\Common\Generic;

Generic::validateDBConfig();

$mySQL   = null;
$counter = 0;

while ($counter < 10) {
    try {
        $mySQL = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;

        sleep(30);
        
        $counter++;
    }
}

if (!$mySQL) {
    Generic::message('Error: Unable to establish MySQL connection, check connection settings.', true);
}

$mySQL->exec("UPDATE `customer_feature` SET `started` = 0");
