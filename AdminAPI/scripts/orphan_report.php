<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;
use FS\Common\IO;

// Connect to database
Generic::message('Starting Solutions customer database check script');

try {
    $pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);

    if (!$pdo) {
        Generic::message('Unable to establish MySQL connection, check connection settings', true);
    }

    // Check if there are customers need to clean
    $customers = $pdo->query("SELECT `name`, `db_host`, `db_user`, `db_password`, `db_user_read`, `db_database` FROM `customer`")->fetchAll();

    $dbReadUser  = [];
    $dbWriteUser = [];
    $dbDatabase  = [];
    $orphanDB    = [];
    $orphanUser  = [];

    foreach ($customers as $customer) {
        array_push($dbReadUser, $customer['db_user_read']);
        array_push($dbWriteUser, $customer['db_user']);
        array_push($dbDatabase, $customer['db_database']);
    }

    $databases = $pdo->query("SELECT TABLE_SCHEMA AS `database` FROM information_schema.TABLES WHERE TABLE_NAME = 'llpgcache'")->fetchAll();

    // If customer database does not in solutions_master, push it into orphan database array
    foreach ($databases as $database) {
        if (!in_array($database['database'], $dbDatabase)) {
            array_push($orphanDB, $database['database']);

            $stmt = $pdo->prepare("SELECT Db, User FROM mysql.db WHERE Db = :db OR Db = :db_slash");
            $stmt->execute(['db' => $database['database'], 'db_slash' => str_replace('_', '\_', $database['database'])]);
            $users = $stmt->fetchAll();

            // Check if the database user used by other database
            foreach ($users as $user) {
                $stmt = $pdo->prepare("SELECT COUNT(1) AS `total` FROM mysql.db WHERE `User` =:dbUser");
                $stmt->execute(['dbUser' => $user['User']]);
                $count = $stmt->fetch();

                if ($count['total'] == 1) {
                    array_push($orphanUser, $user['User']);
                }
            }
        }
    }

    // Send slack message
    $slackMessage = '';

    if (empty($orphanUser) && empty($orphanDB)) {
        $slackMessage = "Solutions orphan check complete, no cleanup required";
    } else {
        $slackMessage = 'Solutions orphan check complete, the following need addressing on ' . DB_HOST . PHP_EOL . PHP_EOL;
        $slackMessage .= !empty($orphanDB) ? "Database: " . implode(", ", $orphanDB) . PHP_EOL : '';
        $slackMessage .= !empty($orphanUser) ? "Database User: " . implode(", ", $orphanUser) . PHP_EOL : '';
    }

    slack($slackMessage);
} catch (Exception $ex) {
    Generic::message($ex->getMessage());
}

function slack($message)
{
    if (defined('SLACK_URL')) {
        IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, $message);
    }
}