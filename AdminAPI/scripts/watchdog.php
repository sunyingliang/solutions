<?php

error_reporting(E_ALL | E_STRICT);

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Validation
if ($argc <= 1) {
    die('Error: feature info should be passed in as the parameter. e.g.: ' . PHP_EOL .
        '{php watchdog.php feature}' . PHP_EOL);
}

require __DIR__ . '/../autoload.php';
require __DIR__ . '/../config/common.php';

use FS\Common\Generic;
use FS\Common\IO;

$feature = $argv[1];

Generic::validateDBConfig();

$mySQL   = null;
$counter = 0;

while ($counter < 10) {
    try {
        $mySQL = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);

        $counter = 10;
    } catch (\Exception $e) {
        $mySQL = null;

        sleep(30);

        $counter++;
    }
}

if (!$mySQL) {
    Generic::message('Error: Unable to establish MySQL connection, check connection settings.', true);
}

$query = "SELECT `customer`.`name`
          FROM `customer`
              INNER JOIN `customer_feature` ON `customer_feature`.`customer_id` = `customer`.`id`
                  AND `customer_feature`.`enabled` = 1
                  AND `customer_feature`.`started` = 0
              INNER JOIN `feature` ON `feature`.`id` = `customer_feature`.`feature_id`
                  AND `feature`.`name` = :feature";

$stmt = $mySQL->prepare($query);

$stmt->execute(['feature' => $feature]);

if ($stmt) {
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $customer = $row['name'];
        $command  = 'php ' . __DIR__ . '/watchdog_executor.php ' . escapeshellarg($feature) . ' ' . escapeshellarg($customer);

        IO::backgroundExec($command);
    }
}
