<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Validation
if ($argc <= 2) {
    die('Error: feature and customer info should be passed in as the parameter. e.g.: ' . PHP_EOL .
        '{php watchdog_executor.php feature customer}' . PHP_EOL);
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

$feature  = $argv[1];
$customer = $argv[2];

FS\Common\NZLumberjack\Logger::configure(__DIR__ . '/../config/lumberjack.xml');

$namespace = 'FS\Integration\Reportit\\' . ucfirst(strtolower($feature));

if (!class_exists($namespace)) {
    die('Integration feature does not exist for ' . $feature);
}

$integration = new $namespace($customer);

if (!$integration instanceof FS\Integration\IntegrationBase) {
    die($namespace . ' does not extend IntegrationBase');
}

$integration->run();
