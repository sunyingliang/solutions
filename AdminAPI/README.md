#prerequisite
php version(>=7) is installed;
  
#configuration  
1. Deploy all these folders & files in web server;
2. Configure 'public' folder as web server's document root, or change 'public' folder name to web server's document root;
4. Rename all configuration files under /config folder to the name without '_example', e.g. change database_example.php to database.php.
5. Run 'php /your-path-to-database.php/database.php' to create required database schema;

#generate token
To generate token(s) for client(s), simply invoke `/path/to/script/token_generate.php` with one parameter, which can be `all` or the client name. 
If the specific `all` is passed in then tokens for all clients will be generated, while the specific client name means only token for that specific client will be generated.

#api (note: make sure configuration files under config folder is setup properly)
##holidays
1. List calendars
    * URI: /api/holiday/list-calendar
    * Method: POST
    * Parames: token=tokenParam
    * Response:
    ```
    {
      "response": {
        "status": "success",
        "data": [
          {
            "calendar": "Englane"
          },
          {
            "calendar": "Scotland"
          },
          {
            "calendar": "Wales"
          },
          {
            "calendar": "Northen Ireland"
          },
        }
      }
    }      
    ```
    
2. List holiday years
    * URI: /api/holiday/list-year
    * Method: POST
    * Parames: token=tokenParam
    ```
    {
    	"request":{
    	    "token": "token_example",
    		"calendar": "England"
    	}
    }
    ```
    * Response:
    ```
    {
      "response": {
        "status": "success",
        "data": [
          {
            "year": 2016
          }
        ]
      }
    }      
    ```
    
3. List holiday by country and year
    * URI: /api/holiday/list
    * Method: POST
    * Parames: token=tokenParam
    ```
    {
    	"request":{
    	    "token": "token_example",
    		"calendar": "England",
    		"year": 2016
    	}
    }
    ```
    * Response:
    ```
    {
      "response": {
        "status": "success",
        "data": [
          {
            "id": "101",
            "holiday": "2016-01-01",
            "notes": ""
          },
          {
            "id": "102",
            "holiday": "2016-01-04",
            "notes": ""
          }
        ]
      }
    }      
    ```    
    
4. Add a holiday
    * URI: /api/holiday/add
    * Method: POST
    * Parames: token=tokenParam
    ```
    {
    	"request":{
    	    "token": "token_example",
    		"calendar": "England",
    		"holiday": "2017-01-01",
    		"notes": ""
    	}
    }
    ```
    * Response:
    ```
    {
      "response": {
        "status": "success"
      }
    }      
    ```    
    
5. Delete a holiday
    * URI: /api/holiday/delete
    * Method: POST
    * Parames: token=tokenParam
    ```
    {
    	"request":{
    	    "token": "token_example",
    		"id": 11
    	}
    }
    ```
    * Response:
    ```
    {
      "response": {
        "status": "success"
      }
    }      
    ```    
6. Edit a holiday notes
    * URI: /api/holiday/edit
    * Method: POST
    * Params: token=tokenParam
    ```
    {
    	"request":{
    		"token": "FD040101-AFB7-15C7-6DD2-5492047F54F8",
    		"id": 116,
    		"notes": "test-notes"
    	}
    }
    ```
    * Response:
    ```
    {
      "response": {
        "status": "success"
      }
    }
    ```
    
##ftp
1. Get FTP logs
    * URI: /api/ftp/log/list
    * Method: POST
    * Params: token=tokenParam&offset=offsetParam&limit=limitParam[&customer=customerParam&fromData=fromDataParam&
    toData=toDataParam&response=responseParam&file_name=file_nameParam]
    * Response:
    ```
    {
      "TotalRowCount": "2",
      "Rows": [
        {
          "ftp_user": "test 3",
          "start_time": "2017-02-07 16:12:06",
          "end_time": null,
          "response": null,
          "file_name": ""
        },
        {
          "ftp_user": "test2",
          "start_time": "2017-02-06 03:45:24",
          "end_time": null,
          "response": null,
          "file_name": "testtesttesttesttesttest"
        }
      ]
    }
    ```
2. Get FTP Customer
    * URI: /api/ftp/log/customer
    * Method: POST
    * Params: token=tokenParam
    
##Customer
1. Get customers' information
    * URI: /api/solutions/customer/list
    * Method: POST
    * Params: token=tokenParam&offset=offsetParam&limit=limitParam[&name=nameParam&ftp_user=ftp_userParam&
                  http_user=http_userParam&enabled=enabledParam&calendar=calendarParam]
    * Response:
    ``` 
    {
      "response": {
        "status": "success",
        "data": {
          "TotalRowCount": "2",
          "Rows": [
            {
              "id": "9",
              "name": "demo",
              "ftp_user": "demo",
              "http_user": "",
              "enabled": "1",
              "db_host": "mysql.firmsteptest.com",
              "db_database": "solutions_demo",
              "enable_reportit": "1",
              "calendar": "New Zealand",
              "notes": "notes"
            },
            {
              "id": "8",
              "name": "test 3",
              "ftp_user": "test 3",
              "http_user": "customer-api",
              "enabled": "0",
              "db_host": "mysql.firmsteptest.com",
              "db_database": "solutions_kiwidave",
              "enable_reportit": "1",
              "calendar": "New Zealand",
              "notes": null
            }
          ]
        }
      }
    }
    ```
2. Get calendar
    * URI: /api/solutions/customer/calendar
    * Method: POST
    * Params: token=tokenParam
    * Response:
    ```
    {
      "response": {
        "status": "success",
        "data": [
          {
            "calendar": "New Zealand"
          },
          {
            "calendar": "South"
          }
        ]
      }
    }
    ```
3. Update customer
    * URI: /api/solutions/customer/update
    * Method: POST
    * Params: 
    ```
    {
    	"request":{
    		"token": "FD040101-AFB7-15C7-6DD2-5492047F54F8",
    		"id": 9,
    		"notes": "test-notes",
    		"enabled": 0,
    		"enable_reportit": 0
    	}
    }
    ```
    * Response:
    ```
    {
      "response": {
        "status": "success",
        "data": {
          "id": 9
        }
      }
    }
    ```

#integration features
The following json config needs to be set up for the feature  
If there are config item with the same key exist for both global and feature config, then the feature config item will override the global config item
##customer global
fillTask setup is required if you plan to push data into forms
lim setup is required if you plan to use the LIM in any of your features
```
{
  "fillTask": {
    "startThreadSite": "http://",
    "startThreadProcessID": "AF-Process-",
    "fillTaskSite": "http://",
    "ucrn": "",
    "appID": "APP-"
  },
  "lim": {
    "url": "",
    "key": "",
    "iv": ""
  }
}

```
##Symology
```
{
  "symology": {
    "location": "",
    "useLIM": true
  }
}
```
##Confirm
```
{
  "confirm": {
    "sftpURL": "",
    "sftpUsername": "",
    "sftpPassword": "",
    "sftpDirectory": "/Documents/Firmstep/",
    "sftpEnquiries": "Firmstep Enquiries",
    "sftpDefects": "Firmstep Defects"
  }
}
```
##Mayrise
```
{
  "symology": {
    "location": "",
    "useLIM": false
  }
}
```
##Uniform
```
{
  "symology": {
    "postUrl": "",
    "serviceLocation": "",
    "useLIM": true
  }
}
```