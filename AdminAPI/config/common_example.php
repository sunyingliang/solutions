<?php

// Define environment constants
define('ENVIRONMENT', '');

// Master database settings
define('DB_HOST', '');
define('DB_NAME', '');
define('DB_TABLE', '');
define('DB_USER', '');
define('DB_PASS', '');

// Default customer database settings
define('DEFAULT_CUSTOMER_DB_HOST', '');
define('DEFAULT_CUSTOMER_DB_USER', '');
define('DEFAULT_CUSTOMER_DB_PASS', '');

// Define slack constants
define('SLACK_URL', '');
define('SLACK_CHANNEL', '');
define('SLACK_USERNAME', '');

// For checkMasterCustomer Schema script
define('DEFAULT_SCHEMA_CUSTOMER', '');

// Define cached file
define('TOKEN_CACHED_FILE', '.txt');

// Define CURL TIMEOUT time
define('CURL_TIMEOUT', 'example');
define('CURL_TIMEOUT_LONG', 'example');
