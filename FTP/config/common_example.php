<?php

// Database Setup
define('DB_HOST', 'mysql:host=127.0.0.1;dbname=example_db_name;charset=utf8');
define('DB_USER', 'example_db_user');
define('DB_PASS', 'example_db_password');
define('DB_TABLE', 'example_db_table');

//Report setup
define('LLPG_DIR', 'llpg');
define('REPORTIT_DIR', 'reportit');
define('MBCOLLECTION_DIR', 'mbcollection');
define('PROCESSED_DIR', 'processed');
define('ERROR_DIR', 'error');

// Service Settings
define('INFRASTRUCTURE_SERVICE_LOCATION', 'example-endpoint');
define('INFRASTRUCTURE_SERVICE_TOKEN', 'example-token');

// Define slack constants
define('SLACK_URL', 'example');
define('SLACK_CHANNEL', 'example');
define('SLACK_USERNAME', 'example');
