<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;
use FS\Import\ImportLLPG;
use FS\Import\ImportAsset;
use FS\Import\ImportMBCollection;

// Validation
if ($argc <= 7) {
    die('Error: Migrate type and customer info should be passed in as the parameter. e.g.: ' . PHP_EOL .
        '{php your_script.php migrate_type customer_path customer_host customer_user customer_password customer_database}' . PHP_EOL);
}

try {
    $customerInfo = [
        'ftp_user'   => $argv[2],
        'path'       => $argv[3],
        'host'       => $argv[4],
        'user'       => $argv[5],
        'password'   => $argv[6],
        'database'   => $argv[7],
        'childPaths' => []
    ];

    for ($i = 8; $i < $argc; $i++) {
        $customerInfo['childPaths'][] = $argv[$i];
    }

    switch ($argv[1]) {
        case 'llpg':
            $import = new ImportLLPG(LLPG_DIR);
            break;
        case 'asset':
            $import = new ImportAsset(REPORTIT_DIR);
            break;
        case 'mbcollection':
            $import = new ImportMBCollection(MBCOLLECTION_DIR);
            break;
        default:
            Generic::message('Error: Invalid migrate type', true);
    }

    if (!isset($import)) {
        throw new \Exception('Import class cannot be created');
    }

    $import->executeImport($customerInfo);
} catch (\Exception $e) {
    Generic::message($e->getmessage(), true);
}
