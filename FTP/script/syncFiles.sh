#!/usr/bin/env bash

ftp_user=$1
llpg=$2
reportit=$3
mbcollection=$4
ftp_path=$5

prefix='/sftp/'${ftp_user}'/data/'
declare -a types=("$llpg" "$reportit" "$mbcollection")
for type in "$types"; do
    dir=${prefix}$type
    if [ -d "$dir" ]; then
        # iterate files and copy to destination dir
        dest=${ftp_path}'/'$type'/'
        files=($(ls "$dir"))
        for file in "${files[@]}"; do
            file=${dir}'/'$file
            if [ -f "$file" ]; then
                cp "$file" "$dest"
            fi
        done
        chown -Rf --reference="$dest" "$dest"

    fi
done

