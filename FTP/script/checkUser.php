<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;

Generic::validateDBConfig();

$mySQL = new PDO(DB_HOST, DB_USER, DB_PASS);

if (!$mySQL) {
    Generic::message('Unable to establish MySQL connection, check connection settings', true);
} else {
    $varsList = [];
    $stmt     = $mySQL->prepare("SELECT `name`, `home_folder`, `enabled` FROM " . DB_TABLE);

    $result = $stmt->execute();

    if ($result) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $varsList[] = [
                'ftp_user' => $row['name'],
                'path'     => $row['home_folder'],
                'enabled'  => $row['enabled']
            ];
        }

        $mySQL = null;

        $scriptName = __DIR__ . '/newUser.sh ';

        foreach ($varsList as $accountVars) {
            if ($accountVars['enabled'] == 1) {
                $llpgDir         = LLPG_DIR;
                $reportItDir     = REPORTIT_DIR;
                $mbcollectionDir = MBCOLLECTION_DIR;
            } else {
                $llpgDir         = 0;
                $reportItDir     = 0;
                $mbcollectionDir = 0;
            }

            $command = $scriptName . $accountVars['ftp_user'] . ' ' . PROCESSED_DIR . ' ' . ERROR_DIR . ' ' . $llpgDir . ' ' . $reportItDir . ' ' . $mbcollectionDir . ' ' . $accountVars['path'] . ' > /dev/null 2>&1';

            echo $command . PHP_EOL;

            exec($command);
        }
    } else {
        Generic::message($mySQL->errorInfo()[2], true);
    }
}
