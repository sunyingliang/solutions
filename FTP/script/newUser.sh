#!/usr/bin/env bash
ftp_user=$1
processed_dir=$2
error_dir=$3
llpg_dir=$4
reportit_dir=$5
mbcollection_dir=$6
home_path=''

if [ -z $ftp_user ] || [ -z $processed_dir ] || [ -z $error_dir ] || [ -z $llpg_dir ] || [ -z $reportit_dir ] || [ -z $mbcollection_dir ]; then
    break
fi

if [ -n $7 ]; then
    home_path=$7
	echo "local_root=$home_path" > /etc/vsftpd_user_conf/$ftp_user
else
    home_path=/home/vsftpd/$ftp_user
fi

if [ ! -d "$home_path" ]; then
    mkdir -p $home_path
fi
if [ $llpg_dir != "0" ]; then
    mkdir -p $home_path/$llpg_dir
    mkdir -p $home_path/$llpg_dir/$processed_dir
    mkdir -p $home_path/$llpg_dir/$error_dir
fi
if [ $reportit_dir != "0" ]; then
    mkdir -p $home_path/$reportit_dir
    mkdir -p $home_path/$reportit_dir/$processed_dir
    mkdir -p $home_path/$reportit_dir/$error_dir
fi
if [ $mbcollection_dir != "0" ]; then
    mkdir -p $home_path/$mbcollection_dir
    mkdir -p $home_path/$mbcollection_dir/$processed_dir
    mkdir -p $home_path/$mbcollection_dir/$error_dir
fi
chown -R vsftpd:nogroup $home_path
chmod a-w $home_path

# Prepare directory structure for sftp
prefix='/sftp/'${ftp_user}'/'
mkdir -p ${prefix}
dir_etc=${prefix}etc/
mkdir -p ${dir_etc}
cp /etc/passwd /etc/group ${dir_etc}
declare -a bins=('/bin/bash' '/bin/ls' '/bin/mkdir' '/bin/date')
dir_bin=${prefix}bin/
mkdir -p ${dir_bin}
for s in "${bins[@]}"; do
        cp $s ${dir_bin}
        # calculate and prepare dynamic dependencies
        ldds=$(ldd ${s})
        regex1='[^/]*/'
        regex2='[^(]*('
        p1=$(expr "$ldds" : $regex1)
        while [ $p1 -ne 0 ]; do
                ldds=${ldds:$p1}
                p2=$(expr "$ldds" : $regex2)
                ((p2=p2-2))
                lib=${ldds:0:$p2}
                ldds=${ldds:$p2}
                lib_path=${prefix}${lib%/*}/
                mkdir -p ${lib_path}
                cp /${lib} ${lib_path}
                p1=$(expr "$ldds" : $regex1)
        done
done
dir_dev=${prefix}dev/
mkdir -p ${dir_dev}
mknod -m 666 ${dir_dev}null c 1 3
mknod -m 666 ${dir_dev}zero c 1 5
mknod -m 666 ${dir_dev}tty  c 5 0
mknod -m 666 ${dir_dev}random c 1 8
data=${prefix}'data/'
mkdir ${data}
if [ $llpg_dir != "0" ]; then
    mkdir $data/$llpg_dir
    mkdir $data/$llpg_dir/$processed_dir
    mkdir $data/$llpg_dir/$error_dir
fi
if [ $reportit_dir != "0" ]; then
    mkdir $data/$reportit_dir
    mkdir $data/$reportit_dir/$processed_dir
    mkdir $data/$reportit_dir/$error_dir
fi
if [ $mbcollection_dir != "0" ]; then
    mkdir $data/$mbcollection_dir
    mkdir $data/$mbcollection_dir/$processed_dir
    mkdir $data/$mbcollection_dir/$error_dir
fi
chown -R ${ftp_user}:sftp $data
chmod -R 0700 $data