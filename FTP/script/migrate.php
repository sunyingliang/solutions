<?php

error_reporting(E_ALL | E_STRICT);

require __DIR__ . '/../classes/Common/Generic.php';
require __DIR__ . '/../config/common.php';

use FS\Common\Generic;

// Migration code
Generic::validateDBConfig();

$mySQL = new \PDO(DB_HOST, DB_USER, DB_PASS);

if (!$mySQL) {
    Generic::message('Error: Unable to establish MySQL connection, check connection settings.', true);
}

$varsList = [];
$query    = "SELECT c1.`name`, c1.`home_folder`, c1.`db_host`, c1.`db_user`, c1.`db_password`, c1.`db_database`, GROUP_CONCAT(c2.home_folder) AS `children_folder`
             FROM " . DB_TABLE . " c1 LEFT JOIN " . DB_TABLE . " c2
                 ON c1.id = c2.master_id
             WHERE c1.`enabled` = 1
             GROUP BY c1.id";

$stmt = $mySQL->query($query);

if ($stmt) {
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $varsList[] = [
            'ftp_user'    => $row['name'],
            'path'        => $row['home_folder'],
            'host'        => $row['db_host'],
            'user'        => $row['db_user'],
            'password'    => $row['db_password'],
            'database'    => $row['db_database'],
            'child_paths' => $row['children_folder']
        ];
    }

    $mySQL = null;

    $types = ['llpg', 'asset', 'mbcollection'];

    foreach ($varsList as $accountVars) {
        $childPathArr = explode(',', $accountVars['child_paths']);

        foreach ($types as $type) {
            $command = 'php ' . __DIR__ . '/migrate_executor.php ' . escapeshellarg($type) . ' ' . escapeshellarg($accountVars['ftp_user']) . ' ' . escapeshellarg($accountVars['path']) . ' ' . escapeshellarg($accountVars['host']) . ' ' . escapeshellarg($accountVars['user']) . ' ' . escapeshellarg($accountVars['password']) . ' ' . escapeshellarg($accountVars['database']);

            foreach ($childPathArr as $childPath) {
                $command .= ' ' . escapeshellarg($childPath);
            }

            backgroundExec($command);
        }
    }
} else {
    Generic::message($mySQL->errorInfo()[2], true);
}


// Utils
function backgroundExec($cmd)
{
    if (strtoupper(substr(php_uname(), 0, 3)) == 'WIN') {
        pclose(popen('start /B ' . $cmd, 'r'));
    } else {
        exec($cmd . ' > /dev/null &');
    }
}
