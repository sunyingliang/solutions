<?php

error_reporting(E_ALL | E_STRICT);

require __DIR__ . '/../classes/Common/Generic.php';
require __DIR__ . '/../config/common.php';

use FS\Common\Generic;
use FS\Import\ImportLLPG;

// Migration code
Generic::validateDBConfig();

$mySQL = new \PDO(DB_HOST, DB_USER, DB_PASS);

if (!$mySQL) {
    Generic::message('Error: Unable to establish MySQL connection, check connection settings.', true);
}

$customerList = [];
$query    = "SELECT `db_host`, `db_user`, `db_password`, `db_database` FROM " . DB_TABLE;

$stmt = $mySQL->query($query);

if ($stmt) {
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $customerList[] = [
            'host'        => $row['db_host'],
            'user'        => $row['db_user'],
            'password'    => $row['db_password'],
            'database'    => $row['db_database']
        ];
    }

    foreach ($customerList as $customer) {
        $connection = new \PDO('mysql:host=' . $customer['host'] . ';dbname=' . $customer['database'], $customer['user'], $customer['password']);

        (new ImportLLPG(LLPG_DIR))->dropLLPGCache($connection, $customer['host'], $customer['database']);
    }
} else {
    Generic::message($mySQL->errorInfo()[2], true);
}
