<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../autoload.php';

use FS\Common\Generic;

Generic::validateDBConfig();

$mySQL = new PDO(DB_HOST, DB_USER, DB_PASS);

if (!$mySQL) {
    Generic::message('Unable to establish MySQL connection, check connection settings', true);
} else {
    $varsList = [];
    $stmt     = $mySQL->prepare("SELECT `name`, `home_folder`, `enabled` FROM " . DB_TABLE);

    $result = $stmt->execute();

    if ($result) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $varsList[] = [
                'ftp_user' => $row['name'],
                'path'     => $row['home_folder']
            ];
        }

        $mySQL = null;

        $scriptName = __DIR__ . '/syncFiles.sh ';

        foreach ($varsList as $accountVars) {
            $llpgDir         = LLPG_DIR;
            $reportItDir     = REPORTIT_DIR;
            $mbcollectionDir = MBCOLLECTION_DIR;

            $command = $scriptName . $accountVars['ftp_user'] . ' ' . $llpgDir . ' ' . $reportItDir . ' ' . $mbcollectionDir . ' ' . $accountVars['path'] . ' > /dev/null 2>&1';

            echo $command . PHP_EOL;

            exec($command);
        }
    } else {
        Generic::message($mySQL->errorInfo()[2], true);
    }
}
