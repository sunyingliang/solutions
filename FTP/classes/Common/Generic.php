<?php

namespace FS\Common;

class Generic
{
    public static function validateDBConfig()
    {
        if (!defined('DB_HOST') || !defined('DB_USER') || !defined('DB_PASS') || !defined('DB_TABLE')) {
            self::message('Error: Configuration constant(s) not defined', true);
        }
    }

    public static function message($message, $die = false)
    {
        $message = PHP_EOL . date('H:i:s') . ' -> ' . $message;

        if ($die) {
            die($message . PHP_EOL);
        }

        echo($message . '...');

        sleep(1);
    }

    public static function logToFile($filePath, $message)
    {
        $fHandler = fopen($filePath, 'a+');

        $message = '[' . date('Y-m-d H:i:s') . '] ' . $message . PHP_EOL;

        fwrite($fHandler, $message);

        fclose($fHandler);
    }

    public static function logToDatabase($ftpUser, $fileName)
    {
        try {
            if (!defined('INFRASTRUCTURE_SERVICE_LOCATION') || !defined('INFRASTRUCTURE_SERVICE_TOKEN')) {
                throw new \Exception('Constants {INFRASTRUCTURE_SERVICE_LOCATION} and {INFRASTRUCTURE_SERVICE_TOKEN} are not defined correctly');
            }

            $data = [
                'token'    => INFRASTRUCTURE_SERVICE_TOKEN,
                'ftpUser'  => $ftpUser,
                'fileName' => $fileName
            ];

            $result = self::getCURLResult(INFRASTRUCTURE_SERVICE_LOCATION . 'log/ftp/create', 'POST', json_encode($data));

            if ($result['response']['status'] == 'success') {
                $retValue = $result['response']['data']['id'];
            } else {
                throw new \Exception($result['response']['message']);
            }
        } catch (\Exception $ex) {
            $retValue = $ex->getMessage();
        }

        return $retValue;
    }

    public static function updateDatabaseLog($id, $response)
    {
        try {
            if (!defined('INFRASTRUCTURE_SERVICE_LOCATION') || !defined('INFRASTRUCTURE_SERVICE_TOKEN')) {
                throw new \Exception('Constants {INFRASTRUCTURE_SERVICE_LOCATION} and {INFRASTRUCTURE_SERVICE_TOKEN} are not defined correctly');
            }

            $data = [
                'token'    => INFRASTRUCTURE_SERVICE_TOKEN,
                'id'       => $id,
                'response' => $response
            ];

            $result = self::getCURLResult(INFRASTRUCTURE_SERVICE_LOCATION . 'log/ftp/update', 'POST', json_encode($data));

            if ($result['response']['status'] == 'success') {
                $retValue = true;
            } else {
                throw new \Exception($result['response']['message']);
            }
        } catch (\Exception $ex) {
            $retValue = $ex->getMessage();
        }

        return $retValue;
    }

    public static function fileTypeCSV($fileName)
    {
        return strtolower(substr($fileName, -3)) == 'csv';
    }

    private static function getCURLResult($url, $type, $data)
    {
        $curl    = curl_init();
        $options = [
            CURLOPT_URL            => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true
        ];

        if (strtoupper($type) != 'GET') {
            $options[CURLOPT_POST]       = true;
            $options[CURLOPT_POSTFIELDS] = $data;
            $options[CURLOPT_HTTPHEADER] = ['Content-Type: application/json'];
        }

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);

        if ($response === false) {
            throw new \Exception('Failed to execute CURL to {' . $url . '}');
        }

        $result = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Failed to decode JSON response from CURL');
        }

        return $result;
    }
}
