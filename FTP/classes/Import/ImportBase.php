<?php

namespace FS\Import;

use FS\Common\Generic;

abstract class ImportBase
{
    protected $type;
    protected $typeDir;
    protected $currentFile;
    protected $errorMessage = [];
    protected $logId;
    protected $logDir;
    protected $logFile;
    protected $sftp         = '/sftp';

    public function __construct($type)
    {
        $this->type = $type;
    }

    abstract protected function startImport($file, \PDO $mysql, $vars);

    abstract protected function endTask(\PDO $mySQL, $vars);

    public function executeImport($vars)
    {
        if (isset($vars['path'])) {
            $this->typeDir = $vars['path'] . '/' . $this->type;
            $this->logDir  = $this->typeDir . '/' . ERROR_DIR;
            $uploadedFiles = scandir($this->typeDir);
            $updatedFlag   = false;

            if (isset($vars['host'], $vars['user'], $vars['password'], $vars['database'])) {
                $dsn = 'mysql:host=' . $vars['host'] . ';dbname=' . $vars['database'];

                try {
                    $mySQL = new \PDO($dsn, $vars['user'], $vars['password']);

                    if (!$mySQL) {
                        $message = 'Unable to establish MySQL connection, check connection settings. ';
                        Generic::logToFile($this->logDir . '/error.log', $message);
                        array_push($this->errorMessage, $message);
                    } else {
                        foreach ($uploadedFiles as $fileName) {
                            if (Generic::fileTypeCSV($fileName)) {
                                $this->currentFile = $fileName;
                                $filePath          = $this->typeDir . '/' . $this->currentFile;
                                $this->logFile     = $this->logDir . '/' . $this->currentFile . '.log';
                                $sftpFilePath      = $this->sftp . '/' . $vars['ftp_user'] . '/data/' . $this->type . '/' . $this->currentFile;
                                $sftpDir           = $this->sftp . '/' . $vars['ftp_user'] . '/data/' . $this->type;

                                if (!file_exists($filePath)) {
                                    $message = 'Error: ' . $this->currentFile . ' does not exist ';
                                    Generic::logToFile($this->logFile, $message);
                                    array_push($this->errorMessage, $message);
                                    break;
                                }

                                $file      = fopen($filePath, 'r');
                                $processed = false;

                                if (!$file) {
                                    $message = 'Unable to open CSV file {' . $this->currentFile . '}, check path and access. ';
                                    Generic::logToFile($this->logFile, $message);
                                    array_push($this->errorMessage, $message);
                                } else {
                                    $this->logId = Generic::logToDatabase($vars['ftp_user'], $this->currentFile);
                                    $processed   = $this->startImport($file, $mySQL, $vars);
                                    Generic::updateDatabaseLog($this->logId, json_encode($this->errorMessage));
                                    fclose($file);
                                }

                                $sftpSyncFlag = is_file($sftpFilePath);
                                if ($processed) {
                                    foreach ($vars['childPaths'] as $childPath) {
                                        if (!empty($childPath)) {
                                            $childFTPFilePath = $childPath . '/' . $this->type . '/' . $this->currentFile;
                                            if (!copy($filePath, $childFTPFilePath)) {
                                                $message = 'Error: Failed to copy file ' . $filePath . ' to child FTP directory ' . $childFTPFilePath . '. ';
                                                Generic::logToFile($this->logFile, $message);
                                                array_push($this->errorMessage, $message);
                                            }
                                            // sync files for sftp
                                            if ($sftpSyncFlag) {
                                                $sftpChildFilePath = $this->sftp . '/' . substr($childPath, strrpos($childPath, '/') + 1) . '/data/' . $this->type . '/' . $this->currentFile;
                                                if (!copy($sftpFilePath, $sftpChildFilePath)) {
                                                    $message = 'Error: Failed to copy file ' . $sftpFilePath . ' to child FTP directory ' . $sftpChildFilePath . '. ';
                                                    Generic::logToFile($this->logFile, $message);
                                                    array_push($this->errorMessage, $message);
                                                }
                                            }
                                        }
                                    }

                                    $updatedFlag = true;

                                    $moved     = rename($filePath, $this->typeDir . '/' . PROCESSED_DIR . '/' . $this->currentFile);
                                    $sftpMoved = rename($sftpFilePath, $sftpDir . '/' . PROCESSED_DIR . '/' . $this->currentFile);
                                } else {
                                    $moved     = rename($filePath, $this->typeDir . '/' . ERROR_DIR . '/' . $this->currentFile);
                                    $sftpMoved = rename($sftpFilePath, $sftpDir . '/' . ERROR_DIR . '/' . $this->currentFile);
                                }

                                if ($moved !== true) {
                                    $message = 'Error: Failed to move file ' . $filePath . '. ';
                                    Generic::logToFile($this->logFile, $message);
                                    array_push($this->errorMessage, $message);
                                }

                                if ($sftpMoved !== true) {
                                    $message = 'Error: Failed to move file ' . $sftpFilePath . '. ';
                                    Generic::logToFile($this->logFile, $message);
                                    array_push($this->errorMessage, $message);
                                }
                            }
                        }

                        if ($updatedFlag === true) {
                            $this->endTask($mySQL, $vars);
                        }

                        $mySQL = null;
                    }
                } catch (\Exception $e) {
                    Generic::message('Error: ' . $e->getMessage());
                }
            }
        }
    }
}
