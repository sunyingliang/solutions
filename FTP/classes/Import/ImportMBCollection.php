<?php

namespace FS\Import;

use FS\Common\Generic;

class ImportMBCollection extends ImportBase
{
    private $fileNames = [
        'COLLECTION TYPE'      => [
            'COLLECTION TYPE', 'DESCRIPTION', 'COLLECT PERMISSIONS GROUP ID'
        ],
        'COLLECTION POINTS'    => [
            'PROPERTY UPRN', 'COLLECTION POINT UPRN', 'COLLECTION POINT USRN'
        ],
        'PROPERTY ROUNDS'      => [
            'ROUND NAME', 'UPRN', 'USRN', 'COLLECT PERMISSIONS GROUP ID'
        ],
        'COLLECTION SCHEDULE'  => [
            'ROUND NAME', 'DATE', 'COLLECTION TYPE'
        ],
        'ASSISTED COLLECTIONS' => [
            'UPRN', 'EXPIRY DATE', 'EXPLANATION', 'NOTES'
        ]
    ];

    private $tables = [
        'COLLECTION TYPE'      => 'collection_type',
        'COLLECTION POINTS'    => 'collection_point',
        'PROPERTY ROUNDS'      => 'collection_round',
        'COLLECTION SCHEDULE'  => 'collection_schedule',
        'ASSISTED COLLECTIONS' => 'collection_assisted'
    ];

    private $columns = [
        'COLLECTION TYPE'      => [
            'COLLECTION TYPE'              => 'name',
            'DESCRIPTION'                  => 'description',
            'COLLECT PERMISSIONS GROUP ID' => 'permissions_group_id'
        ],
        'COLLECTION POINTS'    => [
            'PROPERTY UPRN'         => 'property_uprn',
            'COLLECTION POINT UPRN' => 'point_uprn',
            'COLLECTION POINT USRN' => 'point_usrn'

        ],
        'PROPERTY ROUNDS'      => [
            'ROUND NAME'                   => 'round_name',
            'UPRN'                         => 'uprn',
            'USRN'                         => 'usrn',
            'COLLECT PERMISSIONS GROUP ID' => 'permissions_group_id'
        ],
        'COLLECTION SCHEDULE'  => [
            'ROUND NAME'      => 'round_name',
            'DATE'            => 'collection_date',
            'COLLECTION TYPE' => 'collection_type'
        ],
        'ASSISTED COLLECTIONS' => [
            'UPRN'        => 'uprn',
            'EXPIRY DATE' => 'expiry_date',
            'EXPLANATION' => 'explanation',
            'NOTES'       => 'notes'
        ]
    ];

    protected $headers = [];
    protected $item    = [];

    public function executeImport($vars)
    {
        // check file name against refined file list
        if (isset($vars['path'])) {
            $this->typeDir = $vars['path'] . '/' . $this->type;
            $this->logDir  = $this->typeDir . '/' . ERROR_DIR;
            $uploadedFiles = scandir($this->typeDir);

            // Check uploaded files to refined list
            foreach ($uploadedFiles as $uploadedFile) {
                if (Generic::fileTypeCSV($uploadedFile)) {
                    $fileBaseName = substr($uploadedFile, 0, -4);

                    if (!in_array(strtoupper($fileBaseName), array_keys($this->fileNames))) {
                        $filePath      = $this->typeDir . '/' . $uploadedFile;
                        $this->logFile = $this->logDir . '/' . $uploadedFile . '.log';

                        if (rename($filePath, $this->logDir . '/' . $uploadedFile) !== true) {
                            $message = 'Error: Failed to move file ' . $filePath . '. ';
                            Generic::logToFile($this->logFile, $message);
                            array_push($this->errorMessage, $message);
                        }
                    }
                }
            }

            parent::executeImport($vars);
        }
    }

    protected function startImport($file, \PDO $mySQL, $vars)
    {
        $type          = trim(substr(strtoupper($this->currentFile), 0, -4));
        $this->headers = [];
        $this->item    = [];

        foreach ($this->fileNames[$type] as $header) {
            $this->headers[$header]                     = -1;
            $this->item[$this->columns[$type][$header]] = null;
        }

        $headers    = fgetcsv($file);
        $length     = count($headers);
        $headerKeys = array_keys($this->headers);

        for ($i = 0; $i < $length; $i++) {
            if (in_array(strtoupper($headers[$i]), $headerKeys)) {
                $this->headers[strtoupper($headers[$i])] = $i;
            }
        }

        foreach ($this->headers as $index) {
            if ($index == -1) {
                $message = 'The headers in the uploaded file are incorrect. ';
                Generic::logToFile($this->logFile, $message);
                array_push($this->errorMessage, $message);
                return false;
            }
        }

        // Truncate table
        $sql = "TRUNCATE TABLE `" . $this->tables[$type] . "`";

        if ($type !== 'PROPERTY ROUNDS') {
            if ($mySQL->exec($sql) === false) {
                $message = 'Error: #0117 - Please contact Firmstep Support and provide this code for investigation. ';
                Generic::logToFile($this->logFile, $message);
                array_push($this->errorMessage, $message);
                return false;
            }
        }

        // Import data from files into db
        $columns      = '';
        $placeHolders = '';

        foreach ($headerKeys as $header) {
            $columns .= '`' . $this->columns[$type][$header] . '`, ';
            $placeHolders .= ":" . $this->columns[$type][$header] . ", ";
        }

        $columns      = substr($columns, 0, -2);
        $placeHolders = substr($placeHolders, 0, -2);

        $sql = "INSERT INTO `" . $this->tables[$type] . "`(" . $columns . ") VALUES(" . $placeHolders . ")";

        if (($stmt = $mySQL->prepare($sql)) === false) {
            $message = 'Failed to prepare sql statement for importing the file {' . $this->currentFile . '}. ';
            Generic::logToFile($this->logFile, $message);
            array_push($this->errorMessage, $message);
            return false;
        }

        $count = 1;

        if ($type === 'PROPERTY ROUNDS') {
            $sqlDelete = "DELETE FROM `collection_round` WHERE `round_name` = :" . $this->columns[$type]['ROUND NAME'];

            if (($stmtDelete = $mySQL->prepare($sqlDelete)) === false) {
                $message = 'Failed to prepare sql statement for deleting records from the file {' . $this->currentFile . '}. ';
                Generic::logToFile($this->logFile, $message);
                array_push($this->errorMessage, $message);
                return false;
            }

            while (($record = fgetcsv($file)) !== false) {
                $count++;

                foreach ($this->headers as $header => $index) {
                    if (isset($record[$index])) {
                        $this->item[$this->columns[$type][$header]] = $record[$index];
                    }
                }

                if (strtoupper(trim($this->item[$this->columns[$type]['UPRN']])) == 'CLEAR') {
                    if ($stmtDelete->execute([$this->columns[$type]['ROUND NAME'] => trim($this->item[$this->columns[$type]['ROUND NAME']])]) === false) {
                        $message = 'line ' . $count . ': ' . $stmt->errorInfo()[2];
                        Generic::logToFile($this->logFile, $message);
                        array_push($this->errorMessage, $message);
                        continue;
                    }
                } else if ($stmt->execute($this->item) === false) {
                    $message = 'line ' . $count . ': ' . $stmt->errorInfo()[2];
                    Generic::logToFile($this->logFile, $message);
                    array_push($this->errorMessage, $message);
                    continue;
                }
            }
        } else {
            while (($record = fgetcsv($file)) !== false) {
                $count++;

                foreach ($this->headers as $header => $index) {
                    if (isset($record[$index])) {
                        $this->item[$this->columns[$type][$header]] = $record[$index];

                        if (($type == 'COLLECTION SCHEDULE' && $header == 'DATE') || ($type == 'ASSISTED COLLECTIONS' && $header == 'EXPIRY DATE')) {
                            $this->item[$this->columns[$type][$header]] = $this->formatDate($record[$index]);
                        }
                    }
                }

                if ($stmt->execute($this->item) === false) {
                    $message = 'line ' . $count . ': ' . $stmt->errorInfo()[2];
                    Generic::logToFile($this->logFile, $message);
                    array_push($this->errorMessage, $message);
                    continue;
                }
            }
        }

        return true;
    }

    protected function endTask(\PDO $mySQL, $vars)
    {
    }

    protected function formatDate($date)
    {
        // Regular expression for standard date format
        $regexpSD  = '$(\d{4})-(\d{2})-(\d{1,2}) (\d{2}):(\d{2}):(\d{2})$';
        $regexpSDB = '$(\d{4})-(\d{2})-(\d{1,2})$';

        // Regular expression for firmstep forms date format
        $regexpFS  = '$(\d{1,2})/(\d{2})/(\d{4}) (\d{2}):(\d{2}):(\d{2})$';
        $regexpFSB = '$(\d{1,2})/(\d{2})/(\d{4})$';

        if (preg_match($regexpSD, $date, $matches) === 1) {
            $ret = $matches[1] . '-' . $matches[2] . '-' . (strlen($matches[3]) === 1 ? '0' : '') . $matches[3] . ' ' . $matches[4] . ':' . $matches[5] . ':' . $matches[6];
        } else if (preg_match($regexpSDB, $date, $matches) === 1) {
            $ret = $matches[1] . '-' . $matches[2] . '-' . (strlen($matches[3]) === 1 ? '0' : '') . $matches[3] . ' 00:00:00';
        } else if (preg_match($regexpFS, $date, $matches) === 1) {
            $ret = $matches[3] . '-' . $matches[2] . '-' . (strlen($matches[1]) === 1 ? '0' : '') . $matches[1] . ' ' . $matches[4] . ':' . $matches[5] . ':' . $matches[6];
        } else if (preg_match($regexpFSB, $date, $matches) === 1) {
            $ret = $matches[3] . '-' . $matches[2] . '-' . (strlen($matches[1]) === 1 ? '0' : '') . $matches[1] . ' 00:00:00';
        } else {
            $ret = '0000-00-00 00:00:00';
        }

        return $ret;
    }
}
