<?php

namespace FS\Import;

use FS\Common\CURL;
use FS\Common\Generic;
use FS\External\ConversionsLatLong;

class ImportLLPG extends ImportBase
{
    protected function startImport($file, \PDO $mySQL, $vars)
    {
        $headerRecord = fgetcsv($file);

        if ((int)$this->stripNonAscii($headerRecord[0]) === 10 && substr($headerRecord[7], 0, 3) === '7.3') {
            if ($headerRecord[8] === 'F') {
                $this->rebuildLLPGTable($mySQL);
            }

            $count = 1;

            while (($record = fgetcsv($file)) !== false) {
                $count++;

                $result = $this->migrateLLPGRecord($record, $mySQL);

                if ($result === false) {
                    return false;
                }

                if ($result !== true) {
                    $message = "line {$count}: $result";
                    Generic::logToFile($this->logFile, $message);
                    array_push($this->errorMessage, $message);
                    continue;
                }
            }
        }

        return true;
    }

    protected function endTask(\PDO $mySQL, $vars)
    {
        $this->rebuildLLPGCache($mySQL, $vars);
    }

    private function migrateLLPGRecord($record, \PDO $mySQL)
    {
        switch ($record[0]) {
            case 10:
                return true;
            case 11:
                if (count($record) === 20) {
                    if ($record[1] == 'I') {
                        $stmt = $mySQL->prepare('INSERT INTO STREET_RECORD (PRO_ORDER, USRN, RECORD_TYPE, SWA_ORG_REF_NAMING, STATE, STATE_DATE, STREET_SURFACE, STREET_CLASSIFICATION, VERSION, RECORD_ENTRY_DATE, LAST_UPDATE_DATE, STREET_START_DATE, STREET_END_DATE, STREET_START_X, STREET_START_Y, STREET_END_X, STREET_END_Y, STREET_TOLERANCE) '
                            . 'VALUES(:2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19)');

                        for ($i = 2; $i < 20; $i++) {
                            $stmt->bindValue(':' . $i, $record[$i]);
                        }

                        $result = $stmt->execute();
                    } else {
                        if ($record[1] == 'U') {
                            $stmt = $mySQL->prepare('UPDATE STREET_RECORD SET PRO_ORDER=:2, USRN=:3, RECORD_TYPE=:4, SWA_ORG_REF_NAMING=:5, STATE=:6, STATE_DATE=:7, STREET_SURFACE=:8, STREET_CLASSIFICATION=:9, VERSION=:10, RECORD_ENTRY_DATE=:11, LAST_UPDATE_DATE=:12, STREET_START_DATE=:13, STREET_END_DATE=:14, STREET_START_X=:15, STREET_START_Y=:16, STREET_END_X=:17, STREET_END_Y=:18, STREET_TOLERANCE=:19 WHERE USRN=:3');

                            for ($i = 2; $i < 20; $i++) {
                                $stmt->bindValue(':' . $i, $record[$i]);
                            }

                            $result = $stmt->execute();
                        } else {
                            if ($record[1] == 'D') {
                                $stmt = $mySQL->prepare('DELETE FROM STREET_RECORD WHERE USRN=:3');

                                $stmt->bindValue(':3', $record[3]);

                                $result = $stmt->execute();
                            } else {
                                $result = false;
                            }
                        }
                    }

                    if ($result === false) {
                        if (isset($stmt)) {
                            return 'Error: ' . $stmt->errorInfo()[2];
                        } else {
                            return 'Error: Unknown CHANGE_TYPE: ' . $record[1];
                        }
                    }

                    return true;
                } else {
                    return 'Invalid number of fields for STREET_RECORD - counted ' . count($record) . ' records';
                }
            case 12:
                return true;
            case 13:
                return true;
            case 15:
                if (count($record) === 9) {
                    if ($record[1] == 'I') {
                        $stmt = $mySQL->prepare('INSERT INTO STREET_DESCRIPTOR (PRO_ORDER, USRN, STREET_DESCRIPTOR, LOCALITY_NAME, TOWN_NAME, ADMINISTRATIVE_AREA, LANGUAGE) VALUES(:2, :3, :4, :5, :6, :7, :8)');

                        for ($i = 2; $i < 9; $i++) {
                            $stmt->bindValue(':' . $i, $record[$i]);
                        }

                        $result = $stmt->execute();
                    } else {
                        if ($record[1] == 'U') {
                            $stmt = $mySQL->prepare('UPDATE STREET_DESCRIPTOR SET PRO_ORDER=:2, USRN=:3, STREET_DESCRIPTOR=:4, LOCALITY_NAME=:5, TOWN_NAME=:6, ADMINISTRATIVE_AREA=:7, LANGUAGE=:8 WHERE USRN=:3');

                            for ($i = 2; $i < 9; $i++) {
                                $stmt->bindValue(':' . $i, $record[$i]);
                            }

                            $result = $stmt->execute();
                        } else {
                            if ($record[1] == 'D') {
                                $stmt = $mySQL->prepare('DELETE FROM STREET_DESCRIPTOR WHERE USRN=:3');

                                $stmt->bindValue(':3', $record[3]);

                                $result = $stmt->execute();
                            } else {
                                return false;
                            }
                        }
                    }
                    if ($result === false) {
                        if (isset($stmt)) {
                            return 'Error: ' . $stmt->errorInfo()[2];
                        } else {
                            return 'Error: Unknown CHANGE_TYPE: ' . $record[1];
                        }
                    }

                    return true;
                } else {
                    return 'Error: Invalid number of fields for STREET_DESCRIPTOR' . var_export($record, 1);
                }
            case 21:
                if (count($record) === 23) {

                    $l = new ConversionsLatLong();

                    $longLat = $l->osgb36_to_wgs84($record[9], $record[10]);

                    if ($record[1] == 'I') {
                        $stmt = $mySQL->prepare('INSERT INTO BASIC_LAND_AND_PROPERTY_UNIT (PRO_ORDER, UPRN, LOGICAL_STATUS, BLPU_STATE, BLPU_STATE_DATE, BLPU_CLASS, PARENT_UPRN, X_COORDINATE, Y_COORDINATE, RPC, LOCAL_CUSTODIAN_CODE, START_DATE, END_DATE, LAST_UPDATE_DATE, ENTRY_DATE, ORGANISATION, WARD_CODE, PARISH_CODE, CUSTODIAN_ONE, CUSTODIAN_TWO, CAN_KEY, LNG, LAT) VALUES(:2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :longLat1, :longLat0)');

                        for ($i = 2; $i < 23; $i++) {
                            $stmt->bindValue(':' . $i, $record[$i]);
                        }

                        $stmt->bindValue(':longLat1', $longLat[1]);
                        $stmt->bindValue(':longLat0', $longLat[0]);

                        $result = $stmt->execute();
                    } else {
                        if ($record[1] == 'U') {
                            $stmt = $mySQL->prepare('UPDATE BASIC_LAND_AND_PROPERTY_UNIT SET PRO_ORDER=:2, UPRN=:3, LOGICAL_STATUS=:4, BLPU_STATE=:5, BLPU_STATE_DATE=:6, BLPU_CLASS=:7, PARENT_UPRN=:8, X_COORDINATE=:9, Y_COORDINATE=:10, RPC=:11, LOCAL_CUSTODIAN_CODE=:12, START_DATE=:13, END_DATE=:14, LAST_UPDATE_DATE=:15, ENTRY_DATE=:16, ORGANISATION=:17, WARD_CODE=:18, PARISH_CODE=:19, CUSTODIAN_ONE=:20, CUSTODIAN_TWO=:21, CAN_KEY=:22, LNG=:longLat1, LAT=:longLat0 WHERE UPRN =:3');

                            for ($i = 2; $i < 23; $i++) {
                                $stmt->bindValue(':' . $i, $record[$i]);
                            }

                            $stmt->bindValue(':longLat1', $longLat[1]);
                            $stmt->bindValue(':longLat0', $longLat[0]);

                            $result = $stmt->execute();
                        } else {
                            if ($record[1] == 'D') {
                                $stmt = $mySQL->prepare('DELETE FROM BASIC_LAND_AND_PROPERTY_UNIT WHERE UPRN = :3');

                                $stmt->bindValue(':3', $record[3]);

                                $result = $stmt->execute();
                            } else {
                                return false;
                            }
                        }
                    }

                    if ($result === false) {
                        if (isset($stmt)) {
                            return 'Error: ' . $stmt->errorInfo()[2];
                        } else {
                            return 'Error: Unknown CHANGE_TYPE: ' . $record[1];
                        }
                    }

                    return true;
                } else {
                    return 'Error: Invalid number of fields for BASIC_LAND_AND_PROPERTY_UNIT';
                }
            case 23:
                if (count($record) === 11) {
                    if ($record[1] == 'I') {
                        $stmt = $mySQL->prepare('INSERT INTO APPLICATION_CROSS_REFERENCE( PRO_ORDER, UPRN, XREF_KEY, START_DATE, LAST_UPDATE_DATE, ENTRY_DATE, END_DATE, CROSS_REFERENCE, SOURCE ) VALUES(:2, :3, :4, :5, :6, :7, :8, :9, :10)');

                        for ($i = 2; $i < 11; $i++) {
                            $stmt->bindValue(':' . $i, $record[$i]);
                        }

                        $result = $stmt->execute();
                    } else {
                        if ($record[1] == 'U') {
                            $stmt = $mySQL->prepare('UPDATE APPLICATION_CROSS_REFERENCE SET PRO_ORDER =:2, UPRN =:3, XREF_KEY =:4, START_DATE =:5, LAST_UPDATE_DATE =:6, ENTRY_DATE =:7, END_DATE =:8, CROSS_REFERENCE =:9, SOURCE =:10 WHERE UPRN =:3');

                            for ($i = 2; $i < 11; $i++) {
                                $stmt->bindValue(':' . $i, $record[$i]);
                            }

                            $result = $stmt->execute();
                        } else {
                            if ($record[1] == 'D') {
                                $stmt = $mySQL->prepare('DELETE FROM APPLICATION_CROSS_REFERENCE WHERE UPRN =:3');

                                $stmt->bindValue(':3', $record[3]);
                                $result = $stmt->execute();
                            } else {
                                return false;
                            }
                        }
                    }

                    if ($result === false) {
                        if (isset($stmt)) {
                            return 'Error: ' . $stmt->errorInfo()[2];
                        } else {
                            return 'Error: Unknown CHANGE_TYPE: ' . $record[1];
                        }
                    }

                    return true;
                } else {
                    return 'Error: Invalid number of fields for APPLICATION_CROSS_REFERENCE';
                }
            case 24:
                if (count($record) >= 30) {
                    if ($record[1] == 'I') {
                        $stmt = $mySQL->prepare('INSERT INTO LAND_AND_PROPERTY_IDENTIFIER( PRO_ORDER, UPRN, LPI_KEY, LANGUAGE, LOGICAL_STATUS, START_DATE, END_DATE, ENTRY_DATE, LAST_UPDATE_DATE, SAO_START_NUMBER, SAO_START_SUFFIX, SAO_END_NUMBER, SAO_END_SUFFIX, SAO_TEXT, PAO_START_NUMBER, PAO_START_SUFFIX, PAO_END_NUMBER, PAO_END_SUFFIX, PAO_TEXT, USRN, LEVEL, POSTAL_ADDRESS, POSTCODE, POST_TOWN, OFFICIAL_FLAG, CUSTODIAN_ONE, CUSTODIAN_TWO, CAN_KEY ) VALUES(:2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29)');

                        for ($i = 2; $i < 30; $i++) {
                            $stmt->bindValue(':' . $i, $record[$i]);
                        }

                        $result = $stmt->execute();
                    } else {
                        if ($record[1] == 'U') {
                            $stmt = $mySQL->prepare('UPDATE LAND_AND_PROPERTY_IDENTIFIER SET PRO_ORDER =:2, UPRN =:3, LPI_KEY =:4, LANGUAGE =:5, LOGICAL_STATUS =:6, START_DATE =:7, END_DATE =:8, ENTRY_DATE =:9, LAST_UPDATE_DATE =:10, SAO_START_NUMBER =:11, SAO_START_SUFFIX =:12, SAO_END_NUMBER =:13, SAO_END_SUFFIX =:14, SAO_TEXT =:15, PAO_START_NUMBER =:16, PAO_START_SUFFIX =:17, PAO_END_NUMBER =:18, PAO_END_SUFFIX =:19, PAO_TEXT =:20, USRN =:21, LEVEL =:22, POSTAL_ADDRESS =:23, POSTCODE =:24, POST_TOWN =:25, OFFICIAL_FLAG =:26, CUSTODIAN_ONE =:27, CUSTODIAN_TWO =:28, CAN_KEY =:29 WHERE LPI_KEY =:4');

                            for ($i = 2; $i < 30; $i++) {
                                $stmt->bindValue(':' . $i, $record[$i]);
                            }

                            $result = $stmt->execute();
                        } else {
                            if ($record[1] == 'D') {
                                $stmt = $mySQL->prepare('DELETE FROM LAND_AND_PROPERTY_IDENTIFIER WHERE LPI_KEY =:4');

                                $stmt->bindValue(':4', $record[4]);

                                $result = $stmt->execute();
                            } else {
                                return false;
                            }
                        }
                    }
                    if ($result === false) {
                        if (isset($stmt)) {
                            return 'Error: ' . $stmt->errorInfo()[2];
                        } else {
                            return 'unknown CHANGE_TYPE: ' . $record[1];
                        }
                    }

                    return true;
                } else {
                    return 'Error: Invalid number of fields for LAND_AND_PROPERTY_IDENTIFIER';
                }
            case 29:
                if (count($record) === 17) {
                    $stmt = $mySQL->prepare('INSERT INTO LLPG_METADATA( GAZ_NAME, GAZ_SCOPE, TER_OF_USE, LINKED_DATA, GAZ_OWNER, NGAZ_FREQ, CUSTODIAN_NAME, CUSTODIAN_UPRN, LOCAL_CUSTODIAN_CODE, CO_ORD_SYSTEM, CO_ORD_UNIT, META_DATE, CLASS_SCHEME, GAZ_DATE, LANGUAGE, CHARACTER_SET ) VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16)');

                    for ($i = 1; $i < 17; $i++) {
                        $stmt->bindValue(':' . $i, $record[$i]);
                    }

                    $result = $stmt->execute();

                    if ($result === false) {
                        if (isset($stmt)) {
                            return 'Error: ' . $stmt->errorInfo()[2];
                        }
                    }

                    return true;
                } else {
                    return 'Error: Invalid number of fields for LLPG_METADATA';
                }
            case 98:
                return true;
            case 99:
                return true;
            default:
                return true;
        }
    }

    private function rebuildLLPGTable(\PDO $mySQL)
    {
        $mySQL->exec('DROP TABLE STREET_RECORD');
        $mySQL->exec('DROP TABLE STREET_DESCRIPTOR');
        $mySQL->exec('DROP TABLE BASIC_LAND_AND_PROPERTY_UNIT');
        $mySQL->exec('DROP TABLE APPLICATION_CROSS_REFERENCE');
        $mySQL->exec('DROP TABLE LAND_AND_PROPERTY_IDENTIFIER');
        $mySQL->exec('DROP TABLE LLPG_METADATA');
        $mySQL->exec('CREATE TABLE STREET_RECORD (PRO_ORDER VARCHAR(16), USRN VARCHAR(8), RECORD_TYPE VARCHAR(1), SWA_ORG_REF_NAMING VARCHAR(4), STATE VARCHAR(1), STATE_DATE VARCHAR(16), STREET_SURFACE VARCHAR(1), STREET_CLASSIFICATION VARCHAR(1), VERSION VARCHAR(3), RECORD_ENTRY_DATE VARCHAR(16), LAST_UPDATE_DATE VARCHAR(16), STREET_START_DATE VARCHAR(16), STREET_END_DATE VARCHAR(16), STREET_START_X VARCHAR(16), STREET_START_Y VARCHAR(16), STREET_END_X VARCHAR(16), STREET_END_Y VARCHAR(16), STREET_TOLERANCE VARCHAR(3))');
        $mySQL->exec('CREATE TABLE STREET_DESCRIPTOR (PRO_ORDER VARCHAR(16), USRN VARCHAR(8), STREET_DESCRIPTOR VARCHAR(100), LOCALITY_NAME VARCHAR(35), TOWN_NAME VARCHAR(30), ADMINISTRATIVE_AREA VARCHAR(30), LANGUAGE VARCHAR(3), INDEX `IDX_USRN` (`USRN`))');
        $mySQL->exec('CREATE TABLE BASIC_LAND_AND_PROPERTY_UNIT (PRO_ORDER VARCHAR(16), UPRN VARCHAR(12), LOGICAL_STATUS VARCHAR(1), BLPU_STATE VARCHAR(1), BLPU_STATE_DATE VARCHAR(16), BLPU_CLASS VARCHAR(4), PARENT_UPRN VARCHAR(12), X_COORDINATE VARCHAR(16), Y_COORDINATE VARCHAR(16), RPC VARCHAR(1), LOCAL_CUSTODIAN_CODE VARCHAR(4), START_DATE VARCHAR(16), END_DATE VARCHAR(16), LAST_UPDATE_DATE VARCHAR(16), ENTRY_DATE VARCHAR(16), ORGANISATION VARCHAR(100), WARD_CODE VARCHAR(10), PARISH_CODE VARCHAR(10), CUSTODIAN_ONE VARCHAR(2), CUSTODIAN_TWO VARCHAR(2), CAN_KEY VARCHAR(14), LNG varchar(20), LAT varchar(20), KEY `index2` (`UPRN`) USING BTREE)');
        $mySQL->exec('CREATE TABLE APPLICATION_CROSS_REFERENCE (PRO_ORDER VARCHAR(16), UPRN VARCHAR(12), XREF_KEY VARCHAR(14), START_DATE VARCHAR(16), LAST_UPDATE_DATE VARCHAR(16), ENTRY_DATE VARCHAR(16), END_DATE VARCHAR(16), CROSS_REFERENCE VARCHAR(50), SOURCE VARCHAR(6))');
        $mySQL->exec('CREATE TABLE LAND_AND_PROPERTY_IDENTIFIER (PRO_ORDER VARCHAR(16), UPRN VARCHAR(12), LPI_KEY VARCHAR(14), LANGUAGE VARCHAR(3), LOGICAL_STATUS VARCHAR(1), START_DATE VARCHAR(16), END_DATE VARCHAR(16), ENTRY_DATE VARCHAR(16), LAST_UPDATE_DATE VARCHAR(16), SAO_START_NUMBER VARCHAR(4), SAO_START_SUFFIX VARCHAR(2), SAO_END_NUMBER VARCHAR(4), SAO_END_SUFFIX VARCHAR(2), SAO_TEXT VARCHAR(90), PAO_START_NUMBER VARCHAR(4), PAO_START_SUFFIX VARCHAR(2), PAO_END_NUMBER VARCHAR(4), PAO_END_SUFFIX VARCHAR(2), PAO_TEXT VARCHAR(90), USRN VARCHAR(8), LEVEL VARCHAR(30), POSTAL_ADDRESS VARCHAR(1), POSTCODE VARCHAR(8), POST_TOWN VARCHAR(30), OFFICIAL_FLAG VARCHAR(1), CUSTODIAN_ONE VARCHAR(2), CUSTODIAN_TWO VARCHAR(2), CAN_KEY VARCHAR(14), INDEX `IDX_PAO_TEXT` (`PAO_TEXT`), INDEX `IDX_POSTAL_ADDRESS_LOGICAL_STATUS` (`POSTAL_ADDRESS`, `LOGICAL_STATUS`), INDEX `IDX_USRN` (`USRN`))');
        $mySQL->exec('CREATE TABLE LLPG_METADATA (GAZ_NAME VARCHAR(60), GAZ_SCOPE VARCHAR(60), TER_OF_USE VARCHAR(60), LINKED_DATA VARCHAR(100), GAZ_OWNER VARCHAR(60), NGAZ_FREQ VARCHAR(1), CUSTODIAN_NAME VARCHAR(40), CUSTODIAN_UPRN VARCHAR(12), LOCAL_CUSTODIAN_CODE VARCHAR(4), CO_ORD_SYSTEM VARCHAR(40), CO_ORD_UNIT VARCHAR(10), META_DATE VARCHAR(16), CLASS_SCHEME VARCHAR(40), GAZ_DATE VARCHAR(16), LANGUAGE VARCHAR(3), CHARACTER_SET VARCHAR(30))');

        // Added indexes for BASIC_LAND_AND_PROPERTY_UNIT
        $mySQL->exec("ALTER TABLE `BASIC_LAND_AND_PROPERTY_UNIT` ADD INDEX `index_BLPU_UPRN` USING BTREE (`UPRN` ASC)");

        // Added indexes for STREET_DESCRIPTOR
        $mySQL->exec("ALTER TABLE `STREET_DESCRIPTOR` ADD INDEX `index_SD_USRN` USING BTREE (`USRN` ASC)");
        $mySQL->exec("ALTER TABLE `STREET_DESCRIPTOR` ADD INDEX `index_SD_LANGUAGE` USING BTREE (`LANGUAGE` ASC)");

        // Added indexes for LAND_AND_PROPERTY_IDENTIFIER
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_LPI_KEY` USING BTREE (`LPI_KEY` ASC)");
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_LANG` USING BTREE (`LANGUAGE` ASC)");
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_USRN` USING BTREE (`USRN` ASC)");
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_UPRN` USING BTREE (`UPRN` ASC)");
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_POSTAL_ADDRESS` USING BTREE (`POSTAL_ADDRESS` ASC)");
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_LOGICAL_STATUS` USING BTREE (`LOGICAL_STATUS` ASC)");
        $mySQL->exec("ALTER TABLE `LAND_AND_PROPERTY_IDENTIFIER` ADD INDEX `index_LPI_PAO_TEXT` USING BTREE (`PAO_TEXT` ASC)");

        // Added indexes for STREET_RECORD
        $mySQL->exec("ALTER TABLE `STREET_RECORD` ADD INDEX `index_SR_USRN` USING BTREE (`USRN` ASC)");
    }

    private function rebuildLLPGCache(\PDO $mySQL, $vars)
    {
        Generic::message('Start llpgcache rebuild on ' . $vars['host'] . '(' . $vars['database'] . ')');

        $this->rebuildLLPGImportTable($mySQL);
        $this->rebuildLLPGImportCache($mySQL, $vars);
        $this->updateLLPGImportCache($mySQL);
        $this->dropLLPGCache($mySQL, $vars);
    }

    private function stripNonAscii($string)
    {
        // This will remove all non-ascii characters / special characters from a string.
        return preg_replace('/[^(\x20-\x7F)]*/', '', $string);
    }

    private function NEtoLL($xCoord, $yCoord)
    {
        $E = $xCoord;
        $N = $yCoord;

        $a    = 6377563.396;
        $b    = 6356256.910;              // Airy 1830 major & minor semi-axes
        $F0   = 0.9996012717;                             // NatGrid scale factor on central meridian
        $lat0 = 49 * pi() / 180;
        $lon0 = -2 * pi() / 180;  // NatGrid true origin
        $N0   = -100000;
        $E0   = 400000;                     // northing & easting of true origin, metres

        $e2 = 1 - ($b * $b) / ($a * $a);                          // eccentricity squared
        $n  = ($a - $b) / ($a + $b);
        $n2 = $n * $n;
        $n3 = $n * $n * $n;

        $lat = $lat0;
        $M   = 0;

        do {
            $lat = ($N - $N0 - $M) / ($a * $F0) + $lat;

            $Ma = (1 + $n + (5 / 4) * $n2 + (5 / 4) * $n3) * ($lat - $lat0);
            $Mb = (3 * $n + 3 * $n * $n + (21 / 8) * $n3) * sin($lat - $lat0) * cos($lat + $lat0);
            $Mc = ((15 / 8) * $n2 + (15 / 8) * $n3) * sin(2 * ($lat - $lat0)) * cos(2 * ($lat + $lat0));
            $Md = (35 / 24) * $n3 * sin(3 * ($lat - $lat0)) * cos(3 * ($lat + $lat0));
            $M  = $b * $F0 * ($Ma - $Mb + $Mc - $Md);             // meridional arc
        } while ($N - $N0 - $M >= 0.00001);  // ie until < 0.01mm

        $cosLat = cos($lat);
        $sinLat = sin($lat);
        $nu     = $a * $F0 / sqrt(1 - $e2 * $sinLat * $sinLat);              // transverse radius of curvature
        $rho    = $a * $F0 * (1 - $e2) / pow(1 - $e2 * $sinLat * $sinLat, 1.5);  // meridional radius of curvature
        $eta2   = $nu / $rho - 1;

        $tanLat  = tan($lat);
        $tan2lat = $tanLat * $tanLat;
        $tan4lat = $tan2lat * $tan2lat;
        $tan6lat = $tan4lat * $tan2lat;
        $secLat  = 1 / $cosLat;
        $nu3     = $nu * $nu * $nu;
        $nu5     = $nu3 * $nu * $nu;
        $nu7     = $nu5 * $nu * $nu;
        $VII     = $tanLat / (2 * $rho * $nu);
        $VIII    = $tanLat / (24 * $rho * $nu3) * (5 + 3 * $tan2lat + $eta2 - 9 * $tan2lat * $eta2);
        $IX      = $tanLat / (720 * $rho * $nu5) * (61 + 90 * $tan2lat + 45 * $tan4lat);
        $X       = $secLat / $nu;
        $XI      = $secLat / (6 * $nu3) * ($nu / $rho + 2 * $tan2lat);
        $XII     = $secLat / (120 * $nu5) * (5 + 28 * $tan2lat + 24 * $tan4lat);
        $XIIA    = $secLat / (5040 * $nu7) * (61 + 662 * $tan2lat + 1320 * $tan4lat + 720 * $tan6lat);

        $dE  = ($E - $E0);
        $dE2 = $dE * $dE;
        $dE3 = $dE2 * $dE;
        $dE4 = $dE2 * $dE2;
        $dE5 = $dE3 * $dE2;
        $dE6 = $dE4 * $dE2;
        $dE7 = $dE5 * $dE2;
        $lat = $lat - $VII * $dE2 + $VIII * $dE4 - $IX * $dE6;
        $lon = $lon0 + $X * $dE - $XI * $dE3 + $XII * $dE5 - $XIIA * $dE7;

        $lat = $lat * 180 / pi();
        $lon = $lon * 180 / pi();

        return ['lat' => $lat, 'lng' => $lon];
    }

    private function rebuildLLPGImportTable(\PDO $mySQL)
    {
        $mySQL->exec('DROP TABLE IF EXISTS `llpgcache_import`');

        $sql = "CREATE TABLE `llpgcache_import` ( 
                  ID bigint UNSIGNED, 
                  UPRN varchar(200), 
                  LAT varchar(20), 
                  LNG varchar(20), 
                  PAO varchar(1000), 
                  SAO varchar(1000), 
                  HOUSE_NAME_NUMBER varchar(200), 
                  STREET_NAME varchar(200), 
                  LOCALITY_NAME varchar(200), 
                  TOWN_NAME varchar(200), 
                  COUNTY_NAME varchar(200), 
                  POST_TOWN varchar(200), 
                  POSTCODE varchar(20), 
                  USRN varchar(200), 
                  WARD varchar(200), 
                  PARISH varchar(200), 
                  ADMINISTRATIVE_AREA varchar(200),
                  ORGANISATION VARCHAR(200), 
                  LANGUAGE VARCHAR(3), 
                  BLPU_CLASS VARCHAR(4)
                )";
        $mySQL->exec($sql);

        $mySQL->exec("ALTER TABLE llpgcache_import ADD INDEX `index_cache` USING BTREE (UPRN ASC)");
        $mySQL->exec("ALTER TABLE llpgcache_import ADD INDEX `index_postcode` USING BTREE (POSTCODE ASC)");
    }

    private function rebuildLLPGImportCache(\PDO $mySQL, $vars)
    {
        Generic::message('Start cache(llpgcache_import) rebuild on ' . $vars['host'] . '(' . $vars['database'] . ')');

        $mySQL->exec("INSERT INTO `llpgcache_import` (`ID`, `UPRN`, `LAT`, `LNG`, `HOUSE_NAME_NUMBER`, `PAO`, `SAO`, `STREET_NAME`, `LOCALITY_NAME`, `TOWN_NAME`, `COUNTY_NAME`, `POST_TOWN`, `POSTCODE`, `USRN`, `WARD`, `PARISH`, `ADMINISTRATIVE_AREA`, `LANGUAGE`)
            SELECT DISTINCT `a`.`UPRN` AS `ID`,`a`.`UPRN` AS `UPRN`,NULL AS `LAT`,NULL AS `LNG`,
                tcase(CONCAT(
                    pao_start_number,pao_start_suffix,
                    CASE WHEN pao_end_number<>'' THEN CONCAT('-',pao_end_number,pao_end_suffix) ELSE '' END
                )) as house_name_number,
                tcase(CONCAT(
                    CASE WHEN pao_start_number='' THEN CONCAT(sao_start_number,sao_start_suffix) ELSE '' END,
                    CASE WHEN pao_start_number='' and sao_end_number<>'' THEN CONCAT('-',sao_end_number, sao_end_suffix) ELSE '' END,
                    CASE WHEN pao_start_number='' and sao_start_number<>'' THEN ' ' ELSE '' END,
                    CASE WHEN pao_start_number='' and pao_text<>'' THEN pao_text ELSE '' END,
                    pao_start_number,
                    pao_start_suffix,
                    CASE WHEN pao_end_number<>'' THEN CONCAT('-',pao_end_number,pao_end_suffix) ELSE '' END
                )) as pao,
                tcase(CONCAT(
                    CASE WHEN sao_text<>'' THEN sao_text ELSE '' END,
                    CASE WHEN sao_text<>'' and pao_start_number<>'' THEN ' ' ELSE '' END,
                    CASE WHEN pao_start_number<>'' THEN sao_start_number ELSE '' END,
                    CASE WHEN pao_start_number<>'' THEN sao_start_suffix ELSE '' END,
                    CASE WHEN pao_start_number<>'' and sao_end_number<>'' THEN CONCAT('-',sao_end_number, sao_end_suffix) ELSE '' END,
                    CASE WHEN pao_start_number<>'' and sao_start_number<>'' THEN ' ' ELSE '' END,
                    CASE WHEN pao_start_number<>'' and pao_text<>'' THEN pao_text ELSE '' END
                )) as sao,
                CASE WHEN `c`.RECORD_TYPE=2 OR `c`.RECORD_TYPE=3 THEN '' ELSE tcase(`b`.`STREET_DESCRIPTOR`) END AS `STREET_NAME`,
                tcase(`b`.`LOCALITY_NAME`) AS `LOCALITY_NAME`,
                tcase(`b`.`TOWN_NAME`) AS `TOWN_NAME`,
                tcase(`b`.`ADMINISTRATIVE_AREA`) AS `COUNTY_NAME`,
                tcase(`a`.`POST_TOWN`) AS `POST_TOWN`,
                `a`.`POSTCODE` AS `POSTCODE`,
                `a`.`USRN` AS `USRN`,
                '','',
                tcase(`b`.`ADMINISTRATIVE_AREA`) AS `ADMINISTRATIVE_AREA`,
                `a`.LANGUAGE
            FROM `LAND_AND_PROPERTY_IDENTIFIER` `a` join `STREET_DESCRIPTOR` `b` on (`a`.`USRN` = `b`.`USRN` AND `a`.LANGUAGE=`b`.LANGUAGE) join `STREET_RECORD` `c` on (`b`.`USRN` = `c`.`USRN`)
            WHERE (`a`.`POSTAL_ADDRESS` IN ('Y','A','L') AND a.LOGICAL_STATUS = 1 OR  a.PAO_TEXT='STREET RECORD')");
    }

    private function updateLLPGImportCache(\PDO $mySQL)
    {
        $sql = "UPDATE llpgcache_import, BASIC_LAND_AND_PROPERTY_UNIT 
                set llpgcache_import.LNG=BASIC_LAND_AND_PROPERTY_UNIT.LNG, 
                    llpgcache_import.LAT=BASIC_LAND_AND_PROPERTY_UNIT.LAT, 
                    llpgcache_import.WARD=BASIC_LAND_AND_PROPERTY_UNIT.WARD_CODE, 
                    llpgcache_import.PARISH=BASIC_LAND_AND_PROPERTY_UNIT.PARISH_CODE,
                    llpgcache_import.ORGANISATION=BASIC_LAND_AND_PROPERTY_UNIT.ORGANISATION,
                    llpgcache_import.BLPU_CLASS=BASIC_LAND_AND_PROPERTY_UNIT.BLPU_CLASS
              where llpgcache_import.UPRN=BASIC_LAND_AND_PROPERTY_UNIT.UPRN";
        $mySQL->exec($sql);
    }

    private function dropLLPGCache(\PDO $mySQL, $vars)
    {
        Generic::message('End cache(llpgcache_import) rebuild on ' . $vars['host'] . '(' . $vars['database'] . ')');

        Generic::message('Start comparison validation of `llpgcache_import`');

        $check1 = $mySQL->query("SELECT count(0) as check1 FROM llpgcache_import")->fetchColumn();

        $check2 = $mySQL->query("SELECT count(0) as check2 FROM `LAND_AND_PROPERTY_IDENTIFIER` where POSTAL_ADDRESS in ('Y','A','L')")->fetchColumn();

        $llpgMoreThan20k = true;
        if ($check1 <= 20000) {
            $llpgMoreThan20k = false;
        }

        $halfLPI = true;
        if ($check1 <= ($check2 * 0.5)) {
            $halfLPI = false;
        }

        if ($llpgMoreThan20k && $halfLPI) {
            Generic::message('End comparison validation of `llpgcache_import` successfully, replicate `llpgcache` with `llpgcache_import`');
            $mySQL->query("DROP TABLE llpgcache");
            $mySQL->query("RENAME TABLE llpgcache_import TO llpgcache");
            Generic::message('Finished to replicate `llpgcache` with `llpgcache_import`');

            Generic::message('[' . date('Y-m-d H:i:s') . ']Start geocache rebuild on ' . $vars['host'] . '(' . $vars['database'] . ')');
            $mySQL->exec('CALL `buildgeocache`()');
        } else {
            Generic::message('End comparison validation of `llpgcache_import` with error');

            // Move uploaded file into error directory and log message
            $moved = rename($this->typeDir . '/' . PROCESSED_DIR . '/' . $this->currentFile, $this->typeDir . '/' . ERROR_DIR . '/' . $this->currentFile);

            if ($moved !== true) {
                $message = 'Error: Failed to move file ' . $this->typeDir . '/' . PROCESSED_DIR . '/' . $this->currentFile . '.';
                Generic::logToFile($this->logFile, $message);
            }

            // Move sync sftp file into error directory if exist
            $sftpFilePath = $this->sftp . '/' . $vars['ftp_user'] . '/data/' . $this->type . '/' . PROCESSED_DIR . '/' . $this->currentFile;

            if (is_file($sftpFilePath)) {
                $sftpMoved = rename($sftpFilePath, $this->sftp . '/' . $vars['ftp_user'] . '/data/' . $this->type . '/' . ERROR_DIR . '/' . $this->currentFile);
                if ($sftpMoved !== true) {
                    $message = 'Error: Failed to move file ' . $sftpFilePath . '.';
                    Generic::logToFile($this->logFile, $message);
                }
            }

            $message = 'Error: the import of file {' . $this->currentFile . '} failed because it would have resulted in the number of addresses less than either 20,000 or half the number of LPIs. ' .
                "On partial upload please ensure that record type 'F' is not used as this will reset existing data. If this continues to fail despite correct DTF formatting please contact Firmstep support.";
            Generic::logToFile($this->logFile, $message);

            // Send slack message to inform firmstep
            $data  = '{"channel": "' . SLACK_CHANNEL . '", "username": "' . SLACK_USERNAME . '", "text": "' . '<!channel> LLPG Import validation failed for ' . $vars['host'] . '|' . $vars['database'] . '. llpgcache table not updated. Error: }';
            $error = '';

            if ($llpgMoreThan20k !== true) {
                $error .= 'The number of address in `llpgcache_important` is less than 20k.';
            }

            if ($halfLPI !== true) {
                $error .= (empty($error) ? '' : ' And ') . 'The number of address in table `llpgcache_important` is less than half the number of table `LAND_AND_PROPERTY_IDENTIFIER`.';
            }

            $data .= $error;

            $curlOpts = [
                CURLOPT_URL            => SLACK_URL,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => false,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $data
            ];

            $curl = CURL::getCurl($curlOpts);

            CURL::getResult($curl);
            CURL::closeCurl($curl);
        }
    }

}
