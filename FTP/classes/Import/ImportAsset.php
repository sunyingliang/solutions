<?php

namespace FS\Import;

use FS\Common\Generic;
use FS\External\ConversionsLatLong;

class ImportAsset extends ImportBase
{
    protected $headers = [
        'ID'            => -1,
        'ISSUE_TYPE'    => -1,
        'ASSET_TYPE'    => -1,
        'NAME'          => -1,
        'DESCRIPTION'   => -1,
        'LAT'           => -1,
        'LNG'           => -1,
        'EASTING'       => -1,
        'NORTHING'      => -1,
        'OKICONURL'     => -1,
        'ACTIVEICONURL' => -1,
        'FAULTICONURL'  => -1,
        'RADIUS'        => -1
    ];

    protected $item = [
        'ID'            => null,
        'ISSUE_TYPE'    => null,
        'ASSET_TYPE'    => null,
        'NAME'          => null,
        'DESCRIPTION'   => null,
        'LAT'           => null,
        'LNG'           => null,
        'EASTING'       => null,
        'NORTHING'      => null,
        'OKICONURL'     => null,
        'ACTIVEICONURL' => null,
        'FAULTICONURL'  => null,
        'RADIUS'        => null
    ];

    protected function startImport($file, \PDO $mySQL, $vars)
    {
        // Get header record
        $header_keys = array_keys($this->headers);
        $headers     = fgetcsv($file);
        $length      = count($headers);

        for ($i = 0; $i < $length; $i++) {
            if (in_array(strtoupper($headers[$i]), $header_keys)) {
                $this->headers[strtoupper($headers[$i])] = $i;
            }
        }

        foreach ($this->headers as $index) {
            if ($index == -1) {
                $message = 'The headers in the uploaded file are incorrect. ';
                Generic::logToFile($this->logFile, $message);
                array_push($this->errorMessage, $message);
                return false;
            }
        }

        // Truncate table
        $sql = 'TRUNCATE TABLE `reportit_asset`';

        if ($mySQL->exec($sql) === false) {
            $message = 'Error: #0117 - Please contact Firmstep Support and provide this code for investigation. ';
            Generic::logToFile($this->logFile, $message);
            array_push($this->errorMessage, $message);
            return false;
        }

        // Get non-header rows
        $count = 1;

        while (($record = fgetcsv($file)) !== false) {
            $count++;

            foreach ($this->headers as $header => $index) {
                if (isset($record[$index])) {
                    $this->item[$header] = $record[$index];
                }
            }

            if ($this->item['LAT'] !== '' && $this->item['LNG'] !== '') {
                if ($this->item['EASTING'] === '' || $this->item['NORTHING'] === '') {
                    $eastingNorthing        = (new ConversionsLatLong())->wgs84_to_osgb36($this->item['LAT'], $this->item['LNG']);
                    $this->item['EASTING']  = $eastingNorthing[0];
                    $this->item['NORTHING'] = $eastingNorthing[1];
                }
            } else if ($this->item['EASTING'] !== '' && $this->item['NORTHING'] !== '') {
                $latLng            = (new ConversionsLatLong())->osgb36_to_wgs84($this->item['EASTING'], $this->item['NORTHING']);
                $this->item['LAT'] = $latLng[0];
                $this->item['LNG'] = $latLng[1];
            } else {
                $message = "line {$count}: Either (LAT + LNG) or (EASTING + NORTHING) should be supplied. ";
                Generic::logToFile($this->logFile, $message);
                array_push($this->errorMessage, $message);
                return false;
            }

            // ISSUE #110 RADIUS: valid values numeric 0-100, default 100, 
            if (!isset($this->item['RADIUS']) || empty($this->item['RADIUS']) || !is_numeric($this->item['RADIUS']) || $this->item['RADIUS'] > 100 || $this->item['RADIUS'] < 0) {
                $this->item['RADIUS'] = 100;
            }

            $result = $this->migrateAssetRecord($this->item, $mySQL);

            if ($result !== true) {
                $message = "line {$count}: $result";
                Generic::logToFile($this->logFile, $message);
                array_push($this->errorMessage, $message);
                continue;
            }
        }

        return true;
    }

    protected function endTask(\PDO $mySQL, $vars)
    {
    }

    private function migrateAssetRecord($record, \PDO $mySQL)
    {
        $id            = $record['ID'];
        $issueType     = $record['ISSUE_TYPE'];
        $assetType     = $record['ASSET_TYPE'];
        $name          = $record['NAME'];
        $description   = $record['DESCRIPTION'];
        $lat           = $record['LAT'];
        $lng           = $record['LNG'];
        $easting       = $record['EASTING'];
        $northing      = $record['NORTHING'];
        $okIconUrl     = $record['OKICONURL'];
        $activeIconUrl = $record['ACTIVEICONURL'];
        $faultIconUrl  = $record['FAULTICONURL'];
        $radius        = $record['RADIUS'];

        $sql = 'INSERT INTO `reportit_asset`(`id`, `asset_type`, `name`, `description`, `lat`, `lng`, `easting`, `northing`, `point`, `circle`, `radius`, `issue_type`, `okiconurl`, `activeiconurl`, `faulticonurl`) '
            . 'VALUES(:id, :asset_type, :name, :description, :lat, :lng, :easting, :northing, POINT(:lat, :lng), GETPOLYGON(:lat, :lng, :radius, 12), :radius, :issue_type, :okiconurl, :activeiconurl, :faulticonurl)';

        $stmt = $mySQL->prepare($sql);

        $result = $stmt->execute([
            'id'            => $id,
            'asset_type'    => $assetType,
            'name'          => $name,
            'description'   => $description,
            'lat'           => $lat,
            'lng'           => $lng,
            'easting'       => $easting,
            'northing'      => $northing,
            'radius'        => $radius,
            'issue_type'    => $issueType,
            'okiconurl'     => $okIconUrl,
            'activeiconurl' => $activeIconUrl,
            'faulticonurl'  => $faultIconUrl
        ]);

        if (!$result) {
            return $stmt->errorInfo()[2];
        }

        return true;
    }
}
