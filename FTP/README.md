#Documentation

##Functionality
The FTP project is used to handle the files uploaded by customers, currently the program can cater 'llpg', 'asset' and 'mbcollection' three types of imports.

##Cron jobs
The following scripts have to be run as cron jobs in server side:
1. /script/checkUser.php
    * This script will scan 'customer' table in 'solutions_master' database to ensure that program set up proper works including creating necessary directories for newly added customers.
2. /script/migrate.php [ llpg | asset | mbcollection ]
    * This script is charge of handling the importing of data from uploaded files into responsive tables. Please be noted that only one of these three parameters can be passed to the script at one time
