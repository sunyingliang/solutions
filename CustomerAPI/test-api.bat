rem Start a new window to run web server
start cmd /K php -S 0.0.0.0:8080 -t public

rem Wait 1 second for the web server to start up
timeout /t 1 /nobreak

rem Requires server running on localhost to test
vendor\bin\phpunit tests & pause