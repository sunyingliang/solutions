#documentation
https://docs.google.com/document/d/1YOQEOXj3S_njhoYQkAm0n6MS122aH9glT-kbx8Y1JnY

#prerequisite
php version(>=7) is installed;

#configuration  
1. Deploy all these folders & files in web server;
2. Configure 'public' folder as web server's document root, or change 'public' folder name to web server's document root;
4. Rename all configuration files under /config folder to the name without '_example', e.g. change database_example.php to database.php.
5. Run 'php {webroot}/scripts/database.php' to create required database schema;

#tests
TO RUN TESTS ON LINUX
	1) Run the following command in project root (NOTE: PHPUnit must be installed for this to work)
		a) "phpunit tests"

TO RUN TESTS ON WINDOWS
	1) Install PHPUnit (If you haven't already)
		a) follow instructions at https://phpunit.de/manual/current/en/installation.html
	2) You can confirm the API is installed correctly by successfully running the following batch file
		a) test-api.bat (The api unit tests)
