<?php

use PHPUnit\Framework\TestCase;

class FTPLogReaderTest extends TestCase
{
    public $token;
    public $credential;

    public function setup()
    {
        $this->credential = 'kiwidave:r9YGdV4mUGnSrT4H';

        $response    = $this->runCurl('api/auth', [], null, $this->credential);
        $this->token = json_decode($response['body'])->Response;
    }

    public function testGetFTPLog()
    {
        $response = $this->runCurl('api/log/ftp', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
    }

    public function getServerURL()
    {
        return 'http://kiwidave.local.firmstep.com:8080/';
    }

    public function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
