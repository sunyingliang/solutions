<?php

use PHPUnit\Framework\TestCase;

class LLPGTest extends TestCase
{
    public $token;
    public $credential;

    public function setup()
    {
        $this->credential = 'kiwidave:r9YGdV4mUGnSrT4H';

        $response    = $this->runCurl('api/auth', [], null, $this->credential);
        $this->token = json_decode($response['body'])->Response;
    }

    public function testDefault()
    {
        // Do Basic Auth
        $response = $this->runCurl('default', [], null, $this->credential);
        $this->assertEquals(400, $response['status']);
        $this->assertContains('Required parameters: type, value', $response['body']);

        $response = $this->runCurl('default?type=postcode&value=', [], null, $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('Value must be set', $response['body']);

        $response = $this->runCurl('default?type=&value=test', [], null, $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('Type must be either postcode or uprn', $response['body']);

        // Do token auth
        $response = $this->runCurl('default', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(400, $response['status']);
        $this->assertContains('Required parameters: type, value', $response['body']);

        $response = $this->runCurl('default?type=postcode&value=', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('Value must be set', $response['body']);

        $response = $this->runCurl('default?type=&value=test', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('Type must be either postcode or uprn', $response['body']);
    }

    public function testListAddressByPostcode()
    {
        $response = $this->runCurl('default?type=postcode&value=test', [], null, $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('No address found', $response['body']);

        $response = $this->runCurl('default?type=postcode&value=test', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('No address found', $response['body']);

        $response = $this->runCurl('default?type=postcode&value=unknown_should_not_return', [], null, $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('No address found', $response['body']);

        $response = $this->runCurl('default?type=postcode&value=unknown_should_not_return', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('No address found', $response['body']);
    }

    public function testListAddressesByUPRN()
    {
        $response = $this->runCurl('default?type=uprn&value=200002752139', [], null, $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('<table', $response['body']);

        $response = $this->runCurl('default?type=uprn&value=200002752139', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('<table', $response['body']);

        $response = $this->runCurl('default?type=uprn&value=unknown_should_not_return', [], null, $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('No address found', $response['body']);

        $response = $this->runCurl('default?type=uprn&value=unknown_should_not_return', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('No address found', $response['body']);
    }

    public function getServerURL()
    {
        return 'http://kiwidave.local.firmstep.com:8080/';
    }

    public function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
