<?php

use FS\Integration\Handler\IConfirmWriter;

class MockConfirmWriter implements IConfirmWriter
{
    public function processOperations(string $request): string {
        return '<Response SchemaVersion="1.4" xmlns="">
                <OperationResponse>
                    <NewEnquiryResponse>
                        <Enquiry>
                            <EnquiryNumber>11125449</EnquiryNumber>
                            <ExternalSystemNumber>3</ExternalSystemNumber>
                            <ExternalSystemReference>CASEREF</ExternalSystemReference>
                            <ServiceCode>HIGH</ServiceCode>
                            <ServiceName>Highways</ServiceName>
                            <SubjectCode>HI15</SubjectCode>
                            <SubjectName>Drainage</SubjectName>
                            <EnquiryDescription>DESCRIPTION HERE</EnquiryDescription>
                            <EnquiryLocation>LOCATION HERE</EnquiryLocation>
                            <EnquiryLogNumber>1</EnquiryLogNumber>
                            <EnquiryStatusCode>0050</EnquiryStatusCode>
                            <EnquiryStatusName>New Enquiry Received</EnquiryStatusName>
                            <AssignedOfficerCode>HIGH</AssignedOfficerCode>
                            <AssignedOfficerName>Highway Officer</AssignedOfficerName>
                            <LoggedTime>2017-01-25T14:39:54</LoggedTime>
                            <LogEffectiveTime>2017-01-25T14:39:54</LogEffectiveTime>
                            <SiteCode>9400675</SiteCode>
                            <SiteName>HIGH STREET</SiteName>
                            <SiteTownName>MICKLETON</SiteTownName>
                            <SiteCountyName>GLOUCESTERSHIRE</SiteCountyName>
                            <SiteClassCode>01</SiteClassCode>
                            <SiteClassName>Publicly Maintainable</SiteClassName>
                            <StatusFollowUpTime>2017-02-01T14:39:54</StatusFollowUpTime>
                            <EnquiryLogTime>2017-01-25T14:39:54</EnquiryLogTime>
                            <EnquiryClassCode>RESE</EnquiryClassCode>
                            <EnquiryClassName>Request for Service</EnquiryClassName>
                            <EnquiryX>0</EnquiryX>
                            <EnquiryY>0</EnquiryY>
                            <LoggedByUserName>CRM User</LoggedByUserName>
                            <LoggedByUserId>CRMUSER</LoggedByUserId>
                        </Enquiry>
                    </NewEnquiryResponse>
                </OperationResponse>
            </Response>';
    }
}