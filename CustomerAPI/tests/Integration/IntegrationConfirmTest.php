<?php

require_once __DIR__ . '/../../config/common.php';
require_once __DIR__ . '/../../Autoload.php';
require_once __DIR__ . '/MockConfirmWriter.php';

use FS\Database\Manager;
use FS\Integration\ReportIt\Confirm;
use PHPUnit\Framework\TestCase;

class IntegrationConfirmTest extends TestCase
{
    const TEST_CASE_REFERENCE = 'test-RIC2955051';
    const CUSTOMER            = 'integration';
    const CUSTOMER_PASSWORD   = 'LmBAFMlWqEYhJKMp';
    /** @var  Manager */
    private static $dbManager;

    public static function setUpBeforeClass()
    {
        self::$dbManager = new Manager();
        self::$dbManager->authenticate(self::CUSTOMER, self::CUSTOMER_PASSWORD);
    }

    public static function tearDownAfterClass()
    {
        self::$dbManager = null;
    }

    public function testWebhookToConfirm()
    {
        // Insert through webhook
        $reportIt = [
            'casedetails'   => [
                'case_ref'       => self::TEST_CASE_REFERENCE,
                'process_name'   => 'Report It',
                'stage_name'     => 'action',
                'host'           => 'examples-forms.achieveservice.com',
                'task_id'        => '0d71121ba1c34',
                'customer'       => [
                    'ucrn'          => '775128557',
                    'Title'         => '',
                    'First_Name'    => '',
                    'Surname'       => '',
                    'flat'          => '',
                    'house'         => '',
                    'street'        => '',
                    'locality'      => '',
                    'town'          => '',
                    'county'        => '',
                    'postcode'      => '',
                    'Email_Address' => ''
                ],
                'issuetype'      => 'Pothole in road',
                'infostatements' => 'Offensive - Anti Disability; Offensive - Ageist',
                'location'       => [
                    'gps'               => '',
                    'easting'           => '542697.13895461',
                    'northing'          => '208369.454513534',
                    'uprn'              => '10023416666',
                    'usrn'              => '16500353',
                    'address'           => 'Street Record Red Willow, Harlow',
                    'locationSpecifics' => 'Northbound carriageway'
                ],
                'description'    => 'This is especially for Steve and the new webhook experimentation science.',
                'photofilenames' => 'Image000.jpg,Image001.jpg',
                'photopath'      => '2016-07-20/d64d734e6c2d4',
                'date_created'   => '2016-05-10 17:38:00',
            ],
            'changedetails' => [
                'date_updated'  => '2016-05-10 17:41:54',
                'summaryNote'   => '',
                'activeStatus'  => '',
                'currentStatus' => 'webhook',
                'team'          => 'Computer Technicians',
                'notes'         => 'This is a filltask test',
                'deadlinedate'  => '2016-05-25 00:00:00',
                'warningdate'   => '2016-05-23 00:00:00'
            ]
        ];

        (new Confirm(self::CUSTOMER, self::$dbManager, new MockConfirmWriter()))->execute($reportIt);

        // Check if inserted correctly
        $connection = self::$dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT `case_ref` FROM `confirm_enquiry_case` WHERE `case_ref` = :caseRef');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE]);

        $result = $stmt->fetch();

        $this->assertTrue($result !== false);

        // Clean up
        $stmt = $connection->prepare('DELETE FROM `confirm_enquiry_case` WHERE `case_ref` = :caseRef');

        $stmt->execute(['caseRef' => self::TEST_CASE_REFERENCE]);
    }
}
