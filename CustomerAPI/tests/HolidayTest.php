<?php

use PHPUnit\Framework\TestCase;

class HolidayTest extends TestCase
{
    public $token;
    public $credential;

    public function setup()
    {
        $this->credential = 'kiwidave:r9YGdV4mUGnSrT4H';

        $response    = $this->runCurl('auth', [], null, $this->credential);
        $this->token = json_decode($response['body'])->Response;
    }

    public function testListHoliday()
    {
        $response = $this->runCurl('solutions/holiday/list', ['X-AuthToken: ' . $this->token], '[]');
        $this->assertEquals(200, $response['status']);
    }

    public function testNoParameter()
    {
        $response = $this->runCurl('solutions/holiday/add', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], '[]');
        $this->assertEquals(500, $response['status']);
        $this->assertContains('Parameters {holiday}, {calendar} are required', $response['body']);

        $response = $this->runCurl('solutions/holiday/edit', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], '[]');
        $this->assertEquals(500, $response['status']);
        $this->assertContains('Parameters {id} is required', $response['body']);

        $response = $this->runCurl('solutions/holiday/delete', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], '[]');
        $this->assertEquals(500, $response['status']);
        $this->assertContains('Parameters {id} is required', $response['body']);
    }

    public function testAuthToken()
    {
        //Test list holiday
        $response = $this->runCurl('solutions/holiday/list', ['X-AuthToken: fake'], '[]');
        $this->assertEquals(401, $response['status']);

        // Test create holiday
        $data = 'calendar=Auckland'
            . '&holiday=2018-01-02';
        $response = $this->runCurl('solutions/holiday/add', ['X-AuthToken: fake', 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(401, $response['status']);

        // Test update holiday
        $data = 'id=5'
            .'&calendar=South&holiday=2018-01-02&notes=test';
        $response = $this->runCurl('solutions/holiday/edit', ['X-AuthToken: fake', 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(401, $response['status']);

        // Test delete holiday
        $data = 'id=5';
        $response = $this->runCurl('solutions/holiday/delete', ['X-AuthToken: fake', 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(401, $response['status']);

    }

    public function testCreateListUpdateDeleteHoliday()
    {
        // Test create holiday
        $data = 'calendar=Auckland'
            . '&holiday=2018-01-02';
        $response = $this->runCurl('solutions/holiday/add', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], $data);
        $body     = json_decode($response['body'],true);
        $this->assertEquals(200, $response['status']);
        $id = $body['id'];

        // Test create duplicate holiday
        $data = 'calendar=Auckland'
            . '&holiday=2018-01-02';
        $response = $this->runCurl('solutions/holiday/add', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(500, $response['status']);
        $this->assertContains('This holiday already exists', $response['body']);

        // Test update holiday
        $data = 'id='. $id
            .'&calendar=South&holiday=2018-01-02&notes=test';
        $response = $this->runCurl('solutions/holiday/edit', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(200, $response['status']);

        // Test update holiday with existing holiday
        $data = 'id=' . $id
            . '&calendar=South&holiday=2018-09-18';
        $response = $this->runCurl('solutions/holiday/edit', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(500, $response['status']);
        $this->assertContains('This holiday already exists', $response['body']);

        // Test delete holiday
        $data = 'id=' . $id;
        $response = $this->runCurl('solutions/holiday/delete', ['X-AuthToken: ' . $this->token, 'Content-Type: application/x-www-form-urlencoded'], $data);
        $this->assertEquals(200, $response['status']);
    }

    private function getServerURL()
    {
        return 'http://kiwidave.local.firmstep.com:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
