<?php

use PHPUnit\Framework\TestCase;

class TestReportIt extends TestCase
{
    public $token;
    public $credential;

    public function setup()
    {
        $this->credential = 'kiwidave:r9YGdV4mUGnSrT4H';

        $response    = $this->runCurl('auth', [], null, $this->credential);
        $this->token = json_decode($response['body'])->Response;
    }

    public function testNoParameter()
    {
        $response = $this->runCurl('reportit/check-merge', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(400, $response['status']);
        $this->assertContains('Required parameters: mergedbid', $response['body']);

        $response = $this->runCurl('reportit/check-merge', [], null, $this->credential);
        $this->assertEquals(400, $response['status']);
        $this->assertContains('Required parameters: mergedbid', $response['body']);
    }

    public function testCheckMerge()
    {
        $response = $this->runCurl('reportit/check-merge?mergedbid=test_customer-api_pref', ['X-AuthToken: ' . $this->token]);
        $this->assertJson($response['body']);
        $this->assertContains('"mergedBidok":"1"', $response['body']);

        $response = $this->runCurl('reportit/check-merge?mergedbid=test_customer-api_pref', [], null, $this->credential);
        $this->assertJson($response['body']);
        $this->assertContains('"mergedBidok":"1"', $response['body']);
    }

    public function testCheckStatusChange()
    {
        $response = $this->runCurl('reportit/check-status-change?caseref=test_customer-api_pref&activestatus=test_customer-api_pactivestatus', ['X-AuthToken: ' . $this->token]);
        $this->assertJson($response['body']);
        $this->assertContains('"status_is_new":"No"', $response['body']);

        $response = $this->runCurl('reportit/check-status-change?caseref=test_customer-api_pref&activestatus=test_customer-api_pactivestatus', [], null, $this->credential);
        $this->assertJson($response['body']);
        $this->assertContains('"status_is_new":"No"', $response['body']);
    }

    public function testCheckTaskId()
    {
        $response = $this->runCurl('reportit/check-task-id?mergedbid=task_id', ['X-AuthToken: ' . $this->token]);
        $this->assertJson($response['body']);
        $this->assertContains('"mergedBid":"task_id"', $response['body']);

        $response = $this->runCurl('reportit/check-task-id?mergedbid=task_id', [], null, $this->credential);
        $this->assertJson($response['body']);
        $this->assertContains('"mergedBid":"task_id"', $response['body']);
    }

    public function testGetBulkSelected()
    {
        $response = $this->runCurl('reportit/bulk-selected?selected=123', ['X-AuthToken: ' . $this->token]);
        $this->assertEquals(404, $response['status']);

        $response = $this->runCurl('reportit/bulk-selected?selected=123', [], null, $this->credential);
        $this->assertEquals(404, $response['status']);
    }

    public function testSaveDBId()
    {
        $response = $this->runCurl('reportit/save-db-id', ['X-AuthToken: ' . $this->token], '{"Request":{"dbId": "task6","caseRef": "test_customer-api_pref"}}');
        $this->assertEquals(200, $response['status']);
        $this->assertContains('true', $response['body']);

        $response = $this->runCurl('reportit/save-db-id', [], '{"Request":{"dbId": "task_id","caseRef": "test_customer-api_pref"}}', $this->credential);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('true', $response['body']);
    }

    private function getServerURL()
    {
        return 'http://kiwidave.local.firmstep.com:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
