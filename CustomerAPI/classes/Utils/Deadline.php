<?php

namespace FS\Utils;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;

class Deadline extends \FS\SolutionsCustomerAPIBase
{
    private $calendarDayType = 7;

    public function __construct()
    {
        parent::__construct();
    }

    public function deadline()
    {
        try {
            if (!$this->isAuthenticated()) {
                return $this->unauthorized();
            }

            $data = $this->json();

            if (!is_array($data)) {
                throw new InvalidParameterException('Error: the format of passed in parameter is not correct');
            }

            $result = [];

            foreach ($data as $item) {
                $validate = $this->validate($item);

                if ($validate === false) {
                    continue;
                }

                $result[$item['resultname']] = $validate === true ? $this->calculateDate($item) : $validate;
            }
        } catch (\Exception $e) {
            $result = $e->getMessage();
        }

        return $result;
    }

    private function validate(&$data)
    {
        $validation = $this->required($data, 'resultname', 'start', 'cutoff', 'add', 'calendar');

        if (!$validation['valid']) {
            return false;
        }

        if (!$this->validateDateTime($data['start'])) {
            return 'Error: Passed in parameter {start} is not a correct format. i.e. dd/mm/YYYY HH:ii[:ss]';
        }

        if (!$this->validateTime($data['cutoff'])) {
            return 'Error: Passed in parameter {cutoff} is not a correct format. i.e. HH:ii[:ss]';
        }

        if (!$this->validateInt($data['add'])) {
            return 'Error: Passed in parameter {add} is not an integer';
        }

        if (isset($data['subtract']) && !$this->validateInt($data['subtract'])) {
            return 'Error: Passed in parameter {subtract} is not an integer';
        }

        return true;
    }

    private function validateDateTime($dateTime)
    {
        // Check the length first
        $dateTimeLength = strlen($dateTime);
        if ($dateTimeLength != 16 && $dateTimeLength != 19) {
            return false;
        }

        // regular expression for date-time format
        $regexp = '/\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}(:\d{2})?/';

        return preg_match($regexp, $dateTime) === 1;
    }

    private function validateTime($time)
    {
        // Check the length first
        $timeLength = strlen($time);
        if ($timeLength != 5 && $timeLength != 8) {
            return false;
        }

        // regular expression for time format
        $regexp = '/\d{2}:\d{2}(:\d{2})?/';

        return preg_match($regexp, $time) === 1;
    }

    private function validateInt($int)
    {
        return is_numeric($int);
    }

    private function calculateDate($data)
    {
        $customerConnection = $this->dbManager->getCustomerConnection();

        $sql = "SELECT `holiday` FROM `holiday` WHERE `calendar` = :calendar";

        if (($stmt = $customerConnection->prepare($sql)) === false) {
            throw new PDOCreationException('Error: failed to create PDOStatement');
        }

        if ($stmt->execute(['calendar' => $data['calendar']]) === false) {
            throw new PDOExecutionException('Error: failed to execute PDOStatement');
        }

        $publicHolidays = [];

        if ($rows = $stmt->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach ($rows as $row) {
                $publicHolidays[] = $row['holiday'];
            }
        }

        $startDateTime  = new \DateTime(preg_replace('/(\d{2})\/(\d{2})\/(\d{4})/', '$3-$2-$1', $data['start']));
        $resultDateTime = clone $startDateTime;

        $startDate  = $startDateTime->format('Y-m-d');
        $startTime  = $startDateTime->format('H:i:s');
        $startWeek  = $startDateTime->format('w');
        $cutoffTime = $data['cutoff'];
        $days       = intval($data['add']);

        if (isset($data['subtract'])) {
            $days -= intval($data['subtract']);
        }

        if ($startWeek != 0 && $startWeek != 6 && !in_array($startDate, $publicHolidays)) {
            if ($days > 0 && $startTime > $cutoffTime) {
                $days++;
            }
            if ($days < 0 && $startTime < $cutoffTime) {
                $days--;
            }

        }

        if ($data['calendar'] == $this->calendarDayType) {
            if ($days < 0) {
                return $resultDateTime->sub(new \DateInterval('P' . abs($days) . 'D'))->format('d/m/Y H:i:s');
            } else {
                return $resultDateTime->add(new \DateInterval('P' . $days . 'D'))->format('d/m/Y H:i:s');
            }
        }

        if ($days < 0) {
            if ($startWeek == 0 || $startWeek == 6) {
                $startDateTime->add(new \DateInterval('P' . (($startWeek + 1) % 5) . 'D'));
                $resultDateTime->add(new \DateInterval('P' . (($startWeek + 1) % 5) . 'D'));
                $startWeek = $startDateTime->format('w');
            }

            $resultDateTime->sub(new \DateInterval('P' . abs($this->calculateCalendarDays($startWeek, $days)) . 'D'));

            while ($holidayCount = $this->checkHolidays($publicHolidays, $startDateTime, $resultDateTime)) {
                $startDateTime->sub($startDateTime->diff($resultDateTime, true))->sub(new \DateInterval('P1D'));

                $startWeek = $startDateTime->format('w');

                if ($startWeek == 0 || $startWeek == 6) {
                    $startDateTime->add(new \DateInterval('P' . (($startWeek + 1) % 5) . 'D'));
                }

                $startWeek = $resultDateTime->format('w');

                $resultDateTime->sub(new \DateInterval('P' . abs($this->calculateCalendarDays($startWeek, 0 - $holidayCount)) . 'D'));
            }
        } else {
            $resultDateTime->add(new \DateInterval('P' . $this->calculateCalendarDays($startWeek, $days) . 'D'));

            while ($holidayCount = $this->checkHolidays($publicHolidays, $startDateTime, $resultDateTime)) {
                $startDateTime->add($startDateTime->diff($resultDateTime, true))->add(new \DateInterval('P1D'));

                $startWeek = $resultDateTime->format('w');

                $resultDateTime->add(new \DateInterval('P' . $this->calculateCalendarDays($startWeek, $holidayCount) . 'D'));
            }
        }

        return $resultDateTime->format('d/m/Y H:i:s');
    }

    private function calculateCalendarDays($dayOfWeek, $days)
    {
        $firstPart = $dayOfWeek;

        if ($days < 0) {
            if ($firstPart == 6) {
                $firstPart = 0;
            }

            if (abs($days) < $firstPart) {
                return $days;
            }

            $weeks = intval((abs($days) - $firstPart) / 5) + 1;

            $days += $weeks * (-2);

            if ($dayOfWeek == 6) {
                $days++;
            }
        } else {
            if ($firstPart != 0) {
                $firstPart = 6 - $firstPart;
            }

            if ($days < $firstPart) {
                return $days;
            }

            $addWeeks = intval(($days - $firstPart) / 5) + 1;

            $days += $addWeeks * 2;

            if ($dayOfWeek == 0) {
                $days--;
            }
        }

        return $days;
    }

    private function checkHolidays(array &$publicHolidayArray, \DateTime &$startDateTime, \DateTime &$endDateTime)
    {
        $holidayCount = 0;

        if ($startDateTime > $endDateTime) {
            $start = clone $endDateTime;
            $end   = clone $startDateTime;

            while ($start < $end) {
                if (in_array($start->format('Y-m-d'), $publicHolidayArray)) {
                    $holidayCount++;
                }

                $start->add(new \DateInterval('P1D'));
            }
        } else {
            $start = clone $startDateTime;
            $end   = clone $endDateTime;

            while ($start <= $end) {
                if (in_array($start->format('Y-m-d'), $publicHolidayArray)) {
                    $holidayCount++;
                }

                $start->add(new \DateInterval('P1D'));
            }
        }

        return $holidayCount;
    }
}
