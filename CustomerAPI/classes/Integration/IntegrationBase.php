<?php

namespace FS\Integration;

use FS\Common\NZLumberjack\Logger;
use FS\Common\NZLumberjack\LoggerMDC;
use FS\Database\Manager;
use FS\Integration\Configuration\FeatureConfig;

abstract class IntegrationBase
{
    protected $log;
    protected $customer;
    protected $feature;
    protected $dbManager;
    protected $configuration;

    function __construct($customer, $feature, Manager $dbManager)
    {
        $this->log = Logger::getLogger('Webhook.' . $feature);

        LoggerMDC::put('Customer', $customer);

        $this->customer      = $customer;
        $this->feature       = $feature;
        $this->dbManager     = $dbManager;
        $this->configuration = new FeatureConfig($customer, $feature, $this->dbManager->getMasterConnection());
    }

    public abstract function execute(array $reportIt);
}