<?php

namespace FS\Integration\Handler;

use FS\Integration\Configuration\FeatureConfig;

class ConfirmWriter implements IConfirmWriter
{
    const RETRY_LIMIT = 3;
    private $client;
    private $location;

    public function __construct(FeatureConfig $config)
    {
        $this->location = $config->get('confirm', 'serviceLocation');
    }

    private function call(string $functionName, array $arguments)
    {
        if (!isset($this->client)) {
            $this->client = new \SoapClient(__DIR__ . '/../../../config/confirm.wsdl', [
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                'location' => $this->location
            ]);
        }

        $retry  = 0;
        $result = null;

        while (true) {
            try {
                $result = $this->client->__soapCall($functionName, $arguments);

                break;
            } catch (\Exception $e) {
                $retry++;

                if ($retry > self::RETRY_LIMIT) {
                    throw $e;
                }
            }
        }

        return $result;
    }

    public function processOperations(string $request): string
    {
        $result = $this->call('ProcessOperations', [['any' => $request]]);

        return $result->any;
    }
}