<?php

namespace FS\Integration\Handler;

interface IConfirmWriter
{
    public function processOperations(string $request): string;
}