<?php

namespace FS\Integration\ReportIt;

use FS\Common\NZLumberjack\Logger;
use FS\Common\NZLumberjack\LoggerMDC;
use FS\Integration\IntegrationBase;
use FS\SolutionsCustomerAPIBase;

class Controller extends SolutionsCustomerAPIBase
{
    private $log;

    function __construct()
    {
        parent::__construct();

        $configFile = __DIR__ . '/../../config/lumberjack.xml';

        if (file_exists($configFile)) {
            Logger::configure($configFile);
        }

        $this->log = Logger::getLogger('Webhook');

        LoggerMDC::put('Customer', $this->getCustomer());
    }

    public function webhook()
    {
        try {
            if (!$this->isAuthenticated()) {
                return $this->unauthorized();
            }

            $entity = $this->json();

            LoggerMDC::put('Reference', $entity['casedetails']['case_ref']);

            $this->log->debug('Raw Request: ' . json_encode($entity));

            $issueType  = $entity['casedetails']['issuetype'];
            $connection = $this->dbManager->getCustomerConnection();
            $stmt       = $connection->prepare('SELECT `info_statements`, `feature` 
                                               FROM `webhook_router` 
                                               WHERE `issue_type` = :issueType
                                               ORDER BY `info_statements` DESC');

            $stmt->execute(['issueType' => $issueType]);

            $feature = null;

            while ($row = $stmt->fetch()) {
                if (empty($row['info_statements']) || stripos($entity['casedetails']['infostatements'], $row['info_statements']) !== false) {
                    $feature = $row['feature'];
                    break;
                }
            }

            if (empty($feature)) {
                return $this->warnAndResult('No route found');
            }

            $namespace = 'FS\Integration\Reportit\\' . ucfirst(strtolower($feature));

            if (!class_exists($namespace)) {
                return $this->warnAndResult('Integration feature does not exist for ' . $feature);
            }

            $integration = new $namespace($this->getCustomer(), $this->dbManager);

            if (!$integration instanceof IntegrationBase) {
                return $this->warnAndResult($namespace . ' does not extend IntegrationBase');
            }

            $result = $integration->execute($entity);

            return $this->parseResult($result);
        } catch (\Exception $e) {
            $this->log->error('Error occurred', $e);

            throw $e;
        }
    }

    private function warnAndResult($message)
    {
        $this->log->warn($message);

        return $this->parseResult($message);
    }
}