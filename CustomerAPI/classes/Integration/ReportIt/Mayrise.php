<?php

namespace FS\Integration\ReportIt;

use FS\Database\Manager;
use FS\Integration\IntegrationBase;

class Mayrise extends IntegrationBase
{
    public function __construct($customer, Manager $dbManager)
    {
        parent::__construct($customer, 'Mayrise', $dbManager);
    }

    public function execute(array $reportIt)
    {
        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('INSERT INTO `mayrise_case` (`case_ref`) VALUES (:caseRef) ON DUPLICATE KEY UPDATE `case_ref`=`case_ref`');
        $result     = $stmt->execute(['caseRef' => $reportIt['casedetails']['case_ref']]);

        $this->log->debug('Save reference in Mayrise table');

        return $result;
    }
}