<?php

namespace FS\Integration\ReportIt;

use FS\Database\Manager;
use FS\Integration\Handler\ConfirmWriter;
use FS\Integration\Handler\IConfirmWriter;
use FS\Integration\IntegrationBase;

class Confirm extends IntegrationBase
{
    private $api;

    public function __construct($customer, Manager $dbManager, IConfirmWriter $confirmWriter = null)
    {
        parent::__construct($customer, 'Confirm', $dbManager);

        $this->api = $confirmWriter ?? new ConfirmWriter($this->configuration);
    }

    public function execute(array $reportIt)
    {
        $confirmEnquiry = $this->mapToConfirmRequest($reportIt);

        $this->log->debug('Confirm Enquiry Request: ' . $confirmEnquiry);

        $result = $this->api->processOperations($confirmEnquiry);

        $this->log->debug('Confirm Enquiry Response: ' . $result);

        $confirmEnquiryReference = $this->getConfirmEnquiryReference($result);
        $connection              = $this->dbManager->getCustomerConnection();
        $stmt                    = $connection->prepare('INSERT INTO `confirm_enquiry_case` (`case_ref`, `enquiry_number`) VALUES (:caseRef, :enquiryNumber) ON DUPLICATE KEY UPDATE `case_ref`=`case_ref`');

        $stmt->execute(['caseRef' => $reportIt['casedetails']['case_ref'], 'enquiryNumber' => $confirmEnquiryReference]);

        $this->log->debug('Save reference in Confirm table');

        return $confirmEnquiryReference;
    }

    private function mapToConfirmRequest($reportIt)
    {
        $caseDetails    = $reportIt['casedetails'];
        $location       = $caseDetails['location'];
        $customer       = $caseDetails['customer'];
        $photoFileNames = explode(',', $caseDetails['photofilenames']);
        $photoPath      = $caseDetails['photopath'];
        $serviceCode    = '';
        $subjectCode    = '';

        $connection = $this->dbManager->getCustomerConnection();
        $stmt       = $connection->prepare('SELECT `info_statements`, `service_code`, `subject_code`
                                     FROM `webhook_confirm_router` 
                                     WHERE `issue_type` = :issueType
                                     ORDER BY `info_statements` DESC');

        $stmt->execute(['issueType' => $caseDetails['issuetype']]);

        while ($row = $stmt->fetch()) {
            if (empty($row['info_statements']) || stripos($caseDetails['infostatements'], $row['info_statements']) !== false) {
                $serviceCode = $row['service_code'];
                $subjectCode = $row['subject_code'];
                break;
            }
        }

        $writer = new \XMLWriter();

        $writer->openMemory();
        $writer->startElement('Request');

        $writer->startElement('Authentication');
        $writer->writeElement('Username', $this->configuration->get('confirm', 'serviceUsername'));
        $writer->writeElement('Password', $this->configuration->get('confirm', 'servicePassword'));
        $writer->writeElement('DatabaseId', $this->configuration->get('confirm', 'serviceDatabaseID'));
        $writer->endElement(); // Authentication

        $writer->startElement('Operation');
        $writer->startElement('NewEnquiry');

        $writer->writeElement('EnquiryNumber', 1);
        $writer->writeElement('ExternalSystemNumber', 3);
        $writer->writeElement('ExternalSystemReference', $caseDetails['case_ref']);
        $writer->writeElement('ServiceCode', $serviceCode);
        $writer->writeElement('EnquiryMethodCode', 'AMEY');
        $writer->writeElement('CustomerTypeCode', 'MOPU');
        $writer->writeElement('PointOfContactCode', 'SELF');
        $writer->writeElement('SubjectCode', $subjectCode);
        $writer->writeElement('EnquiryDescription', $caseDetails['description'] . ' ' . $caseDetails['infostatements']);
        $writer->writeElement('EnquiryLocation', $location['locationSpecifics'] . ' near:' . $location['address']);
        $writer->writeElement('SiteCode', $location['usrn']);
        $writer->writeElement('EnquiryX', $location['easting']);
        $writer->writeElement('EnquiryY', $location['northing']);

        $writer->startElement('EnquiryCustomer');
        $writer->writeElement('CustomerTitle', $customer['Title']);
        $writer->writeElement('CustomerSurname', $customer['Surname'] ?? 'N/A');
        $writer->writeElement('CustomerForename', $customer['First_Name']);
        $writer->writeElement('CustomerPhone', '');
        $writer->writeElement('CustomerMobilePhone', '');
        $writer->writeElement('CustomerFax', '');
        $writer->writeElement('EnquiryMethodCode', 'AMEY');
        $writer->writeElement('CustomerTypeCode', 'MOPU');
        $writer->writeElement('PointOfContactCode', 'SELF');
        $writer->writeElement('CustomerEmail', $customer['Email_Address']);
        $writer->writeElement('CustomerStreetDesc', $customer['street']);
        $writer->writeElement('CustomerLocalityName', $customer['locality']);
        $writer->writeElement('CustomerTownName', $customer['town']);
        $writer->writeElement('CustomerCountyName', $customer['county']);
        $writer->writeElement('CustomerPostCode', $customer['postcode']);
        $writer->writeElement('AutomatedContactMethod', '');
        $writer->endElement(); // EnquiryCustomer

        foreach ($photoFileNames as $photoFileName) {
            if (!empty($photoFileName)) {
                $writer->startElement('EnquiryDocument');
                $writer->writeElement('DocumentLocation', $this->configuration->get('webHook', 'attachmentPath') . $photoPath . '/' . $photoFileName);
                $writer->endElement(); // EnquiryDocument
            }
        }

        $writer->endElement(); // NewEnquiry
        $writer->endElement(); // Operation

        $writer->endElement(); // Request

        return $writer->outputMemory();
    }

    private function getConfirmEnquiryReference($confirmResult)
    {
        $confirmEnquiryReference = '';
        $xmlReader               = new \XMLReader();

        $xmlReader->xml($confirmResult);

        while ($xmlReader->read()) {
            if ($xmlReader->name == 'Reason' && $xmlReader->nodeType == \XMLReader::ELEMENT) {
                throw new \Exception($xmlReader->readString());
            } else if ($xmlReader->name == 'EnquiryNumber' && $xmlReader->nodeType == \XMLReader::ELEMENT) {
                $confirmEnquiryReference = $xmlReader->readString();
            }
        }

        $xmlReader->close();

        return $confirmEnquiryReference;
    }
}