<?php

namespace FS\Platform;

use FS\Common\Exception\InvalidParameterException;

class Controller extends \FS\SolutionsCustomerAPIBase
{
    public function renderIndex()
    {
        header('WWW-Authenticate: Basic');

        if (!$this->isBasicAuthenticated()) {
            return $this->unauthorized('html');
        }

        $response = $this->renderHeader();
        $response .= $this->renderFooter();

        header('Content-Type: text/html');

        return $response;
    }

    public function renderDefault()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized('html');
        }

        $input      = $_GET;
        $validation = $this->required($input, 'type', 'value');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message'], 'html');
        }

        $LLPG     = new \FS\LLPG\Controller($this->dbManager);
        $response = '';

        try {
            // Extra error handling for empty variables
            if (empty($input['value'])) {
                throw new InvalidParameterException('Value must be set');
            }

            switch (strtolower($input['type'])) {
                case 'postcode':
                    $result = $LLPG->listAddressesByPostcode($input['value']);
                    break;
                case 'uprn':
                    $result = $LLPG->listAddressesByUPRN($input['value']);
                    break;
                default:
                    throw new InvalidParameterException('Type must be either postcode or uprn');
                    break;
            }
            
            $response .= $this->renderTable((object)$result);
        } catch (\Exception $e) {
            $response .= 'Error: ' . $e->getMessage();
        }

        http_response_code(200);
        header('Content-Type: text/html');

        return $response;
    }

    private function renderHeader()
    {
        return "
            <html>
                <head>
                    <title>Customer Platform</title>
                </head>
                <body>
                    <div id='header'>
                        <h1>Customer Platform</h1>
                        <table>
                            <tr>
                                <td><strong>POSTCODE:&nbsp;</strong></td>
                                <td><input id='postcode' type='text' /></td>
                                <td><button id='btnPostcode' onclick='render(\"postcode\");'>Search</button></td>
                            </tr>
                            <tr>
                                <td><strong>UPRN:</strong></td>
                                <td><input id='uprn' type='text' /></td>
                                <td><button id='btnUPRN' onclick='render(\"uprn\");'>Search</button></td>
                            </tr>
                        </table>
                    </div>
                    <div id='content' style='margin-top: 15px; margin-left: 5px; margin-right: 5px; margin-bottom: 5px; width: calc(100% - 10px); word-break: break-all; '>

        ";
    }

    private function renderTable($data)
    {
        $response = '';

        if (count($data->Response) > 0) {
            $response = '<table border="1" cellspacing="0" cellpadding="2">';

            // Do header
            $response .= '<tr style="word-break: normal;">';

            foreach ($data->Response[0] as $db_key => $db_value) {
                $response .= '<th>' . strtoupper($db_key) . '</th>';
            }

            $response .= '</tr>';

            // Do content
            foreach ($data->Response as $db_response) {
                $response .= '<tr style="vertical-align: text-top;">';

                foreach ($db_response as $db_key => $db_value) {
                    $response .= '<td>' . $db_value . '</td>';
                }

                $response .= '</tr>';
            }

            $response .= '</table>';
        } else {
            $response .= 'No address found';
        }

        return $response;
    }

    private function renderFooter()
    {
        return "
                    </div>
                    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js' type='text/javascript'></script>
                    <script type='text/javascript'>
                        function render(type) {
                            if (!$.trim($('#' + type).val()).length) {
                                alert('Value required for ' + type);
                                return;
                            }

                            $.get( 'default?type=' + type + '&value=' + $('#' + type).val(), function(data){
                                $('#content').html(data);
                            });
                        }
                    </script>
                </body>
            </html>
        ";
    }
}
