<?php

namespace FS\Logging;

class FTPLogReader extends \FS\SolutionsCustomerAPIBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFTPLogs()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $fromDate = $this->get('fromDate');
        $toDate   = $this->get('toDate');
        $response = $this->get('response');
        $fileName = $this->get('fileName');
        $offset   = $this->get('offset', 0);
        $limit    = $this->get('limit', 30);

        $masterConnection = $this->dbManager->getMasterConnection();
        $masterConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $masterConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $searchString = '';
        $parameters   = [];

        $searchString              = $this->appendSearch($searchString, 'customer_id = :customer_id');
        $parameters['customer_id'] = $this->dbManager->getCustomerId();

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'start_time >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'start_time <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($response) || $response == 'All') {
            $searchString           = $this->appendSearch($searchString, 'response LIKE :response');
            $parameters['response'] = '%' . $response . '%';
        }

        if (!empty($fileName) || $fileName == 'All') {
            $searchString           = $this->appendSearch($searchString, 'file_name = :file_name');
            $parameters['file_name'] =  $fileName;
        }

        $stmt = $masterConnection->prepare('SELECT COUNT(1) FROM ftp_log ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql                  = 'SELECT start_time, end_time, response, file_name FROM ftp_log ' . $searchString . ' ORDER BY id DESC LIMIT :offset,:limit';
        $parameters['offset'] = $offset;
        $parameters['limit']  = $limit;

        $stmt = $masterConnection->prepare($sql);
        $stmt->execute($parameters);
        $result = $stmt->fetchAll();

        $response =  [
            'TotalRowCount' => $rowCount,
            'Rows'          => $result
        ];
        
        return $response;
    }

    protected function appendSearch($searchString, $query)
    {
        return $searchString . (strpos($searchString, 'WHERE') === false ? ' WHERE ' : ' AND ') . $query;
    }

    protected function get($parameter, $default = null)
    {
        return isset($_GET[$parameter]) ? $_GET[$parameter] : $default;
    }
}
