<?php

namespace FS\Holiday;

use FS\Common\Exception\DuplicateDataException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;

class Controller extends \FS\SolutionsCustomerAPIBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getHoliday()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $fromDate   = $this->post('fromDate');
        $toDate     = $this->post('toDate');
        $firmstepId = $this->post('firmstepId');
        $calendar   = $this->post('calendar');
        $notes      = $this->post('notes');
        $offset     = $this->post('offset', 0);
        $limit      = $this->post('limit', 30);

        $customerConnection = $this->dbManager->getCustomerConnection();
        $customerConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $customerConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $customerConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'holiday >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'holiday <= :toDate');
            $parameters['toDate'] = $toDate;
        }

        if (!empty($firmstepId)) {
            $searchString             = $this->appendSearch($searchString, 'firmstep_id = :firmstepId');
            $parameters['firmstepId'] = $firmstepId;
        }

        if (!empty($calendar)) {
            $searchString           = $this->appendSearch($searchString, 'calendar LIKE :calendar');
            $parameters['calendar'] = '%' . $calendar . '%';
        }

        if (!empty($notes)) {
            $searchString        = $this->appendSearch($searchString, 'notes LIKE :notes');
            $parameters['notes'] = '%' . $notes . '%';
        }

        $stmt = $customerConnection->prepare('SELECT COUNT(1) FROM `holiday` ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sqlForList           = 'SELECT * FROM `holiday` ' . $searchString . ' ORDER BY id DESC LIMIT :offset,:limit';
        $parameters['offset'] = $offset;
        $parameters['limit']  = $limit;

        $stmt = $customerConnection->prepare($sqlForList);
        $stmt->execute($parameters);
        $result = $stmt->fetchAll();

        $response = [
            'TotalRowCount' => $rowCount,
            'Rows'          => $result
        ];

        return $response;
    }

    public function addHoliday()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        try {
            $holiday  = $this->post('holiday');
            $notes    = $this->post('notes');
            $calendar = $this->post('calendar');

            if (empty($holiday) || empty($calendar)) {
                throw new InvalidParameterException('Parameters {holiday}, {calendar} are required');
            }

            $customerConnection = $this->dbManager->getCustomerConnection();
            $customerConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $customerConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $customerConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $sqlCheck = "SELECT COUNT(1) AS `total` FROM `holiday` WHERE `holiday` = :holiday AND `calendar` = :calendar";
            $stmt     = $customerConnection->prepare($sqlCheck);
            $stmt->execute(['holiday' => $holiday, 'calendar' => $calendar]);

            if ($row = $stmt->fetch()) {
                if ($row['total'] > 0) {
                    throw new DuplicateDataException('This holiday already exists');
                }
            }

            $sqlInsert = "INSERT INTO `holiday`(`holiday`, `calendar`, `notes`) VALUES(:holiday, :calendar, :notes)";
            $stmt      = $customerConnection->prepare($sqlInsert);
            $stmt->execute(['holiday' => $holiday, 'calendar' => $calendar, 'notes' => $notes]);
            $response = ['id' => $customerConnection->lastInsertId()];

            return $response;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function editHoliday()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $id       = $this->post('id');
        $holiday  = $this->post('holiday');
        $calendar = $this->post('calendar');
        $notes    = $this->post('notes');

        if (empty($id)) {
            throw new InvalidParameterException('Parameters {id} is required');
        }

        if (is_null($holiday) && is_null($calendar) && is_null($notes)) {
            throw new InvalidParameterException('You should pass in at least one field to update holiday');
        }

        $params = '';
        $values = ['id' => $id];

        if (!is_null($holiday)) {
            if (!empty($holiday)) {
                $params .= empty($params) ? ('`holiday` = :holiday') : (', `holiday` = :holiday');
                $values['holiday'] = $holiday;
            } else {
                throw new InvalidParameterException('Parameter {holiday} cannot be empty');
            }
        }

        if (!is_null($calendar)) {
            if (!empty($calendar)) {
                $params .= empty($params) ? ('`calendar` = :calendar') : (', `calendar` = :calendar');
                $values['calendar'] = $calendar;
            } else {
                throw new InvalidParameterException('Parameter {calendar} cannot be empty');
            }
        }

        if (!is_null($notes)) {
            $params .= empty($params) ? ('`notes` = :notes') : (', `notes` = :notes');
            $values['notes'] = $_POST['notes'];
        }

        $customerConnection = $this->dbManager->getCustomerConnection();
        $customerConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $customerConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $customerConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $sqlCheck = "SELECT `firmstep_id` FROM `holiday` WHERE `id` = :id";
        $stmt     = $customerConnection->prepare($sqlCheck);

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('Failed to execute ' . $sqlCheck);
        }

        $row = $stmt->fetch();
        if (!empty($row['firmstep_id'])) {
            throw new InvalidParameterException('Error: You cannot edit holiday with firmstep_id');
        }

        // Check if the holiday is unique
        $sqlUniqueCheck = "SELECT COUNT(1) AS `total` FROM `holiday` WHERE `holiday` = :holiday AND `calendar` = :calendar AND `id` != :id";
        $stmt           = $customerConnection->prepare($sqlUniqueCheck);
        $stmt->execute(['holiday' => $holiday, 'calendar' => $calendar, 'id' => $id]);

        if ($row = $stmt->fetch()) {
            if ($row['total'] > 0) {
                throw new DuplicateDataException('This holiday already exists');
            }
        }

        $sqlUpdate = "UPDATE `holiday` SET " . $params . " WHERE `id` = :id";
        $stmt      = $customerConnection->prepare($sqlUpdate);

        if ($stmt->execute($values) === false) {
            throw new PDOExecutionException('Failed to execute ' . $sqlUpdate);
        }

        return 'Success';
    }

    public function deleteHoliday()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $id = $this->post('id');

        if (empty($id)) {
            throw new InvalidParameterException('Parameters {id} is required');
        }

        $customerConnection = $this->dbManager->getCustomerConnection();
        $customerConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $customerConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

        $sqlCheck = "SELECT `firmstep_id` FROM `holiday` WHERE `id` = :id";
        $stmt     = $customerConnection->prepare($sqlCheck);

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('Failed to execute sql "' . $sqlCheck . '"');
        }

        $row = $stmt->fetch();
        if (!empty($row['firmstep_id'])) {
            throw new InvalidParameterException('Error: You cannot delete holiday with firmstep_id');
        }

        $sqlDelete = "DELETE FROM `holiday` WHERE `id` = :id";
        $stmt      = $customerConnection->prepare($sqlDelete);
        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('Failed to execute ' . $sqlDelete);
        }

        return 'Success';
    }

    protected function post($parameters, $default = null)
    {
        if (is_numeric($default)) {
            return (isset($_POST[$parameters]) && is_numeric($_POST[$parameters])) ? intval($_POST[$parameters]) : $default;
        } else {
            return (isset($_POST[$parameters]) && !is_null($_POST[$parameters])) ? $_POST[$parameters] : $default;
        }
    }

    protected function appendSearch($searchString, $query)
    {
        return $searchString . (strpos($searchString, 'WHERE') === false ? ' WHERE ' : ' AND ') . $query;
    }

}