<?php

namespace FS\ReportIt;

class Controller extends \FS\SolutionsCustomerAPIBase
{   
    public function checkMerge()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $input      = $_GET;
        $validation = $this->required($input, 'mergedbid');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);

        try {
            $result = $database->checkMerge($input);
        } catch (\Exception $e) {
            return 'Error: ' . $e->getMessage();
        }

        return $this->parseResult($result);
    }

    public function checkStatusChange()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $input      = $_GET;
        $validation = $this->required($input, 'caseref', 'activestatus');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);

        try {
            $result = $database->checkStatusChange($input);
        } catch (\Exception $e) {
            return 'Error: ' . $e->getMessage();
        }

        return $this->parseResult($result);
    }

    public function checkTaskId()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $input      = $_GET;
        $validation = $this->required($input, 'mergedbid');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);

        try {
            $result = $database->checkTaskId($input);
        } catch (\Exception $e) {
            return 'Error: ' . $e->getMessage();
        }

        return $this->parseResult($result);
    }

    public function getBulkSelected()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $input      = $_GET;
        $validation = $this->required($input, 'selected');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);

        try {
            $result = $database->getBulkSelected($input);
        } catch (\Exception $e) {
            return 'Error: ' . $e->getMessage();
        }


        return $this->parseResult($result);
    }

    public function saveDBId()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $json       = $this->json('Request');
        $validation = $this->required($json, 'dbId', 'caseRef');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);
        $result   = $database->saveDBId($json);

        return $this->parseResult($result);
    }

    public function saveIssue()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $json       = $this->json('Request');
        $validation = $this->required($json, 'caseRef', 'nearestUPRN', 'nearestUSRN', 'pinpointMap', 'action',
            'issueTypeLabel', 'summary', 'description', 'incidentTypeRadius', 'easting', 'northing', 'newEmail', 'team',
            'mergeRef', 'nearestAddress', 'activeStatus', 'currentStatus', 'infoStatements', 'mapAssetId', 'mergeDbId', 'teamName');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);
        $result   = $database->saveIssue($json);

        return $this->parseResult($result);
    }

    public function savePrint()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $json = $this->json('Request');

        $validation = $this->required($json, 'printMap', 'issueTypeLabel', 'caseRef', 'contactDetailsTitle',
            'contactDetailsFirstName', 'contactDetailsSurname', 'contactDetailsFlat', 'contactDetailsHouse',
            'contactDetailsStreet', 'contactDetailsLocality', 'contactDetailsTown', 'contactDetailsCounty',
            'contactDetailsPostcode', 'infoStatements', 'description', 'nearestAddress', 'team', 'pinpointLoc',
            'caseCreatedDt', 'deadlineDate');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);
        $result   = $database->savePrint($json);

        return $this->parseResult($result);
    }

    public function saveSubscriber()
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $json = $this->json('Request');

        $validation = $this->required($json, 'caseRef', 'subscriber');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\ReportIt($this->dbManager);
        $result   = $database->saveSubscriber($json);

        return $this->parseResult($result);
    }
}
