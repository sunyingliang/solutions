<?php

namespace FS\Database;

use FS\Common\Exception\ModuleDisabledException;

class LLPG extends Manager
{
    public function __construct($dbManager)
    {
        $this->customerId         = $dbManager->customerId;
        $this->customerDBUsername = $dbManager->customerDBUsername;
        $this->customerDBPassword = $dbManager->customerDBPassword;
        $this->customerDBHost     = $dbManager->customerDBHost;
        $this->customerDBDatabase = $dbManager->customerDBDatabase;

        $this->checkEnabled();
    }

    private function checkEnabled()
    {
        $connection = $this->getMasterConnection();

        $sql = "SELECT 1 FROM customer WHERE `enabled` = 1 AND `id` = :customerId LIMIT 1";
        $stmt = $connection->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('Failed determining LLPG module status [1/2]');
        }

        if ($stmt->execute(['customerId' => $this->getCustomerId()]) === false) {
            throw new PDOCreationException('Failed determining LLPG module status [2/2]');
        }

        $result = $stmt->fetch();
        
        if ($result === false) {
            throw new ModuleDisabledException('LLPG module is not enabled');
        }

        $connection = null;
    }

    public function listAddressesByPostcode($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare("
                SELECT
                    CONCAT(TRIM(CONCAT(sao, ' ', pao)), ' ', street_name, ', ', town_name, ', ', postcode) AS display,
                    uprn AS name,
                    sao AS flat,
                    pao AS house,
                    street_name AS street,
                    town_name AS town,
                    county_name AS county,
                    postcode,
                    post_town,
                    uprn,
                    ward,
                    lng,
                    lat,
                    locality_name AS locality,
                    usrn,
                    street_name AS order0,
                    pao AS order1,
                    sao AS order2,
                    'false' as manual
                FROM llpgcache
                WHERE 
                    (
                        (REPLACE(postcode, ' ','') = REPLACE(?,' ',''))
                            OR
                        (concat(trim(concat(sao,' ',pao)),' ',street_name) like concat('%',?,'%'))
                    )
                    AND LENGTH(REPLACE(?,' ','')) > 5
                    AND pao != 'Street Record'
        ");

        $result = $stmt->execute([$parameters['postcode'], $parameters['postcode'], $parameters['postcode']]);

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    // Unique property reference number
    public function listAddressesByUPRN($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare("
            SELECT 
                sao AS flat,
                pao AS house,
                street_name AS street,
                town_name AS town,
                '' AS county,
                postcode,
                post_town,
                uprn,
                ward,
                lng,
                lat,
                locality_name AS locality,
                usrn
           FROM llpgcache
           WHERE uprn = :uprn
                AND :uprn <> ''
           LIMIT 1
        ");

        $result = $stmt->execute($this->boundInputParameters($parameters, ['uprn'], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }
}
