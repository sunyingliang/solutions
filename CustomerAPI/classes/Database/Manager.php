<?php

namespace FS\Database;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOFetchException;

class Manager
{
    public $customerId;
    public $customerName;
    public $customerDBUsername;
    public $customerDBPassword;
    public $customerDBHost;
    public $customerDBDatabase;
    public $masterPDO;
    public $customerPDO;

    function __destruct()
    {
        $this->masterPDO   = null;
        $this->customerPDO = null;
    }

    public function authenticate($username = '', $password = '', $token = '')
    {
        $authenticated = false;

        // Validate username, password, customername
        if (empty($username) && empty($password) && empty($token)) {
            return false;
        }

        // Validate host, user, pass details defined
        if (!defined('DB_HOST') || !defined('DB_USER') || !defined('DB_PASS') || !defined('DB_TABLE')) {
            return false;
        }

        // Check user exists, if not fail auth
        $masterConnection = $this->getMasterConnection();

        if (!empty($username) && !empty($password)) {
            // Note: Is basic auth
            $sql = "SELECT `id`, `name`, `db_host`, `db_user`, `db_password`, `db_database` 
                    FROM  " . DB_TABLE . "
                    WHERE `name` = :name AND `http_pass` = :http_pass";

            $values = ['name' => $username, 'http_pass' => $password];
        } else {
            // Note: Is token auth
            $sql = "SELECT `id`, `name`, `db_host`, `db_user`, `db_password`, `db_database` 
                    FROM  " . DB_TABLE . "
                    WHERE `name` = :name AND `token` = :token";

            $values = ['name' => $username, 'token' => $token];
        }

        $stmt = $masterConnection->prepare($sql);

        $stmt->execute($values);

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!$row) {
            return false;
        }

        $this->customerId         = $row['id'];
        $this->customerName       = $row['name'];
        $this->customerDBUsername = $row['db_user'];
        $this->customerDBPassword = $row['db_password'];
        $this->customerDBHost     = $row['db_host'];
        $this->customerDBDatabase = $row['db_database'];

        try {
            $customerConnection = $this->getCustomerConnection();

            $authenticated = true;
        } catch (\Exception $e) {
            $authenticated = false;
        }

        return $authenticated;
    }

    public function getCustomerToken($username, $password)
    {
        $token = false;

        if (empty($username) || empty($password)) {
            return false;
        }

        if (!defined('DB_HOST') || !defined('DB_USER') || !defined('DB_PASS') || !defined('DB_TABLE')) {
            return false;
        }

        // Check user exists, if not fail auth
        $masterConnection = $this->getMasterConnection();

        $sql = "SELECT `token` 
                FROM  " . DB_TABLE . "
                WHERE `name` = :name AND `http_pass` = :http_pass";

        $values = [];

        $values['name']      = $username;
        $values['http_pass'] = $password;

        $stmt = $masterConnection->prepare($sql);

        $stmt->execute($values);

        if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $token = $row['token'];
        }

        return $token;
    }

    public function getMasterConnection()
    {
        if (!isset($this->masterPDO)) {
            if (!defined('DB_HOST') || !defined('DB_USER') || !defined('DB_PASS') || !defined('DB_TABLE')) {
                throw new InvalidParameterException('Required config variable undefined');
            }

            $this->masterPDO = new \PDO(DB_HOST, DB_USER, DB_PASS);
        }

        if (!$this->masterPDO) {
            throw new PDOCreationException('Could not connect to master database');
        }

        return $this->masterPDO;
    }

    public function getCustomerConnection()
    {
        if (!isset($this->customerPDO)) {
            if (!isset($this->customerId) || !isset($this->customerDBUsername) || !isset($this->customerDBPassword) || !isset($this->customerDBHost) || !isset($this->customerDBDatabase)) {
                throw new InvalidParameterException('Required customer database detail undefined');
            } else if (empty($this->customerId) || empty($this->customerDBUsername) || empty($this->customerDBPassword) || empty($this->customerDBHost) || empty($this->customerDBDatabase)) {
                throw new InvalidParameterException('Required customer database detail invalid');
            }

            $this->customerPDO = new \PDO('mysql:host=' . $this->customerDBHost . ';dbname=' . $this->customerDBDatabase, $this->customerDBUsername, $this->customerDBPassword);
            $this->customerPDO->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        }

        if (!$this->customerPDO) {
            throw new PDOCreationException('Could not connect to customer database');
        }

        return $this->customerPDO;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    protected function boundInputParameters(
        $parameters,
        $parameterNames,
        $lowerCase = false
    )
    {
        $inputParameters = [];

        foreach ($parameterNames as $parameterName) {
            $inputParameters[':' . $parameterName] = $parameters[$lowerCase ? strtolower($parameterName) : $parameterName];
        }

        return $inputParameters;
    }
}
