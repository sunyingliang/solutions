<?php

namespace FS\Database;

use FS\Common\Exception\ModuleDisabledException;

class ReportIt extends Manager
{
    public function __construct($dbManager)
    {
        $this->customerId         = $dbManager->customerId;
        $this->customerDBUsername = $dbManager->customerDBUsername;
        $this->customerDBPassword = $dbManager->customerDBPassword;
        $this->customerDBHost     = $dbManager->customerDBHost;
        $this->customerDBDatabase = $dbManager->customerDBDatabase;

        $this->checkEnabled();
    }

    private function checkEnabled()
    {
        $connection = $this->getMasterConnection();

        $sql = "SELECT 1 FROM customer WHERE `enable_reportit` = 1 AND `id` = :customerId LIMIT 1";
        $stmt = $connection->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('Failed determining ReportIt module status [1/2]');
        }

        if ($stmt->execute(['customerId' => $this->getCustomerId()]) === false) {
            throw new PDOCreationException('Failed determining ReportIt module status [2/2]');
        }

        $result = $stmt->fetch();
        
        if ($result === false) {
            throw new ModuleDisabledException('ReportIt module is not enabled');
        }

        $connection = null;
    }

    public function checkMerge($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'SELECT CASE WHEN EXISTS('
            . 'SELECT `taskid` '
            . 'FROM reportit_issue '
            . "WHERE reference = :mergedBid and status = 'open' "
            . ') THEN 1 ELSE 0 END AS mergedBidok FROM dual');

        $result = $stmt->execute($this->boundInputParameters($parameters, ['mergedBid'], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function checkStatusChange($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'SELECT CASE WHEN EXISTS('
            . 'SELECT 0 FROM `reportit_issue` '
            . "WHERE reference = :caseRef AND :activeStatus <> '' AND usedstatus LIKE CONCAT('%',:activeStatus,'%') "
            . ") THEN 'No' else 'Yes' END AS status_is_new "
            . 'FROM dual');

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'caseRef',
            'activeStatus'
        ], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function checkTaskId($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare('SELECT `taskid` AS mergedBid FROM reportit_issue WHERE taskid = :mergedBid');

        $result = $stmt->execute($this->boundInputParameters($parameters, ['mergedBid'], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function deadline($parameters)
    {
        $connection = $this->getCustomerConnection();

        $start = $parameters['start'];

        if ($start > $parameters['cutoffTime']) {
            $start = $parameters['cutoffTime'];
        }

        $startDate      = new \DateTime($start);
        $deadlineDate   = (new \DateTime($start))->add(new \DateInterval('P' . abs($parameters['deadlineDays']) . 'D'));
        $warningDate    = (new \DateTime($start))->add(new \DateInterval('P' . abs($parameters['warningDays']) . 'D'));
        $result         = [];

        $stmt = $connection->prepare('SELECT COUNT(1) AS `holidays` FROM `holiday` WHERE `holiday` BETWEEN :start AND :end');

        if ($stmt->execute([
                'start' => $startDate->format('Y-m-d H:i:s:'),
                'end'   => $deadlineDate->format('Y-m-d H:i:s:'),
            ]) === false
        ) {
            return $stmt->errorInfo()[2];
        }

        $row                    = $stmt->fetch(\PDO::FETCH_ASSOC);
        $result['deadlinedate'] = $deadlineDate->add(new \DateInterval('P' . $row['holidays'] . 'D'))->format('Y-m-d H:i:s:');

        if ($stmt->execute([
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end'   => $warningDate->format('Y-m-d H:i:s')
            ]) === false
        ) {
            return $stmt->errorInfo()[2];
        }

        $row                   = $stmt->fetch(\PDO::FETCH_ASSOC);
        $result['warningdate'] = $warningDate->add(new \DateInterval('P' . $row['holidays'] . 'D'))->format('Y-m-d H:i:s:');

        return $result;
    }

    public function getBulkSelected($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'SELECT `taskid` as ref, `print` '
            . 'FROM reportit_issue '
            . "WHERE CONCAT(',',:selected,',') LIKE CONCAT('%,',taskid,',%')");

        $result = $stmt->execute($this->boundInputParameters($parameters, ['selected'], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function listSimilarActiveIssuesForList($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'SELECT `reference` AS name, '
                . "CONCAT(reference,' ', COALESCE(infoStatements,''),' ', COALESCE(activestatus,''),' ', COALESCE(activeattributes,''),' ', description,' [Near ',address,', first raised ',date_format(date_created,'%a %D %b %Y'),']') AS display, "
                . '`lat`, `lng` '
                . 'FROM reportit_issue '
                . 'WHERE issue_type = :issueTypeLabel'
                    . "AND status = 'open'"
                    . 'AND (reference <> :caseRef or :caseRef is null)'
                    . "AND (team = :bulkAssignedTo or :bulkAssignedTo is null or :assignedTo = '')"
                . 'ORDER BY `summary`'
                . 'LIMIT 100');

        $result = $stmt->execute($this->boundInputParameters($parameters,[
            'issueTypeLabel',
            'caseRef',
            'bulkAssignedTo',
            'assignedTo'
        ], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function listSimilarActiveIssuesForMap($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt   = $connection->prepare(
            'SELECT `reference` AS display, '
            . "CONCAT(reference,' ', COALESCE(infoStatements,''),' ', COALESCE(activestatus,''), ' [Near ',address, ', first raised ',date_format(date_created,'%a %D %b %Y'),']') AS name, "
            . '`lat`, `lng`, `activestatus`, `reference` AS mergeRef '
            . 'FROM reportit_issue '
            . 'WHERE issue_type = :issueTypeLabel '
                . "AND status = 'open'"
                . 'AND (reference <> :caseRef or :caseRef is null)'
                . "AND (CONCAT(',',:selected,',') like CONCAT('%,',taskid,',%') or :selected is null or :selected='')"
            . 'ORDER BY `summary`');

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'issueTypeLabel',
            'caseRef',
            'selected'
        ], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function listSimilarActiveIssuesForMerge($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'SELECT `reference` AS name, '
            . "CONCAT(reference,' ', coalesce(infoStatements,''),' ', coalesce(activestatus,''),' ', coalesce(activeattributes,''),' ', description, ' [Near ',address, ', first raised ',date_format(date_created,'%a %D %b %Y'),']') AS display, "
            . '`lat`, `lng` '
            . 'FROM reportit_issue '
            . 'WHERE issue_type=:issueTypeLabel '
                . "AND status='open' "
                . 'AND (reference<>:caseRef or :caseRef is null) '
                . "AND CONCAT(',',:selected,',') not like CONCAT('%,',taskid,',%') "
            . 'ORDER BY `summary`');

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'issueTypeLabel',
            'caseRef',
            'selected'
        ], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function listSubscribers($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            '(SELECT `email` AS subscriberemail FROM reportit_subscriber WHERE reference = :caseRef ORDER BY 1) '
            . 'UNION ALL '
            . "(select :newEmail AS subscriberemail FROM dual WHERE :newEmail <> '')");

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'caseRef',
            'newEmail'
        ], true));

        return $result ? $stmt->fetchAll() : $stmt->errorInfo()[2];
    }

    public function saveDBId($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt   = $connection->prepare('UPDATE reportit_issue SET taskid = :dbId WHERE reference = :caseRef');
        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'dbId',
            'caseRef'
        ]));

        return $result ? true : $stmt->errorInfo()[2];
    }

    public function saveIssue($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare('call reportit_updateissue(:caseRef, :nearestUPRN,:nearestUSRN,:pinpointMap,:action,:issueTypeLabel,:summary,:description,:incidentTypeRadius, :easting, :northing,:newEmail,:team,:mergeRef,:nearestAddress,:activeStatus,:currentStatus,:infoStatements, :mapAssetId, :mergeDbId, :teamName)');

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'caseRef',
            'nearestUPRN',
            'nearestUSRN',
            'pinpointMap',
            'action',
            'issueTypeLabel',
            'summary',
            'description',
            'incidentTypeRadius',
            'easting',
            'northing',
            'newEmail',
            'team',
            'mergeRef',
            'nearestAddress',
            'activeStatus',
            'currentStatus',
            'infoStatements',
            'mapAssetId',
            'mergeDbId',
            'teamName'
        ]));

        return $result ? true : $stmt->errorInfo()[2];
    }

    public function savePrint($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'UPDATE reportit_issue '
            . 'SET print=CONCAT(\'<div id="bulk" style="page-break-inside:avoid; width: 100%; overflow: hidden; font-family: arial, helvetica, sans-serif;"><div id="map" style="position: absolute; left: 475px; margin-top:20px">\',:printMap,\'</div><div style="max-width: 475px;"><h2><span>\',coalesce(:issueTypeLabel,\'\'),\'<br />Ref \',:caseRef,\'</span></h2><div>Reported by: \',coalesce(:contactDetailsTitle,\'\'),\' \',coalesce(:contactDetailsFirstName,\'\'),\' \',coalesce(:contactDetailsSurname,\'\'),\'</div><div>Address: \',coalesce(:contactDetailsFlat,\'\'),\' \',coalesce(:contactDetailsHouse,\'\'),\' \',coalesce(:contactDetailsStreet,\'\'),\', \',coalesce(:contactDetailsLocality,\'\'),\', \',coalesce(:contactDetailsTown,\'\'),\', \',coalesce(:contactDetailsCounty,\'\'),\', \',coalesce(:contactDetailsPostcode,\'\'),\'<br /> <br /> Issue details: \',coalesce(:issueTypeLabel,\'\'),\',\',coalesce(:infoStatements,\'\'),\'</div><div>\',coalesce(:description,\'\'),\'</div><div>Close to&nbsp;\',coalesce(:nearestAddress,\'\'),\'</div><div>Assigned to: \',coalesce(:team,\'\'),\'</div><div>Coordinates: \',coalesce(:pinpointLoc,\'\'),\'</div><div>Created at \',coalesce(:caseCreatedDt,\'\'),\'</div><div>Deadline: \',coalesce(:deadlineDate,\'\'),\'</div></div></div><hr />\') '
            . 'WHERE reference = :caseRef');

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'printMap',
            'issueTypeLabel',
            'caseRef',
            'contactDetailsTitle',
            'contactDetailsFirstName',
            'contactDetailsSurname',
            'contactDetailsFlat',
            'contactDetailsHouse',
            'contactDetailsStreet',
            'contactDetailsLocality',
            'contactDetailsTown',
            'contactDetailsCounty',
            'contactDetailsPostcode',
            'infoStatements',
            'description',
            'nearestAddress',
            'team',
            'pinpointLoc',
            'caseCreatedDt',
            'deadlineDate'
        ]));

        return $result ? true : $stmt->errorInfo()[2];
    }

    public function saveSubscriber($parameters)
    {
        $connection = $this->getCustomerConnection();

        $stmt = $connection->prepare(
            'INSERT INTO reportit_subscriber(reference, email) '
            . 'SELECT :caseRef, :subscriber FROM dual '
            . 'WHERE :subscriber IS NOT NULL '
                . "AND :subscriber <> '' "
                . 'AND NOT EXISTS (SELECT 0 FROM reportit_subscriber WHERE reference = :caseRef AND email = :subscriber)');

        $result = $stmt->execute($this->boundInputParameters($parameters, [
            'caseRef',
            'subscriber'
        ]));

        return $result ? true : $stmt->errorInfo()[2];
    }
}
