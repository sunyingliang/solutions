<?php

namespace FS;

use FS\Database\Manager;

class SolutionsCustomerAPIBase
{
    private   $customer;
    protected $dbManager;

    function __construct()
    {
        $this->setCustomer($this->getHostName());

        $this->dbManager = new Manager();
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    private function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    protected function getHostName()
    {
        $domainParts = explode('.', $_SERVER['HTTP_HOST']);

        return count($domainParts) > 3 ? $domainParts[0] : '';
    }

    protected function required($input, ...$args)
    {
        $required = [];

        foreach ($args as $arg) {
            if (!isset($input[$arg]) || is_null($input[$arg])) {
                $required[] = $arg;
            }
        }

        if (empty($required)) {
            return ['valid' => true];
        }

        return [
            'valid'   => false,
            'message' => 'Required parameters: ' . implode(', ', $required)
        ];
    }

    protected function parseResult($result)
    {
        $status = 200;

        if (empty($result)) {
            $status = 404;
        } else if (is_string($result)) {
            $status = 500;
        }

        http_response_code($status);

        return ['Response' => $result];
    }

    protected function invalidRequest($message, $type = 'json')
    {
        http_response_code(400);

        if ($type == 'json') {
            return ['Response' => $message];
        }

        return $message;
    }

    protected function isAuthenticated()
    {
        if ($this->isBasicAuthenticated()) {
            // Note: ->authenticate($username... is DNS {username} and PHP_AUTH_USER are checked as they must be identical
            if ($this->getCustomer() != $_SERVER['PHP_AUTH_USER']) {
                return false;
            }
            return $this->dbManager->authenticate($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
        }

        return $this->validateToken();
    }

    protected function isBasicAuthenticated()
    {
        return isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
    }

    protected function getCustomerToken()
    {
        if ($this->getCustomer() != $_SERVER['PHP_AUTH_USER']) {
            return false;
        }

        return $this->dbManager->getCustomerToken($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
    }

    protected function validateToken()
    {
        $token = '';

        if (isset($_SERVER['HTTP_X_AUTHTOKEN'])) {
            $token = $_SERVER['HTTP_X_AUTHTOKEN'];
        }

        if (!empty($this->getQueryParameters()['token'])) {
            $token = $this->getQueryParameters()['token'];
        }

        if (!empty($this->getPostParameters()['token'])) {
            $token = $this->getPostParameters()['token'];
        }

        if (!empty($token)) {
            return $this->dbManager->authenticate($this->getCustomer(), '', $token);
        }

        return false;
    }

    protected function getQueryParameters()
    {
        return $_GET;
    }

    protected function getPostParameters()
    {
        $contentType = '';

        if (isset($_SERVER['CONTENT_TYPE'])) {
            $contentType = strtolower($_SERVER['CONTENT_TYPE']);
        }

        if (strpos($contentType, 'json') !== false) {
            $input = json_decode(file_get_contents('php://input'), true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                $input = null;
            }
        } else {
            $input = $_POST;
        }

        return $input;
    }

    protected function unauthorized($type = 'json')
    {
        http_response_code(401);

        $unauthorizedMessage = 'Unauthorized';

        if ($type == 'json') {
            return ['Response' => $unauthorizedMessage];
        }

        return $unauthorizedMessage;
    }

    protected function json($key = null)
    {
        // Reads the request body
        $input = json_decode(file_get_contents('php://input'), true);

        if (empty($key)) {
            return $input;
        } else {
            return $input[$key];
        }
    }
}
