<?php

namespace FS\Common\Exception;

class PDOFetchException extends FSException
{
    public function __construct($message, $code = 202, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
