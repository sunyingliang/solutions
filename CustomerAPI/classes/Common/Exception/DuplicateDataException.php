<?php

namespace FS\Common\Exception;

class DuplicateDataException extends FSException
{
    public function __construct($message, $code = 102, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
