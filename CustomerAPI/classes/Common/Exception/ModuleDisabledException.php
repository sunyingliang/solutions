<?php

namespace FS\Common\Exception;

class ModuleDisabledException extends FSException
{
    public function __construct($message, $code = 422, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
