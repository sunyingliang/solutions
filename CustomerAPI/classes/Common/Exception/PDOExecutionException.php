<?php

namespace FS\Common\Exception;

class PDOExecutionException extends FSException
{
    public function __construct($message, $code = 201, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
