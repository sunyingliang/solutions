<?php

namespace FS\LLPG;

class Controller extends \FS\SolutionsCustomerAPIBase
{
    public function __construct($dbManager)
    {
        $this->dbManager = $dbManager;
    }

    public function listAddressesByPostcode($postcode)
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $json['postcode'] = $postcode;

        $validation = $this->required($json, 'postcode');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\LLPG($this->dbManager);

        $result = $database->listAddressesByPostcode($json);

        return $this->parseResult($result);
    }

    public function listAddressesByUPRN($uprn)
    {
        if (!$this->isAuthenticated()) {
            return $this->unauthorized();
        }

        $json['uprn'] = $uprn;

        $validation = $this->required($json, 'uprn');

        if (!$validation['valid']) {
            return $this->invalidRequest($validation['message']);
        }

        $database = new \FS\Database\LLPG($this->dbManager);

        $result = $database->listAddressesByUPRN($json);

        return $this->parseResult($result);
    }
}
