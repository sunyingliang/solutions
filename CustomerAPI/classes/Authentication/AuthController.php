<?php

namespace FS\Authentication;

class AuthController extends \FS\SolutionsCustomerAPIBase
{
    public function token()
    {
        if ($this->isBasicAuthenticated() && ($token = $this->getCustomerToken()) !== false) {
            return ['Response' => $token];
        }

        http_response_code(401);

        return ['Response' => 'Unauthorized'];
    }
}
