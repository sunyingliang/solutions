<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../Autoload.php';

use FS\Routers\Routes;

$router = new Routes();

// Public Access Platform
$router->get('/', function () {
    //return (new \FS\Platform\Controller())->renderIndex();
    return "<html><head><title>Firmstep Customer UI</title></head><body style='background-color: #FFCC66;'><div style='display: inline-block; height: 100%; width: 100%; vertical-align: middle; text-align: center; line-height: 65vh; font-size: 24pt;'>UNDER CONSTRUCTION</div><body></html>";
});
$router->get('/default', function () {
    return (new \FS\Platform\Controller())->renderDefault();
});

// Authentication
$router->get('/api/auth', function () {
    return (new \FS\Authentication\AuthController())->token();
});

// API - ReportIt
$router->get('/api/reportit/check-merge', function () {
    return (new \FS\ReportIt\Controller())->checkMerge();
});
$router->get('/api/reportit/check-status-change', function () {
    return (new \FS\ReportIt\Controller())->checkStatusChange();
});
$router->get('/api/reportit/check-task-id', function () {
    return (new \FS\ReportIt\Controller())->checkTaskId();
});
$router->get('/api/reportit/bulk-selected', function () {
    return (new \FS\ReportIt\Controller())->getBulkSelected();
});
$router->post('/api/reportit/save-db-id', function () {
    return (new \FS\ReportIt\Controller())->saveDBId();
});
$router->post('/api/reportit/save-issue', function () {
    return (new \FS\ReportIt\Controller())->saveIssue();
});
$router->post('/api/reportit/save-print', function () {
    return (new \FS\ReportIt\Controller())->savePrint();
});
$router->post('/api/reportit/save-subscriber', function () {
    return (new \FS\ReportIt\Controller())->saveSubscriber();
});

// API - LLPG
$router->get('/api/llpg/list-addresses-by-postcode', function () {
    return (new \FS\Platform\Controller())->renderDefault();
});
$router->get('/api/llpg/list-addresses-by-uprn', function () {
    return (new \FS\Platform\Controller())->renderDefault();
});

// API - FTP Logs
$router->get('/api/log/ftp', function () {
    return (new \FS\Logging\FTPLogReader())->getFTPLogs();
});

// Utils
$router->post('/api/util/deadline', function () {
    return (new \FS\Utils\Deadline())->deadline();
});

// Holiday
$router->post('/api/solutions/holiday/list', function () {
    return (new \FS\Holiday\Controller())->getHoliday();
});
$router->post('/api/solutions/holiday/add', function () {
    return (new \FS\Holiday\Controller())->addHoliday();
});
$router->post('/api/solutions/holiday/edit', function () {
    return (new \FS\Holiday\Controller())->editHoliday();
});
$router->post('/api/solutions/holiday/delete', function () {
    return (new \FS\Holiday\Controller())->deleteHoliday();
});

// Integration
$router->post('/api/integration/reportit/webhook', function () {
    return (new \FS\Integration\ReportIt\Controller())->webhook();
});

$router->execute();
